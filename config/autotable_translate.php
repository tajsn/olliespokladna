<?php

$cols_translate = [
  'Products' => [
    'id' => __('ID'),
    'name' => __('Název'),
    'product_group_id' => __('Skupina'),
    'price' => __('Cena'),
    'num' => __('Číslo'),
    'code' => __('Kód'),
    'group_trzba_id' => __('Skupina tržby'),
    'price_tax_id' => __('DPH'),
    'price_zam' => __('Cena zam.'),
    'group_id1' => __('Skupina 1'),
    'group_id2' => __('Skupina 2'),
    'group_id3' => __('Skupina 3'),
  ], 
  'ProductGroups' => [
    'name' => __('Název'),
    'parent_id' => __('Nadřazená skupina'),
    'color' => __('Barva'),
    'printer' => __('Tiskárna'),
  ],
  'Statistics' => [
    'name' => __('Název'),
    'product_id' => __('ID'),
    'price' => __('Cena s DPH'),
    'price_without_tax' => __('Cena bez DPH'),
    'count' => __('Množství'),
    'price_tax_id' => __('DPH'),
    'user_id' => __('Pracovník'),
    'storno' => __('Storno'),
    'created' => __('Naposledy prodáno'),
    'price_sum_without_tax' => __('Cena cel. bez DPH'),
    'price_sum' => __('Cena cel.'),
    'nakup_price' => __('Nakup bez DPH'),
    'nakup_price_vat' => __('Nakup s DPH'),
  ],  
  'SkladStavs' => [
    'created' => __('Vytvořeno'),
    'from_sklad_id' => __('ID skladu od'),
    'to_sklad_id' => __('ID skladu do'),
  ],
  'SkladJednotkas' => [
    'jednotka_id' => __('Jednotka'),
    'dodavatel_jednotka' => __('Jednotka dodavatel'),
    'prevod' => __('Převodní konstanta'),
  ],
  'SkladItems' => [
    'name' => __('Název'),
    'ean' => __('EAN'),
    'bidfood_id' => __('BidFood ID'),
    'created' => __('Vytvořeno'),
    'dodavatel_id' => __('Dodavatel'),
    'group_id' => __('Skupina'),
    'jednotka_id' => __('Jednotka'),
    'min' => __('Min.'),
    'max' => __('Max.'),
    'tax_id' => __('DPH'),
    'stav_real' => __('Reálný stav'),
  ],
  'Sklads' => [
    'faktura' => __('Faktura'),
    'created' => __('Vytvořeno'),
    'sklad_item_id' => __('Položka'),
    'sklad_item_product_id' => __('Produkt'), 
    'sklad_type_id' => __('Typ'),
    'value' => __('Množství'),
    'tax_id' => __('DPH'),
    'tax_id' => __('Nákupní cena'),
    'nakup_price' => __('Nákupní cena celkem'),
    'nakup_price_total' => __('Nákupní cena celkem'),
    'jednotka_id' => __('Jednotka'),
    'sum_value' => __('Celkem množství'),
    'sklad_item'=>[
		'jednotka_id' => __('Jednotka'),
		'tax_id' => __('DPH'),
	]
  ],
  'Logs' => [
    'controller' => __('Modul'),
    'action' => __('Akce'),
    'value' => __('Hodnota'),
    'system_id' => __('Provozovna'),
    'user_id' => __('Uživatel'),
  ],
  'ProductAddons' => [
    'name' => __('Název'),
    'price' => __('Cena'),
    'group_id' => __('Typ'),
  ],
  'Uzaverkas' => [
    'open_date' => __('Otevření'),
    'done_date' => __('Uzaření'),
    'created' => __('Vytvořeno'),
	'total_price' => __('Celkem'),
	'pay_hotovost' => __('Hotovost'),
	'pay_karta' => __('Karta'),
	'pay_bonus' => __('Bonus'),
	'pay_poukazka' => __('Poukázka'),
	'pay_sleva' => __('Sleva'),
	'pay_zam' => __('Zam.'),
	'order_id_from' => __('ID od'),
	'order_id_to' => __('ID do'),
	'user_id' => __('Uživatel'),
  ],
  'Trzbas' => [
    'id' => __('ID'),
    'created' => __('Vytvořeno'),
    'open_date' => __('Otevření stolu'),
    'done_date' => __('Datum zaplacení'),
    'table_name' => __('Stůl'),
    'total_price' => __('Tržba'),
    'pay_hotovost' => __('Hotovost'),
    'pay_karta' => __('Karta'),
    'pay_bonus' => __('Bonus'),
    'pay_poukazka' => __('Poukázka'),
    'pay_sleva' => __('Sleva'),
    'pay_zam' => __('Zam.'),
    'client_name' => __('Klient'),
    'doklad' => __('Doklad'),
    'storno' => __('Storno'),
    'count_products' => __('Počet'),
	'user_id' => __('Uživatel'),
  ],
  'TrzbaItems' => [
    'code' => __('Kod'),
    'name' => __('Produkt'),
    'product_group_id' => __('Skupina'),
    'order_id' => __('Účet'),
    'price' => __('Částka'),
    'created' => __('Vytvořeno'),
    'user_id' => __('Pracovník'),
	'group_trzba_id' => __('Tržba'),
	
  ],
  'Tables' => [
    'name' => __('Název'),
    'num' => __('Číslo'),
    'group_id' => __('Skupina'),
  ],
  'Orders' => [
    'id' => __("#"),
    'name' => __("Jméno zákazníka"),
    'total_price' => __("Částka"),
  ],
  'Users' => [
    'name' => 'Jméno',
    'username' => 'Uživatelké jméno',
    'group_id' => 'Skupina'
  ],
];