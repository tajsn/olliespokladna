/*
---
description: Adds a search box to existing select box to filter items

license: MIT-style

authors:
- Amitay Horwitz

requires:
- core/1.3.1: '*'

provides:
- Searchable

...
*/

(function($) {

this.Searchable = new Class({

    Implements: [Options, Events],

    options: {
        container: {'class': 'searchable'},
        query: {'class': 'query', 'placeholder': 'Hledej...', 'type': 'text'},
        results: {'class': 'multiple_select', 'multiple': false},
        keepSelected: true,
        addMassSelect: false
    },

    initialize: function(select, options) {
        //console.log(select.getStyles('width', 'height', 'padding','float','margin','display'));
		var wrap_select = new Element('div').inject(select,'before');
		wrap_select.setStyles(select.getStyles('width', 'height', 'float','margin','display'));
		select.setStyles(select.getStyles('width', 'height', 'padding','float','margin','display'));
		
		wrap_select.adopt(select);
		
		
		this.setOptions(options);
        this.query = new Element('input', this.options.query);
        var top_move = -select.getStyle('height').toInt();
		if (window.getSize().x < 1000){
			top_move = top_move /2;
		}
		this.query.setStyles({
			'position':'absolute',
			'border':'1px solid #666',
			'background':'#efefef',
			'top':top_move,
		});
		this.query.addClass('none');
		// show search field
		select.addEvent('click',function(e){
			if (this.query.hasClass('none')){
				this.query.removeClass('none')
				this.query.focus();
			} else {
				this.query.addClass('none')
			}
		}.bind(this));
		
		// hide search outside click
		document.body.addEvent('click',function(e){
			var isClickInside = select.contains(e.target);
			//console.log(this.query);
			//console.log(isClickInside);
			if (!isClickInside){
				this.query.addClass('none');
			}
			
		}.bind(this));
		
		this.results = $(select).set(this.options.results);
        this.optionElements = this.results.getElements('option');

        var container = new Element('div', this.options.container);
        container.inject(this.results, 'after').adopt(this.query, this.results);
        
        this.query.addEvent('keyup', this.onKeyUp.bind(this));
        this.results.addEvent('change', this.fireEvent.bind(this, 'change'));

        if (this.options.addMassSelect && MassSelect) {
            new MassSelect(this.results);
        }
    },

    clear: function(all) {
        var selector = !!all ? 'option' : 'option:not(:selected)',
            options = this.results.getElements(selector);
        options.dispose();
        this.results.fireEvent('change');
        return this;
    },

    add: function(value, name, force) {
        if (!force) {
            // Don't add if value already exists
            var checkValue = function(el) { return el.get('value') == value; };
            if (this.optionElements.some(checkValue)) {
                return this;
            }
        }
        var option = new Element('option', {value: value, text: name});
        this.optionElements.push(option);
        option.inject(this.results);
        return this;
    },

    onKeyUp: function() {
        var value = this.query.get('value'), options = this.optionElements, regex;
        this.clear(!this.options.keepSelected);
        if (value !== '') {
            regex = new RegExp(value, 'i');
            options = options.filter(function(option) {
                return (option.get('selected') && this.options.keepSelected) || regex.test(option.get('text'));
            }, this);
        }
        this.results.adopt(options);
        this.fireEvent('keyUp');
    }

});

})(document.id);