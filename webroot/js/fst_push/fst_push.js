/*
Fst Push
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var FstPush = this.FstPush = new Class({
	Implements:[Options,Events],
	debug : false,
	isPushEnabled: false,
	options: {
		'push_button':'subscriptionButton',
	},
	lang: {
		'vypnout_zpravy':'Vypnout zprávy',
		'zapnout_zpravy':'Zapnout zprávy',
	},
	
	// init global function
	initialize:function(options){
		this.setOptions(options);
		if (Browser.name == 'chrome'){
			this.create_service();
		}
	},
	
	
	// create service
	create_service: function(){
		if (this.debug == true)	 console.log('FST_PUSH.js create service');
		
		this.create_button();
		
		this.pushButton = $(this.options.push_button);  
		this.pushButton.addEvent('click', (function(e) {  
			if (this.isPushEnabled) {  
			  this.unsubscribe();  
			} else {  
			  this.subscribe();  
			}  
		}).bind(this));
	
		// Check that service workers are supported, if so, progressively  
		// enhance and add push messaging support, otherwise continue without it.  
		function getSubscription() {
		  return navigator.serviceWorker.ready
			.then(function(registration) {
			  return registration.pushManager.getSubscription();
			});
		}
		if ('serviceWorker' in navigator) {  
			this.worker = navigator.serviceWorker.register('sw.js')   
			.then(this.initialiseState.bind(this));  
		} else {  
			console.warn('Service workers aren\'t supported in this browser.');  
		} 

		

		
		
	},
	
	create_button(){
		if (!$(this.options.push_button)){
			var push_over = new Element('div',{'id':'push_over'}).inject($('addon'));
			var button = new Element('input',{'type':'button','id':'subscriptionButton','value':'Odebírat zprávy','class':'button small'}).inject(push_over);
			
		}
	},
	
	// Once the service worker is registered set the initial state  
	initialiseState: function(reg) {  
		if (this.debug == true)	console.log('FST_PUSH.js initialisState');
		
		this.service = reg;
		
		// Are Notifications supported in the service worker?  
		if (!('showNotification' in ServiceWorkerRegistration.prototype)) {  
			console.warn('Notifications aren\'t supported.');  
			return;  
		}

		// Check the current Notification permission.  
		// If its denied, it's a permanent block until the  
		// user changes the permission  
		if (Notification.permission === 'denied') {  
			console.warn('The user has blocked notifications.');  
			return;  
		}

		// Check if push messaging is supported  
		if (!('PushManager' in window)) {  
			console.warn('Push messaging isn\'t supported.');  
			return;  
		}
		
		//this.subscribeNotify();
		
		//console.log(navigator.serviceWorker.ready);
		
		// We need the service worker registration to check for a subscription  
		navigator.serviceWorker.ready.then((function(serviceWorkerRegistration) {  
			
			// Do we already have a push message subscription?  
			serviceWorkerRegistration.pushManager.getSubscription()  
			  .then((function(subscription) {  
				// Enable any UI which subscribes / unsubscribes from  
				// push messages.  
				
				if (!subscription) {  
				  // We aren't subscribed to push, so set UI  
				  // to allow the user to enable push  
				  return;  
				}

				// Keep your server in sync with the latest subscriptionId
				//sendSubscriptionToServer(subscription);
				if (this.debug == true) console.log('FST_PUSH.js ENDPOINT: '+subscription.endpoint);
				
				//new Uint8Array(subscription.getKey('auth'));
				//new Uint8Array(subscription.getKey('p256dh'));
				
				this.pushButton.value = this.lang['vypnout_zpravy']; 
				
				//this.json_keys = JSON.decode(JSON.stringify(subscription));
				
				//var endpoint = subscription.endpoint.split('/');
				//endpoint_save = endpoint[endpoint.length-1];
				
				this.store_endpoint(subscription);
				this.save_hash(1);
				
				//localStorage.setItem('endpoint', endpoint_save);
				//load_endpoint = localStorage.getItem('endpoint');
				//console.log(load_endpoint);
				
				// Set your UI to show they have subscribed for  
				// push messages   
				this.isPushEnabled = true;
			  }).bind(this))  
			  .catch(function(err) {  
				console.warn('Error during getSubscription()', err);  
			  });  
		}).bind(this));  
		
	},
	
	
	save_hash: function(id,hash){
		if (this.debug == true)	console.log('FST_PUSH.js save hash');
		hash = this.endpoint_save;
		if (typeof this.json_keys.keys == 'undefined') this.json_keys.keys = {'p256dh':'','auth':''};
		
		var save_hash_req = new Request.JSON({
			url:'/save_push/'+hash,	
			data: {
				'p256dh':this.json_keys.keys.p256dh,
				'auth':this.json_keys.keys.auth
			},
			onComplete: (function(json){
				
				if (this.debug == true)	{
					console.log('---FST_PUSH.js result from save hash ---');
					console.log(json);
				}
			}).bind(this)
		});
		save_hash_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		save_hash_req.send();
		
	},
	
	store_endpoint: function(subscription){
		this.json_keys = JSON.decode(JSON.stringify(subscription));
		var endpoint = subscription.endpoint.split('/');
		this.endpoint_save = endpoint[endpoint.length-1];
			
	},
		
	
	subscribe: function(){
		if (this.debug == true)	console.log('FST_PUSH.js subscribe');
		/*
		this.service.pushManager.subscribe({
            userVisibleOnly: true
        }).then(function(sub) {
            console.log('endpoint:', sub.endpoint);
        });
		*/

	  navigator.serviceWorker.ready.then((function(serviceWorkerRegistration) {  
		serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})  
		  .then((function(subscription) {  
			// The subscription was successful  
			this.isPushEnabled = true;  
			this.pushButton.value = this.lang['vypnout_zpravy'];  
			//this.pushButton.set('text','Disable Push Messages');  
			//console.log(endpoint_save);	
			this.store_endpoint(subscription);
			this.save_hash(1);
			// TODO: Send the subscription.endpoint to your server  
			// and save it to send a push message at a later date
			//return sendSubscriptionToServer(subscription);  
		  }).bind(this))
		  .catch((function(e) {  
			if (Notification.permission === 'denied') {  
			  // The user denied the notification permission which  
			  // means we failed to subscribe and the user will need  
			  // to manually change the notification permission to  
			  // subscribe to push messages  
			  console.warn('Permission for Notifications was denied');  
			  //this.pushButton.disabled = true;  
			} else {  
			  // A problem occurred with the subscription; common reasons  
			  // include network errors, and lacking gcm_sender_id and/or  
			  // gcm_user_visible_only in the manifest.  
			  //console.error('Unable to subscribe to push.', e);  
			  //pushButton.disabled = false;  
			  //this.pushButton.set('text','Enable Push Messages');  
			  
				this.pushButton.value = this.lang['vypnout_zpravy'];
			}  
		  }).bind(this));  
	  }).bind(this));
		
	},
	
	unsubscribe: function() {  
	  if (this.debug == true)	console.log('FST_PUSH.js unsubscribe');
	  
	  navigator.serviceWorker.ready.then((function(serviceWorkerRegistration) {  
	  // To unsubscribe from push messaging, you need get the  
		// subscription object, which you can call unsubscribe() on.  
		serviceWorkerRegistration.pushManager.getSubscription().then(  
		  (function(pushSubscription) {  
			// Check we have a subscription to unsubscribe  
			if (!pushSubscription) {  
			  // No subscription object, so set the state  
			  // to allow the user to subscribe to push  
			  this.isPushEnabled = false;  
			  
				this.pushButton.value = this.lang['zapnout_zpravy'];
			  //this.pushButton.set('text','Enable Push Messages');  
			  return;  
			}  

			var subscriptionId = pushSubscription.subscriptionId;  
			// TODO: Make a request to your server to remove  
			// the subscriptionId from your data store so you
			// don't attempt to send them push messages anymore

			// We have a subscription, so call unsubscribe on it  
			pushSubscription.unsubscribe().then((function(successful) {  
			  	this.pushButton.value = this.lang['zapnout_zpravy'];
			
			  //this.pushButton.set('text','Enable Push Messages');  
			  this.isPushEnabled = false;  
			}).bind(this)).catch((function(e) {  
			  // We failed to unsubscribe, this can lead to  
			  // an unusual state, so may be best to remove
			  // the users data from your data store and
			  // inform the user that you have done so

			  console.log('Unsubscription error: ', e);  
			  this.pushButton.value = this.lang['zapnout_zpravy'];
			
			  //pushButton.set('text','Enabale Push Messages');
			}).bind(this));  
		  }).bind(this)).catch(function(e) {  
			console.error('Error thrown while unsubscribing from push messaging.', e);  
		  });  
	  }).bind(this));  
	}
	
	
	
	
		
	
	
});
window.addEvent('load',function(){
	var fst_push = new FstPush();
});