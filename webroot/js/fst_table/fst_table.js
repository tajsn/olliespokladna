/*
Fst History
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var FstTable = this.FstTable = new Class({
	Implements:[Options,Events],
	debug : false,
	count : 0,
	myDrag: {},
	save_data: {},
	options: {
		'grid':50,
	},
	// init fce
	initialize:function(options){
		this.canvas_size = $('draw_canvas').getCoordinates();
		
		this.init_all_tables();
		
		this.init_save_data();
		this.add_table();
		this.switch_group_table();
		this.delete_table_from_canvas();
		this.load_save_data_to_storage();
	},
	
	init_save_data: function(){
		$$('.canvas_group').each(function(canv){
			this.save_data[canv.get('data-id')] = {};
		}.bind(this));
		
	},
	
	load_save_data_to_storage: function(){
		$('draw_canvas').getElements('.canvas_group').each(function(group){
			group.getElements('.table').each(function(table){
				//console.log(table);
				this.storage_data(table,group.get('data-id'));
			}.bind(this));
		}.bind(this));
		//this.storage_data(el);
	},
	
	delete_table_from_canvas: function(){
		$('draw_canvas').getElements('.del').removeEvents('click');
		$('draw_canvas').getElements('.del').addEvent('click',function(e){
			e.stop();
			e.target.getParent('.table').removeProperty('style');
			e.target.getParent('.table').inject($('table_groups').getElement('.group_active'));
			
			this.add_table();
			
			Vars.save_table_data = new Request.JSON({
				url:'/orders/deleteTable/'+e.target.getParent('.table').get('data-id'),	
				onComplete: (Vars.save_table_data_json = function(json){
					if (json.result == true){
						if (json.message)
						FstAlert(json.message);
					} else {
						FstError(json.message);
					}				
					json = null;
				}).bind(this)
			});
			
			Vars.save_table_data.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			Vars.save_table_data.send();
			
			delete this.save_data[$('draw_canvas').getElement('.active_canvas').get('data-id')][e.target.getParent('.table').get('data-id')];
			//console.log(this.save_data);
			this.save_storage();
			
		}.bind(this));
	},
	
	storage_data: function(el,group_id){
		//console.log(el);
		data = {
			'id':el.get('data-id'),
			'name':el.get('data-name'),
			'width':el.getStyle('width'),
			'height':el.getStyle('height'),
			'top':el.getStyle('top').toInt(),
			'left':el.getStyle('left').toInt(),
		}
		//console.log($('draw_canvas').getElement('.active_canvas'));
		if (group_id){
		this.save_data[group_id][data.id] = data;
			
		} else {
		this.save_data[$('draw_canvas').getElement('.active_canvas').get('data-id')][data.id] = data;
			
		}
		console.log(this.save_data);
		
		
		
	},
	
	save_storage: function(){
		
		
		Vars.save_table_data = new Request.JSON({
			url:'/orders/saveTable/'+JSON.encode(this.save_data),	
			onComplete: (Vars.save_table_data_json = function(json){
				if (json.result == true){
					if (json.message)
					FstAlert(json.message);
				} else {
					FstError(json.message);
				}				
				json = null;
			}).bind(this)
		});
		Vars.save_table_data.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		Vars.save_table_data.send();
	},
	
	init_all_tables: function(){
		$('draw_canvas').getElements('.table').each(function(table,k){
			this.init_drag(table,k);
			this.init_resize(table,k);
			this.count ++;
		}.bind(this));
	},
	
	init_drag: function(table,k){
		
		this.myDrag[k] = new Drag(table, {
			snap: 0,
			handle: table.getElement('.move'),
			grid : this.options.grid,
			limit : {x: [0, this.canvas_size.width - table.getSize().x], y: [0, this.canvas_size.height- table.getSize().y]},
			onSnap: function(el){
				//console.log(this.myDrag[k].limit.y);
				//el.addClass('dragging');
			}.bind(this),
			onComplete: function(el){
				//console.log(el);
				this.storage_data(el);
				this.save_storage();
				//el.setProperty('data-params',Object.toQueryString(data));
				//el.removeClass('dragging');
			}.bind(this)
		});
			
	},
	
	init_resize: function(table,k){
	
		table.makeResizable({
			grid: this.options.grid/2,
			handle: table.getElement('.res'),
			limit : {x: [this.options.grid, this.canvas_size.width - table.getSize().x], y: [this.options.grid, this.canvas_size.height- table.getSize().y]},
			onDrag: function(el){
				
			},
			onComplete: function(el){
				//this.myDrag[k].limit = {x: [0, this.canvas_size.width - el.getCoordinates().width], y: [0, this.canvas_size.height- el.getCoordinates().height]}
				//myDrag.limit = 0;
				this.myDrag[k].setOptions({
					limit: {x: [0, this.canvas_size.width - el.getCoordinates().width], y: [0, this.canvas_size.height- el.getCoordinates().height]}
				});
				this.storage_data(el);
				this.save_storage();
				//this.myDrag[k].cancel();
				//console.log(this.myDrag[k].limit.y);
			}.bind(this)
		});
			
	},
	
	add_table: function(){
		$('canvas_list').getElements('.table').removeEvents('click');
		$('canvas_list').getElements('.table').addEvent('click',function(e){
			e.stop();
			e.target.inject($('draw_canvas').getElement('.active_canvas'));
			//this.init_all_tables();
			this.count ++;
			e.target.removeEvents('click');
			this.init_drag(e.target,this.count);
			this.init_resize(e.target,this.count);
			this.delete_table_from_canvas();
	
		}.bind(this));
	},
	
	switch_group_table: function(){
		$('group_select').getElements('li').addEvent('click',function(e){
			$('group_select').getElements('li').removeClass('active');
			e.target.addClass('active');
			
			$$('.table_group').addClass('none');
			$$('.table_group').removeClass('group_active');
			
			$('table_groups').getElement('.table_group'+e.target.get('data-id')).removeClass('none');
			$('table_groups').getElement('.table_group'+e.target.get('data-id')).addClass('group_active');
			
			$$('.canvas_group').addClass('none');
			$$('.canvas_group').removeClass('active_canvas');
			
			$('draw_canvas').getElement('.canvas_group'+e.target.get('data-id')).removeClass('none');
			$('draw_canvas').getElement('.canvas_group'+e.target.get('data-id')).addClass('active_canvas');
		});
	},
});

window.addEvent('domready', function () {
	window.fsttable = new FstTable();
		
});