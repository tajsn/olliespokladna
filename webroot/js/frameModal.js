/**
 * Created by Marin Hrabal on 20.1.2016.
 */

var frameModal = new Class({
	
	options: {
		morph_plus:80
	},
	
	initialize : function(element, options) {
		this.contentDiv = new Element("div.modal-contentdiv");
		this.modal_preloader = new Element('div#modal_preloader');
		this.frame = new Element("iframe#"+element);
		this.overdiv = new Element(".modal-overdiv");
		this.closeBtn = new Element("div.close",{'id':'close_modal'}).adopt(new Element("span.fa.fa-times"));
		this.refreshBtn = new Element("div.refresh").adopt(new Element("span.fa.fa-refresh"));

		if(options){
		  this.frame.options = options;
		}
	  },

  show : function(){
    $(document.body).adopt(this.overdiv)
    $(this.contentDiv).adopt(this.frame).adopt(this.closeBtn).adopt(this.refreshBtn).adopt(this.modal_preloader);
   $(document.body).adopt(this.contentDiv);

    this.overdiv.show();
    this.contentDiv.show();

    this.closeBtn.addEvent("click", this.hide.bind(this));
    this.refreshBtn.addEvent("click", (function(){this.refresh()}).bind(this));

    this.frame.setStyles({'height' : '500', 'width' : '100%'});
  },

  setContent : function(url){

    var frame = this.frame;
    
	//this.frame.contentWindow.onload = (function(e){
	this.frame.onload  = (function(e){
		
		if (typeof this.frame.contentWindow.document.body.getElementById == 'undefined'){
			this.modal_preloader.destroy();
		
		} else {
			
			//console.log(this.frame.contentWindow.document.body.getElementById('modal_in').getSize().y);
			if (this.frame.contentWindow.document.body.getElementById('modal_in')){
				var height = this.frame.contentWindow.document.body.getElementById('modal_in').getSize().y+this.options.morph_plus;
				frame.morph({'height' : height});
				this.modal_preloader.set('morph',{
					onComplete: (function(){
						this.modal_preloader.destroy();
		
					}).bind(this)
				});
				this.modal_preloader.morph({'height' : height});
			} else {
				frame.morph({'height' : window.document.body.offsetHeight});
			}
			
		}
		
		
	}).bind(this)
	//console.log(window.location.port);
    this.frame.set("src", this.frame.options.base+((window.location.port != '')?':'+window.location.port:'')+url);
	
  },

  hide : function(e){
	//console.log(e.target.getParent('div'));
	//return false;
	if (e.target.get('data-result')){
		var result = e.target.get('data-result');
		var message = e.target.get('data-message');
		if (e.target.get('data-id')){
			var last_id = e.target.get('data-id');
		}
	}
	//console.log(result);
	//return false;
    this.contentDiv.destroy();
    this.frame.destroy();
    this.overdiv.destroy();
    this.closeBtn.destroy();
	
	if (result){
		if (result == 'true') {
			FstAlert(message);
		}
		if (result == 'false') {
			FstError(message);
		}
		// last ID
		if (last_id){
			window.fsthistory.last_id = last_id;
		}
		window.fsthistory.reinit_history();
	}
	
	last_id = null;
	result = null;
	message = null;
	
  },

  resize_modal : function(height){
	window.parent.document.getElementById('modalWindow').morph({'height' : height+this.options.morph_plus});
  },

  refresh : function(){
    this.frame.contentWindow.location.reload(true);
  }

});

