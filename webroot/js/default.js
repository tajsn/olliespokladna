/**
 * Created by Marin Hrabal on 13.1.2016.
 */
 
function table_scroll(){
   if ($('filtration')){
   var page = $('body').getSize();
   var cor = $('filtration').getCoordinates();
   //console.log(page);
   var height = page.y - 80;
   if ($('table_over')){
	   $('table_over').setStyle('height',height);
	   if ($('table_over').getElement('.table-formated'))
	   $('table_over').getElement('.table-formated').setStyle('margin-top',0);
   }
   //console.log($('table_over'));
   //console.log(height);
   if ($('table_over'))
   $('table_over').addEvent('scroll',function(e){
		ptop = $('table_over').getScrollTop();
		pleft = $('table_over').getScrollLeft();
		
		if ($('table_over') && $('table_over').getElement('.table-formated')){
			$('table_over').getElement('.table-formated').getElements('.fixed_top').setStyle('top',ptop);
		}
	});
   }
}  
 function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}
function b64DecodeUnicode(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}
 

function send_error(data,message){
	//console.log(message);
	if (message){
	$('SendErrorMessage').value = message;
	
	}
	$('SendErrorText').value = data;
	
	new Request.JSON({
		url:$('send_error_form').action,	
		onComplete:function(json){
			if (!json || json.result == false){
				FstError('Chyba odeslani chyby');
			} else {
				(function(){
					FstAlert(json.message);
					
				}).delay(1000);
			}	
		}
	}).post($('send_error_form'));
}


function price(price){
	symbol_before 	= '';
	kurz			= '1';
	count			= '1';
	decimal			= 2;
	symbol_after	= ''; 
		
	price = (price/kurz)* count;		
	return symbol_before + number_format(price, decimal, '.', ' ') + symbol_after;	
}


function date_picker(){
	//window.addEvent('domready', function() {
	
	if ($$('.date_range').length>0){
	var picker = new Picker.Date.Range($$('.date_range'), {
		timePicker: false,
		columns: 3,
		months: lang.mesice, 
		months_title: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		shortDate: '%Y-%m-%d',
		dateOrder: ['year', 'month','date'],
		shortTime: '%H:%M:%S',
		format:'%Y-%m-%d',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	}
	if ($$('.date').length>0){
	var picker = new Picker.Date($$('.date'), {
		timePicker: false,
		columns: 1,
		//months: lang[CURRENT_LANGUAGE].mesice, 
		//'abbr',{
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		//},
		//shortDate: '%d-%m-%Y',
		//dateOrder: ['date', 'month','year`'],
		//shortTime: '%H:%M:%S',
		format:'%d.%m.%Y',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	
	}
	if ($$('.date_time').length>0){
	$$('.date_time').each(function(item){
		if (item.value == '0000-00-00 00:00:00')
			item.value = '';
	});
	var picker = new Picker.Date($$('.date_time'), {
		
		timePicker: true,
		columns: 1,
		months: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
			//months_title: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
			//days_abbr: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
		
		
		shortDate: '%Y-%m-%d',
		dateOrder: ['year', 'month','date'],
		shortTime: '%H:%M:%S',
		format:'%d.%m.%Y %H:%M:%S',
		allowEmpty:true,
		
		//months_abbr: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
		
		positionOffset: {x: 5, y: 0}
	});
	}
	
	//});
}

function count_maxlength(){
	// počítadlo kolik znaků ještě můžeš zadat u inputů
	  if($$("input, textarea")[0]){
		$$("input[type=text], textarea").each(function(inp){
		  if($(inp).hasClass("maxlength")){
			var counter = new Element("span.counter");
			counter.set("html", $(inp).get("maxlength"));
			$(inp).addEvent("keyup", function(){
			  counter.set("html", $(inp).get("maxlength") - $(inp).get("value").length);
			});
			$(inp).getParent().adopt(counter);
		  }
		})
	  }
}	


function FstAlert(value,modal){
	
	//console.log($(window));
	//console.log($(parent.window));
	new mBox.Notice({
		type: 'ok',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 1500,
		position: {
				x: 'center',
				y: 'center'
			},
		content: value
	});
}
function FstError(value,modal){
	new mBox.Notice({
		type: 'error',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 1500,
		position: {
				x: 'center',
				y: 'center'
			},
		content: value
	});
}

function fst_checkbox(){
		if (Browser.ie && Browser.version == 8){
		
		} else {
		
		$$('input.checkbox').each(function(item){
			if (item.getParent('.fst_ch_obal')){
				return false;
			}
			obal = new Element('div',{'class':'fst_ch_obal'}).inject(item,'before');
			obal.adopt(item);
			check = new Element('div',{'class':'fst_ch'}).inject(obal);
			if (item.checked){
				check.addClass('active');
				check.set('html','&#xf00c;');
			}
			
			
			check.addEvent('click',function(e){
				var ch = this.getParent('.fst_ch_obal').getElement('input.checkbox');
				if (this.hasClass('active')){
					this.removeClass('active');
					this.set('html','');
					ch.removeProperty('checked');
				} else {
					this.addClass('active');
					this.set('html','&#xf00c;');
					ch.setProperty('checked','checked');
				}
				ch.fireEvent('change',ch);
				
			});
			
		});
		}
}
 
window.addEvent("domready", function() {
	
	count_maxlength();
 

});



Drag.Group = new Class({
 
	Implements : [Options],
 
	options : {
		'active' : true,
		'store' : 'drag-group-item',
		'filter' : true,
		'drag' : {}
	},
 
	elements : [],
 
	initialize : function(options){
		this.setOptions(options);
	},
 
	add : function(el,options){
		var drag = new Drag.Group.Item(el, this, $merge(this.options.drag,options))
		el.store(this.options.store, drag );
		this.elements.push(el);
		return drag;
	},
 
	start : function(el,event){
		if(!this.options.active || !this.options.filter(el)) 
			el.retrieve(this.options.store).start(event,true);
		else {
			this.elements.filter(this.options.filter).each(function(match){
				match.retrieve(this.options.store).start(event,true);
			},this);
		}
	}
});
 
Drag.Group.Item = new Class({
 
	Extends : Drag.Move,
 
	initialize : function(el,group,options){
		this.group = group;
		this.parent(el,options);
	},
 
	start : function(event,alerted){
		if(alerted) this.parent(event);
		else this.group.start(this.element,event);
	}
 
});


function zakazka_client(){
	window.addEvent('domready',function(){
		
		function inject_option(opt,el){
			opt = opt.split('|');
			option = new Option(opt[1], opt[0]);
			option.setProperties({
				'data-ulice':opt[2], 
				'data-mesto':opt[3], 
				'data-psc':opt[4], 
				'data-stat':opt[5], 
				'data-lat':opt[6], 
				'data-lng':opt[7], 
				'data-type':opt[8]
			});
			el.add(option,null);
						
		}
		
		var myAutocompleter = new GooCompleter('client-name', {
				action: '/clients/autocomplete/',	// JSON source
				param: 'search',			// Param string to send		
				preloader: 'auto_loading',
				fire_event: function(result){
					if (Object.getLength(result)>0){
						var data_load = JSON.decode(result.key);
						var data = {};
						Object.each(data_load,function(item,k){
							if (item == true) item = 1;
							if (item == false) item = 0;
							if (k != 'adresa_list')
								data[k.replace('_','-')] = item;
							else 
								data[k] = item;
						});
						//console.log(data);
						
						//console.log(data);
						$$('.client_adopt').each(function(item){
							
							//item = item.replace('_','-')
							id = item.get('id').substr(7,20);
							if (data[id]){
								item.value = data[id];
							}
						});
						
						if ($('SelectAddress1').getElements('option'))
						Object.each($('SelectAddress1').getElements('option'),function(opt,k){
							
							if (opt.value > 0){
								opt.destroy();
							}
						});
						
						if ($('SelectAddress2').getElements('option'))
						Object.each($('SelectAddress2').getElements('option'),function(opt,k){
							if (opt.value > 0){
								opt.destroy();
							}
						});
						//console.log(data);
						//console.log(data.adresa_list);
						
						if (Object.getLength(data.adresa_list)>0){
							Object.each(data.adresa_list[1],function(opt,k){
								
								inject_option(opt,$('SelectAddress1'));
							});
							
							Object.each(data.adresa_list[2],function(opt,k){
								inject_option(opt,$('SelectAddress2'));
							});
						}
						
						
							
						//console.log(data);
					}
					//console.log(result);
					
				},
				minlen: 0,
				listbox_offset: { y: 2 },	// Listbox offset for Y			
				delay: 500					// Request delay to 0.5 seconds
		});
		
	
	});
}