/*
FstPokladnaClient 
Copyright Fastest Solution s.r.o. 
Author: Jakub Tyson
*/
var FstPokladnaClient =  new Class({
	Implements:[Options,Events],
	debug: true,
	options: {
		element: null,
	},
	
	initialize: function(options){
	
		this.setOptions(options);
		
		this.websocket_client();
		
			
	},
	
	
	send_data_printer: function(data){
		//console.log(data);
		//var d = new Uint8Array(file_data);console.log(d);
		
		if (this.ws.readyState == 3){
			FstError('Není připojena tiskárna');
		} else {		
		
			this.ws.send(JSON.encode(data));
		}
		//console.log(this.ws);
		//this.ws.send(file_data);
		
	},
	
	init_pokladna_socket: function(system_id){
		//console.log(this.ws);
		//var d = new Uint8Array(file_data);console.log(d);
		//console.log($('SystemId'));		
		this.ws.send(JSON.encode({
			'type':'init',
			'system_id':$('SystemId').value,
		}));
		
		//console.log(this.ws);
		//this.ws.send(file_data);
		
	},
	
	get_printer: function(){
		//console.log(this.ws);
		//var d = new Uint8Array(file_data);console.log(d);
				
		this.ws.send(JSON.encode({
			'type':'get_printer',
			'system_id':$('SystemId').value,
		}));
		
		//console.log(this.ws);
		//this.ws.send(file_data);
		
	},
	
	// parse from server.js
	parse_response: function(response){
		if (response.printer_list){
			Object.each(response.printer_list, function(printer){
				new Element('li',{}).set('text',printer).inject($('printer_list'));
			});
			$('printers_data').set('text',JSON.encode(response.printer_list));
		}
	},
	
	websocket_client: function(url){
		
		if (!url) url = location.host;		
		if ("WebSocket" in window){
		 var port = 8080;
		 url = url.split(':');
		 //url[0] = '127.0.0.1';
		 
		 this.ws = new WebSocket('ws://'+url[0]+':'+port+'/');
		 //this.ws.binaryType = 'blob';
		
		 // websocket is opened
		 this.ws.onopen = (function(evt){
			
			this.ws_log('Spojení se serverem v porádku','ok');
			this.init_pokladna_socket();
			this.get_printer();
			this.check_connection(1);
			
			//if (this.options.provoz_id){
				/*
				this.ws.send(JSON.encode({
					'type':'print',
					'pokladna':true,
					'provoz_id':this.options.provoz_id
				}));
				*/
			//}
			
		 }).bind(this);
		 
		 
		 // websocket sending message
		 this.ws.onmessage = (function (evt) { 
			response = evt.data;
			response = JSON.parse(response);
			//console.log(response);
			this.parse_response(response);
			
		 }).bind(this);
		 
		 // websocket is closed.
		 this.ws.onclose = (function(e){ 
			//this.ws_log('Chyba spojení se serverem');
			this.check_connection(0);
				
		 }).bind(this);
	  } else {
		 // The browser doesn't support WebSocket
		alert('Tento prohlížec není podporován');
		this.ws_log('Websocket není podporován v tomto prohlížeci');
	  }
	  
	},
	
		
	// Make the function wait until the connection is made...
	check_connection: function(status){
		if (!$('websocket_status')){
			return false;
		}
		if (status == 1){
			
			$('websocket_status').addClass('success');
			$('websocket_status').removeClass('failed');
			$('websocket_status').set('text','Spojení s tiskárnou v pořádku');
			VarsModal.hide_status_socket = function(){
				$('websocket_status').fade(0);
			}
			VarsModal.hide_status_socket.delay(1000);
		} else {
			if ($('websocket_status')){
				$('websocket_status').removeClass('success');
				$('websocket_status').addClass('failed');
				$('websocket_status').set('text','Není spojení s tiskárnou');
			}
		}
	},
		
	
	ws_log: function(data,ok){
		console.log('Logace: '+data);
		
		if ($('websocket_log')){
			if (ok){
				$('websocket_log').addClass('ok');
			} else {
				$('websocket_log').removeClass('ok');
			
			}
			$('websocket_log').removeClass('none');
			$('websocket_log').set('text',data);
		}
	}
	
});
window.addEvent('domready', function () {
window.fstClient = new FstPokladnaClient();	
});