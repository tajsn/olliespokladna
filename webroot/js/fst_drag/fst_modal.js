/*
Fst Modal Pospiech 
created Jakub Tyson Fastest Solution 
copyright 2016

*/
	
var FstModal = new Class({
	Implements:[Options,Events],
	options :{
		'canvas_div':'load_canvas'
	},
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		
	},
	
	// create modal
	create: function(opt){
		//console.log(opt);
		if ($('modal_load')){
			this.switch_modal = true;
			this.modal_close();
		} else {
			this.switch_modal = false;
		}
		
		if (!opt.side){
			VarsModal.modal_over = new Element('div',{'id':'modal_over'}).inject($('addon'));
		}
		
		//this.modal_over();
		
		VarsModal.modal = new Element('div',{'id':'modal','class':'preloader'}).inject($('addon'));
		if (opt.side){
			VarsModal.modal.addClass('side');
			VarsModal.modal.setStyles({
				'width':$('side_menu').getSize().x - 5,
				'height':$('side_menu').getSize().y - 5
			});
		}
		VarsModal.modal_load = new Element('div',{'id':'modal_load'}).inject(VarsModal.modal);
		//console.log(this.options);
		if (opt.close_side){
		VarsModal.modal_close = new Element('div',{'class':'modal_close'}).set('text','Zavřit okno').inject(VarsModal.modal);
		} else {
		VarsModal.modal_close = new Element('div',{'class':'modal_close fa fa_close'}).inject(VarsModal.modal);
			
		}
		VarsModal.modal_close.addEvent('click',(function(){
			this.switch_modal = false;
			this.modal_close();
		}).bind(this));
		
		if (opt['type'] == 'zakazky'){
			url = '/dashboard/load_modal/zakazky/'+opt['car_id']+'/';
			
		} 
		if (opt['type'] == 'driver'){
			url = '/dashboard/load_modal/driver/'+opt['car_id']+'/'+opt['driver_id'];
		} 
		if (typeof(url) != 'undefined'){
			VarsModal.load_req = new Request({
				url:url,
				onComplete :(function(json){
					json = JSON.decode(json);
					if (json.result == true){
						html = Elements.from(json.html);
						
						VarsModal.modal.removeClass('preloader');
						html.inject(VarsModal.modal_load);
						
						window.fst_drag.modal_events();
					}
				
				}).bind(this)
			})
			VarsModal.load_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.load_req.send();
		}
	},
	
	
	
	// modal close
	modal_close: function(e){
		if (!this.switch_modal){
			$$('.car').removeClass('active');
		}
		
		if (e){
			e.stop();
		}
		//VarsModal.modal_over.destroy();
		if (VarsModal.modal_over)
		fstvars.destroyEl(VarsModal.modal_over);
		fstvars.destroyEl(VarsModal.modal);
		fstvars.modal_clean();
		//this.modal_over();
		
	},
	
	// modal over
	modal_over: function(){
		
		if ($('switch_content')){
			if ($('switch_content').hasClass('modal_over')){
				$('switch_content').removeClass('modal_over');
			} else {
				$('switch_content').addClass('modal_over');
			}
		}
	},
	
	
	
	
	
});