/*
Fst Car Pospiech 
created Jakub Tyson Fastest Solution 
copyright 2016

*/
	
var FstCar = new Class({
	Implements:[Options,Events],
	options :{
		'canvas_div':'load_canvas'
	},
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		
	},
	
	// create car 
	create: function(car,opt){
		//var rel_div = new Element('div',{'class':'drag_relative'}).inject($('load_canvas'));
		// el.get('data-id');
		// el.get('data-name');
		// el.get('data-car_type_id');
		if (typeof car.get != 'undefined'){
			var car_id = car.get('data-id');
			var car_name = car.get('data-name');
			var car_spz = car.get('data-spz');
			var car_type_id = car.get('data-car_type_id');
			var car_type_name = car.get('data-car_type_name');
			var car_size_all = car.get('data-size_all');
			var car_weight_all = car.get('data-size_all2');
			var car_color = car.get('data-color');
			var car_weight_load = 0;
			var car_size_load = 0;
			var driver_count = 0;
			var zakazka_count = 0;
			var predano = 0;
			
			
		} else {
			var car_id = car.id;
			var car_name = car.name;
			var car_spz = car.spz;
			var car_type_id = car.car_type;
			var parent_id = car.parent_id;
			var car_type_name = car.car_type_name;
			var car_size_all = car.size_all;
			var car_weight_all = car.weight_all;
			var car_color = car.color;
			var car_weight_load = car.weight_load;
			var car_size_load = car.size_load;
			var driver_count = car.driver_count;
			var zakazka_count = car.zakazka_count;
			var predano = car.predano;
			var adresa_nalozeni = car.adresa_nalozeni;
			var adresa_vylozeni = car.adresa_vylozeni;
		}
		var car_div = new Element('li',{'class':'car fst_drop fst_drag car_'+car_id,'id':'car_'+car_id}).inject($(this.options.canvas_div).getElement('ul'));
		var zakazka_ids = new Element('div',{'class':'zakazka_ids'}).inject(car_div);
		var spz = new Element('span',{'class':'spz'}).set('text',car_spz).inject(car_div);
		var name = new Element('span',{'class':'name'}).set('text',car_name).inject(car_div);
		
		var progress_over = new Element('div',{'class':'progress_over'}).inject(car_div);
		var progress = new Element('div',{'class':'progress progr'}).inject(progress_over);
		var progress2 = new Element('div',{'class':'progress2 progr'}).inject(progress_over);
		
		new Element('div.pr').inject(progress);
		new Element('div.pr').inject(progress2);
		new Element('span').inject(progress);
		new Element('span').inject(progress2);
			
		
		var buttons = new Element('div',{'class':'driver_buttons'}).inject(car_div);
		
		//console.log(car);
		if (car_type_id == 7){
			var car_join = new Element('div',{'class':'ico car_join fa fa-chain'}).inject(buttons);
		}
		
		
		var prelozeno = new Element('div',{'class':'ico prelozeno fa fa-exclamation-triangle'}).inject(buttons);
		var car_checkbox = new Element('input',{'type':'checkbox','class':'checkbox'}).inject(buttons);
		//console.log(adresa_nalozeni);
		if (typeof adresa_nalozeni != 'undefined' && adresa_nalozeni != 'null'){
			var adr_nalozeni = new Element('div',{'class':'ico adresa_list fa  fa-map'}).inject(buttons);
			var adr_nalozeni_div = new Element('div',{'class':'adresa_list_div none'}).inject(buttons);
			//console.log(adresa_nalozeni);
			adr_nalozeni_div.set('html',adresa_nalozeni.stat+' '+adresa_nalozeni.psc+'<br />'+adresa_nalozeni.mesto+' '+adresa_nalozeni.ulice); 
			//adr_nalozeni_div.set('text',adresa_nalozeni.psc); 
		}
		var driver_ico = new Element('div',{'class':'ico driver_detail fa fa-user'}).inject(buttons);
			var driver_ico_count = new Element('div',{'class':'driver_count count'}).set('text',driver_count).inject(driver_ico);
			if (driver_count == 0) {
				driver_ico.addClass('noload'); 
				driver_ico_count.addClass('none'); 
			}
			
		var show_detail = new Element('div',{'class':'ico show_detail fa fa-list'}).inject(buttons);
			var show_detail_count = new Element('div',{'class':'zakazka_count count'}).set('text',zakazka_count).inject(show_detail);
			if (zakazka_count == 0) show_detail_count.addClass('none');
			
		var delete_car = new Element('div',{'class':'ico delete_car fa fa-trash'}).inject(buttons);
		var c_color = new Element('div',{'class':'ico car_color'}).inject(buttons);
		if (predano == 1){
			c_color.addClass('predano');
		}
		
		// set property data-value
		//console.log(car_color);
		car_div.set('data-parent_id',parent_id);
		car_div.set('data-size_all',car_size_all);
		car_div.set('data-size_all2',car_weight_all);
		car_div.set('data-size_load',car_size_load);
		car_div.set('data-weight_load',car_weight_load);
		car_div.set('data-type_id',car_type_id);
		car_div.set('data-type_name',car_type_name);
		car_div.set('data-id',car_id);
		car_div.set('data-car_color',car_color);
		car_div.set('data-drop','basket');
		car_div.set('data-type','car');
		car_div.set('data-zakazka_count',zakazka_count);
		car_div.set('data-driver_count',driver_count);
		
		
		this.set_car_color(car_div); 
		this.progress_bar_car(car_div);
		
		//this.init_dropables();
		
		/*
		this.remove_drag_element();
		
		this.save_car_req('car_to_canvas',el.get('data-id'));
		*/
		
	},
	
	// car color
	set_car_color: function(car){
		var car_color = car.get('data-car_color');
		var car_type = car.get('data-type_id');
		//console.log(car);
		car.getElement('.car_color').setStyle('background-color','#'+car_color);
		
		
		if (car_type == 6){
			car.addClass('tahac');
		}
		
	},
	
	remove_driver: function(car_id){
		if ($('car_'+car_id).getElement('.driver_count').get('text').toInt() == 1){
			$('car_'+car_id).getElement('.driver_count').addClass('none');
			$('car_'+car_id).getElement('.driver_detail').addClass('noload');
		}
		$('car_'+car_id).set('data-driver_count',$('car_'+car_id).get('data-driver_count').toInt() - 1);
		$('car_'+car_id).getElement('.driver_count').set('text',$('car_'+car_id).getElement('.driver_count').get('text').toInt() - 1);	
		
				
	},
	
	add_driver: function(car_id){
		if ($('car_'+car_id).getElement('.driver_count').get('text').toInt() == -1){
			$('car_'+car_id).getElement('.driver_count').set('text',0);
		}
		$('car_'+car_id).set('data-driver_count',$('car_'+car_id).get('data-driver_count').toInt() + 1);
		$('car_'+car_id).getElement('.driver_count').set('text',$('car_'+car_id).getElement('.driver_count').get('text').toInt() + 1);		
		if ($('car_'+car_id).getElement('.driver_count').get('text').toInt() > 0){
			$('car_'+car_id).getElement('.driver_count').removeClass('none');
			$('car_'+car_id).getElement('.driver_detail').removeClass('noload');
		}
				
	},
	
	remove_progress: function(car_id,size,weight){
		if ($('car_'+car_id).getElement('.zakazka_count').get('text').toInt() == 1){
			$('car_'+car_id).getElement('.zakazka_count').addClass('none');
		}
		
		//console.log(car_id);
		$('car_'+car_id).set('data-weight_load',$('car_'+car_id).get('data-weight_load').toInt() - weight);
		$('car_'+car_id).set('data-size_load',$('car_'+car_id).get('data-size_load').toInt() - size);
		$('car_'+car_id).set('data-zakazka_count',$('car_'+car_id).get('data-zakazka_count').toInt() - 1);
		$('car_'+car_id).getElement('.zakazka_count').set('text',$('car_'+car_id).getElement('.zakazka_count').get('text').toInt() - 1);		
		
		this.progress_bar_car($('car_'+car_id));
				
	},
	
	// progress bar on car
	progress_bar_car: function(car,drag_element){
		//console.log(car);
		//if (car) {
			this.dropEl = car;
			
			var load_size  = car.get('data-size_load');
			var load_size2 = car.get('data-weight_load');
			
			// pokud je drop na auto zakazka
			if (drag_element && drag_element.get('data-size')){
				var size = drag_element.get('data-size');
				var weight = drag_element.get('data-size2');
				
				
				car.set('data-weight_load',car.get('data-weight_load').toInt() + weight.toInt());
				car.set('data-size_load',car.get('data-size_load').toInt() + size.toInt());
				
				var load_size  = this.dropEl.get('data-size_load');
				var load_size2 = this.dropEl.get('data-weight_load');
			}
		/*
		} else {
			
			var size = this.dragEl.get('data-size');
			
			var weight = this.dragEl.get('data-size2');
			car_id = this.dropEl;
			car_id.set('data-weight_load',car_id.get('data-weight_load').toInt() + weight.toInt());
			car_id.set('data-size_load',car_id.get('data-size_load').toInt() + size.toInt());
			
			var load_size  = this.dropEl.get('data-size_load');
			var load_size2 = this.dropEl.get('data-weight_load');
			
		}
		*/
		
		//this.car_color(this.dropEl);
		
		if (this.dropEl.getElement('.progress')){
			// rozmer
			//console.log(this.dropEl);
			
			if (!this.dropEl.get('data-size_all')){this.dropEl.set('data-size_all',0); }
			if (!this.dropEl.get('data-size_all2')){this.dropEl.set('data-size_all2',0); }
			
			var progress = this.dropEl.getElement('.progress').getElement('.pr');
			var progress_max = this.dropEl.get('data-size_all').toInt();
			var progress_width = ((100 / progress_max) * (load_size.toInt()));
			if (progress_width > 100){
				this.dropEl.addClass('full');
			} else {
				this.dropEl.removeClass('full');
			}
			progress.setStyle('width',Math.round(progress_width)+'%');
			//progress.getParent('.progr').getElement('span').set('html',lang['rozmer']+': '+Math.round(progress_width)+'% ('+load_size +'/'+progress_max+' ks) '+((load_size>progress_max)?'<span class="red">+'+(load_size - progress_max)+'</span>':''));
			progress.getParent('.progr').getElement('span').set('html',Math.round(progress_width)+'% ('+load_size +'/'+progress_max+' ks) '+((load_size>progress_max)?'<span class="red">+'+(load_size - progress_max)+'</span>':''));
			
			// vaha
			var progress2 = this.dropEl.getElement('.progress2').getElement('.pr');
			var progress_max2 = this.dropEl.get('data-size_all2').toInt();
			var progress_width2 = ((100 / progress_max2) * load_size2.toInt());
			
			if (progress_width2 > 100){
				this.dropEl.addClass('full2');
			} else {
				this.dropEl.removeClass('full2');
			}
			
			progress2.setStyle('width',Math.round(progress_width2)+'%');
			
			//progress2.getParent('.progr').getElement('span').set('html',lang['vaha']+': '+Math.round(progress_width2)+'% ('+load_size2 +'/'+progress_max2+' kg) '+((load_size>progress_max)?'<span class="red">+'+(load_size2 - progress_max2)+'</span>':''));
			progress2.getParent('.progr').getElement('span').set('html',Math.round(progress_width2)+'% ('+load_size2 +'/'+progress_max2+' kg) '+((load_size2>progress_max2)?'<span class="red">+'+(load_size2 - progress_max2)+'</span>':''));
			
			if (drag_element){
				car.getElement('.zakazka_count').removeClass('none');
				car.set('data-zakazka_count',car.get('data-zakazka_count').toInt() + 1);
				car.getElement('.zakazka_count').set('text',car.getElement('.zakazka_count').get('text').toInt() + 1);
			}
		}
	},
	
	
	// remove events from element
	remove_events: function(element){
		element.getElements('*').each(function(item){
			
			events = item.retrieve('events');
			if (events)
			Object.each(events,function(it,kk){
				
				item.removeEvents(kk);
			});
		});
		if (element.retrieve('events')){
		Object.each(element.retrieve('events'),function(item,k){
			//console.log(k);
			element.removeEvents(k);
		});
		}
	},
	
});