/*
Fst Drag 
created Jakub Tyson Fastest Solution 
copyright 2016

*/
	
var FstDrag = this.FstDrag = new Class({
	Implements:[Options,Events],
	options :{
		drag_el: 'fst_drag',
		drop_el: 'fst_drop',
	},
	error_msg: {
		'zakazka': 'Zakázku musíte vložit na auto',
		'car': 'Auto musíte vložit na pracovní plochu',
		'driver': 'Řidiče musíte vložit na auto',
		'car_full': 'Auto je již plné',
	},
	timer_count : 10000,
	req_load_data: {},
	mouse_position: {},
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		this.set_canvas_size();
		this.switch_tabs();
		
		this.filtr_canvas();
		
		this.reload_elements('all');
		
		this.FstCar = new FstCar();
		this.FstModal = new FstModal();
		
		this.load_car_data();
		this.right_menu();
		this.right_menu_side();
		this.show_side_map();
		
		
		this.canvas_count_cars();
		
		//this.basket_events();
		
		
	},
	
	// show side map
	show_side_map: function(){
		
		$$('.show_side_map').addEvent('click',(function(e){
			
			this.parent_tab = e.target.getParent('.tabs');
			
			if ($('show_side_map')) $('show_side_map').destroy();
			var map_div = new Element('div',{'id':'show_side_map'}).inject($('load_map_side'));
			var map_close = new Element('div',{'id':'show_side_map_close'}).set('text','X').inject(map_div);
			var map_inject = new Element('div',{'id':'show_side_map_inject'}).inject(map_div);
			
			$('map_filtr').removeClass('none');
			$('map_filtr').setStyles({
				'width':$('canvas_filtr').getCoordinates().width - 1,
			});
			
			map_div.setStyles({
				'width':$('load_canvas').getCoordinates().width-2,
				'height':$('load_canvas').getCoordinates().height-2,
				'top':$('load_canvas').getCoordinates().top+1,
				'left':$('load_canvas').getCoordinates().left+1,
			});
			
			// store cars from canvas
			$('map-car-filtr').empty();
			new Element('option',{'value':'','text':'Naložit na auto'}).inject($('map-car-filtr'));
			
			$('load_canvas').getElements('.car').each(function(car){
				new Element('option',{'value':car.get('data-id'),'text':car.getElement('.name').get('text')}).inject($('map-car-filtr'));
			});
			//$('map-car-filtr')
			
			map_close.addEvent('click',function(e){
				e.stop();
				map_div.destroy();
				
				$('map_filtr').addClass('none');
			});
			
			this.init_big_map();
			
		}).bind(this));
	},
	
	// init big map
	init_big_map: function(){
		
		if (typeof google == 'undefined'){
				
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&callback=window.fst_drag.create_big_map";
			document.body.appendChild(script);
		
		} else {
			//alert('');
			this.create_big_map();
		}
	},
	
	// store zakazka list
	store_zakazka_list: function(){
		var zak_list = this.parent_tab.getElements('.element_drag');
		
		this.nalozeni_list = [];
		this.vylozeni_list = [];
		
		zak_list.each((function(item){
			gps = item.getElements('.gps');
			
			name = item.getElement('.name').get('text');
			zakazka_id = item.getElement('.zakazka_id').get('text');
			gps.each((function(g){
				if (g.hasClass('nalozeni')){
					latLng = g.get('text');
					if (latLng != '' || latLng != ','){
						latLng = latLng.split(',');
						this.nalozeni_list.push({'lat':latLng[0],'lng':latLng[1],'name':name,'zakazka_id':zakazka_id});
					}
				}
				if (g.hasClass('vylozeni')){
					latLng = g.get('text');
					if (latLng != '' || latLng != ','){
						latLng = latLng.split(',');
						this.vylozeni_list.push({'lat':latLng[0],'lng':latLng[1],'name':name,'zakazka_id':zakazka_id});
					}
				}
			}).bind(this));
		}).bind(this));
	},
	
	// change tab redraw map
	change_tab_map: function(parent_tab){
		this.parent_tab = parent_tab;
		this.store_zakazka_list();
		this.create_markers(this.nalozeni_list);
		
	},
	
	// create big map
	create_big_map: function(){
		var myLatLng = {lat: 49.8144897, lng: 18.2092833};
		this.map = new google.maps.Map($('show_side_map_inject'), {
			zoom: 5,
			//disableDefaultUI: true,
			center: myLatLng
		});
		
		this.marker_list = {};
		
		this.store_zakazka_list();
		this.create_markers(this.nalozeni_list);
		
		
		
		$('map-adresa').addEvent('change',(function(e){
			e.stop();
			
			// nalozeni
			if (e.target.value == 1){
				this.create_markers(this.nalozeni_list);
			}
			// vylozeni
			if (e.target.value == 2){
				this.create_markers(this.vylozeni_list);
				
			}
			
		}).bind(this));
		
		
		//console.log(this.marker_list);
		//console.log(nalozeni_list);
		//console.log(vylozeni_list);
		
	},
	
	// create markers
	create_markers: function(data){
		this.selected_list = [];
		
		Object.each(this.marker_list,function(mar){
			mar.setMap(null);
		});
		
		data.each((function(item){
			this.marker_list[item.zakazka_id] = new google.maps.Marker({
				position: {'lat':item.lat.toFloat(),'lng':item.lng.toFloat()},
				map: this.map,
				title: item.name,
				zakazka_id: item.zakazka_id,
			});
			
			this.marker_list[item.zakazka_id].setIcon('https://maps.google.com/mapfiles/ms/icons/red-dot.png');
			
			// mouseenter marker
			google.maps.event.addListener(this.marker_list[item.zakazka_id], 'mouseover', (function() {
				if ($('zakazka_'+item.zakazka_id)){
					$('zakazka_'+item.zakazka_id).fireEvent('mouseenter',$('zakazka_'+item.zakazka_id));
				}
				
				this.marker_list[item.zakazka_id].setIcon('https://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
			}).bind(this));
			
			// mouseout marker
			google.maps.event.addListener(this.marker_list[item.zakazka_id], 'mouseout', (function() {
				if ($('zakazka_'+item.zakazka_id)){
					$('zakazka_'+item.zakazka_id).fireEvent('mouseleave',$('zakazka_'+item.zakazka_id));
				}
				if (!this.marker_list[item.zakazka_id].select){
					this.marker_list[item.zakazka_id].setIcon('https://maps.google.com/mapfiles/ms/icons/red-dot.png');
				} else {
					this.marker_list[item.zakazka_id].setIcon('https://maps.google.com/mapfiles/ms/icons/green-dot.png');
				}
			}).bind(this));
			
			// click marker
			google.maps.event.addListener(this.marker_list[item.zakazka_id], 'click', (function() {
				
				if (!this.marker_list[item.zakazka_id].select){
					this.selected_list.push(item.zakazka_id);
					this.marker_list[item.zakazka_id].select = true;
					this.marker_list[item.zakazka_id].setIcon('https://maps.google.com/mapfiles/ms/icons/green-dot.png');
				} else {
					this.selected_list.erase(item.zakazka_id);
					this.marker_list[item.zakazka_id].select = false;
					this.marker_list[item.zakazka_id].setIcon('https://maps.google.com/mapfiles/ms/icons/red-dot.png');
					
				}
				
				this.marker_selected_event();
				//console.log(this.selected_list);
				//marker.setIcon("Your Image");                                    
				//infowindow.open(map);
			}).bind(this));
			
			
		}).bind(this));
		this.marker_selected_event();
	},
	
	// marker selected event
	marker_selected_event: function(){
		$('map_count').set('text',this.selected_list.length);
		
		$('map-car-filtr').removeEvents('change');
		$('map-car-filtr').addEvent('change',(function(e){
			if (this.selected_list.length == 0){
				FstError('Musíte vybrat nějaké zakázky');
				$('map-car-filtr').value = '';
				return false;
			}
			var car_id = e.target.value;
			//console.log(car_id);
			
			this.selected_list.each((function(item){
				if ($('zakazka_'+item) && $('car_'+car_id)){
					this.dropEl = $('car_'+car_id);
					this.dragEl = $('zakazka_'+item);
					
					this.marker_list[item].setMap(null);
					//console.log(this.dragEl);
					//console.log(this.dropEl);
					this.add_zakazka_to_car();
				}
			}).bind(this));
			
		}).bind(this));
	},
	
	// switch tabs 
	switch_tabs: function(){
		var tab_list = $('switch_content').getElements('.tabs');
		tab_list.each(function(tab,k){
			if (k > 0){
				tab.addClass('none');
			}
		});
		
		$('switch_content_menu').getElements('a').addEvent('click',(function(e){
			e.stop();
			$('switch_content_menu').getElements('a').removeClass('active');
			e.target.addClass('active');
			tab_list.addClass('none');
			this.load_data(e.target.get('data-load'));
			tab_list[e.target.get('data-type')].removeClass('none');
			
			$('switch_content').getElements('.tabs').each((function(item){
				if (!item.hasClass('none')){
					this.change_tab_map(item);
			
				}
			}).bind(this));
			
			
		}).bind(this));
		
		
	},
	
	// reload all elements
	reload_elements: function(type){
		if (type == 'all'){
			//this.load_data('canvas');
			this.load_data('zakazky');
			this.load_data('sklad');
			this.load_data('auta');
			this.load_data('driver');
		}
		
		if (type == 'zakazky'){
			this.load_data('zakazky');
			
		}
		if (type == 'canvas'){
			this.load_car_data();
			
		}
		if (type == 'sklad'){
			this.load_data('sklad');
			
		}
		if (type == 'auta'){
			this.load_data('auta');
			
		}
		if (type == 'driver'){
			this.load_data('driver');
			
		}
	},
	
	// canvas size
	set_canvas_size: function(){
		if (Cookie.read('side_menu_width')){
			side_menu_width = Cookie.read('side_menu_width');
		}
		if (typeof side_menu_width != 'undefined'){
			//console.log(side_menu_width);
			$('side_menu').setStyle('width',side_menu_width.toInt());
		}
		
		$('body').addClass('overflow');
		
		$('dashboard').setStyle('height',window.getSize().y-100);
		
		$('canvas_left').setStyle('width',window.getSize().x - $('side_menu').getSize().x -15);
		$('side_menu').setStyle('height',window.getSize().y-60);
		
		
		$$('.filtrace').each((function(filtr){
			var fh = filtr.getSize().y;
			var lh = filtr.getNext('.load_data').getSize().y;
			
			filtr.getNext('.load_data').setStyle('height',lh - fh - 10);
		}));
		$('load_canvas').setStyle('height',$('side_menu').getSize().y - $('canvas_filtr').getSize().y - 10);
		
		var myResize = $('side_menu').makeResizable({ 
			handle:$('side_drag'),
			invert :true,
			modifiers: {x: 'width', y: false},
			limit: {x: [400, 1800]},
			onComplete: function(e){
				$('canvas_left').setStyle('width',window.getSize().x - $('side_menu').getSize().x -15);
				Cookie.write('side_menu_width',$('side_menu').getStyle('width').toInt());
				//alert('Done resizing.');
			}
		});
		
		
	},
	
	
	// load data
	load_data: function(type){
			$('load_'+type).addClass('preloader');
			
			this.store_filtr_params();
			
			this.req_load_data[type] = new Request({
				
				url:'/dashboard/load_data/'+type+'/'+this.filtr_params,
				onComplete :(function(json){
					var json = JSON.decode(json);
					if (json && json.result == true){
						
						$('load_'+type).removeClass('preloader');
						html = Elements.from(json.html);
						$('load_'+type).empty();
						html.inject($('load_'+type));
						
						this.req_load_data[type].cancel();
						
						this.init_after_load();
						//this.init_over_dropables();
						//this.init_dropables();
						if (!$('side_menu_auta').hasClass('active')){
							window.fstRightMenu.options.list[100].disable = true;
							window.fstRightMenu.options.list[101].disable = true;
						} else {
							window.fstRightMenu.options.list[100].disable = false;
							window.fstRightMenu.options.list[101].disable = false;
							
						}
						//console.log(window.fstRightMenu);
						/*
						if (type == 'canvas'){
							this.init_events();
							this.init_car_events();
							this.default_progress_bar_car();
						}
						*/
						
					}
				}).bind(this)
			})
			this.req_load_data[type].setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_load_data[type].send();
	},
	
	// init after load element
	init_after_load: function(){
		this.init_dropables();
						
		this.init_sortables();
		date_picker();
		
	},
	
	// init sortables
	init_sortables: function(){
		$$('.sort_list').each((function(item){
			
			// odebrat hlavni drag drop z elementu
			item.getElements('.handle_sort').addEvent('mousedown',(function(e){
				this.drag_move_function[e.target.getParent('.element_drag').get('id')].detach();
			}).bind(this));
			
			var mySortables = (new Sortables(item, {
				onComplete: (function(e){
					
					this.drag_move_function[e.get('id')].attach();
					
					var ser = mySortables.serialize();

					// zjisteni ID drag elementu
					var ids = [];
					ser.each(function(it){
						ids.push(it.substr(8,20).toInt());
					});
					
					// ulozeni poradi do DB
					this.save_order_req = new Request.JSON({
						url:'/dashboard/save_order/zakazka/'+ids,
						onComplete :(function(json){
							this.save_order_req.cancel();
						
						}).bind(this)
					})
					this.save_order_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					this.save_order_req.send();
				
				}).bind(this),
				//clone: true,
				//revert: true,
				handle:'.handle_sort',
				revert: { duration: 500, transition: 'elastic:out' }
			}));
			
		}).bind(this));
	},
	
	
	// init drag and drop to elements
	init_dropables: function(){
		
		this.init_elements_events();
		
		//return true;
		
		/*
		var mySortables = (new Sortables(null, {
				clone: true,
				revert: true,
				opacity: 0.7,
				onComplete: (function(e){
					console.log(e);
				})
		}));
		$$('.ul_drag').each((function(drag){
			mySortables.addLists(drag);
		}));
		
		return false;
		*/
		//console.log(this.options);
		//console.log($$('.'+this.options.drag_el));
		this.drag_move_function = {};
		$$('.'+this.options.drag_el).each((function(drag){
			drag.removeEvents('mousedown');
			drag.removeEvents('emptydrop');
			
			// zjisteni drop elementu mimo hlastni element
			dropables_els = $$('.'+this.options.drop_el);
			dropables_els.each(function(it,k){
				if (it == drag){
					delete dropables_els[k];
				}
			});
			
			this.drag_move_function[drag.get('id')] = new Drag.Move(drag, {
				droppables: dropables_els,
				includeMargins:false,
				onDrop: (function(element, droppable, event){
					if (!droppable) {
						//console.log(element, ' dropped on nothing');
					} else  {
						this.mouse_position = {
							x:event.page.x,
							y:event.page.y
						}
						//console.log(event); 
						//console.log(element, 'dropped on', droppable, 'event', event);
						this.event_drop(droppable,element);
						droppable.removeClass('can_drop');
					}
				}).bind(this),
				
				onDrag: (function(element, droppable){
					//console.log(element);
					// odebrani overflow sidemenu
					if (element.getParent('.load_data')){
						element.getParent('.load_data').removeClass('overflow');
					}
					if ($('modal_load')){
						$('modal_load').removeClass('overflow');
						
					}
					// schovani tooltip 
					if (element.getElement('.detail_hover')){
						element.getElement('.detail_hover').addClass('none');
					}
				}).bind(this),

				onEnter: (function(element, droppable){
					//console.log('enter');
					
					droppable.addClass('can_drop');
					//console.log(element, 'entered', droppable);
				}).bind(this),

				onLeave: function(element, droppable){
					//console.log('leave');
					droppable.removeClass('can_drop');
					//console.log(element, 'left', droppable);
				}
			});
			drag.addEvent('emptydrop', function(){
					//this.setStyle('background-color', '#faec8f');
			});
		}).bind(this));
		
		
	},
	 
	 
	 
	// event drop 
	event_drop: function(drop_element,drag_element){
		//console.log(drag_element);
		this.dragEl_type = drag_element.get('data-type');
		this.dragEl_drop = drag_element.get('data-drop');
		
		this.dropEl_type = drop_element.get('data-type');
		this.dropEl = drop_element;
		this.dragEl = drag_element;
		
		if (drag_element.get('data-car_id')){
			this.dragEl_car_id = drag_element.get('data-car_id');
			this.dragEl_size = drag_element.get('data-size');
			this.dragEl_weight = drag_element.get('data-weight');
		}
		
		//console.log(this.dragEl_drop);	
		//console.log(this.dropEl_type);	
		
		// pokud muzu umistit na drop tak zavolej drop function
		if (this.dragEl_drop == this.dropEl_type){
			this.drop_function(this.dropEl,this.dragEl);
			
		} else {
			
			if (this.dragEl_drop != 'basket'){
				var error = this.error_msg[this.dragEl_type];
				FstError(error);
			}
		}
		
	},
	
	
	// drop function init after drop element
	drop_function: function(){
		//console.log(this.dragEl_drop);
		
		// pridani overflow na side menu
		if (this.dragEl.getParent('.load_data')){
			this.dragEl.getParent('.load_data').addClass('overflow');
		}
		
		if ($('modal_load')){
			$('modal_load').addClass('overflow');
						
		}
		// drop auto to canvas
		if (this.dragEl_type == 'car' && this.dragEl_drop == 'canvas'){
			this.car_create();
		}
		// drop auto to basket
		if (this.dragEl_type == 'car' && this.dragEl_drop == 'basket'){
			this.car_to_basket();
		}
		// drop zakazka
		if (this.dragEl_type == 'zakazka'){
			
			// remove zakazka from car to another car
			if (this.dragEl_car_id){
				this.FstCar.remove_progress(this.dragEl_car_id,this.dragEl_size,this.dragEl_weight);
			}
			
			this.add_zakazka_to_car();
		
		}
		// drop ridic to car
		if (this.dragEl_type == 'driver'){
			this.drop_driver();
		
		}
		
	},
	
	
	no_car_canvas: function(){
		if (!$('load_canvas').getElement('ul').getElement('li')){
			new Element('li',{'class':'noresult'}).set('text',lang['nenalezeny_auta']).inject($('load_canvas').getElement('ul'));
		}
	},
	
	load_car_data: function(){
		// load car data from db
		$('load_canvas').addClass('preloader');
		if ($('load_canvas').getElement('.noresult')){
			$('load_canvas').getElement('.noresult').destroy();
		}		
		this.load_car_data_req = new Request.JSON({
			url:'/dashboard/load_data/canvas/'+this.filtr_params,
			onComplete :(function(json){
				$('load_canvas').removeClass('preloader');
				$('load_canvas').getElement('ul').empty();
				//console.log(json.data);
				//if (json.data.length == 0){
					//new Element('li',{'class':'noresult'}).set('text',lang['nenalezeny_auta']).inject($('load_canvas').getElement('ul'));
				//}
				
				json.data.each((function(car){
					this.FstCar.create(car);
					this.init_dropables();
					this.init_car_events();
					//fst_checkbox();
				}).bind(this));
				this.load_car_data_req.cancel();
				
				this.no_car_canvas();
		
				
			}).bind(this)
		})
		this.load_car_data_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.load_car_data_req.send();
		
	},
	
	// create zakazka after drop
	add_zakazka_to_car: function(){
		
		
		// progress bar
		this.FstCar.progress_bar_car(this.dropEl,this.dragEl);
		
		// fade and remove drag element
		this.remove_drag_element();
		
		// save connect
		this.save_connect_req('zakazka_to_car',this.dropEl.get('data-id'),this.dragEl.get('data-id'),this.dragEl.get('data-size2'),this.dragEl.get('data-size'));
		//console.log(this.dragEl.retrieve('events'));
		
		//}
		
	},
	
	// remove drag element
	remove_drag_element: function(){
		// remove events from drag element 
		this.remove_events(this.dragEl);
		
		this.dragEl.set('tween',{
			duration:100,
			onComplete: (function(){
				this.dragEl.destroy();
						
			}).bind(this)
		});
		this.dragEl.fade(0);
	},
	
	
	// save connection request
	save_connect_req: function(type,car_id,zakazka_id,weight,size){
		var url = '/dashboard/save_data/';
		if (type) url += type+'/';
		if (car_id) url += car_id+'/';
		if (zakazka_id) url += zakazka_id+'/';
		if (weight) url += weight+'/';
		if (size) url += size+'/';
		
		//console.log(url);
		//return false;
		
		Vars.save_car_request = new Request({
			url:url,
			onError: Vars.backup_error = (function(data){
				console.log(data);
				send_error(data,'Chyba JSON');
			}).bind(this),
			onComplete :(function(json){
				json = JSON.decode(json);
				if (json.result == true){
					
					if (type == 'driver_to_list'){
						this.load_data('driver');
						this.FstCar.remove_driver(car_id);
					}
					if (type == 'driver_to_car'){
						$('load_canvas').getElements('.car').each((function(car){
							this.FstCar.remove_driver(car.get('data-id'));
						}).bind(this));
						
						this.FstCar.add_driver(car_id);
					
					
					}
					if (type == 'driver_to_sklad'){
						this.load_data('driver');
					}
					if (type == 'delete_from_car'){
						this.load_data('zakazky');
					}
					if (type == 'car_from_canvas'){
						this.load_data('auta');
					}
					if (type == 'unjoin_prives'){
						this.load_data('auta');
					}
					if (type == 'join_prives'){
						this.load_data('auta');
					}
					if (type == 'car_to_sklad'){
						this.load_car_data();
					}
					
					if (type == 'car_zakazky_to_sklad'){
						this.load_car_data();
						this.load_data('sklad');
					}
					
					if (type == 'zakazka_to_car'){
						this.load_data('zakazky');
						this.load_data('sklad');
					}
					
				
				}
				if (json.result == false){
					if (json.message){
						FstError(json.message);
						
						this.load_car_data();
					}
				}
				
			}).bind(this)
		});
		Vars.save_car_request.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		Vars.save_car_request.send();
	},
	
	
	// init car events
	init_car_events: function(){
		// delete car
		$$('.delete_car').each(function(it){
			it.removeEvents('click');
			it.addEvent('click',(function(e){
				if (confirm(lang['opravdu_smazat_vratit_car'])){
					var car = e.target.getParent('.car');
					e.stop();
					// auto ma nalozeny zakazky
					if (car.get('data-zakazka_count').toInt() > 0 || car.get('data-driver_count').toInt() > 0){
						FstError(lang['auto_ma_zakazky']);
						return false;
					}
					
					this.save_connect_req('car_from_canvas',car.get('data-id'));
						
					this.remove_events(car);
					car.destroy();
					this.no_car_canvas();
				}
				
			}).bind(this));
		}.bind(this));
		
		// show zakazky on car 
		$$('.show_detail').each(function(it){
			it.removeEvents('click');
			it.addEvent('click',this.open_modal.bind(this));
		}.bind(this));
		
		// show drivers on car
		$$('.driver_detail').each(function(it){
			it.removeEvents('click');
			it.addEvent('click',this.open_modal.bind(this));
		}.bind(this));
		
		// show adresa actual
		$$('.adresa_list').each(function(it){
			it.removeEvents('mouseenter');
			it.removeEvents('mouseleave');
			it.addEvents({
				'mouseenter': function(e){
					
					e.target.getParent('.driver_buttons').getElement('.adresa_list_div').toggleClass('none');
				},
				
				'mouseleave': function(e){
					e.target.getParent('.driver_buttons').getElement('.adresa_list_div').toggleClass('none');
				}
				
			});
		}.bind(this));
		
		// car join prives ico
		$$('.car_join').each(function(it){
			it.removeEvents('click');
			it.addEvent('click',this.car_join.bind(this));
		}.bind(this));
	
		// after load car join
		$$('.car').each((function(car){
			//console.log(car);
			if (car.get('data-parent_id') && car.get('data-parent_id').toInt() > 0){
				//console.log(car);
				var car_id = car.get('data-id');
				var parent_id = car.get('data-parent_id');
				if ($('load_canvas').getElement('.car_'+parent_id)){
					if (parent_id > 0){
						//console.log(car_id);
						this.car_join($('load_canvas').getElement('.car_'+car_id).getElement('.car_join'),true);
					}
				}
			}
		}).bind(this));
	
	},
	
	// open modal
	open_modal: function(e){
		//new Event(e).stop();
		e.stop();
		if (e.target.hasClass('noload')){
			FstError(lang['neni_ridic']);
			return false;
		}
		
		if (e.target.hasClass('show_detail')){
			var opt = { 
				'type':'zakazky',
				'car_id':e.target.getParent('.car').get('data-id'),
				'close_side':true,
				'side':true,
			}
			$$('.car').removeClass('active');
			e.target.getParent('.car').addClass('active');
			
		}
		if (e.target.hasClass('driver_detail')) {
			var opt = { 
				'type':'driver',
				'driver_id':e.target.get('data-id'),
				'car_id':e.target.getParent('.car').get('data-id'),
				'close_side':true,
				'side':true,
			}
			$$('.car').removeClass('active');
			e.target.getParent('.car').addClass('active');
		}
		//console.log(opt);
		this.FstModal.create(opt);
	},
	
	// modal events
	modal_events: function(){
		
		// init dropables
		this.init_dropables();
		this.init_sortables();
		
		
		// delete driver from modal
		$$('.send_zakazka').addEvent('click',VarsModal.send_event = (function(e){
			e.stop();
			if (e.target.hasClass('sended')){
				FstError('Již bylo posláno');
				return false;
			}
			
			if (e.target.get('data-confirm') == 'sklad_show'){
				e.target.getNext('.show_sklad').removeClass('none');
				e.target.addClass('none');
				return false;
			}
			
			if (e.target.get('data-confirm') == 'ridic'){
				var confirm_title = lang['opravdu_poslat_ridic'];
				
			} else {
				var confirm_title = lang['opravdu_poslat_sklad'];
				var sklad_send = true;
			}
			
			
			if (confirm(confirm_title)){
				
				if (sklad_send){
					// remove progress
					this.FstCar.remove_progress(e.target.get('data-car_id'),e.target.get('data-size'),e.target.get('data-size2'));
				}
				
				this.send_zakazka_req = new Request.JSON({
					url:e.target.href,
					onComplete :(function(json){
						this.send_zakazka_req.cancel();
						if (json.r){
							FstAlert(json.m);
							if (json.delete){
								e.target.getParent('.zakazky_list').destroy();
							} else {
								e.target.addClass('sended');
							}
						}
						//console.log(e.target.get('data-car_id'));
						//this.progress_bar_car(car_id);
						
						// reload zakazky
						this.load_data('zakazky');
						this.load_data('sklad');
					
					}).bind(this)
				})
				this.send_zakazka_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.send_zakazka_req.send();
				
			}
		}).bind(this));
		
		
		// delete driver from modal
		$$('.delete_driver').addEvent('click',VarsModal.delete_driver_event = (function(e){
			//new Event(e).stop();
			e.stop();
			if (confirm(lang['opravdu_smazat'])){
				// delete from canvas driver ID
				/*
				if ($('modal_load').getElements('.driver_list').length == 1){
				$('load_canvas').getElements('.car').each(VarsModal.car_each = function(car){
					if (car.get('data-id') == e.target.get('data-car_id')){
						car.getElement('.driver_detail').addClass('noload');
						car.getElement('.driver_detail').removeProperty('data-id');
					}
				});
				}
				*/
				this.save_connect_req('driver_to_list',e.target.get('data-car_id'),e.target.get('data-id'));
				
				
				// remove driver from modal
				e.target.getParent('.element_drag').destroy();
				
				this.modal_noresult();
				
				// modal close
				if ($('modal_load').getElements('.driver_list').length == 0){
					//this.modal_close();
				}
			}
		}).bind(this));
		
		
		// delete zakazka from modal
		$$('.delete_zakazka').addEvent('click',VarsModal.delete_zakazka_event = (function(e){
			e.stop();
			if (confirm(lang['opravdu_smazat_vratit'])){
				// delete from canvas driver ID
				this.FstCar.remove_progress(e.target.get('data-car_id'),e.target.get('data-size'),e.target.get('data-size2'));
				
				this.save_connect_req('delete_from_car',e.target.get('data-car_id'),e.target.get('data-id'));
				
				// remove driver from modal
				e.target.getParent('.element_drag').destroy();
				
				this.modal_noresult();
				
				
				// modal close
				//this.modal_close();
				
			}
		}).bind(this));
		
		// done zakazka from modal
		$$('.done_zakazka').addEvent('click',VarsModal.delete_zakazka_event = (function(e){
			e.stop();
			if (confirm(lang['opravdu_zakazka_done'])){
				// delete from canvas driver ID
				this.FstCar.remove_progress(e.target.get('data-car_id'),e.target.get('data-size'),e.target.get('data-size2'));
				
				this.save_connect_req('done_zakazka',e.target.get('data-car_id'),e.target.get('data-id'));
				
				// remove driver from modal
				e.target.getParent('.element_drag').destroy();
				
				this.modal_noresult();
				
				
				// modal close
				//this.modal_close();
				
			}
		}).bind(this));
	},
	
	// modal no result
	modal_noresult: function(el){
		var modal = $('modal_load');
		//console.log(modal.getElement('li'));
		if (!modal.getElement('li')){
			new Element('p',{'class':'noresult'}).set('text',lang['no_result']).inject(modal);
		}
	},
	
	// drop driver to car
	drop_driver: function(){
		this.dropEl.getElement('.driver_detail').removeClass('noload');
		this.dropEl.getElement('.driver_detail').set('data-id',this.dragEl.get('data-id'));
				
		this.save_connect_req('driver_to_car',this.dropEl.get('data-id'),this.dragEl.get('data-id'));		
		
		// fade and remove drag element
		this.remove_drag_element();
		this.load_data('driver');
		
		
		
	},
	
	// car create
	car_create: function(){
		if ($('load_canvas').getElement('.noresult')){
			$('load_canvas').getElement('.noresult').destroy();
		}		
		
		this.FstCar.create(this.dragEl);
		
		if (this.dragEl.getElement('.element_drag')){
			this.FstCar.create(this.dragEl.getElement('.element_drag'));
		}
		
		this.init_dropables();
		this.init_car_events();		
		this.save_connect_req('car_to_canvas',this.dragEl.get('data-id'));		
		
		this.load_data('auta');
		// fade and remove drag element
		this.remove_drag_element();
		
		
		if (this.dragEl.getElement('.element_drag')){
			var join_list = $('load_canvas').getElements('.car_join');
			join_list[join_list.length-1].fireEvent('click',join_list[join_list.length-1]);
		}
		
		
	},
	
	
	// filtr canvas
	filtr_canvas: function(){
		if (window.location.hash){
			params = window.location.hash.split('f=');
			params = JSON.decode(Base64.decode(params[1]));
			//console.log(params);
			$$('.filtrace').each((function(filtr){
				
				filtr.getElements('.get_params').each(function(item){
					//console.log(params);
					if (params[item.get('data-type')] && !item.get('data-value')){
						item.value = params[item.get('data-type')];
					}
				});
			}));
			//console.log('a');return false;
		}
		$$('.filtrace').each((function(item){
			item.getElements('.get_params').removeEvents('change');
			
			item.getElements('.get_params').addEvent('change',(function(e){
				//console.log(item.get('data-type'));
				this.store_filtr_params();
				this.reload_elements(item.get('data-type'));
				
			}).bind(this));
		}).bind(this));
		
		$$('.select_checkboxs').addEvent('click',function(e){
			e.stop();
			if (e.target.get('data-type') == 'all'){
				$('load_canvas').getElements('.car').each(function(car){
					car.getElement('.checkbox').set('checked','checked');
				});
			}
			if (e.target.get('data-type') == 'none'){
					
				$('load_canvas').getElements('.car').each(function(car){
					car.getElement('.checkbox').removeProperty('checked','checked');
				});
			}
		});
		
	},
	
	
	// store filtr params
	store_filtr_params: function(){
		var params = {};
		$$('.filtrace').each((function(item){
			item.getElements('.get_params').each(function(item){
				if (item.value != '' || item.value != 0){
					params[item.get('data-type')] = item.value;
				}
			});
		}).bind(this));
		params = JSON.encode(params);
		//console.log(params);
		window.history.pushState(null,null,'#f='+Base64.encode(params));
		
		this.filtr_params = params;
	},
	
	// elements events
	init_elements_events: function(){
		//console.log(window.fstsystem);
		window.fstsystem.drag_elements_events();
		
	},
	
	
	// remove events from element
	remove_events: function(element){
		element.getElements('*').each(function(item){
			
			events = item.retrieve('events');
			if (events)
			Object.each(events,function(it,kk){
				
				item.removeEvents(kk);
			});
		});
		if (element.retrieve('events')){
		Object.each(element.retrieve('events'),function(item,k){
			//console.log(k);
			element.removeEvents(k);
		});
		}
	},
	
	right_menu: function(){
		var list_menu = {};
		var sklad_list = JSON.decode($('sklad_list').get('text'));
		Object.each(sklad_list,function(item,k){
			list_menu[k] = {'fce':'send_selected','id':k,'name':'Odeslat auto do skladu '+item}
		});
		
		//console.log(sklad_list);
		list_menu[Object.getLength(sklad_list)+1] = {'fce':'send_selected','id':99,'name':'Odeslat auto mimo sklad'}
		list_menu[Object.getLength(sklad_list)+2] = {'fce':'send_another_dispecer','id':100,'name':'Odeslat auto jinému dispečerovi'}
		Object.each(sklad_list,function(item,k){
			list_menu[Object.getLength(sklad_list)+k] = {'fce':'send_selected_zakazky','id':k,'name':'Odeslat zakázky z auta do skladu '+item}
		});
		Object.each(sklad_list,function(item,k){
			list_menu[Object.getLength(sklad_list)+k*2] = {'fce':'send_selected_report','id':k,'name':'Odeslat report auta do skladu '+item}
		});
		var opt = {
			'element':$('load_canvas'),
			'list':list_menu//{
				//1: {'fce':'send_selected','name':'Odeslat vybrané'},
				//2: {'fce':'send_selected2','name':'Další funkce'},
				//3: {'fce':'send_selected2','name':'Další funkce2'},
			//}
		}
		window.fstMenu = new FstMenu(opt);
		
	},
	
	right_menu_side: function(){
		var list_menu = {};
		var sklad_list = JSON.decode($('sklad_list').get('text'));
		Object.each(sklad_list,function(item,k){
			list_menu[k] = {'fce':'send_selected_item','id':k,'name':'Odeslat položku do '+item}
		});
		
		//console.log(sklad_list);
		list_menu[100] = {'fce':'join_prives','id':10,'name':'Spojit auto s přívěsem'}
		list_menu[101] = {'fce':'unjoin_prives','id':11,'name':'Rozpojit auto s přívěsem'}
		//list_menu[Object.getLength(sklad_list)+2] = {'fce':'send_another_dispecer','id':100,'name':'Odeslat auto jinému dispečerovi'}
		
		var opt = {
			'element':$('side_menu'),
			'list':list_menu//{
				//1: {'fce':'send_selected','name':'Odeslat vybrané'},
				//2: {'fce':'send_selected2','name':'Další funkce'},
				//3: {'fce':'send_selected2','name':'Další funkce2'},
			//}
		}
		window.fstRightMenu = new FstMenu(opt);
		
	},
	
	// car to basket
	car_to_basket: function(x,y,car_list){
		if (x) this.mouse_position.x = x;
		if (y) this.mouse_position.y = y;
		//console.log(this.mouse_position);
		
		$('select_dispecer').removeClass('none');
		//console.log($('select_dispecer').getElement('div').getSize().x);
		$('select_dispecer').getElement('div').setStyles({
			'left':this.mouse_position.x-($('select_dispecer').getElement('div').getSize().x/2 - 50),
			'top':this.mouse_position.y
		});
		this.basket_events(car_list);
		//this.remove_drag_element();
	},
	
	// event basket action
	basket_events: function(car_list){
		
		$('CloseSelectDispecer').addEvent('click',function(e){
			e.stop();
			$('select_dispecer').addClass('none');
			
		});
		
		$('SelectDispecer').addEvent('change',(function(e){
			if (this.dragEl){
				var car = this.dragEl;
				this.save_connect_req('car_to_basket',car.get('data-id'),e.target.value);
				this.remove_drag_element();
				$('select_dispecer').addClass('none');
			}
			if (car_list){
				car_list.each((function(car){
					//console.log($('car_'+car));
					if ($('car_'+car)){
						this.save_connect_req('car_to_basket',car,e.target.value);
						
						$('car_'+car).destroy();
						$('select_dispecer').addClass('none');
					}
				}).bind(this));
			}
			
			
		}).bind(this));
		
	},
	
	// car join with previous
	car_join: function(e,noremove){
		if (!e.target){
			e.target = e;
		}
		//console.log(e.target.getParent('li').getPrevious('li'));
		//e.stop();
		//if (e.target.getParent('li.fst_drag').getPrevious('li.fst_drag')){
		
			if (e.target.hasClass('fa-chain')){
				var previous = e.target.getParent('li').getPrevious('li');
				var current = e.target.getParent('li');
				current.removeEvents('mousedown');
				current.removeClass('fst_drag');
				
				e.target.removeClass('fa-chain')
				e.target.addClass('fa-chain-broken')
				e.target.getNext('.driver_detail').addClass('none');
				e.target.getNext('.delete_car').addClass('none');
				
				if (previous.hasClass('tahac')){
					FstError('S prechozím autem nelze spojit');
					return true;
				}
				
				current.inject(previous);
				
				current.setStyles({
					'position':'absolute',
					'left':previous.getSize().x-3,
					'top':-3,
					//'width':previous.getSize().x,
					//'height':previous.getSize().y,
					
				});
				
				previous.addClass('prives');
				this.save_connect_req('join_prives',previous.get('data-id')+','+current.get('data-id'));
		
				//previous.getElement('.car_join').addClass('none');
			} else {
				if (noremove) {
					return false;
				}
				var previous = e.target.getParent('li').getParent('li.fst_drag');
				var current = e.target.getParent('li');
				
				e.target.addClass('fa-chain')
				e.target.removeClass('fa-chain-broken')
				e.target.getNext('.driver_detail').removeClass('none');
				e.target.getNext('.delete_car').removeClass('none');
				current.addClass('fst_drag');
				//console.log(current);
				//console.log(previous);
				previous.removeClass('prives');
				current.setStyles({
					'position':'',
					'left':'',
					'right':'',
					'top':'',
					'width':'',
					'height':'',
					
				});
				//this.save_connect_req('remove_prives',previous.get('data-id'),current.get('data-id'));
				this.save_connect_req('unjoin_prives',previous.get('data-id')+','+current.get('data-id'));
		
				current.inject(previous,'after');
				//this.init_over_dropables();
				this.init_dropables();
				
			}
			
			/*
			previous.setStyles({
				'width':512,
			});
			current.setStyles({
				'position':'absolute',
				'right':-previous.getSize().x,
				'top':0,
				'width':previous.getSize().x,
				'height':previous.getSize().y,
				
			});
			current.getElement('.car').removeClass('fst_drag');
			//current.inject(together);
			*/
			//current.getElement('.car').removeClass('fst_drag');
			//previous.getElement('.car').removeClass('fst_drag');
			
			//this.init_over_dropables();
			//this.init_dropables();
			//previous.adopt(current);
		/*
		} else {
			FstError('Není pred autem auto pro spojení');
		}
		*/
	},
	
	
	// canvas count cars
	canvas_count_cars: function(){
		$('refresh_canvas').removeEvents('click');
		$('refresh_canvas').addEvent('click',(function(e){
			e.stop();
			this.load_car_data();
			this.reload_elements('all');
		
		}).bind(this));
		this.canvas_count_cars_periodical();
		
	},
	// canvas count cars periodical
	canvas_count_cars_periodical: function(){
		this.timer_count_canvas = setTimeout(
			this.canvas_count_cars_req.bind(this)
		, this.timer_count);
	},
	
	// canvas count cars request
	canvas_count_cars_req: function(){
		
		this.canvas_count_req = new Request.JSON({
			url:'/dashboard/count_canvas/',
			
			onComplete :(function(json){
				var canvas_cnt = Object.getLength($('load_canvas').getElements('.car'));
				//console.log($('load_canvas').getElements('.car'));
				
				//console.log(canvas_cnt);
				if (canvas_cnt != json.count){
					var rozdil = json.count.toInt() - canvas_cnt.toInt();
					if (rozdil > 0){
						$('canvas_count').removeClass('none');
					} else {
						$('canvas_count').addClass('none');
					}
					$('canvas_count').set('text',rozdil);
				}
				
				
				this.canvas_count_req.cancel();
				
				canvas_cnt = null;
				json = null;
				rozdil = null;
				
				this.canvas_count_req =  null;
				
				this.canvas_count_cars_periodical();
			}).bind(this)
		});
		this.canvas_count_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.canvas_count_req.send();
		
		
	},
	
	
	
	
	
	
	
	
	
	/*
	
	// init all events
	init_events: function(opt){
		if (!opt){
			this.options = {
				drag_el: 'fst_drag',
				drop_el: 'fst_drop',
			}
		}
		
		
		
		//this.init_over_dropables();
		this.init_dropables();
		
		
		
			
			
		
	},
	*/
	
	
	/*
	// default progress bar in car
	default_progress_bar_car: function(){
		$('dashboard').getElements('.car').each((function(car){
			this.progress_bar_car(car);
		}).bind(this));
	},
	
	*/
	
	
	
	
	/*
	// open modal driver
	open_modal_driver: function(e){
		//new Event(e).stop();
		e.stop();
		if (e.target.hasClass('noload')){
			FstError(lang['neni_ridic']);
		} else {
			var opt = {
				'type':'driver',
				'driver_id':e.target.get('data-id'),
				'car_id':e.target.getParent('.car').get('data-id'),
				'side':true
			}
			this.create_modal(opt);
			opt = null;
		}
	},
	
	
	// request delete zakazka
	request_delete_zakazka: function(href){
		this.req_delete_zakazka = new Request.JSON({
			url:href,
			onComplete :(function(json){
				console.log('delete done');
				this.req_delete_zakazka.cancel();
				
				// reload zakazky
				this.load_data('zakazky');
				this.load_data('sklad');
			
			}).bind(this)
		})
		this.req_delete_zakazka.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.req_delete_zakazka.send();
	},
	*/
	/*
	*/
	/*
	// create over dropable div
	init_over_dropables: function(){
		$$('.'+this.options.drag_el).each((function(drag){
			var height = drag.getStyle('height');
			
			if (drag.getParent('.car')){
				var element = drag.getParent('.car');
			} else {
				var element = drag;
			}
			
			if (!element.getParent('.drag_relative') ){ 
				// create over div over drag element
				rel_div = new Element('div',{'class':'drag_relative'}).inject(element,'after');
				if (element.getElement('.drag_relative')){
					element.getElements('.drag_relative').each(function(item){
						item.setStyles({
							'margin':'0 2px'
						});
					});
					rel_div.setStyle('width',(element.getSize().x>0)?element.getSize().x:element.getStyle('width'));
					
				} 
				
				rel_div.setStyle('height',(element.getSize().y>0)?element.getSize().y:element.getStyle('height'));
					
				
				
				if (element.hasClass('resize_height')&& height > 0){ 
					
					rel_div.setStyle('height',height);
				}
				rel_div.adopt(element);
			
			}
		}).bind(this));
	},
	*/
	/*
	// request save zakazka
	request_save_zakazka: function(car,zakazka){
		this.req_save_zakazka = new Request.JSON({
			url:'/dashboard/save_data/zakazka/'+car.get('data-id')+'/'+zakazka.get('data-id')+'/'+zakazka.get('data-size2')+'/'+zakazka.get('data-size'),
			onComplete :(function(json){
				console.log('save_data done');
				this.req_save_zakazka.cancel();
				
			
			}).bind(this)
		})
		this.req_save_zakazka.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.req_save_zakazka.send();
		
	},
	*/
	// request car to basket
	request_basket: function(el,user_id){
		var el_id = el.get('data-id');
		var link = '/dashboard/save_data/basket/'+el_id+'/'+user_id
		//console.log(link);
		
		this.req_save_zakazka = new Request.JSON({
			url:link,
			onComplete :(function(json){
				//console.log('save_basket done');
				$('select_dispecer').addClass('none');
		
				this.req_save_zakazka.cancel();
			}).bind(this)
		});
		this.req_save_zakazka.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.req_save_zakazka.send();
		
		link = null;
	},
	
	
	
	
	
	
});

//})();	
window.addEvent('domready', function () {
	window.fst_drag = new FstDrag();
});