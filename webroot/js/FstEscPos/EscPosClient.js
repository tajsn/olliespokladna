/*
EscPosClient
Copyright Fastest Solution s.r.o. 
Author: Jakub Tyson
*/
var EscPosClient =  new Class({
	Implements:[Options,Events],
	debug: true,
	enable_print : true,
	debug_print : false,
	options: {
		element: null,
		table_header_row : 2, 
		
	},
	
	initialize: function(options){
		this.setOptions(options);
		
		this.websocket_client();
		
			
	},
	// tisk logo mobil
	print_logo: function(){
		var img = '<center><IMAGE>'+this.json_data.path_logo+'<BR>';
		return img;
	},
	
	// odeslani na mobilni aplikaci
	sendToQuickPrinter: function(){
		//console.log(this.json_data);
		print_data = '';
		if (this.json_data.is_logo == true){
			print_data += this.print_logo();
		}
		// header
		Object.each(this.json_data.header,function(item){
			print_data += this.format_quickPrinter(item);
			print_data += item.data + '<br>';
		}.bind(this));
		print_data += '<br>';
		
		
		// table
		print_data += this.create_bill_table();
		
		// footer
		print_data += '<br>';
		Object.each(this.json_data.footer,function(item){
			print_data += this.format_quickPrinter(item);
			print_data += item.data + '<br>';
		
		}.bind(this));
		
		//var text = "<br>test printer<br><cut>";
		var textEncoded = encodeURI(print_data);
		if (this.debug_print){
		console.log(print_data);
		console.log(textEncoded);
		}
		if (this.enable_print){
			window.location.href="quickprinter://"+textEncoded;
		}
	},
	
	// formatovani pro mobilni tisk
	format_quickPrinter: function(data){
		var out = '';
		if (typeof data.align != 'undefined'){
			out += '<'+data.align+'>';
		}
		if (typeof data.format != 'undefined'){
			
			if (data.format == 'bold'){
				//console.log(data.format);
				out += '<'+data.format+'>';
			}
		}
		if (typeof data.font != 'undefined'){
			if (data.font == 'B'){
				out += '<big>';
			} else {
				out += '<small>';
			}
		}
		//out += data.data + '<br>';
		return out;
	},
	
	// vytvoreni tabulky
	create_bill_table: function(){
		if (typeof this.json_data == 'undefined'){
			return false;
		}
		
		var format_table = {
			//number: function(value, header) { return value.toFixed(2); }
		}
		
		// table align from JSON data
		if (typeof this.json_data.table_align != 'undefined'){
			var format_table_align = {};
			Object.each(this.json_data.table_align,function(align,item){
				format_table_align[item] = function(value, header) {
				  return {
					value: value,
					format: {
					  alignment: align
					}
				  };
				};
			});
		}
		//console.log(format_table);
		
		// table data from JSON table_data
		if (typeof this.json_data.table_data != 'undefined'){
			table_data = [];
			this.json_data.table_data.each(function(item,row_id){
				table_data.push(item.data);
			});
		}
		
		// vytvoreni objektu tabulka
		table = window.stringTable.create(table_data, {
		  outerBorder: ' ',
		  innerBorder: ' ',
		  //headers: ['age', 'name'],
		  typeFormatters: format_table,
		  formatters: format_table_align,
		  
		});
		this.table = table;
		this.table_rows = this.table.split("\n");
		//console.log(this.table);return false;
		
		// formatovani radku a prevod na EscPos
		out = '';
		this.table_rows.each(function(row,i){	
			i = i-this.options.table_header_row;
			//console.log(this.json_data.table_data);
			if (typeof this.json_data.table_data[i] != 'undefined'){
				//console.log('a',this.json_data.table_data[i]); 
				//console.log('row data: ',i,row);
				out += this.format_quickPrinter(this.json_data.table_data[i]);			
				out += row + '<br>';
			} else {
				//console.log('row data: ',i,row);
				out += row + '<br>';
			}
		}.bind(this));
		return out;
	},
	
	// tuto funkci volat ze systemu pro tisk z json pole
	printDataEscPos: function(json_data){
		this.jsonDataToPrint(json_data);
		
		if (Browser.Platform.name == 'android'){
			this.sendToQuickPrinter();
		}
		else if (Browser.Platform.name == 'win'){
			if (this.debug_print){
				this.sendToQuickPrinter();
			} else {
				this.send_data_to_printer();
			}
			//console.log(this.json_data);
		} else {
			alert('Váš systém není podporován');
		}
	},
	
	// nastaveni prijatych dat to globalni promene
	jsonDataToPrint: function(data){
		this.json_data = data;
		//console.log(this.json_data);
		
	},
	
	// send JSON data to socket
	send_data_to_printer: function(){
		data = this.json_data;
		//console.log(data);
		//var d = new Uint8Array(file_data);console.log(d);
		
		if (this.ws.readyState == 3){
			FstError('Není pripojena tiskárna');
		} else {		
			send_data = {
				'type':'print_data',
				'json_data':data
			}
			//console.log(send_data);
			this.ws.send(JSON.encode(send_data));
		}
		//console.log(this.ws);
		//this.ws.send(file_data);
		
	},
	
	// init pokladna socket
	init_pokladna_socket: function(system_id){
		//console.log(this.ws);
		//var d = new Uint8Array(file_data);console.log(d);
		//console.log($('SystemId'));		
		this.ws.send(JSON.encode({
			'type':'init',
			'system_id':$('SystemId').value,
		}));
		
		//console.log(this.ws);
		//this.ws.send(file_data);
		
	},
	
	get_printer: function(){
		console.log('get PRINTER',this.ws);
		//var d = new Uint8Array(file_data);console.log(d);
				
		this.ws.send(JSON.encode({
			'type':'get_printer',
			'system_id':$('SystemId').value,
		}));
		
		//console.log(this.ws);
		//this.ws.send(file_data);
		
	},
	
	// parse from server.js
	parse_response: function(response){
		//console.log(response);
		if (response.printer_list){
			Object.each(response.printer_list, function(printer){
				new Element('li',{}).set('text',printer).inject($('printer_list'));
			});
			if ($('printers_data'))
			$('printers_data').set('text',JSON.encode(response.printer_list));
		}
	},
	
	websocket_client: function(url){
		
		if (!url) url = location.host;		
		if ("WebSocket" in window){
		 var port = 8080;
		 url = url.split(':');
		 url[0] = '127.0.0.1';
		 
		 
		url[0] = 'localhost';
		  
		 this.ws = new WebSocket('ws://'+url[0]+':'+port+'/');
		 //this.ws.binaryType = 'blob';
		
		 // websocket is opened
		 this.ws.onopen = (function(evt){
			
			this.ws_log('Spojení se serverem v porádku','ok');
			this.init_pokladna_socket();
			this.get_printer();
			this.check_connection(1);
			
			//if (this.options.provoz_id){
				/*
				this.ws.send(JSON.encode({
					'type':'print',
					'pokladna':true,
					'provoz_id':this.options.provoz_id
				}));
				*/
			//}
			
		 }).bind(this);
		 
		 
		 // websocket sending message
		 this.ws.onmessage = (function (evt) { 
			response = evt.data;
			response = JSON.parse(response);
			//console.log(response);
			this.parse_response(response);
			
		 }).bind(this);
		 
		 // websocket is closed.
		 this.ws.onclose = (function(e){ 
			//this.ws_log('Chyba spojení se serverem');
			this.check_connection(0);
			this.nosocket_data();
				
		 }).bind(this);
	  } else {
		 // The browser doesn't support WebSocket
		alert('Tento prohlížec není podporován');
		this.ws_log('Websocket není podporován v tomto prohlížeci');
	  }
	  
	},
	
	nosocket_data: function(){
		if ($('nosocket_data')){
			$('nosocket_data').addClass('none');
		}
	},
		
	// Make the function wait until the connection is made...
	check_connection: function(status){
		if (!$('websocket_status')){
			return false;
		}
		if (status == 1){
			
			$('websocket_status').addClass('success');
			$('websocket_status').removeClass('failed');
			$('websocket_status').set('text','Spojení s tiskárnou v porádku');
			VarsModal.hide_status_socket = function(){
				$('websocket_status').fade(0);
			}
			VarsModal.hide_status_socket.delay(1000);
		} else {
			if ($('websocket_status')){
				$('websocket_status').removeClass('success');
				$('websocket_status').addClass('failed');
				$('websocket_status').set('text','Není spojení s tiskárnou');
			}
		}
	},
		
	
	ws_log: function(data,ok){
		console.log('Logace: '+data);
		
		if ($('websocket_log')){
			if (ok){
				$('websocket_log').addClass('ok');
			} else {
				$('websocket_log').removeClass('ok');
			
			}
			$('websocket_log').removeClass('none');
			$('websocket_log').set('text',data);
		}
	}
	
});
