var FstTreeMenu = this.FstTreeMenu = new Class({
	Implements:[Options,Events],
	options:{
		'id':'tree_menu',
		'json_data':null,
	},
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		
		//console.log(this.options.data);
		this.getDataJson();
		this.createTree();
		
		this.initTree();
		this.saveJsonData();
		this.changeName();
		this.newItem();
		
	},
	
	initTree: function(){
		this.tree = new Tree(this.options.id, {
			checkDrag: function(element){
				if (element.getElement('input.text')){
					element.getElement('input.text').focus();
				}
				return !element.hasClass('nodrag');
			},

			checkDrop: function(element, options){
				return !element.hasClass('nodrop');
			},
			onChange: function(el){
				//console.log(tree.serializeJSON());
				//console.log(this.treeData);
				this.treeData = this.tree.serializeJSON();
				console.log(this.treeData);
			}.bind(this)
		});
		if (!this.treeData)
		this.treeData = this.tree.serializeJSON();
		console.log(this.treeData);

	},
	
	changeName: function(){
		$('tree_menu').getElements('span').removeEvents('click');
		$('tree_menu').getElements('span').addEvent('click',function(e){
			e.stop();
			el = e.target;
			el_input = new Element('input',{'type':'text','value':el.get('text'),'class':'text'}).inject(el,'after');
			el_input.focus();
			el.addClass('none');
			el.getParent('li').addClass('nodrag');
			
			
			
			el_input.addEvent('keydown',function(e){
				if (e.key == 'enter'){
					el_input.fireEvent('change',el_input);
				}
			});
			
			el_input.addEvent('change',function(e){
				if (!e.target) e.target = e;
				el = e.target;
				id = el.getParent('li').get('id');
				span = el.getParent('li').getElement('span');
				el.getParent('li').removeClass('nodrag');
				span.set('text',el.value);
				span.removeClass('none');;
				//console.log(id);
				//console.log(this.treeData);
				this.updateData(el);
				//this.treeData[id].name = el.value;
				el.destroy();
				//console.log(this.treeData[id]);
				//console.log(el.value);
			}.bind(this));
			
		}.bind(this));
	},
	
	updateData: function(el){
		if (el.getParent('li')){
			li = el.getParent('li');
			params = JSON.decode(li.get('data-params'));
			params.name = el.value;
			li.set('data-params',JSON.encode(params));
			
			this.treeData = this.tree.serializeJSON();
			//console.log(this.treeData);

		}
		/*
		if (data[id]){
			data[id].name = value;
		} else {
			Object.each(data,function(item){
				this.updateData(id,item.children,value);
			}.bind(this));
		}
		console.log(data);
		*/
	},
	
	saveJsonData: function(){
		if ($('saveCats')){
			$('saveCats').addEvent('click',function(e){
				e.stop();
				button_preloader($('saveCats'));
				console.log('save',this.treeData);
				Vars.save_reg = new Request.JSON({
					url:'menusSave/',	
					data: this.treeData,
					onComplete: (Vars.save_reg_complete = function(json){
						
						button_preloader($('saveCats'));
						if (json.r == true){
							FstAlert('Uloženo');
							(function(){
							window.location = window.location;
							}).delay(1000);
						}
						json = null;
					}).bind(this)
				});
				Vars.save_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				Vars.save_reg.send();
			}.bind(this));
		}
	},
	
	getDataJson: function(){
		if (this.options.data){
			this.json_data = JSON.decode(this.options.data);
			console.log(this.json_data);
		}
	},
	
	createTree: function(){
		
		if (this.json_data && $(this.options.id)){
			this.ul_menu = $(this.options.id);
			this.json_data.each(function(item){
				this.createLi(item,this.ul_menu);
				//console.log(item);
			}.bind(this));
			this.deleteButtons();
		}
	},
	createLi: function(item,inject_el){
		
		var li = new Element('li',{'id':'item'+item.id,'data-params':JSON.encode(item)}).inject(inject_el);
		var span = new Element('span').set('text',item.name).inject(li);
		var expand = new Element('a',{'class':'expand'}).inject(li);
		var delete_button = new Element('a',{'class':'delete_button fa fa-close'}).inject(li);
		
		if (item.children && item.children.length > 0){
			var ul_children = new Element('ul').inject(li);
			
			item.children.each(function(item_children){
				this.createLi(item_children,ul_children);
			}.bind(this));
		}
	},
	
	newItem: function(){
		$('newCats').addEvent('click',function(e){
			e.stop();
			last_id = 0;
			$('tree_menu').getElements('li').each(function(it){
				id = it.get('id').replace('item','').toInt();
				if (last_id < id){
					last_id = id;
				}
			});
			last_id = last_id + 1;
			//console.log(last_id);
			json_new = {
				'id':last_id,
				'level':1,
				'name':'Nová položka',
			}
			//var li = new Element('li',{'id':'item'+last_id,'data-params':JSON.encode(json_data)}).inject($('tree_menu'));
			
			
			this.json_data.push(json_new);
			this.createLi(json_new,this.ul_menu);
			this.deleteButtons();
			this.treeData['item'+last_id] = json_new;
			//console.log('item'+last_id);
			//console.log(this.treeData);
			
			this.changeName();
		}.bind(this));
	},
	
	deleteButtons: function(){
		$$('.delete_button').removeEvents('click');
		$$('.delete_button').addEvent('click',function(e){
			e.stop();
			li = e.target.getParent('li');
			id = li.get('id');
			if (confirm('Opravdu smazat?')){
				delete this.treeData[id];
				li.destroy();
			}
		}.bind(this));
	}
});