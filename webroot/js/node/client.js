/*
FstPokladnaClient 
Copyright Fastest Solution s.r.o. 
Author: Jakub Tyson
*/
var FstPokladnaClient =  new Class({
	Implements:[Options,Events],
	debug: true,
	options: {
		element: null,
	},
	
	initialize: function(options){
	
		this.setOptions(options);
		
		this.websocket_client();
		
			
	},
	
	// run print
	run_print(){
		this.ws.send(JSON.encode({
			'type':'print',
			'order_id':123,
			'file':'test.pdf'
		}));
	},
	
	// parse from server.js
	parse_response: function(response){
		if (response.open_win){ 
			mobile = response.mobile.replace(" ","");
			//var link = new Element('a',{'href':'/zakaznici/edit/?mobil='+mobile,'title':'Nová objednávka'}); 
			
			var link = new Element('a',{'href':'/orders/edit/?mobil='+mobile,'title':'Nová objednávka'}); 
			modal_win(link);
		}
	},
	
	websocket_client: function(url){
		
		if (!url) url = location.host;		
		if ("WebSocket" in window){
		 var port = 9998;
		 this.ws = new WebSocket('ws://'+url+':'+port+'/');
		 
		 // websocket is opened
		 this.ws.onopen = (function(evt){
			
			this.ws_log('Spojení se serverem v pořádku','ok');
			
			if (this.options.provoz_id){
			
				this.ws.send(JSON.encode({
					'pokladna':true,
					'provoz_id':this.options.provoz_id
				}));
			}
			
		 }).bind(this);
		 
		 
		 // websocket sending message
		 this.ws.onmessage = (function (evt) { 
			response = evt.data;
			response = JSON.parse(response);
			console.log(response);
			this.parse_response(response);
			
		 }).bind(this);
		 
		 // websocket is closed.
		 this.ws.onclose = (function(e){ 
			this.ws_log('Chyba spojení se serverem');
				
		 }).bind(this);
	  } else {
		 // The browser doesn't support WebSocket
		alert('Tento prohlížec není podporován');
		this.ws_log('Websocket není podporován v tomto prohlížeci');
	  }
	},
	
	
	ws_log: function(data,ok){
		console.log('Logace: '+data); 
		
		if ($('websocket_log')){
			if (ok){
				$('websocket_log').addClass('ok');
			} else {
				$('websocket_log').removeClass('ok');
			
			}
			$('websocket_log').removeClass('none');
			$('websocket_log').set('text',data);
		}
	}
	
});

var FstPokladnaClient = new FstPokladnaClient();	