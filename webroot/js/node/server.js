//var WebSocketServer = require('websocket').server;
var http = require('http');
var Moo = require('mootools');
var os = require("os");

var FstPokladnaServer = new Class({
    
	domain: 'http://'+os.hostname(),
	
	
	Implements: [process.EventEmitter],
	
	pokladnas : [], 
	connectionIDCounter : [],
	
	initialize: function(options){
		
		this.create_server();
		//this.get_printer();
	},
	
	// create server
	create_server: function(){
		var express = require('express');
		var app = express();

		app.get('/', function (req, res) {
		  res.send('Hello World!');
		});
		
		this.get_printer();
		
		app.listen(9998, function () {
		  console.log('Example app listening on port 9998!');
		});

		/*
		var server = http.createServer(function(request, response) {
			console.log((new Date()) + ' Received request for ' + request.url);
			response.writeHead(404); 
			response.end();
		});
		server.listen(9998, function() {
			console.log((new Date()) + ' Server is listening on port 9998');
		});
		*/
	},
	
	get_printer: function(){
		this.printer_class = require("printer");
		
		var printers_list = this.printer_class.getPrinters();
		var printers_name = [];
		
		printers_list.each(function(printer){
			printers_name.push(printer.name);
		});
		//console.log(printers_name);
		this.printers_name = printers_name;
		
		//this.print_file();
	},
	
	print_file: function(){
		this.printers_name.each((function(printer_name){
			//var filename = '../print.html';
			var filename = 'node_modules/printer/examples/test.pdf';
			this.printer_class.printFile({
				type:'PDF',
				filename:filename,
				printer: printer_name, // printer name, if missing then will print to default printer
				success:function(jobID){
				  console.log("sent to printer with ID: "+jobID);
				},
				error:function(err){
				  console.log(err);
				}
			});	
		}).bind(this));
	},
	
	
	
	
	
	
	
	
	
	
	// create server
	create_server2: function(){
		
		var server = http.createServer(function(request, response) {
			console.log((new Date()) + ' Received request for ' + request.url);
			response.writeHead(404); 
			response.end();
		});
		server.listen(9998, function() {
			console.log((new Date()) + ' Server is listening on port 9998');
		});

	wsServer = new WebSocketServer({ httpServer: server });

		function originIsAllowed(origin) {
		  // put logic here to detect whether the specified origin is allowed.
		  return true;
		}

		var connections = {};
		
		
		var _this = this;
		
		wsServer.on('request', (function(request) {
			if (!originIsAllowed(request.origin)) {
			  // Make sure we only accept requests from an allowed origin
			  request.reject();
			  console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
			  return;
			}
			
			var connection = request.accept(null, request.origin);

			connection.on('message', (function(message) {
				if (message.type === 'utf8') {
					console.log('Received Message: ' + message.utf8Data);
					message = message.utf8Data;
					
					if (message){
						var message = JSON.parse(message);
						provoz_id = message.provoz_id;
						type = message.type;
						if (message.pokladna){
							
							if (!this.connectionIDCounter[provoz_id]) this.connectionIDCounter[provoz_id] = 0;
							if (!connections[provoz_id]) connections[provoz_id] = {}
							if (!connections[provoz_id]['pokladna']) connections[provoz_id]['pokladna'] = {}
							connection.id = this.connectionIDCounter[provoz_id] ++;
							connections[provoz_id]['pokladna'][connection.id] = connection;
							this.con_pokladna = connections;
							console.log(connection);
							this.create_pokladnas(message,connections,connection.id,provoz_id);
							console.log('pocet pokladen '+provoz_id+': '+Object.keys(this.pokladnas[provoz_id]).length);
							
						}
						if (message.type == 'hang'){
							if (this.con_pokladna && Object.keys(this.pokladnas[provoz_id]).length > 0)
							this.send_phone(connections,provoz_id,message.data)
						}
						
					} 
					
					
					
					
				}
				else if (message.type === 'binary') {
					console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
					//connection.sendBytes(message.binaryData);
				}
			
			}).bind(this));
			
			
			connection.on('close', (function(reasonCode, description) {
				console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected. ' +
							"Connection ID: " + connection.id);

				// Make sure to remove closed connections from the global pool
					if (connections[provoz_id] && this.pokladnas[provoz_id][connection.id]){
					
						delete connections[provoz_id];
						delete (this.pokladnas[provoz_id][connection.id]);
						
						//this.update_pocet_orders(connections,provoz_id,count_orders);
						
						console.log('pocet klientu pokladny '+provoz_id+': '+Object.keys(this.pokladnas[provoz_id]).length);	
					}
				
				
			}).bind(this));
		}).bind(this));
	},
	
	// create cisniks before send to CLIENT.JS
	create_pokladnas: function(message,connections,connection_pokladna_id,provoz_id){
		//console.log(connection_id);
		if (!this.pokladnas[provoz_id]) this.pokladnas[provoz_id] = {};
		
		this.pokladnas[provoz_id][connection_pokladna_id] = {'id':connection_pokladna_id};
		
		
		
	},
	
	
	// send to CLIENT.JS call obsluha
	send_phone: function(connections,provoz_id,phone){
		
		if (this.con_pokladna[provoz_id]['pokladna'])
		Object.each(this.con_pokladna[provoz_id]['pokladna'],(function(cc,k){
				
				if (this.con_pokladna[provoz_id]['pokladna'][k]){ 
					this.con_pokladna[provoz_id]['pokladna'][k].send(JSON.stringify({'open_win':true,'mobile':phone}));
					console.log('open win');
				}
				
		}).bind(this));	 
				
		
	},
	
	
	
	
});
var FstPokladnaServer = new FstPokladnaServer();	


// Broadcast to all open connections
function broadcast(data) {
    Object.keys(connections).forEach(function(key) {
        var connection = connections[key];
        if (connection.connected) {
            connection.send(data);
        }
    });
}

// Send a message to a connection by its connectionID
function sendToConnectionId(connectionID, data) {
    var connection = connections[connectionID];
    if (connection && connection.connected) {
        connection.send(data);
    }
}