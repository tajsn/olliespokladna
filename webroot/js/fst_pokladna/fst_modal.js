/*
Fst Context Menu
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var FstPokladnaModal = this.FstPokladnaModal = new Class({
	Implements:[Options,Events],
	debug : true,
	// init fce
	initialize:function(options){
		this.setOptions(options);
		if (this.debug) console.log('start fst pokladna modal');
		
		if ($('SaveModalOrder')){
			this.modal_loaded();
			
		}
	
	},
	
	
	// init event to element right click
	open_modal: function(href){
		if (this.debug) console.log('open');
		
		VarsModal.fst_modal = new Element('div',{'id':'fst_modal'}).inject($('addon'));
		VarsModal.fst_modal_in = new Element('div',{'id':'fst_modal_in'}).inject(VarsModal.fst_modal);
		VarsModal.fst_modal_close_button = new Element('div',{'class':'fst_modal_close modal_close fa fa-close'}).inject(VarsModal.fst_modal);
		
		
		VarsModal.open_reg = new Request.JSON({
			url:href,	
			onComplete: this.modal_complete_loaded.bind(this)
		});
		
		VarsModal.open_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.open_reg.send();
		
		this.modal_events();
	},
	
	close_modal_event: function(e){
		if (typeof e == 'undefined') return true;
		
		if (!e.target) e.target  = e;
		fstvars.clean_modal();
		fstvars.destroyEl(e.target.getParent('#fst_modal'));
	},
	
	// save modal request
	save_modal_event: function(e){
		e.stop();
		
		VarsModal.save_reg = new Request.JSON({
			url:e.target.getParent('form').action,	
			onComplete: this.modal_complete_save.bind(this)
		});
		
		VarsModal.save_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.save_reg.post(e.target.getParent('form'));
	},
	
	modal_events: function(){
	
		VarsModal.fst_modal_close_button.addEvent('click',this.close_modal_event.bind(this));
	},
	
	// oncomplete after ajax load 
	modal_complete_save: function(json){
		if (json.r == true){
			this.close_modal_event(VarsModal.fst_modal_close_button);
		} else {
			FstError(json.m);
			if (json.invalid){
				$$('.invalid').removeClass('invalid');
					json.invalid.each(function(item){
						item = item.replace('_','-');
						//console.log(item);
						if($(item)){
							$(item).addClass('invalid');	
						}
					});
			}
		}
		json = null;
	},
	
	// oncomplete after ajax load 
	modal_complete_loaded: function(json){
		if (json.r == true){
			html = Elements.from(json.html);
			html.inject(VarsModal.fst_modal_in);
			//console.log(html);
			this.modal_loaded();
		}
		json = null;
	},
	
	// events after draw elements ajax
	modal_loaded: function(){
		//console.log($('SaveModalOrder'));
		if (typeof window.fstPokladna != 'undefined')
		window.fstPokladna.init_after_modal_load();
		
		if ($('SaveModalOrder')){
			
			VarsModal.save_button = $('SaveModalOrder');
			VarsModal.save_button.addEvent('click',this.save_modal_event.bind(this));
			
		}
		
		$$('.modal_close').addEvent('click',this.close_modal_event.bind(this));
		
		
		// product list resize height
		if ($('product_list')){
			if ($('product_list').getParent('form').getSize().y>window.getSize().y){
				if ($('client_data'))
				$('product_list').setStyle('height',window.getSize().y - $('client_data').getSize().y-160);
			}
			
		}
	
	},
	
	
});

window.addEvent('domready', function () {
	//window.fstMenu = new FstMenu();
	//var s = $('body').getDimensions({computeSize: true});
	//console.log(s);
	//window.fstsystem.alert_fce('test');
});