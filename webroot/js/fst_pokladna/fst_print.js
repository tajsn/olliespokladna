var FstPrint = this.FstPrint = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		//console.log('init print');
		window.addEvent('load',function(){
			this.setting_printers();
		}.bind(this));
		//this.print_data();
	},
	
	// print data
	print_data: function(data){		
		
		console.log('print data: ',data);
		window.EscPosClient.printDataEscPos(data);
		FstAlert('Tisk');
	},
	
	// print data
	print_data_part: function(print_data){		
		Object.each(print_data,function(data){
			//console.log('print data: ',data);
			window.EscPosClient.printDataEscPos(data);
		});
		FstAlert('Tisk pro výrobu');
	},
	
	getPrinterSetting: function(){
		printer_setting = localStorage.getItem("printers_setting");
		if (printer_setting){
			printer_setting = JSON.decode(printer_setting);
		}
		return printer_setting;
	},
	
	getPrinterDefault: function(){
		printer_setting = localStorage.getItem("default_printer");
		if (printer_setting){
			printer_setting = JSON.decode(printer_setting);
		}
		return printer_setting;
	},
	
	
	// setting printers to product group
	setting_printers: function(){
		//console.log($('SettingPrinters'));
		if ($('SettingPrinters'))
		$('SettingPrinters').addEvent('click',function(e){
			e.stop();
			var printer_list = $('printers_data').get('text');
			//console.log(printer_list);
			if (printer_list == ''){
				FstError('Nejsou připojeny žádné tiskárny');
			} else {
				printer_list = JSON.decode(printer_list);
				
				var def = JSON.decode(localStorage.getItem('default_printer'));
				var printer_settings_save = JSON.decode(localStorage.getItem('printers_setting'));
				//console.log(printer_settings);
				if (def != '')
				$('printer_default').set('text',def);
				//console.log(def);
				//console.log(printer_list);
				$('modal_printer_setting').getElements('.printer_list').each(function(printer_select){
					printer_select.empty();
					printer_list.each(function(printer){
						new Element('option',{'value':printer,'text':printer}).inject(printer_select);
					});
					
					//console.log($('printer_default').get('text'));
					// pokud NENI ulozeno nastaveni
					if (typeof VarsModal.printer_setting == 'undefined'){
						printer_select.value = $('printer_default').get('text');
					} else {
						printer_select.value = VarsModal.printer_setting[printer_select.get('data-id')];
						//console.log(VarsModal.printer_setting);
					}
					
					//console.log(printer_select.value);
					if (printer_select.value == ''){
						//console.log(def);
						//console.log(printer_select);
						printer_select.value = def;
					}
				});
				
				$('modal_printer_setting').removeClass('none');
			}
		}.bind(this));
		if ($('printer_default_select'))
		$('printer_default_select').addEvent('change',function(){
			$('printer_default').set('text',$('printer_default_select').value);
			localStorage.setItem(
			  "default_printer", 
			  JSON.stringify($('printer_default_select').value)
			);
			var cookiePrinterDefault = new Hash.Cookie('printer_default', {duration: 360000});
			cookiePrinterDefault.erase();
			cookiePrinterDefault.set(
				'data',JSON.encode($('printer_default_select').value)
			);
			//console.log($('printer_default').get('text'));
		});
		
		if ($('printer_default_select')){
		$('SavePrinterSetting').removeEvents('click');
		$('SavePrinterSetting').addEvent('click',function(e){
			e.stop();
			var save_setting = {}
			$('modal_printer_setting').getElements('.printer_list').each(function(printer_select){
				save_setting[printer_select.get('data-id')] = printer_select.value;
			});
			//console.log(save_setting);
			
			localStorage.setItem(
			  "printers_setting", 
			  JSON.stringify(save_setting)
			);
			var cookiePrinter = new Hash.Cookie('printers_setting', {duration: 360000});
			cookiePrinter.erase();
			cookiePrinter.set(
				'data',JSON.encode(save_setting)
			);
			//console.log(JSON.stringify(save_setting));
			
			FstAlert('Uloženo');
			
			$('modal_printer_setting').addClass('none');
			
		});
		}
	},
	
	
	
	
	// get printer list from NODE.js
	get_printer_list: function(){
		//console.log($$('.show_printer'));
		$$('.show_printer').addEvent('click',function(e){
			
			var el = e.target;
			$('printer_list').removeClass('none');
			$('printer_list').setStyles({
				'top':e.target.getCoordinates().top,
				'left':e.target.getCoordinates().left
			});
			$('printer_list').getElements('li').addEvent('click',function(e){
				el.value = e.target.get('text');
				$('printer_list').addClass('none');
			});
			
			FstPokladnaClient.get_printer($('SystemId').value);
		
		}.bind(this));
		
	},
	
});
fstPrint = new FstPrint();