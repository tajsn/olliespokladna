var FstTable = this.FstTable = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		window.addEvent('domready',function(){
			if ($('basket_function')){
				this.load_data_table();
				this.product_to_basket();
				this.basket_function();
				this.product_parser();
				this.rozdelit_stul();
				this.showSettingMenu();
			}
		
		}.bind(this));
	},
	
	// zobrazeni menu v footer
	showSettingMenu: function(){
		if ($('showFooterMenu')){
			$('showFooterMenu').addEvent('click',function(e){
				e.stop();
				$('footer_order_menu').toggleClass('none');
			});
		}
		
		$('order_top_menu2').setStyle('left',$('basket_products_obal').getCoordinates().left);
	},
	
	// basket function
	basket_function: function(){
		
		
		$$('.order_modal_close').addEvent('click',VarsModal.basket_modal_close = (function(){
			$$('.order_modal').addClass('none');
		}).bind(this));
		
		$('basket_function').getElements('button').addEvent('click',VarsModal.basket_fce = (function(e){
			if (!e.target) e.target = e;
			// rozdelit
			if (e.target.get('data-fce-type') == 'rozdelit'){
				this.rozdelit_window();
			}
			if (e.target.get('data-fce-type') == 'ukoncit'){
				this.ukoncit_order();
			}
			if (e.target.get('data-fce-type') == 'zaplatit'){
				//console.log(FstPay.showModal());
				fstPay.showModal();
			}
			if (e.target.get('data-fce-type') == 'rozdelit'){
				this.rozdelit_order();
			}
			
		}).bind(this));
	},
	
	// overeni vybrano 2 stoly
	check_select_table: function(){
		if (!$('table_right').getElement('.active')) {
			FstError('Musíte zvolit druhý stůl');
			return false;
		}
		if (!$('table_left').getElement('.active')) {
			FstError('Musíte zvolit první stůl');
			return false;
		}
		return true;
	},
	
	// presun mezi stoly
	rozdelit_stul: function(){
		
		// vybrat vse na stole
		$('stul_rozdelit').getElements('.select_all').addEvent('click',VarsModal.click_all = function(e){
			e.stop();
			
			this.check_select_table();
			
			e.target.getPrevious('.itms').getElements('.product').each(VarsModal.imts_each = function(item){
				result = this.change_between_table_product(item);
				if (result == false) return false;
			}.bind(this));
		}.bind(this));
		
		// rozdelit stul
		if ($('RozdelitStul')){
			
			$('RozdelitStul').addEvent('click',VarsModal.click_rozdelit = function(e){
				e.stop();
				//console.log(window.table_id);
				$('stul_rozdelit').toggleClass('none');
				
				// vytvoreni seznamu stolu
				clone_els_or = $('table_list').getElements('.tg');
				
				if (!$('table_left').getElement('.tg')){
					Object.each(clone_els_or,VarsModal.cl_each = function(item){
						if (typeof item == 'object'){
							clone_els_left = item.clone();
							clone_els_right = item.clone();
							clone_els_left.removeClass('active');
							clone_els_right.removeClass('active');
							//item.removeClass('none');
							clone_els_left.inject($('table_left'));
							
							clone_els_right.inject($('table_right'));
							$('table_left').getElements('.tg').removeClass('none');
							$('table_right').getElements('.tg').removeClass('none');
						}
					});
					
							
				} else {
					$$('.tg').removeClass('active');
					fstvars.clearEl($('table_prod_left').getElement('.itms'));
					fstvars.clearEl($('table_prod_right').getElement('.itms'));

				}
				
				
				// vybrani stolu ze seznamu
				$('stul_rozdelit').getElements('.tg').addEvent('click',VarsModal.click_table_sel = function(e){
					e.stop();
					button_table = e.target;
					var parent = button_table.getParent('.table_select');
					parent.getElements('.tg').removeClass('active');
					button_table.addClass('active');
					//console.log(VarsModal.order_data);
					table_id = button_table.get('data-id');
					
					var itms = $(parent.get('data-type')).getElement('.itms');
					
					fstvars.clearEl(itms);
					//console.log('table_id',table_id);
					
					// load table data
					button_preloader(button_table);
					VarsModal.load_table = new Request.JSON({
						url:'/orders/load_table/'+button_table.get('data-id'),	
						
						onComplete: VarsModal.load_table_complete = (function(json){
							var itms = $(parent.get('data-type')).getElement('.itms');
							// vyprazdnit stul element
							fstvars.clearEl(itms);
							
							button_preloader(button_table);
							new Element('input',{'value':json.table_id,'class':'table_id','type':'hidden'}).inject(itms);
									
							if (json.r == true){
								//console.log(json.data);
								// pokud jsou na stole produkty
								if (json.table_data.data){
									Object.each(json.table_data.data.items,VarsModal.data_each = function(item,key){
										
										product = new Element('input',{'type':'button','class':'button product','data-key':key,'data-id':item.id,'value':item.name,'id':'SelectStulProduct'+item.id,'data-ks':item.ks}).inject($(parent.get('data-type')).getElement('.itms'));
										
										product.addEvent('click',this.change_between_table_product.bind(this));
									}.bind(this));
									
								// pokud je stul prazdny
								} else {
									$(parent.get('data-type')).getElement('.itms').addClass('empty');
								}
							} else {
							}
						}.bind(this))
					});
					VarsModal.load_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					VarsModal.load_table.send();
					
				}.bind(this));
				
				//console.log($('table_left').getElement('.tableId'+window.table_id));
				//console.log($('table_list').getElements('.tg'));
				if ($('table_list').getElement('.active')){
					var table_id = $('table_list').getElement('.active').get('data-id');
				} else {	
					var table_id = window.table_id;
				}
				$('table_left').getElement('.tableId'+table_id).click();
				
				
			}.bind(this));
			
			// zpet na homepage
			$('stul_back').addEvent('click',VarsModal.click_rozdelit_back = function(e){
				e.stop();
				
				$('stul_rozdelit').toggleClass('none');
			});
		}
	},
	
	// click change product on table
	change_between_table_product: function(e){
		if (!e.target){
			e.target = e;
		}
		var el = e.target;
		//console.log('change');
		var next_table = $(e.target.getParent('.stul_col').get('data-adopt'));
		var curr_table = $(e.target.getParent('.stul_col'));
		//next_table.getElement('.itms').adopt(e.target);
		
		if (curr_table.getElement('.table_id')){
			curr_table_id = curr_table.getElement('.table_id').value;
		} else {
			FstError('Vyberte stůl z levé části');
			return false;
		}
		if (next_table.getElement('.table_id')){
			next_table_id = next_table.getElement('.table_id').value;
		} else {
			FstError('Vyberte stůl z pravé části');
			return false;
		}
		
		var link = '/orders/change_table/';
		link += curr_table_id+'/';
		link += next_table_id+'/';
		link += el.get('data-key')+'/';
		
		//console.log(link);
		button_preloader(el);	
			VarsModal.change_table = new Request.JSON({
				url:link,	
					
				onComplete: VarsModal.change_table_compl = (function(json){
					
					button_preloader(el);
					if (json.result == true){
						// presun do prava
						next_table.getElement('.itms').adopt(el);
					} else {
						FstError('Chyba uložení');
					}
				}.bind(this))
			});
			VarsModal.change_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.change_table.send();
			
	},
	
	
	
	
	// ukoncit order
	ukoncit_order: function(delete_function){
		if (delete_function){
			this.delete_function = true;
		} else {
			this.delete_function = false;
		}
		//console.log(VarsModal.local_table_data);
		if (VarsModal.local_table_data)
		Object.each(VarsModal.local_table_data,VarsModal.data_each = function(item,table_id){
			if (typeof VarsModal.local_table_data[table_id]['items'] != 'undefined')
			Object.each(VarsModal.local_table_data[table_id]['items'],VarsModal.data_each2 = function(item_p,line_id){
				VarsModal.local_table_data[table_id]['items'][line_id].done = 1;
			});
		});
		if (window.debug) console.log('save table data',VarsModal.local_table_data[window.table_id]);
		
		VarsModal.save_data = new Request.JSON({
			url:'/orders/save_table_data/',
			data:VarsModal.local_table_data,
			onComplete: VarsModal.compl_fce = (function(json){
						
				if (json && json.r == true){
					this.reset_default_status();		
					
					// print data
					if (!this.delete_function){
						this.printPartData(json);
					}
					//fstPrint.print_data();
		
				} else {
				}
				VarsModal.save_data.cancel();
				json = null;
			}.bind(this))
		});
		VarsModal.save_data.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.save_data.send();// load orders request
	},
	
	// print part data to specific printer
	printPartData: function(json){
		if (this.check_websocket()){
			fstPrint.print_data_part(json.print_data);
		}
	},
	
	// kontrola websocket
	check_websocket: function(){
		if ($('websocket_status').hasClass('failed')){
			FstError('Není připojena tiskárna');
			return false;
		} else {
			return true;
		}
	},
	
	// reset default status
	reset_default_status: function(){
		$('CheckUkoncit').value = 0;	
				
		//$('bill_back').click();
		//$('zrusit').click();
		if (!this.delete_function){
			$('table_list').getElement('.active').click();
			FstAlert('Objednávka byla ukončena');
		}
		
	},
	
	// load from DB to local storage
	load_data_table: function(){
		if ($('table_list'))
		$('table_list').getElements('.tg').addEvent('click',function(e){
			if (e.target){
				e.stop();
			} else {
				e.target = e;
			}
			if ($('CheckUkoncit') && $('CheckUkoncit').value == 1){
				FstError('Musíte nejprve ukončit stůl');
				return false;
			}
			
			var table_id = e.target.get('data-id').toInt();
			
			//console.log(table_id);
			// prepare TEMPO template
			if (!window.template_basket){
				window.template_basket = Tempo.prepare('basket_body');
			}
			if (typeof VarsModal.local_table_data == 'undefined'){ 
				$('basket_products_obal').getElement('.basket_empty').setProperty('style','display:block');
			} else {
				$('basket_products_obal').getElement('.basket_empty').setProperty('style','display:none');
				
			}
			$('basket_products').addClass('preloader');
			
			//console.log('/orders/load_table_data/'+table_id);
			VarsModal.load_data_table = new Request.JSON({
				url:'/orders/load_table_data/'+table_id,
				onComplete: VarsModal.compl_fce = (function(json){
					//console.log(json.data);	
					$('basket_products').removeClass('preloader');
					if (json && json.r == true){
						VarsModal.local_table_data = {}
							
						//console.log(json.data);
						//console.log('aaa',json.data);
						if (json.data == null || json.data == '' || json.data == 'null'){
							VarsModal.local_table_data = {}
							VarsModal.local_table_data[table_id] = {}
							//console.log(VarsModal.local_table_data);
						} else {
							if (typeof VarsModal.local_table_data == 'undefined'){
								VarsModal.local_table_data = {}
							}
							VarsModal.local_table_data[table_id] = JSON.decode(json.data);
							//console.log('bb',JSON.decode(json.data));
						}
						
						//if (window.debug)
						console.log('load table data request',VarsModal.local_table_data[table_id]);
						//return false;
						
						localStorage.setItem(
							"local_table_data", 
							JSON.stringify(VarsModal.local_table_data)
						);
						
						// draw table data to right element
						this.draw_table_data(table_id);
						
							
					} else {
						if (json){
							FstError(json.m);
							$('TableId'+table_id).removeClass('active');
						}
					}
					VarsModal.load_data_table.cancel();
					json = null;
					table_id = null;
				}.bind(this))
			});
			VarsModal.load_data_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.load_data_table.send();// load orders request
			
		}.bind(this));
		
		
	},
	
	// load table data 
	draw_table_data: function(table_id){
		
		// show basket data right side
		this.show_basket();
		
		if (!table_id){
			table_id = $('TableId').value.toInt();
		}
		window.table_id = table_id;
		if (window.debug) console.log('table_id select',window.table_id);
		
		this.loadStorage();
		
		// clear basket body
		// pokud jsou data tak zobrazit
		if (VarsModal.local_table_data[window.table_id] != null && Object.getLength(VarsModal.local_table_data[window.table_id])>0){
			//this.create_line_from_data(VarsModal.order_data[table_id]['done_items']);
			if (typeof VarsModal.local_table_data[window.table_id]['items'] != 'undefined'){
				this.create_line_from_data(VarsModal.local_table_data[window.table_id]['items']);
			} else {
				$('basket_body').getElements('.product_line').destroy();
				$('basket_body').getElement('.basket_empty').removeProperty('style');
			}
		} else {
			this.renderBasketData();
		}
		
		
	},
	
	// load local storage to VarsModal.order_data
	loadStorage: function(){
		
		// load printer setting
		var printer_setting = localStorage.getItem("printers_setting");
		if (printer_setting) {
			printer_setting_json = null;	
			var printer_setting_json = JSON.parse(printer_setting);
		
		}
		
		
		VarsModal.printer_setting = printer_setting_json;
		
		printer_setting = null;
		
		if (typeof VarsModal.local_table_data == 'undefined') VarsModal.local_table_data = {};
		
		if (window.debug){
			console.log('load table storage data',VarsModal.local_table_data);
			//console.log('printer settings ',VarsModal.printer_setting);
		}
	},
	
	
	
	// count product in basket
	count_products_basket_price: function(nosave){
		this.basket_recount();
		
		var pocet = $('basket_body').getElements('tr').length;
		if ($('basket_noproducts')) {
			if (pocet == 0){
				$('basket_noproducts').removeClass('none');
			} else {
				$('basket_noproducts').addClass('none');	
			}
		}
		
		return pocet;
	},
	
	
	// recount price in basket
	basket_recount: function(){
		var total_price = 0;
		//console.log('basket recount ',VarsModal.local_table_data[window.table_id]);
		$('total_price').set('text',price(total_price));
		$('TotalPrice').value = total_price;
		
		if (typeof VarsModal.local_table_data[window.table_id] != 'undefined' && VarsModal.local_table_data[window.table_id]  && Object.getLength(VarsModal.local_table_data[window.table_id])>0)
		Object.each(VarsModal.local_table_data[window.table_id]['items'],VarsModal.data_each = function(item){
			total_price += item.row_price.toInt();
		});
		
		
		// show total price left side
		if ($('TableCnt'+window.table_id)){
			if (total_price > 0){
				$('TableCnt'+window.table_id).set('text',price(total_price));
			} else {
				$('TableCnt'+window.table_id).set('text','');
			}
		}
		
		$('total_price').set('text',price(total_price));
		$('TotalPrice').value = total_price;
		//console.log('total price',total_price);
		if (VarsModal.local_table_data[window.table_id])
		VarsModal.local_table_data[window.table_id].total_price = total_price;
		console.log('after recount ',VarsModal.local_table_data[window.table_id]);
		
		
		// save local storage table
		localStorage.setItem(
			"local_table_data", 
			JSON.stringify(VarsModal.local_table_data)
		);
		
		active_el = null;
		
	},
	
	
	// product to basket events
	product_to_basket: function(){
		if ($('product_list')){
			$('product_list').getElements('.back_product').addEvent('click',this.product_back_click.bind(this));
			//$('product_list').getElements('.without_addon').addEvent('click',this.without_addon_click.bind(this));
			$('product_list').getElements('.product').addEvent('click',this.product_to_basket_click.bind(this));
		}
	},
	
	// zpet pri vyberu addons 
	product_back_click: function(e){
		if (!e.target)  e.target = e;
		this.add_addon = false;
		e.target.getParent('.product_addons').addClass('none');
	},
	
	
	// create line from data
	create_line_from_data: function(data){
		//console.log(VarsModal.local_table_data[window.table_id]);
		if (Object.getLength(VarsModal.local_table_data[window.table_id])>0){
			Object.each(VarsModal.local_table_data[window.table_id]['items'],VarsModal.data_each = function(item,key){
				if (item.done == 1){
					VarsModal.local_table_data[window.table_id]['items']['d'+key] = VarsModal.local_table_data[window.table_id]['items'][key];
					VarsModal.local_table_data[window.table_id]['items']['d'+key].line_id = 'd'+VarsModal.local_table_data[window.table_id]['items'][key].line_id;
					delete VarsModal.local_table_data[window.table_id]['items'][key];
				}
			});
			//console.log(VarsModal.local_table_data[window.table_id]);
			this.renderBasketData();
			
		}
	},
	
	
	// product to basket event click
	product_to_basket_click: function(e){
		if (e.stop) e.stop();
		var elSelect = e.target;
		if (elSelect.hasClass('back_product')){
			return false;
		}
		
		if (elSelect.hasClass('without_addon')){
			elSelect = elSelect.getParent('.product');
			//console.log(e.target);
			without_addon = true;
			elSelect.getElement('.product_addons').addClass('none');
			//return false;
		} else {
			without_addon = false;
		}
		
		if (!$('type_rozvoz').hasClass('active') && $('TableId').value == ''){
			FstError('Musíte zvolit stůl');
			return false;
		}
		
		if (elSelect.getElement('.product_addons') && !without_addon){
			elSelect.getElement('.product_addons').removeClass('none');
			return false;
		}
		//console.log(elSelect);
		//console.log(JSON.decode(elSelect.get('data-product')));
		data_product = JSON.decode(elSelect.get('data-product'));
		this.create_line_basket(elSelect,data_product);
		
		
		$('CheckUkoncit').value = 1;
		
		// pokud je addon schovej vyber
		if (elSelect.hasClass('paddon')){
			this.add_addon = false;
			elSelect.getParent('.product_addons').addClass('none');
		}
	},
	
	
	// create new line in basket
	create_line_basket: function(elSelect,opt){
		
		if (typeof VarsModal.button_delete == 'undefined') VarsModal.button_delete = {};
		if (typeof VarsModal.button_delete_fce == 'undefined') VarsModal.button_delete_fce = [];
		if (typeof VarsModal.local_table_data[window.table_id] == 'undefined') VarsModal.local_table_data[window.table_id] = {}
		if (typeof VarsModal.local_table_data[window.table_id]['items'] == 'undefined') VarsModal.local_table_data[window.table_id]['items'] = {}
		if (typeof VarsModal.local_table_data[window.table_id]['created'] == 'undefined') VarsModal.local_table_data[window.table_id]['created'] = new Date().format('db');
		if (typeof VarsModal.local_table_data[window.table_id]['total_price'] == 'undefined') VarsModal.local_table_data[window.table_id]['total_price'] = 0;
		if (typeof VarsModal.local_table_data[window.table_id]['done_items'] == 'undefined') VarsModal.local_table_data[window.table_id]['done_items'] = [];
		//console.log(VarsModal.local_table_data[window.table_id]);
		//console.log(opt);
		
		// pokud je addon najdi nadrazeny
		if (elSelect.hasClass('paddon')){
			parentProduct = elSelect.getParent('.product');
			dataParentProduct = JSON.decode(parentProduct.get('data-product'));
			dataParentProduct.count = 1;
			dataParentProduct.row_price = price(dataParentProduct.count*dataParentProduct.price.toInt());
			dataParentProduct.user_id = window.logged_user_id;
			dataParentProduct.terminal_id = window.logged_terminal_id;
			dataParentProduct.created = new Date().format('db');
			console.log(dataParentProduct);
			opt.parent_id = dataParentProduct.id;
			opt.addon = 1;
			opt.tr_class = 'tr_addon';
			opt.created = new Date().format('db');
			opt.count = 1;
			opt.user_id = window.logged_user_id;
			opt.terminal_id = window.logged_terminal_id;
			opt.row_price = ((opt.price)?price(opt.count*opt.price.toInt()):0);
		
		// solo product
		} else {
			dataParentProduct = false;
			opt.created = new Date().format('db');
			opt.count = 1;
			opt.user_id = window.logged_user_id;
			opt.terminal_id = window.logged_terminal_id;
			opt.row_price = price(opt.count*opt.price.toInt());
		}
		
		line_prefix = 'line_';
		// pokud je jiz ulozeno zmen ID v poli
		if (opt.done){
			opt.line_id = 'd'+opt.id+'_'+opt.line_count;
		} else {
			opt.line_id = line_prefix+opt.id;
			//console.log(opt);
		}
		if (opt.addon == 1){
			dataParentProduct.line_id = line_prefix+opt.parent_id;
			opt.line_id = line_prefix+opt.parent_id+'V'+opt.id;
			
			if (!dataParentProduct.addonListName){
				dataParentProduct.addonList = [];
				dataParentProduct.addonListName = [];
				dataParentProduct.addonListPrice = 0;
			}
			dataParentProduct.addonListName.push(opt.name);
			dataParentProduct.addonList.push(opt.line_id);
			dataParentProduct.addonListPrice += opt.price;
			
			if (dataParentProduct){
				dataParentProduct.line_id = dataParentProduct.line_id + opt.line_id;
				
			}
			opt.parent_line_id = dataParentProduct.line_id;
			
		} else {
			dataParentProduct.line_id = dataParentProduct.id;
		}
		
		
		
		// pokud je parent produkt k addon
		if (dataParentProduct){
			console.log(line_prefix);
			//VarsModal.local_table_data[window.table_id]['items'][line_prefix+dataParentProduct.id] = dataParentProduct;
			VarsModal.local_table_data[window.table_id]['items'][dataParentProduct.line_id] = dataParentProduct;
			
		}
		// pokud existuje uprav pocet COUNT a ROW PRICE
		if (VarsModal.local_table_data[window.table_id]['items'][opt.line_id]){
			VarsModal.local_table_data[window.table_id]['items'][opt.line_id].count ++;
			VarsModal.local_table_data[window.table_id]['items'][opt.line_id].row_price = price(VarsModal.local_table_data[window.table_id]['items'][opt.line_id].price * VarsModal.local_table_data[window.table_id]['items'][opt.line_id].count);
		
		} else {
			VarsModal.local_table_data[window.table_id]['items'][opt.line_id] = opt;
		
		}
		//console.log(VarsModal.local_table_data[window.table_id]['items']);
		
		this.renderBasketData();
		render_data = null;
		return false;
		
		
			
		
	},
	
	
	// create product parse line
	product_parser: function(){
		if ($('product_parser')){
			$('product_parser').getElements('li').addEvent('click',VarsModal.click_parser = function(e){
				VarsModal.local_table_data[window.table_id]['items'][e.target.get('data-id')] = {
					'name':e.target.get('text'),
					'tr_class':'parser',
					'parse_class':'none',
					'name_class':'col_parser',
					'name_colspan':3,
					'id':e.target.get('data-id'),
					'line_id':e.target.get('data-id'),
					'price':0,
					'row_price':0,
					'count':0,
				}
				this.renderBasketData();
				//console.log(VarsModal.local_table_data[window.table_id]);
				
			}.bind(this));
		}
	},
	
	renderBasketData: function(){
		var render_data = [];
		if (VarsModal.local_table_data[window.table_id] && typeof VarsModal.local_table_data[window.table_id]['items'] == 'undefined'){
			$('basket_products_obal').getElement('.basket_empty').setProperty('style','display:block');
		} else {
			
		$('basket_products_obal').getElement('.basket_empty').setProperty('style','display:none');
		//console.log(VarsModal.local_table_data[window.table_id]['items']);
		
		// prepare render data as ARRAY from JSON object
		if (VarsModal.local_table_data[window.table_id])
		Object.each(VarsModal.local_table_data[window.table_id]['items'],VarsModal.data_each = function(item){
			//item.created = Date.parse(item.created).format("%d.%m.%Y %H:%M:%S");
			item.terminal_name = ((window.terminal_list[item.terminal_id])?window.terminal_list[item.terminal_id]:'Nevyplněno');
			item.user_name = ((window.user_list[item.user_id])?window.user_list[item.user_id]:'Nevyplněno');
			render_data.push(item);
		});
		}
		//return false;
		render = false;
		window.template_basket.when(TempoEvent.Types.RENDER_COMPLETE, function (event) {
			
			if (!render){
			render = true;
			this.renderBasketComplete();
			}
		
		}.bind(this)).render(render_data);
	},
	
	// init after tempo render
	renderBasketComplete: function(){
		this.basketLineEvent();
		this.count_products_basket_price();
	},
	
	// basket line events
	basketLineEvent: function(){
		// poznamka k produktu
		$('basket_body').getElements('.note').removeEvents('change');
		$('basket_body').getElements('.note').addEvent('change',function(e){
			
			data_line = e.target.get('data-line');
			item = VarsModal.local_table_data[window.table_id]['items'][data_line];
			item.note = e.target.value;	
			console.log(VarsModal.local_table_data[window.table_id]['items']);
		});
		
		// poznamka k produktu
		$('basket_body').getElements('.detail').removeEvents('click');
		$('basket_body').getElements('.detail').addEvent('click',function(e){
			e.target.toggleClass('active');
			e.target.getNext('.note').toggleClass('none');
			if (e.target.getNext('.note').hasClass('none')){
			e.target.getNext('.note').getElement('textarea').focus();
			FldLength = e.target.getNext('.note').getElement('textarea').value.length;
			e.target.getNext('.note').getElement('textarea').setSelectionRange(FldLength, FldLength);
			}
		});
		
		// more info k produktu
		$('basket_body').getElements('.more').removeEvents('click');
		$('basket_body').getElements('.more').addEvent('click',function(e){
			e.target.toggleClass('active');
			e.target.getNext('.more_show').toggleClass('none');
			
		});
		
		
		// zmena ks / smazani
		$('basket_body').getElements('.change_ks').removeEvents('click');
		$('basket_body').getElements('.change_ks').addEvent('click',function(e){
			e.stop();
			data_line = e.target.get('data-line');
			
			
			// pokud je smazat button
			if (e.target.hasClass('delete_button')){
				item = VarsModal.local_table_data[window.table_id]['items'][data_line];
				item.count --;
				item.row_price =  ((item.price)?price(item.price.toInt() * item.count):0);
				// pokud je 0 tak smaz produkt
				if (item.count < 1){
					if (VarsModal.local_table_data[window.table_id]['items'][data_line].parent_line_id){
						var parentEl = VarsModal.local_table_data[window.table_id]['items'][VarsModal.local_table_data[window.table_id]['items'][data_line].parent_line_id];
						if (parentEl){
							delIndex = parentEl.addonList.indexOf(data_line);
							parentEl.addonList.splice(delIndex, 1);
							parentEl.addonListName.splice(delIndex, 1);
							parentEl.addonListPrice -= VarsModal.local_table_data[window.table_id]['items'][data_line].price;
						}
						//console.log(parentEl);
					}
					delete VarsModal.local_table_data[window.table_id]['items'][data_line];
					$('line_id_'+data_line).destroy();
				// odeber pocet ks a prepocti row price
				} else {
					if ($('line_id_'+data_line).getElement('.ks_input')){
					$('line_id_'+data_line).getElement('.ks_input').value = item.count;
					$('line_id_'+data_line).getElement('.row_price').set('text',item.row_price);
					} else {
						$('line_id_'+data_line).destroy();
						delete VarsModal.local_table_data[window.table_id]['items'][data_line];
					}
				}
				
				this.count_products_basket_price();
				this.ukoncit_order(true);
				
				// print data
				//fstPrint.print_data();
		
				//console.log(VarsModal.local_table_data[window.table_id]['items']);
			}
			
			// pokud je zmena pocet ks
			if (e.target.hasClass('ks_input')){
				$('basket_change_ks').removeClass('none');
				window.data_line_id = $('line_id_'+data_line);
				//console.log($('line_id_'+data_line));
				$('ChangeKs').value = $('line_id_'+data_line).getElement('.ks_input').value;
				$('ChangeKs').focus();
				$('ChangeKs').select();
				
				
				$('ChangeKsButton').removeEvents('click');
				$('ChangeKsButton').addEvent('click',VarsModal.click_change_ks = (function(e){
					$('ChangeKs').fireEvent('keydown','fire_click');
					
				}).bind(this));
				
				$('ChangeKs').removeEvents('keydown');
				$('ChangeKs').addEvent('keydown',VarsModal.keydown_fce = function(e){
					
					item = VarsModal.local_table_data[window.table_id]['items'][window.data_line_id.get('data-line')];
					item.count = $('ChangeKs').value.toInt();
					item.row_price =  price(item.price.toInt() * item.count);
					window.data_line_id.getElement('.ks_input').value = item.count;
					window.data_line_id.getElement('.row_price').set('text',item.row_price);
					
					this.count_products_basket_price();
					
					if ($('ChangeKs').value < 1){
						$('basket_change_ks').addClass('none');
					}
				
					if (e.key && e.key == 'enter'){
						//window.data_line_id.getElement('.ks_input').fireEvent('change',window.data_line_id.getElement('.ks_input'));
						e.stop();
						$('basket_change_ks').addClass('none');
						
					}
					if (e == 'fire_click'){
						//window.data_line_id.getElement('.ks_input').fireEvent('change',window.data_line_id.getElement('.ks_input'));
						$('basket_change_ks').addClass('none');
					}
				}.bind(this));
			}
			
			data_line = null;
			e = null;
			item = null;
		}.bind(this));
	},
	
	// show basket / hide basket
	show_basket: function(hide){
		if (hide == true){
			$('basket_products_obal').getElements('.hide_basket').toggleClass('none');
			
			//$('basket_function').addClass('none');
			//$('basket_products').addClass('none');
			//$('notable_select').removeClass('none');
			//$('table_list').getElements('.tg').removeClass('active');
			//$('TableId0').fireEvent('click',$('TableId0'));
		
		} else {
			$('basket_products_obal').getElements('.hide_basket').removeClass('none');
			$('notable_select').addClass('none');
			//$('basket_function').removeClass('none');
			//$('basket_products').removeClass('none');
			//$('notable_select').addClass('none');
		
		}
		
	},
	
	
	
	
});