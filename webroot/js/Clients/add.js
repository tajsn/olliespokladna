if($("add-contact")){
  $("add-contact").addEvent("click", function (e) {
      var url = $(this).get("href");

      var req = new Request.JSON({
        url : url,
        method: 'get',
        onRequest: function(){
          $("client-contacts").set('html','<h3>Loading ...</h3>');
        },
        onComplete: function(res){
          $("client-contacts").set("html", res);
        }
      }).send();

      e.preventDefault();
  });

  if($("addClientBtn")){
    $("addClientBtn").addEvent("click", function(){
      $("add-client-container").toggle();
    });
  }
}