window.addEvent('domready',function(){
	//alert('');
	if ($('SaveModal')){
		// input limit
		$('SaveModal').getParent('form').getElements('.float, .integer').inputLimit();
		
		// save modal event
		$('SaveModal').addEvent('click',window.fstsystem.modal_save);
		
		// calendar
		date_picker();
		//console.log(window);
		//parent.document.getElement('#printer_list')
		//FstPokladnaClient.get_printer($('SystemId').value);
		//window.fstPokladna.get_printer_list();
	}
	//console.log(window.modal);
	// modal tabs
	$$('.tabs').each(function(tabs){
		var li_list = tabs.getElement('.modal_tabs').getElements('li');
		var tab_list = tabs.getElement('.modal_contents').getElements('.modal_tab');
		
		// default active first tab
		li_list[0].addClass('active');
		
		tab_list.each(function(tab,k){
			if (k>0){
				tab.addClass('none');
			}
		});
		
		// event tabs li
		li_list.addEvent('click',function(e){
			e.stop();
			li_list.removeClass('active');
			e.target.addClass('active');
			
			var index = li_list.indexOf(e.target);
			tab_list.addClass('none');
			tab_list[index].removeClass('none');
			//console.log($('modal_in').getSize().y);
			window.modal.resize_modal($('modal_in').getSize().y);
		});
	});
	
	$$('.select_all_fieldset').addEvent('click',function(e){
		parent = this.getParent('fieldset');
		parent.getElements('.checkbox').setProperty('checked','checked');
	});
	
	$$('.half').each(function(item){
		parent = item.getParent('div');
		parent.addClass('half');
	});
	
	$$('.half2').each(function(item){
		parent = item.getParent('div');
		parent.addClass('half2');
	});
	
	$$('.long').each(function(item){
		parent = item.getParent('div');
		parent.addClass('long');
	});
	
	$$('.password').addEvent('click',function(e){
		e.target.value = '';
		$$('.password2').each(function(item){
			item.value = '';
		});
	});
	
	$$('.search_select').each(function(sel){
		console.log(sel);
		new Searchable(sel, {
			keepSelected: false,
			addMassSelect: false,
		});
	});
	
	$$('.hide_fieldset').each(function(field){
		new Element('span').set('html','&#xf107;').inject(field.getElement('legend'));
		field.addClass('close_field');
		
		field.getElement('legend').addEvent('click',function(e){
			parent_field = this.getParent('fieldset');			
			if (parent_field.hasClass('close_field')){
				parent_field.removeClass('close_field');
				parent_field.getElement('legend').getElement('span').set('html','&#xf106;');
				
			} else {
				parent_field.addClass('close_field');
				parent_field.getElement('legend').getElement('span').set('html','&#xf107;');
			}
		})
	});
	
	
	// find ares request
	$$('.find_ares').addEvent('change',function(e){
		if (e.target.value != '' && e.target.value.length == 8){
			var form = e.target.getParent('form');
			var label = e.target.getPrevious('label');
			label.addClass('preloader');
			VarsModal.ares_req = new Request.JSON({
				url:'/pages/ares/'+e.target.value,	
				onComplete: (VarsModal.ares_req_complete = function(json){
					label.removeClass('preloader');
					if (json.result == true){
						form.getElement('.ares_firma').value = json.firma;
						form.getElement('.ares_dic').value = json.dic;
						form.getElement('.ares_ulice').value = json.ulice;
						form.getElement('.ares_mesto').value = json.mesto;
						form.getElement('.ares_psc').value = json.psc;
						form.getElement('.ares_stat').value = json.stat;
						//console.log(json);
						//FstAlert(json.message);
					} else {
									
						FstError(json.message);
					}
					json = null;
				}).bind(this)
			});
			VarsModal.ares_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.ares_req.send();
		}
	});
	
	// find DPH request
	$$('.find_dph').addEvent('change',function(e){
		if (e.target.value != '' && e.target.value.length >= 10){
			var form = e.target.getParent('form');
			var label = e.target.getPrevious('label');
			label.addClass('preloader');
			VarsModal.ares_req = new Request.JSON({
				url:'/pages/vat_number/'+e.target.value,	
				onComplete: (VarsModal.ares_req_complete = function(json){
					if (!json){
						FstError('DIČ nenalezeno');
					}
					label.removeClass('preloader');
					if (json.result == true){
						form.getElement('.ares_firma').value = json.name;
						form.getElement('.ares_ico').value = json.ico;
						form.getElement('.ares_ulice').value = json.address;
						form.getElement('.ares_mesto').value = json.city;
						form.getElement('.ares_psc').value = json.zip;
						form.getElement('.ares_stat').value = json.state;
						form.getElement('.ares_valid_dph').value = json.valid_dph;
						//console.log(json);
						//FstAlert(json.message);
					} else {
									
						FstError(json.message);
					}
					json = null;
				}).bind(this)
			});
			VarsModal.ares_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.ares_req.send();
		}
	});
	
});

