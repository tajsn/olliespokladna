/*
Fst History
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var FstSystem = this.FstSystem = new Class({
	Implements:[Options,Events,FstPrint],
	debug : false,
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		//console.log('run');
		
		this.table_items();
		this.init_complete_history();
		this.element_size();
		//console.log(window.getDimensions());
	},
	
	element_size: function(){
		if ($('filtration')){
			$('filtration').setStyle('height',window.getSize().y - 80);
		}
	},
	
	
	// init all table items
	table_items: function(){
		var myURI = new URI(window.location);
	
		// nastavení
		//var base = "https://"+myURI.parsed.host;
		var base = 'http://'+myURI.parsed.host;
		this.modal = window.modal = new frameModal("modalWindow", {base : base});
		
		this.table_icons();
		this.dropdown_menu();
		this.table_mouse();
		this.table_filtration();
		table_scroll();
		
		this.table_top_action();
	  
		
	},
	
	// top table action
	table_top_action: function(){
		date_picker();
		/*
		$$('.pdf').addEvent('click',function(e){
			e.stop();
			var win = window.open(e.target.href, '_blank');
		});
		*/
		
		checked_list = [];
		if ($('top_action'))
		$('top_action').getElements('.export_checked').addEvent('click',function(e){
			e.stop();
			$$('.checked_list').each(function(item){
				if (item.checked){
					checked_list.push(item.get('data-id'));
				}
			});
			if (checked_list.length == 0){
				FstError('Není nic vybráno');
				return false;
			}
			window.open(e.target.href+'?ids='+checked_list.join(), '_blank');
			//console.log(checked_list);
		
		});
		
		if ($('top_action')){
			if ($('top_action').getElement('.show_storno')){
				$$('.show_storno').addEvent('click',function(e){
					e.stop();
					$$('.storno_line').toggleClass('none');
					$$('.normal_line').toggleClass('none');
				});
			}
		}
		
		if ($('top_action'))
		$('top_action').getElements('.selected_items').addEvent('click',function(e){
			e.stop();
			$$('.selected').each(function(item){
				checked_list.push(item.get('click-id').toInt());
				
			});
			if (checked_list.length == 0){
				FstError('Není nic vybráno');
				return false;
			}
			
			
			
			// storno bill
			if (e.target.hasClass('storno_bill')){
				if (confirm('Opravdu stornovat?')){
					Vars.load_reg = new Request.JSON({
						url:'/trzbas/storno_bill/'+checked_list,	
						onComplete: (Vars.delete_req_complete = function(json){
							if (json.r == true){
								$$('.selected').each(function(item){
									item.addClass('storno_line none');
								});
								
								window.fsthistory.reinit_history();
								//console.log(json);
							} else {
								FstError(json.m);
							}		
							json = null;
						}).bind(this)
					});
					Vars.load_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					Vars.load_reg.send();
				}
				
			}
			
			
			// print bill
			if (e.target.hasClass('print_bill')){
				Vars.load_reg = new Request.JSON({
					url:'/trzbas/load_trzba/'+checked_list,	
					onComplete: (Vars.delete_req_complete = function(json){
						if (json.r == true){
							this.print_data(json.print_data);
							//console.log(json);
						} else {
							FstError(json.m);
						}		
						json = null;
					}).bind(this)
				});
				Vars.load_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				Vars.load_reg.send();
				
			}
			//console.log(checked_list);
			//window.open(e.target.href+'?ids='+checked_list.join(), '_blank');
			//console.log(checked_list);
		
		}.bind(this));
		
		$$('.autocom').each(function(item){
			var id = item.get('id')+'s';
			//console.log(id);
					
			var myAutocompleter = new GooCompleter(item.get('id'), {
				action: item.get('data-url')+'/'+item.get('data-type'),	// JSON source
				param: 'search',			// Param string to send		
				preloader: 'auto_loading_'+item.get('id'),
				fire_event: function(result){
					if ($(id)){
						$(id).value = result.key;
						$(id).fireEvent('change',$(id));
					}
					//console.log($(id));
					//console.log(result);
					
				},
				minlen: 0,
				listbox_offset: { y: 2 },	// Listbox offset for Y			
				delay: 500					// Request delay to 0.5 seconds
			});
		});
		
	},
	
	// init after fst_history
	init_complete_history: function(){
		window.fsthistory.complete_fce = (function(){
			this.table_items();
		}).bind(this);
		//console.log(window.fsthistory);
	},
	
	// close modal
	close_modal: function(){
		
		if ($('close_modal')){
			_click($('close_modal'));
		}
		
	},
	
	
	// tabulky ovládané myšítkem
	table_mouse: function(){
		if($$(".item-row")[0]){
			var c = new Clickable();
			var timer;

			$$(".item-row").addEvent("click", (function(e){
			  $$('.titems').each(function(item){
				if (!item.hasClass('none')){
					item.addClass('none');
				}
				});
			  clearTimeout(timer);
			  timer = (function(){
				c.select(e.target.getParent('tr'));
			  }).delay(200, this);
			}).bind(this))

			$$(".item-row").addEvent("dblclick", (function(e){
			  //clearTimeout(timer);
				if (e.target.getParent('tr').getElement('.edit')){
					_click(e.target.getParent('tr').getElement('.edit'));
				} else {
					$$('.titems').each(function(item){
						if (!item.hasClass('none')){
							console.log(item);
							item.addClass('none');
						}
					});
					if (e.target.getParent('tr').getElement('.titems')){
						e.target.getParent('tr').getElement('.titems').removeClass('none');
					}
					if (e.target.getParent('tr').get('data-link')){
						window.location = e.target.getParent('tr').get('data-link');
					}
				}
			}).bind(this));

		  }	
	},
	
	// rozevírací menu momentálně nefunkční
	dropdown_menu: function(){
		$$(".dropdown").addEvent("click", (function(e){
			e.target.getElement("ul").addClass("sub").toggle();
		}).bind(this));

	},
	
	// table icons function
	table_icons: function(){
		// close big modal
		$$('.big_modal_close').addEvent('click',VarsModal.pay_modal_close = function(e){
			if (e.target.getParent('.modal_win'))
			e.target.getParent('.modal_win').addClass('none');
					
					
		});
		if($$("a")){
			$$("a").each(function(item){
				if (!item.hasClass('fst_h'))
				item.removeEvents('click');
			});
			$$("a").addEvent("click", (Vars.click_icon_fce = function (e) {
				// copy
				if (e.target.hasClass('copy')){
					e.stop();
					alert('');
					//window.fm.open_modal(e.target);
				}
				// ajax linky pro otevírání modálů
				if (e.target.get("ajax")) {
					var url = e.target.get("href");
					this.modal.show();
					this.modal.setContent(url);
					e.stop();
				}
				// pdf
				if (e.target.hasClass('pdf')){
					e.stop();
					var myURI = new URI(window.location);
					if (myURI.parsed.query){
						var par = myURI.parsed.query.split('h=');
					}
					var win = window.open(e.target.href+'?h='+((par[1])?par[1]:''), '_blank');
			
				}
				// modal pokladna
				if (e.target.hasClass('open_modal_pokl')){
					e.stop();
					//window.fm.open_modal(e.target);
				}
				
				// trash button
				if (e.target.hasClass('trash_button')){
					e.stop();
					var parent = e.target.getParent('tr');
					if (confirm(lang['opravdu_smazat'])){
						Vars.delete_reg = new Request.JSON({
							url:e.target.href,	
							onComplete: (Vars.delete_req_complete = function(json){
								if (json.result == true){
									FstAlert(json.message);
									parent.destroy();
								} else {
									
									FstError(json.message);
									
								}
								area_data = null;
								json = null;
							}).bind(this)
						});
						Vars.delete_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
						Vars.delete_reg.send();
		
					}
				}
				// nahled button
				if (e.target.hasClass('print_nahled')){
					e.stop();// clone button
					$('print_modal').removeClass('none');
					$('uzaverkas_frame').addClass('preloader_solo');
					
					VarsModal.gen_uzaverka = new Request.JSON({
						url:e.target.href,	
						onError: function(){
							FstError('Chyba generování uzávěrka');
							
							$('uzaverkas_frame').removeClass('preloader_solo');
						},
						onComplete:function(json){
							$('uzaverkas_frame').removeClass('preloader_solo');
						
							if (json.r == true){
								// pokud jsou tiskove data
								if (json.print_data != ''){
									fstPrint.print_data(json.print_data);
								} else {
									$('uzaverkas_frame').set('html',json.html);
									$('uzaverkas_frame').setStyle('height',window.getSize().y - 70);
								}
								//console.log(json.html);	
							} else {
								FstError(json.m);
							}
							json = null;
						}.bind(this)
					});
					VarsModal.gen_uzaverka.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					VarsModal.gen_uzaverka.send();
				}
				
				
				
				if (e.target.hasClass('clone')){
					e.stop();
					var parent = e.target.getParent('tr');
					if (confirm(lang['opravdu_klonovat'])){
						Vars.clone_reg = new Request.JSON({
							url:e.target.href,	
							onComplete: (Vars.clone_req_complete = function(json){
								if (json.result == true){
									FstAlert(json.message);
									window.fsthistory.reinit_history();
								} else {
									FstError(json.message);
									
								}
								json = null;
							}).bind(this)
						});
						Vars.clone_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
						Vars.clone_reg.send();
		
					}
				}
			}).bind(this));
		}	
		
	},
	
	// modal save events
	modal_save: function(e){
		e.stop();
		button_preloader(e.target);
							
		VarsModal.save_reg = new Request.JSON({
				url:e.target.getParent('form').action,	
				onError: function(){
					button_preloader(e.target);
					
				},
				onComplete: (VarsModal.save_reg_complete = function(json){
					button_preloader(e.target);
					if (json.r == true){
						//console.log(window.top.body);
						//window.fstsystem.show_alert(json.m);
						//FstAlert(json.m,true);
						//FstError(json.m,true);
						if (typeof parent.document == 'undefined'){
							parent.document = window.top;
						}
						if (parent.document && $(parent.document.body).getElement('#close_modal')){
							$(parent.document.body).getElement('#close_modal').set('data-result',json.r);
							$(parent.document.body).getElement('#close_modal').set('data-message',json.m);
							if (json.id){
								$(parent.document.body).getElement('#close_modal').set('data-id',json.id);
							}
							_click($(parent.document.body).getElement('#close_modal'));
							//$(parent.document.body).getElement('#close_modal').fireEvent('click',$(parent.document.body).getElement('#close_modal'));
						} else {
							FstAlert(json.m);
							(function(){
								window.close();
							}).delay(1000);
						}
						//FstAlert(json.m,true);
						
					} else {			
						FstError(json.m);
						if (json.invalid){
							$$('.invalid').removeClass('invalid');
							json.invalid.each(function(item){
								item = item.replace('_','-');
								//console.log(item);
								if($(item)){
									$(item).addClass('invalid');	
								}
							});
						}
					}
					json = null;
				}).bind(this)
			});
			VarsModal.save_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.save_reg.post(e.target.getParent('form'));
	},
	
	// filtration table
	table_filtration: function(){
		if ($('filtration') && $('FiltrCancel')){
			$('FiltrCancel').removeEvents('click');
			$('FiltrCancel').addEvent('click',function(e){
				e.stop();
				
				window.fsthistory.clear_history();
			});
		}
		
	},
	
	drag_elements_events:function(){
		// show map
		$$('.show_map').removeEvents('click');
		$$('.show_map').addEvent('click',this.show_map.bind(this));
		
		$$('.element_drag').removeEvents('mouseenter');
		$$('.element_drag').addEvents({
			'mouseenter':function(e){
				if (!e.target){
					e.target = e;
				}
				
				
				if (e.target.getElement('.detail_hover')){
					var pos = e.target.getCoordinates();
					
					e.target.getElement('.detail_hover').removeClass('none');
					
					
					if (pos.top + e.target.getElement('.detail_hover').getSize().y > window.getSize().y){
						pos.top = pos.top - e.target.getElement('.detail_hover').getSize().y + e.target.getSize().y;
					}
					var cor = e.target.getElement('.detail_hover').getCoordinates();
					if (cor.bottom > window.getSize().y){
						pos.top = window.getSize().y - e.target.getElement('.detail_hover').getSize().y;
					}
					
					e.target.getElement('.detail_hover').setStyles({
						'top':pos.top,
						'left':pos.left-400,
					});
				}
			},
			'mouseleave':function(e){
				if (e.target.getElement('.detail_hover')){
					e.target.getElement('.detail_hover').addClass('none');
				}
			}
		});
		
		$$('.dsel').each(function(dsel){
		dsel.removeEvents('click');	
		dsel.addEvent('click',function(e){
			e.stop();
			var date = e.target.get('data-date');
			var date_from = e.target.get('data-from');
			var date_to = e.target.get('data-to');
			
			
			if (e.target.hasClass('active')){
				e.target.removeClass('active');
				$$('.element_drag').removeClass('none');
				
				return false;
			}
			$$('.dsel').removeClass('active');
			
			e.target.addClass('active');
			
			//console.log(date_to);	
			$$('.element_drag').each(function(item){
				
				item.addClass('none');
				if (item.hasClass('zakazky_list')){
				
				if (date_from && date_to){
						if (item.get('data-date_nalozeni').toInt() >= date_from.toInt() && item.get('data-date_nalozeni').toInt() <= date_to.toInt()){
							item.removeClass('none');
						}
						if (item.get('data-date_nalozeni2').toInt() >= date_from.toInt() && item.get('data-date_nalozeni2').toInt() <= date_to.toInt()){
							item.removeClass('none');
						}
				}
				else if (date_from){
				
					if (item.get('data-date_nalozeni') >= date_from){
						item.removeClass('none');
						
					}
					if (item.get('data-date_nalozeni2') >= date_from){
						item.removeClass('none');
						
					}
				}
				else{
					if (item.get('data-date_nalozeni') == date){
						item.removeClass('none');
					}
					if (item.get('data-date_nalozeni2') == date){
						item.removeClass('none');
					}
				}
				
				}
			});
			//console.log(e.target.get('data-to'));
			//console.log(e.target.get('data-from'));
		});
		});
			
	},
	
	// show map 
	show_map: function(e){
		if (e){
			e.stop();
			if ($('show_map')){
				$('show_map').destroy();
			}
			if (!e.target.hasClass('open')){
				new Element('id',{'id':'show_map','data-lat':e.target.get('data-lat'),'data-lng':e.target.get('data-lng')}).inject(e.target);
				e.target.addClass('open');
			} else {
				e.target.removeClass('open');
				
			}
	
		}
		if (typeof google == 'undefined'){
				
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&callback=init_google";
			document.body.appendChild(script);
		
		} else {
			//alert('');
			this.create_map();
		}
	},
	
	// check polygon
	check_polygons: function(latlng,oblast_id){
		var myPosition = new google.maps.LatLng(latlng.lat, latlng.lng);
		
		if (this.polygons_array){
			//console.log('aaa');
			var count_oblast = 0;
		
			this.polygons_array.each(function(poly){
			//console.log(myPosition);				
			
			if (google.maps.geometry.poly.containsLocation(myPosition, poly)) {
				poly_name = poly.name;
				poly_uid = poly.uid;
				//console.log('je');
				//console.log(poly);
				count_oblast = 1;
			} else {
				//$('PokladnaOblastName').value = '';
				//$('PokladnaOblastId').value = '';
				//console.log('neni');
				//count_oblast = 0;
			}
							
			});
			if(count_oblast > 0){
				//console.log(poly_uid);
				//$('PokladnaOblastName').value = poly_name;
				$(oblast_id).value = poly_uid;
				
			} else {
				//$('PokladnaOblastName').value = '';
				$(oblast_id).value = '';
					
			}
			
			count_oblast = null;
			poly = null;
			poly_name = null;
			poly_uid = null;
			//this.polygons_array = null;
			polygons_array = null;
			
				
						
		}
	},
	
	// draw map area
	initialize_area: function(area_data){
		area_data = JSON.decode(area_data);
		//console.log(this.map);
			
			if (area_data){
				polygons = {};
				polygons_array = [];
				Object.each(area_data,(function(item,k){
					//console.log(item);
					poly = new google.maps.Polygon({
						strokeWeight:0,
						fillOpacity:0.25,
						fillColor:item.color,
						path: google.maps.geometry.encoding.decodePath(String(item.coords)), 
						//levels: decodeLevels(String(polyLayer[i][1])), 
					}); 
					if (!polygons[k]){ 
					
						polygons[k] = {
							'shape':poly,
							'name':item.name,
							'color':item.color
						};
					
					}
					
					
					poly.uid = k;
					poly.name = item.name;
					polygons_array.push(poly);
					
					poly.setMap(this.map);
				}).bind(this));
				poly = null;
				polygons = null;
				this.polygons_array = polygons_array;
				
				polygons_array = null;
				area_data = null;
			}
		
	},
	
	// create map
	create_map: function(){
		//var myLatLng = {lat: 49.8144897, lng: 18.2092833};
		//console.log($('show_map').get('data-lng'));
		//console.log(myLatLng);
		if ($('show_map')){
			var myLatLng = {lat: $('show_map').get('data-lat').toFloat(), lng: $('show_map').get('data-lng').toFloat()};
			var oblast_id = $('show_map').get('data-oblast');
			
			this.map = new google.maps.Map($('show_map'), {
				zoom: 4,
				disableDefaultUI: true,
				center: myLatLng
			});

			var marker = new google.maps.Marker({
				position: myLatLng,
				draggable: ((this.edit_map)?true:false),
				map: this.map,
				title: 'Adresa'
			});
			
			if (this.edit_map){
				google.maps.event.addListener(marker, 'drag', function() {
					//console.log(marker.getPosition().toJSON());
					var parent = $('show_map').getParent('fieldset');
					
					parent.getElement('.adr_lat').value = marker.getPosition().toJSON().lat;
					parent.getElement('.adr_lng').value = marker.getPosition().toJSON().lng;
				});
			}
			
			this.map.setZoom(12);
			
			this.initialize_area($('map_areas_load').get('text'));
			this.check_polygons(myLatLng,oblast_id);
			
		}

		
	}
	
	
	
	
	
});

function init_google(edit){
	if (edit == true)
	window.fstsystem.edit_map = true;
	window.fstsystem.show_map();
}

//})();	
window.addEvent('domready', function () {
	window.fstsystem = new FstSystem();
	//var s = $('body').getDimensions({computeSize: true});
	//console.log(s);
	//window.fstsystem.alert_fce('test');
});


function _click(el){
	
		if (Browser.name == 'ie' || Browser.name == 'unknown'){
			el.click();
		} else {
			event = document.createEvent( 'HTMLEvents' );
			event.initEvent( 'click', true, true );
			el.dispatchEvent( event ); 
					
		}
}
