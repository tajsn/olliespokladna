/*
Fst Context Menu
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var FstMenu = this.FstMenu = new Class({
	Implements:[Options,Events],
	debug : false,
	// init fce
	initialize:function(options){
		this.setOptions(options);
		this.elem = options.element;
		
		this.init_events();
	},
	
	
	// init event to element right click
	init_events: function(){
		if (this.elem){
			this.elem.addEvent('contextmenu',this.open_menu.bind(this));
		}
	},
	
	// open context menu on cursor position 
	open_menu: function(e){
		e.stop();
		this.targetEl = e.target;
		window.pos_x = this.pos_x = e.client.x;
		window.pos_y = this.pos_y = e.client.y;
		
		this.create_menu();
		this.menu_events();
		this.close_menu_body();
	
	},
	
	// create ul>li list to menu
	create_menu: function(){
		this.menu = new Element('ul',{'id':'context_menu'}).inject($('addon'));
		this.menu.setStyles({
			'top':this.pos_y,
			'left':this.pos_x+10
		});
		if (this.options.list){
			Object.each(this.options.list,(function(item,k){
				if (!item.disable){
					new Element('li',{'data-fce':item.fce,'data-id':item.id}).set('text',item.name).inject(this.menu);
				}
			}).bind(this));
		}
		
	
	},
	
	// menu events on LI click
	menu_events: function(){
		this.menu.getElements('li').addEvent('click',(function(e){
			var fce = e.target.get('data-fce');
			var id = e.target.get('data-id');
			if (typeof window[fce] != 'undefined'){
				window[fce](this.targetEl,id);
				
				this.close_menu();
			} else {
				alert('Funkce neni definovana');
			}
		}).bind(this));
	},
	
	// close context menu
	close_menu: function(){
		
		if (this.menu){
			this.menu.getElements('li').each(function(item){
				item.removeEvents('click');
			});
			this.menu.destroy();
		}
	},
	
	// close context menu from body click
	close_menu_body: function(){
		$('body').addEvent('click',(function(e){
			if (this.menu){
				if (this.get_inner(e.client,this.menu)){
					//console.log('je');
				} else {
					//console.log('neni');
					this.close_menu();
				}
			}
		}).bind(this));
	},
	
	// check if cursor inside element
	get_inner: function(cpos,element){
		var pos = element.getPosition();
		var size = element.getSize();
		if ((cpos.x > pos.x && cpos.x < pos.x+size.x) && (cpos.y > pos.y && cpos.y < pos.y+size.y)){
			return true;
		} else {
			return false;
		}
	}
	
	
	
	
		
	
	
});

function get_checked_car(type,id){
	var car_list = [];
	$('load_canvas').getElements('.car').each(function(car){
		if (car.getElement('.checkbox').checked){
			car_list.push(car.get('data-id'));
		}
	});
	if (car_list.length > 0){
		//return car_list;
		if (type == 'sklad'){
			if (id == 99){
				id = null;
			}
			window.fst_drag.save_connect_req('car_to_sklad',car_list.join(','),id);
		} else if (type == 'sklad_zakazky'){
			window.fst_drag.save_connect_req('car_zakazky_to_sklad',car_list.join(','),id);
		} else {
			window.fst_drag.car_to_basket(window.pos_x,window.pos_y,car_list);
			
		}
	} else {
		FstError('Vyberte alespoň jedno auto');
		return false;
	}
}


function get_checked_side(type,id,target){
	var item_list = [];
	target.getParent('.load_data').getElements('.element_drag').each(function(item){
		
		if (item.getElement('input.checkbox').checked){
			item_list.push(item.get('data-id'));
		}
	});
	//console.log(item_list);
	if (item_list.length > 0){
		//return car_list;
		if (type == 'sklad'){
			window.fst_drag.save_connect_req('driver_to_sklad',item_list.join(','),id);
		}
		if (type == 'join_prives'){
			window.fst_drag.save_connect_req('join_prives',item_list.join(','),id);	
		}
		if (type == 'unjoin_prives'){
			window.fst_drag.save_connect_req('unjoin_prives',item_list.join(','),id);
		}
		
	} else {
		FstError('Vyberte alespoň jednu položku');
		return false;
	}
}


// send car to another dispecer
function send_another_dispecer(target,id){
	car_list = get_checked_car();
}

// send selected car
function send_selected(target,id){
	//console.log(id);
	car_list = get_checked_car('sklad',id);
}

// send selected side
function send_selected_item(target,id){
	//console.log(target);
	//console.log(id);
	item_list = get_checked_side('sklad',id,target);
}
// join car prive
function join_prives(target,id){
	//console.log(target);
	//console.log(id);
	item_list = get_checked_side('join_prives',id,target);
}
// unjoin_prives
function unjoin_prives(target,id){
	//console.log(target);
	//console.log(id);
	item_list = get_checked_side('unjoin_prives',id,target);
}
// send selected car
function send_selected_zakazky(target,id){
	//console.log(id);
	car_list = get_checked_car('sklad_zakazky',id);
}

window.addEvent('domready', function () {
	//window.fstMenu = new FstMenu();
	//var s = $('body').getDimensions({computeSize: true});
	//console.log(s);
	//window.fstsystem.alert_fce('test');
});