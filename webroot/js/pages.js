var FstPages = this.FstPages = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		if ($('top_menu'))this.top_menu();
		length_dom = ('http://'+window.location.host).length;
		length_url = window.location.href.length;
		url_href = window.location.href.substr(length_dom,length_url);
		url_name = url_href.replace(/\//g,""); 
		
		window.addEvent('domready', function () {
			if (typeof EscPosClient != 'undefined')
			window.EscPosClient = new EscPosClient();	
			
		});

	},
	
	top_menu: function(){
		
		$('top_menu').getElements('a').removeClass('active');
		if ($('top_menu').getElement('.menu_'+$('body').get('data-controller')))
		$('top_menu').getElement('.menu_'+$('body').get('data-controller')).addClass('active');
		
		
		$('menu_open').addEvent('click',function(){
			if ($('top_menu').hasClass('open')){
				$('top_menu').removeClass('open');
				$('top_menu').setStyle('left',-1000);
			} else {
			
				$('top_menu').addClass('open');
				$('top_menu').setStyle('left',0);	
			}
		});
		
			
	},
	
	
	
});
window.addEvent('domready',function(){
	var fst_pages = new FstPages();
	
	
});

