<?php
//if (! function_exists('src1_fetch')) {function src1_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/c_core.js');}}
//if (! function_exists('src1_fetch')) {function src1_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/core1.6.js');}}
//if (! function_exists('src2_fetch')) {function src2_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/c_more.js');}}
//if (! function_exists('src2_fetch')) {function src2_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js');}}

//if (! function_exists('css1_fetch')) {function css1_fetch() {return file_get_contents('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');}}


$src1 = new Minify_Source(array('id' => 'source1','getContentFunc' => 'src1_fetch','contentType' => Minify::TYPE_JS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));
$src2 = new Minify_Source(array('id' => 'source2','getContentFunc' => 'src2_fetch','contentType' => Minify::TYPE_JS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));

$css1 = new Minify_Source(array('id' => 'source1_css','getContentFunc' => 'css1_fetch','contentType' => Minify::TYPE_CSS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));





return array(
    'js' => array(
        '//js/mootools/core1.6.js',
        '//js/mootools/c_more1.6.js',
        '//js/clickable.js',
        '//js/frameModal.js',
        '//js/mbox/min/mBox.All.min.js',
		'//js/autocompleter/autocomplete.js',
		'//js/tempo.js',
        '//js/pages.js', 
		'//js/selectize/MassSelect.js',
		'//js/selectize/selectize.js',
		
        //'//js/fst_pokladna/fst_print.js', 
        '//js/fst_history/fst_history.js', 
		'//js/fst_system/fst_menu.js',   //right click menu
		//'//js/fst_system/fst_system.js',   //global function system
		//'//js/websocket/client.js',   //global function system
		
		'//js/calendar/Picker.js',
		'//js/calendar/Picker.Attach.js',
		'//js/calendar/Picker.Date.js',
		'//js/calendar/Picker.Date.Range.js',
		
		//'//js/html2canvas/jspdf.js',
		'//js/html2canvas/html2canvas.js',
		
        '//js/lang/cz.js',   //language soubor pro JS
        
        '//js/default.js',   //obdoba c_page_load.js
        
        '//js/fst_vars/fst_vars.js', 
    ),
	
    'css' => array(
          //$css1,				
        //'//css/fonts/fonts.css',
        //'//css/fonts/awesome.css',
        //'//css/fonts/fontawesome-all.min.css',
        '//css/ajax.css',
		//'//js/maparea/maparea.css',
		'//js/mbox/assets/mBoxNotice.css',
        '//css/base.css',
        '//css/mixins.css',
        '//css/print_ucet.css',
        '//css/default.css',
        '//css/ollies.css',
		'//css/response.css',
        
		'//js/calendar/datepicker_vista/datepicker_vista.css',
    ),
);
