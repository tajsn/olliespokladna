/*
Fst Push sw.js service worker
created by Jakub Tyson Fastest Solution 
copyright 2016
*/
var debug = true;
if (debug == true) console.log('SW.js Starta', self);

// install service
self.addEventListener('install', function(event) {
  if (self.skipWaiting) { self.skipWaiting(); } // kvuli memory cache
  if (debug == true) console.log('SW.js instalovano', event);
  
});

// activate service
self.addEventListener('activate', function(event) {
  if (debug == true) console.log('SW.js aktivovano', event);
  //console.log(self);
  //self.postMessage('a');

});  
var ENDPOINT = '/send_push/';

// on message
self.addEventListener('message', function(event) {
	if (debug == true) console.log('SW.js message toa');
	self.importScripts('gps.js');
	//console.log(self);
	i = 1;
	var intervalID = setInterval(function(){
		//console.log(i);
		self.postMessage(i);
		i++;
	}, 1000);
	
});



// on click message
self.addEventListener('notificationclick', function(event) {
		//console.log(event);
		//console.log(clients);
		event.notification.close();

		  // This looks to see if the current is already open and
		  // focuses if it is
		  event.waitUntil(clients.matchAll({
			type: "window"
		  }).then(function(clientList) {
			for (var i = 0; i < clientList.length; i++) {
			  var client = clientList[i];
			  url = client.url.split('/');
			  
			  if (url[2] == 'p-pospiech.fastest.cz')
				client.focus();
				client.navigate(client.url);
				return true;
			}
			if (clients.openWindow)      
			  var url = '/driver-orders/';    
			  clients.openWindow(url);
			  return true;
		  }));
});

// on push message
self.addEventListener('push', function(event) {  
  if (debug == true) console.log('SW.js push detect'); 
	// Since there is no payload data with the first version  
  // of push messages, we'll grab some data from  
  // an API and use it to populate a notification  
  
  //return false;
  
	var title = 'Pospiech Transport';  
    var message = 'Zkontrolujte si novou položku';  
    var icon = '/css/layout/favicon.png';  
    //var notificationTag = data.notification.tag;
 	console.log(self.registration);
	return self.registration.showNotification(title, {  
		body: message,
		icon: icon,
		sound: 1,
		vibrate: 1,
		//tag: notificationTag
	});
	/*
		event.waitUntil(clients.matchAll({
			type: "window"
		}).then(function(clientList) {
			for (var i = 0; i < clientList.length; i++) {
			  var client = clientList[i];
			  //console.log(client.url);
			  //if (client.url == 'driver-orders/' && 'focus' in client)
				//return client.focus();
			}
			if (clients.openWindow)
			  return clients.openWindow('/driver-orders/');
		}));
		*/
		/*
		clients.matchAll().then(function(clients) {
			var client = clients.find(c => new URL(c.url).pathname.indexof('/chat/') === 0 && c.type == 'window') || new Client('/chat/');
			console.log(client);
			//console.log(clients);
		});
		*/
		//console.log(clients.matchAll({includeUncontrolled: true, type: 'window'}));
		//clients.openWindow('https://p-pospiech.fastest.cz/driver-orders');
		//clients.focus();
	//});
	//return notification;

  //console.log(event); 
  //console.log(event.data); 
  
  //fetch(ENDPOINT).then(function(response) { 
	//console.log(response);
  //});  
  /*
  event.waitUntil(  
	fetch(ENDPOINT).then(function(response) {  
	console.log('response from ENDPOINT');
	//console.log(response);
      if (response.status !== 200) {  
        // Either show a message to the user explaining the error  
        // or enter a generic message and handle the
        // onnotificationclick event to direct the user to a web page  
        console.log('Looks like there was a problem. Status Code: ' + response.status);  
        throw new Error();  
      }

      // Examine the text in the response  
	
   
	 return response.json().then(function(data) {  
        if (data.error || !data.notification) {  
          console.error('The API returned an error.', data);  
          throw new Error();  
        }  

        var title = data.notification.title;  
        var message = data.notification.message;  
        var icon = data.notification.icon;  
        var sound = data.notification.sound;  
        var vibrate = data.notification.vibrate;  
        var notificationTag = data.notification.tag;

        return notify_bubble = self.registration.showNotification(title, {  
          body: message,  
          icon: icon,  
          vibrate: vibrate,  
          sound: sound,  
          tag: notificationTag  
        });  
		
		notify_bubble.addEventListener('click', function() {
			if (clients.openWindow) {
			  clients.openWindow('https://p-pospiech.fastest.cz');
			}
		});
		return true;
      });  
	  
    }).catch(function(err) {  
      console.error('Unable to retrieve data', err);
      //console.error(err);

      var title = 'An error occurred';
      var message = 'We were unable to get the information for this push message';  
      var icon = 'http://www.fastest.cz/css/fastest/layout/logo.png';  
      var notificationTag = 'notification-error';  
      return self.registration.showNotification(title, {  
          body: message,  
          icon: icon,  
          tag: notificationTag  
        });  
    })  
	
  );
*/  
  
});

// event subscription change
self.addEventListener('pushsubscriptionchange', function(event) {
  if (debug == true) console.log('Subscription expired'); 
	
  event.waitUntil(
    self.registration.pushManager.subscribe({ userVisibleOnly: true })
    .then(function(subscription) {
      console.log('Subscribed after expiration', subscription.endpoint);
      return fetch('register', {
        method: 'post',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          endpoint: subscription.endpoint
        })
      });
    })
  );
});


/*
self.addEventListener('push', function(event) {
  event.waitUntil(
    self.registration.pushManager.getSubscription().then(function(subscription) {
      fetch('/test.html', {
        method: 'post',
        headers: {
          'Authorization': 'Bearer ' + self.token,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(subscription)
      })
      .then(function(response) { return response.json(); })
      .then(function(data) {
        console.log(data);
		self.registration.showNotification(data.title, {
          body: data.body,
          icon: 'favicon-196x196.png'
        });
      })
      .catch(function(err) {
        console.log('err');
        console.log(err);
      });
    })
  );
}); 
*/