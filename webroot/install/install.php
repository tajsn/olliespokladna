<?php 
// 109.123.216.75
// fastestsolution_olliesback
// gltmKQS_5K3
header('Content-Type: text/html; charset=utf-8'); 
function pr($val){
        echo '<pre>';
        print_r($val);
        echo  '</pre>';
}

function checkDevVersion(){
    if($_SERVER['HTTP_HOST'] == 'pokladna.localhost'){
        die('neni povoleno na vyvojove verzi');
    }
    
}

class InstallPokladna {
	//var $configFile = '..'. DIRECTORY_SEPARATOR .'configPokladna.php';
    //$sep = DIRECTORY_SEPARATOR;
	
	//var $configFile = '../'.$sep;    
    public function __construct() {
		//$this->removeFolder();
		$this->configFile = '..'. DIRECTORY_SEPARATOR .'configPokladna.php';
		//$this->configFile = 'webroot'. DIRECTORY_SEPARATOR .'configPokladna.php';
		//$this->configFile = 'webroot'. DIRECTORY_SEPARATOR .'configPokladna.php';
		//$this->configFile = DIRECTORY_SEPARATOR .'configPokladna.php';
		if (!isset($_GET['method'])) $_GET['method'] = '';
        switch($_GET['method']){
            /**
             * check version and update via GIT PULL
             */
            case 'checkVersion' :
                $this->checkVersion();
            break;
            /**
             * GIT CLONE new repository
             */
            case 'newInstall':
                $this->newInstall();
            break;
        
            /**
             * nastaveni config file
             */
            case 'config':
                $this->setConfig();
            break;
        
			case 'setChmod':
                $this->setChmod();
            break;
            
		   
            
            default:
        
        }
        echo '<style>
        body {
            font-family:arial;
            font-size:12px;
        }
        .alert {
            color:#fff;
            background:#ff0000;
            padding:5px;
            margin:10px auto;
            display:block;
            text-align:center;
        }
        label {
            display:inline-block;
            width:20%;
            margin:2px;
            padding:2px;
        }
        .sll,.slr {
            display:inline-block;
            vertical-align:top;
            width:49%;
        }
        input.text {
            width:50%;
            margin:2px;
            border:1px solid #ccc;
            padding:2px;
        }
        fieldset {
            border:1px solid #ccc;
        }
        .log {
            padding:5px;
            font-family:times;
            border:1px solid #ccc;
        }
        </style>';
        echo '
        <ul>
            <li><a href="install.php?method=newInstall">Nová instalace z GIT (git clone)</li>
            <li><a href="install.php?method=config">Vytvoření konfiguračního souboru</li>
            <li><a href="install.php?method=checkVersion">Kontrola aktuálni verze z GIT (git pull)</li>
            <li><a href="index.php">Index</li>
        </ul>
        ';
        
        
    }

    
   
	public function setChmod(){
		//chmod -R 777
		$script = "chmod -R 777";
        exec($script." 2>&1",$out);
        $msg = (isset($out[0])?$out[0]:'Chyba chmod ');
		die('end');
	}

	// prevod do jineho adresare po rozbaleni z git
	public function removeFolder(){
		$files = scandir("olliespokladna");
		$oldfolder = "olliespokladna/";
		$newfolder = "";
		  foreach($files as $fname) {
			  if($fname != '.' && $fname != '..') {
				  rename($oldfolder.$fname, $newfolder.$fname);
			  }
		  }
	}


    /**
     * check version and update
     */
    public function checkVersion(){
        if (file_exists($this->configFile)){
            require $this->configFile;
            $fileLoad = true;
        }
        checkDevVersion();
        
        $script = "git pull ".configGitRepository;
        exec($script." 2>&1",$out);
        $msg = (isset($out[0])?$out[0]:'Chyba check version ');
        
        if ($msg == 'Already up-to-date.'){
            $msg = 'Máte aktuální verzi';
        }
        
        echo '<strong class="alert">'.$msg.'</strong>';
        //echo $message;
    }

    // nova instalace
    public function newInstall(){
        if (!empty($_POST)){
            
            //exec("git clone https://tajsn@bitbucket.org/Fastest_cz/pokladna_angular4.git test");
            $out = shell_exec("git init  2>&1");
            $out = shell_exec('git pull '.$_POST['git_rep_clone'] ." 2>&1");
            pr($out);
			echo '<p class="log">'.$out[0].'</p>';
            $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
            $result = 'GIT LOADED '.round($time,2).'s';
            $this->removeFolder();
            echo '<strong class="alert">'.$result.'</strong>';
        } else {
            echo '<form method="post">';
            echo '<fieldset>';    
                echo '<legend>Nastavení</legend>';
                echo '<div class="sll">';
                    echo '<label>GIT repository: </label><input type="text" name="git_rep_clone" class="text" /><br />';
					echo 'https://tajsn@bitbucket.org/tajsn/olliespokladna.git';
                echo '</div>';
                echo '<div class="slr">';
                echo '</div>';
            echo '</fieldset>';   
    
           
            echo '<input type="submit" value="Stáhnout z GIT" />';
        echo '</form>';
        }
    }

    // nastaveni config    
    public function setConfig(){
            
        if (!empty($_POST)){
            //print_r($_POST);
            $file_data = "<?php\n";
            $file_data .="define(\"configSystemId\",\"".$_POST['system_id']."\");\n";
            $file_data .="define(\"configTerminalId\",\"".$_POST['terminal_id']."\");\n";
            $file_data .="define(\"configSqlServer\",\"".$_POST['sql_server']."\");\n";
            $file_data .="define(\"configSqlUser\",\"".$_POST['sql_user']."\");\n";
            $file_data .="define(\"configSqlPass\",\"".$_POST['sql_pass']."\");\n";
            $file_data .="define(\"configSqlDb\",\"".$_POST['sql_db']."\");\n";
            $file_data .="define(\"configSqlPort\",\"".$_POST['sql_port']."\");\n";
            $file_data .="define(\"configSqlUser2\",\"".$_POST['sql_user2']."\");\n";
            $file_data .="define(\"configSqlPass2\",\"".$_POST['sql_pass2']."\");\n";
            $file_data .="define(\"configSqlDb2\",\"".$_POST['sql_db2']."\");\n";
            $file_data .="define(\"configFtpServer\",\"".$_POST['ftp_server']."\");\n";
            $file_data .="define(\"configFtpUser\",\"".$_POST['ftp_user']."\");\n";
            $file_data .="define(\"configFtpPass\",\"".$_POST['ftp_pass']."\");\n";
            $file_data .="define(\"configFtpFolder\",\"".$_POST['ftp_folder']."\");\n";
            $file_data .="define(\"configGitRepository\",\"".$_POST['git_repository']."\");\n";
            
            $file_data .= "?>";
            
            file_put_contents($this->configFile, $file_data);
            echo '<strong class="alert">Uloženo</strong>';
            } 
            //else {
				//pr($this->configFile);
                if (file_exists($this->configFile)){
                    require $this->configFile;
                    $fileLoad = true;
                }
                
                echo '<form method="post">';
					echo 'https://tajsn@bitbucket.org/tajsn/olliespokladna.git<br />';
					echo '109.123.216.75<br />';
					echo 'fastestsolution_olliesback<br />';
					echo 'gltmKQS_5K3<br />';
                    echo '<fieldset>';    
                        echo '<legend>Nastavení</legend>';
                        echo '<div class="sll">';
                            echo '<label>Pokladna ID: </label><input type="text" name="system_id" class="text" value="'.(defined("configSystemId")?configSystemId:'').'" /><br />';
                            echo '<label>Terminal ID: </label><input type="text" name="terminal_id" class="text" value="'.(defined("configTerminalId")?configTerminalId:'').'" /><br />';
                            //echo '<label>Uživatel: </label><input type="text" name="ftp_user" value="'.(defined("configFtpUser")?configFtpUser:'').'"/><br />';
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>GIT repozitař produkční: </label><input type="text" name="git_repository" class="text" value="'.(defined("configGitRepository")?configGitRepository:'').'"/><br />';
                        echo '</div>';
                    echo '</fieldset>';   
            
                    echo '<fieldset>';    
                        echo '<legend>FTP</legend>';
                        echo '<div class="sll">';
                            echo '<label>Server: </label><input type="text" name="ftp_server"  class="text"value="'.(defined("configFtpServer")?configFtpServer:'').'" /><br />';
                            echo '<label>Uživatel: </label><input type="text" name="ftp_user" class="text" value="'.(defined("configFtpUser")?configFtpUser:'').'"/><br />';
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>Heslo: </label><input type="text" name="ftp_pass"  class="text" value="'.(defined("configFtpPass")?configFtpPass:'').'"/><br />';
                            echo '<label>Složka: </label><input type="text" name="ftp_folder"  class="text" value="'.(defined("configFtpFolder")?configFtpFolder:'').'"/><br />';
                        echo '</div>';
                    echo '</fieldset>';    
                    
                    echo '<fieldset>';    
                        echo '<legend>Mysql</legend>';
                        echo '<div class="sll">';
                            echo '<label>Server: </label><input type="text" name="sql_server"  class="text" value="'.(defined("configSqlServer")?configSqlServer:'').'" /><br />';
                            echo '<label>Uživatel: </label><input type="text" name="sql_user"  class="text" value="'.(defined("configSqlUser")?configSqlUser:'').'"/><br />';
                            echo '<label>Port: </label><input type="text" name="sql_port"  class="text" value="'.(defined("configSqlPort")?configSqlPort:'').'"/>3311 / 3306<br />';
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>Heslo: </label><input type="text" name="sql_pass" class="text" value="'.(defined("configSqlPass")?configSqlPass:'').'"/><br />';
                            echo '<label>DB: </label><input type="text" name="sql_db"  class="text" value="'.(defined("configSqlDb")?configSqlDb:'').'"/><br />';
                        echo '</div>';
                    echo '</fieldset>';    
                    echo '<fieldset>';    
                        echo '<legend>Mysql OLD</legend>';
                        echo '<div class="sll">';
                            echo '<label>Uživatel: </label><input type="text" name="sql_user2"  class="text" value="'.(defined("configSqlUser2")?configSqlUser2:'').'"/><br />';
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>Heslo: </label><input type="text" name="sql_pass2" class="text" value="'.(defined("configSqlPass2")?configSqlPass2:'').'"/><br />';
                            echo '<label>DB: </label><input type="text" name="sql_db2"  class="text" value="'.(defined("configSqlDb2")?configSqlDb2:'').'"/><br />';
                        echo '</div>';
                    echo '</fieldset>';    
                    echo '<input type="submit" value="Uložit konfiguraci" />';
                echo '</form>';
            
        
        }
    
}
$InstallPokladna = new InstallPokladna();

?>