
//var WebSocketServer = require('websocket').server;
//if (typeof window == 'undefined') window = {}
var http = require('http');
var Moo = require('mootools');
var os = require("os");
var fs = require("fs");
var node_printer = require("printer");
var WebSocketServer = require('websocket').server;
var exec = require('child_process').exec,child;

var printer = require("printer");
var Printjob = require('./lib/printjob');
var Image = require('./lib/image');
var PNG = require('node-png').PNG;		
var removeDiacritics = require('diacritics').remove;	

var FstPokladnaServer = new Class({
    
	domain: 'http://'+os.hostname(),
	//domain: '127.0.0.1',
	logo_file: 'logo.png',
	
	Implements: [process.EventEmitter],
	
	socket_clients : {}, 
	connectionIDCounter : [],
	
	initialize: function(options){
		this.create_server();
		//var printers_list = node_printer.getPrinters();
		//console.log(printers_list);
		//this.print_file();
		this.create_websocket_server();
		//this.print_data();
		
		
		
	},
	/** PRICE FORMAT */
	price: function(price,params){
			if (!params){
			params = {
				'symbol_before':'',
				'kurz':'1',
				'count':'1',
				'decimal': 2,
				'symbol_after':',-'
			}
			}
			price = (price/params.kurz)* params.count;		
			return Number(price.toFixed(2)).toLocaleString();
			//return params.symbol_before + number_format(price, params.decimal, '.', ' ') + params.symbol_after;	
	},
	
	print_data: function(data){
		if (!data){
			data = {};
			data.printer_name = 'PRP-300';
			data.file_data = {0:{}};
		}
		//console.log(data);
		String.prototype.toBytes = function() {
			var arr = []
			for (var i=0; i < this.length; i++) {
				arr.push(this[i].charCodeAt(0))
			}
			return arr;
		}
		
		stream = fs.createReadStream(__dirname + '/'+this.logo_file).pipe(new PNG({
			  filterType: 4
		})).on('parsed', function(logo) {
			//Object.each(data.file_data,function(ucet){
				//console.log(ucet);
				this.print_template(stream,data.file_data,data.printer_name); 
			
			//}.bind(this));
			//console.log(stream);
		}.bind(this));	
	},
	
	// print template
	print_template: function(stream,print_data,printer_name){
		
		print_data_job = new Printjob()
			
			//.setTextAlignment('center').text('Fastest!').newLine(1)
			.setTextAlignment('center').imageAdd(stream).newLine(2)
			.setTextAlignment('left').text('Test')
			.newLine(1)
			.setBold(true)
			
			;
		print_data_job
			
			.text('Produkt',20)
			.text('Cena',8)
			
			.text('Ks',3)
			
			.text('Celkem',8).newLine(1)
			
			.separator().newLine(1)
			.setBold(false)
			;
				
		
		
		//console.log(print_data);return false;
		if (print_data && Object.getLength(print_data)>0){
			
			print_data.order_items.each(function(item){
				//name = removeDiacritics(item.name); 
				print_data_job
				.text(item.name,20)
				.text(this.price(item.price),8,'price')
				.text(item.count+'x',3)
				.text(this.price(item.row_price),8,'price')
				.newLine(1);
				;
			}.bind(this));
		
			print_data_job
				.newLine(2)
				//.fontSize()
		
				.setBold(true)
				.setTextAlignment('right')
				.text('Celkem: ')
				.text(print_data.total_price,0,'price')
				.newLine(1);
		}
		
		
		print_data_job
			.newLine(6)
			.cut();
		
		//console.log(print_data_job.printData());	
		buf2 = print_data_job.printData();
		//printer_name = 'PRP-301';
		console.log(printer_name);
		console.log(print_data);
		console.log(print_data_job);
		//return false;
		//console.log(buf2.toString());
		return false;	
		printer.printDirect({
			data: print_data_job.printData()
			//, printer:'PRP-300'
			, printer:printer_name
			, type: 'RAW'
			, success:function(jobID){
				console.log("sent to printer with ID: "+jobID);
			}
			, error:function(err){console.log(err);}
		});	
	},
	
	
	print_file: function(){
		String.prototype.toBytes = function() {
			var arr = []
			for (var i=0; i < this.length; i++) {
				arr.push(this[i].charCodeAt(0))
			}
			return arr;
		}

		
		//var printDataTxt = "Hello world!".toBytes().concat([0x1B, 0x64, 10, 0x1d, 0x56, 0x00]);
		
		
		
		fs.createReadStream(__dirname + '/'+this.logo_file).pipe(new PNG({
			  filterType: 4
		})).on('parsed', function(logo) {
			//console.log(logo);
			print_data_job = new Printjob()
				.newLine(1)
				.setTextAlignment('center').text('Testovací diakritika ešcržýáíé ŠCRŽÝÁÍÉ').newLine(1)
				//.setTextAlignment('center').imageAdd(this).newLine(5)
				
				//.setBold().text('test!').newLine(5)
				
				
				

			;
			/*
			print_data_job
				.setTextAlignment('left')
				.text('produkt').setTab().text('cena').setTab().text('cena celkem').newLine(1)
				.separator().newLine(1)
				.setBold(false);
			
			print_data_job
				.text('test').setTab().text('100,-').setTab().text('200,-').newLine(1)
				.text('test2').setTab().text('200,-').setTab().text('300,-').newLine(1)
			*/
			print_data_job
				.newLine(6)
				.cut();
			
			//console.log(print_data_job.printData());return false;
			
			printer.printDirect({
				//data: new Buffer(printDataTxt)
				//data: 'a'
				data: print_data_job.printData()
				, printer:'PRP-300'
				, type: 'RAW'
				, success:function(jobID){
					console.log("sent to printer with ID: "+jobID);
				}
				, error:function(err){console.log(err);}
			});
			
		});
		
	},
	
	
	// receive data from client
	socket_receive: function(connection,message){
		if (message.type === 'utf8') {
			console.log('Received Message: ' + message.utf8Data);
			message = message.utf8Data;
			if (message){
				var mdata = JSON.parse(message);
						
				// pokud je tisk
				if (mdata.type == 'print'){
					this.print_data(mdata);
				}
						
				// get printer list
				if (mdata.type == 'get_printer'){
					this.get_printer(mdata.system_id);
				}
				
				// init connection list
				if (mdata.type == 'init'){
					system_id = mdata.system_id;
							
					if (!this.connectionIDCounter[system_id]) this.connectionIDCounter[system_id] = 0;
					if (!this.connections[system_id]) {
						this.connections[system_id] = {
							'system_id':system_id,
							'id':null,
							'pokladna':{}
						}
					}
					connection.id = this.connectionIDCounter[system_id] ++;
					this.connections[system_id]['pokladna'][connection.id] = connection;
					//console.log('id',connection.id);
					
					this.create_socket_client(system_id,connection.id,this.connections);
					console.log('count websocket clients system ID'+system_id+': '+Object.keys(this.socket_clients[system_id]).length+' ks');
							
				}
						
			} 
		} else if (message.type === 'binary') {
			//console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
			//connection.sendBytes(message.binaryData);
		}
	},
	
	
	// create socket client before send to CLIENT.JS
	create_socket_client: function(system_id,connection_id,connections){
		connections = connections[system_id];
		system_id = system_id.toInt();
		
		if (!this.socket_clients[system_id]) this.socket_clients[system_id] = {};
		
		this.socket_clients[system_id] = {
			connection_id : {
				'id':connection_id
			}
		
		};
		
		
		
	},
	
	// get printer list from PC
	get_printer: function(system_id){
		
		var printers_list = node_printer.getPrinters();
		var printers_name = [];
		
		printers_list.each(function(printer){
			printers_name.push(printer.name);
		}.bind(this));
		
		this.printers_name = printers_name;
		if (system_id) {
			Object.each(this.connections[system_id]['pokladna'],(function(cc,k){
				this.connections[system_id]['pokladna'][k].send(JSON.stringify({'printer_list':printers_name}));
			}).bind(this));
		}
		
	},
	
	
	// create websocket server
	create_websocket_server: function(){
		this.domain = 'localhost';
		var server = http.createServer(function(request, response) {
			console.log((new Date()) + ' Received request for ' + request.url);
			response.writeHead(404); 
			response.end();
		});
		server.listen(8080, function() {
			console.log((new Date()) + ' Server is domain '+this.domain);
			console.log((new Date()) + ' Server is listening on port 8080');
		}.bind(this));

		wsServer = new WebSocketServer({ httpServer: server });

		
		function originIsAllowed(origin) {
		  // put logic here to detect whether the specified origin is allowed.
		  return true;
		}

		this.connections = {};
		
		
		var _this = this;
		
		wsServer.on('request', (function(request) {
			
			if (!originIsAllowed(request.origin)) {
			  // Make sure we only accept requests from an allowed origin
			  request.reject();
			  console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
			  return;
			}
			
			var connection = request.accept(null, request.origin);

			connection.on('message', (function(message) {
				//console.log(message);
				this.socket_receive(connection,message);
			}).bind(this));
			
			
			
			connection.on('close', (function(reasonCode, description) {
				console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected. ' + "Connection ID: " + connection.id);

				// Make sure to remove closed connections from the global pool
					/*
					if (connections[system_id] && this.pokladnas[system_id][connection.id]){
					
						delete connections[system_id];
						delete (this.pokladnas[system_id][connection.id]);
						
						//this.update_pocet_orders(connections,system_id,count_orders);
						
						console.log('pocet klientu pokladny '+system_id+': '+Object.keys(this.pokladnas[system_id]).length);	
					}
					*/
				
				
			}).bind(this));
			
		}).bind(this));
	},
	
	// create server
	create_server: function(){
				
		var http = require("http");
		var server = http.createServer(function(request, response) {
		  response.writeHead(200, {"Content-Type": "text/html"});
		  response.write("<!DOCTYPE \"html\">");
		  response.write("<html>");
		  response.write("<head>");
		  response.write("</head>");
		  response.write("<body>");
		  response.write("</body>");
		  response.write("</html>");
		  response.end();
		});

		server.listen(80);
		console.log("Server is listening");
	}
	
	
});
var pokladna_server = new FstPokladnaServer();	

