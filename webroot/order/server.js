//var WebSocketServer = require('websocket').server;
//if (typeof window == 'undefined') window = {}
var http = require('http');
var WebSocketServer = require('websocket').server;
var Moo = require('mootools');
var os = require("os");
var fs = require("fs");
var node_printer = require("printer");
var exec = require('child_process').exec,child;
			

var FstPokladnaServer = new Class({
    
	domain: 'http://'+os.hostname(),
	//domain: '127.0.0.1',
	
	
	Implements: [process.EventEmitter],
	
	pokladnas : [], 
	connectionIDCounter : [],
	
	initialize: function(options){
		//console.log(window);
		this.get_printer();
		
		this.create_server();
		
		var css = fs.readFileSync('print.css').toString();
		//console.log(css);
		
		
	},
	
	// print in windows
	print_windows: function(printer_name,data){
		/*
		var base64Data = data.replace(/^data:image\/png;base64,/, "");
		
		fs.writeFile("print.png", base64Data, 'base64',function(err) {
			//console.log('save png to disk');
		});
		*/
		//var html = '<img src="print.png">';
		var css = fs.readFileSync('print.css').toString();
		//var html = "<style>body{font-family:arial;}.none{display:none;}#print_ucet{width:58mm;border:1px solid red;position:relative;font-size:12px}.header,.footer{background:none;color:#000;text-align:center;font-size:12px;line-height:14px}label{display:inline-block;width:33%;padding:0;margin:0;float:none;line-height:13px;font-size:12px;}span{font-size:12px}table{width:100%}tr th,tr td{padding:2px;background:#fff;font-size:12px;border:0;text-align:left;}tr.total tr th{font-weight:700}.copyright{font-size:6px}</style>";
		var html = "<style>"+css+"</style>";
		//var html = '<link href="print.css" rel="stylesheet" type="text/css" /';
		html += data;
		//console.log(html);
		
		//console.log(html);
		fs.writeFile("print.html", html, function(err) {
			if(err) {
				return console.log(err);
			}

			 //console.log("The file was saved!");
			
			//child = exec('printhtml.exe html="ahoj<strong>aaa</strong><img src="'+data+'">" printername="'+printer_name+'" header="" footer="" leftmargin=0 rightmargin=0 topmargin=0 bottommargin=0',
			child = exec('printhtml.exe file="print.html" printername="'+printer_name+'" header="" footer="" leftmargin=0.2 rightmargin=0.2 topmargin=0.2 bottommargin=0.2',
			  function (error, stdout, stderr) {
				//console.log('stdout: ' + stdout);
				//console.log('stderr: ' + stderr);
				if (error !== null) {
				  //console.log('exec error: ' + error);
				}
			});
			
		});
		
	},
	
	// create server
	create_server: function(){
		var server = http.createServer(function(request, response) {
			console.log((new Date()) + ' Received request for ' + request.url);
			response.writeHead(404); 
			response.end();
		});
		server.listen(9998, function() {
			console.log((new Date()) + ' Server is domain '+this.domain);
			console.log((new Date()) + ' Server is listening on port 9998');
		}.bind(this));

		wsServer = new WebSocketServer({ httpServer: server });

		
		function originIsAllowed(origin) {
		  // put logic here to detect whether the specified origin is allowed.
		  return true;
		}

		var connections = {};
		
		
		var _this = this;
		
		wsServer.on('request', (function(request) {
			
			if (!originIsAllowed(request.origin)) {
			  // Make sure we only accept requests from an allowed origin
			  request.reject();
			  console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
			  return;
			}
			
			var connection = request.accept(null, request.origin);

				connection.on('message', (function(message) {
				//console.log(message);
				if (message.type === 'utf8') {
					//console.log('Received Message: ' + message.utf8Data);
					message = message.utf8Data;
					
					if (message){
						var mdata = JSON.parse(message);
						
						// pokud je tisk
						if (mdata.type == 'print'){
							this.send_to_printer(mdata);
						}
						
						// get printer list
						if (mdata.type == 'get_printer'){
							this.get_printer(mdata.provoz_id);
						}
						
						if (mdata.type == 'init'){
							provoz_id = mdata.provoz_id;
							
							if (!this.connectionIDCounter[provoz_id]) this.connectionIDCounter[provoz_id] = 0;
							if (!connections[provoz_id]) connections[provoz_id] = {}
							if (!connections[provoz_id]['pokladna']) connections[provoz_id]['pokladna'] = {}
							connection.id = this.connectionIDCounter[provoz_id] ++;
							connections[provoz_id]['pokladna'][connection.id] = connection;
							this.con_pokladna = connections;
							//console.log(connection);
							this.create_pokladnas(message,connections,connection.id,provoz_id);
							console.log('pocet pokladen '+provoz_id+': '+Object.keys(this.pokladnas[provoz_id]).length);
							
						}
						
					} 
				} else if (message.type === 'binary') {
					//console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
					//connection.sendBytes(message.binaryData);
				}
			
			}).bind(this));
			
			
			connection.on('close', (function(reasonCode, description) {
				console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected. ' + "Connection ID: " + connection.id);

				// Make sure to remove closed connections from the global pool
					/*
					if (connections[provoz_id] && this.pokladnas[provoz_id][connection.id]){
					
						delete connections[provoz_id];
						delete (this.pokladnas[provoz_id][connection.id]);
						
						//this.update_pocet_orders(connections,provoz_id,count_orders);
						
						console.log('pocet klientu pokladny '+provoz_id+': '+Object.keys(this.pokladnas[provoz_id]).length);	
					}
					*/
				
				
			}).bind(this));
		}).bind(this));
	},
	
	send_to_printer: function(data){
		console.log('send_to_printer '+data.printer_name);
		
		//printer_name = 'PDF reDirect v2';
		
		this.print_windows(data.printer_name,data.file_data);
		//this.print_file(data);
		//console.log(data);
	},
	
	get_printer: function(provoz_id){
		//this.printer_class = node_printer;
		
		var printers_list = node_printer.getPrinters();
		var printers_name = [];
		
		printers_list.each(function(printer){
			printers_name.push(printer.name);
		}.bind(this));
		
		//console.log(provoz_id);
		//console.log(printers_name);
		this.printers_name = printers_name;
		if (provoz_id) {
			Object.each(this.con_pokladna[provoz_id]['pokladna'],(function(cc,k){
				//console.log(this.con_pokladna[provoz_id]['pokladna'][k]);
				this.con_pokladna[provoz_id]['pokladna'][k].send(JSON.stringify({'printer_list':printers_name}));
			}).bind(this));
		}
		//console.log(printers_name);
		//console.log(this.printer_class.getSupportedPrintFormats());
		
		//this.con_pokladna[provoz_id]['pokladna'][k].send(JSON.stringify({'open_win':true,'mobile':phone}));
		//console.log("supported formats are:\n"+this.printer_class.getSupportedPrintFormats());
		//this.print_file();
	},
	/*
	print_file_old_version: function(data){
		var file = data.file;	
		if( process.platform != 'win32') {
		  printer.printFile({filename:filename,
			printer: process.env[3], // printer name, if missing then will print to default printer
			success:function(jobID){
			  console.log("sent to printer with ID: "+jobID);
			},
			error:function(err){
			  console.log(err);
			}
		  });
		} else {
			
		}
	},
	*/
	
	
	// create cisniks before send to CLIENT.JS
	create_pokladnas: function(message,connections,connection_pokladna_id,provoz_id){
		//console.log(connection_id);
		if (!this.pokladnas[provoz_id]) this.pokladnas[provoz_id] = {};
		
		this.pokladnas[provoz_id][connection_pokladna_id] = {'id':connection_pokladna_id};
		
		
		
	},
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// create server
	create_server2: function(){
		
		var server = http.createServer(function(request, response) {
			console.log((new Date()) + ' Received request for ' + request.url);
			response.writeHead(404); 
			response.end();
		});
		server.listen(9998, function() {
			console.log((new Date()) + ' Server is listening on port 9998');
		});

	wsServer = new WebSocketServer({ httpServer: server });

		function originIsAllowed(origin) {
		  // put logic here to detect whether the specified origin is allowed.
		  return true;
		}

		var connections = {};
		
		
		var _this = this;
		
		wsServer.on('request', (function(request) {
			if (!originIsAllowed(request.origin)) {
			  // Make sure we only accept requests from an allowed origin
			  request.reject();
			  console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
			  return;
			}
			
			var connection = request.accept(null, request.origin);

			connection.on('message', (function(message) {
				if (message.type === 'utf8') {
					console.log('Received Message: ' + message.utf8Data);
					message = message.utf8Data;
					
					if (message){
						var message = JSON.parse(message);
						provoz_id = message.provoz_id;
						type = message.type;
						if (message.pokladna){
							
							if (!this.connectionIDCounter[provoz_id]) this.connectionIDCounter[provoz_id] = 0;
							if (!connections[provoz_id]) connections[provoz_id] = {}
							if (!connections[provoz_id]['pokladna']) connections[provoz_id]['pokladna'] = {}
							connection.id = this.connectionIDCounter[provoz_id] ++;
							connections[provoz_id]['pokladna'][connection.id] = connection;
							this.con_pokladna = connections;
							console.log(connection);
							this.create_pokladnas(message,connections,connection.id,provoz_id);
							console.log('pocet pokladen '+provoz_id+': '+Object.keys(this.pokladnas[provoz_id]).length);
							
						}
						if (message.type == 'hang'){
							if (this.con_pokladna && Object.keys(this.pokladnas[provoz_id]).length > 0)
							this.send_phone(connections,provoz_id,message.data)
						}
						
					} 
					
					
					
					
				}
				else if (message.type === 'binary') {
					console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
					//connection.sendBytes(message.binaryData);
				}
			
			}).bind(this));
			
			
			connection.on('close', (function(reasonCode, description) {
				console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected. ' +
							"Connection ID: " + connection.id);

				// Make sure to remove closed connections from the global pool
					if (connections[provoz_id] && this.pokladnas[provoz_id][connection.id]){
					
						delete connections[provoz_id];
						delete (this.pokladnas[provoz_id][connection.id]);
						
						//this.update_pocet_orders(connections,provoz_id,count_orders);
						
						console.log('pocet klientu pokladny '+provoz_id+': '+Object.keys(this.pokladnas[provoz_id]).length);	
					}
				
				
			}).bind(this));
		}).bind(this));
	},
	
	
	
	
	// send to CLIENT.JS call obsluha
	send_phone: function(connections,provoz_id,phone){
		
		if (this.con_pokladna[provoz_id]['pokladna'])
		Object.each(this.con_pokladna[provoz_id]['pokladna'],(function(cc,k){
				
				if (this.con_pokladna[provoz_id]['pokladna'][k]){ 
					this.con_pokladna[provoz_id]['pokladna'][k].send(JSON.stringify({'open_win':true,'mobile':phone}));
					console.log('open win');
				}
				
		}).bind(this));	 
				
		
	},
	
	
	
	
});
var pokladna_server = new FstPokladnaServer();	


// Broadcast to all open connections
function broadcast(data) {
    Object.keys(connections).forEach(function(key) {
        var connection = connections[key];
        if (connection.connected) {
            connection.send(data);
        }
    });
}

// Send a message to a connection by its connectionID
function sendToConnectionId(connectionID, data) {
    var connection = connections[connectionID];
    if (connection && connection.connected) {
        connection.send(data);
    }
}