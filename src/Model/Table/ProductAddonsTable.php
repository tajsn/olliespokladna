<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Cache\Cache;

use Cake\Network\Session;

class ProductAddonsTable extends Table{

	public function initialize(array $config){
		parent::initialize($config);
		$this->addBehavior('Timestamp');
	}
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//pr($query->_repository);
		$query->where([$event->subject()->alias().'.system_id' => $system_id]);
	}
 

	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
	  
		return $event;
	}
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			
		;
		return $validator;
	}
  
	
  
}
