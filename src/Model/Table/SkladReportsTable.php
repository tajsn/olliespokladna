<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;


class SkladReportsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->addBehavior('Timestamp');
	$this->belongsTo('Cars');
	$this->belongsTo('Drivers');
  }
	
	
	
	
	
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['SkladReports.system_id' => $system_id]);
		//pr($event);
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		
		return $event;
	}
  
  public function validationDefault(Validator $validator){
    
	$validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
		//->requirePresence('jmeno', 'create',   __("Musíte vyplnit jméno"))
		->notEmpty('jmeno',__("Musíte vyplnit jméno"))
		
		//->requirePresence('prijmeni', 'create',   __("Musíte vyplnit príjmení"))
		->notEmpty('prijmeni',__("Musíte vyplnit príjmení"))
		
		//->requirePresence('number', 'create',   __("Musíte vyplnit císlo"))
		->notEmpty('number',__("Musíte vyplnit císlo"))
		->add("number", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Toto císlo již nekdo používá")])

		
	;	

    return $validator;
  }
  
}
