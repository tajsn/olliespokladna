<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Users Model
 *
 */
class ProjectsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
    $this->table('projects');
  }

  public function findProjects($query, $options = null)
  {
    return $query->join(
      [
        "table" => "group_users",
        "alias" => "GroupUsers",
        'type' => 'left',
        'conditions' => 'Projects.group_id = GroupUsers.group_id'
      ]

    )
      ->orwhere(["Projects.user_id" => $options["user_id"]])
      ->orwhere(["GroupUsers.user_id" => $options["user_id"]]);
  }
}
