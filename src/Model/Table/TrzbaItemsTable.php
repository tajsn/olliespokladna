<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Utility\Hash;

use Cake\Network\Session;

class TrzbaItemsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->table('order_items');
	$this->addBehavior('Timestamp');
	$this->belongsTo("Products");
  }
  
	public function beforeFind($event, $query, $options, $primary){
		
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['TrzbaItems.system_id' => $system_id]);
		
		//pr($event->data);
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		//$user_id = $session->read('System.user_id');
		//$event->data['entity']["user_id"] = $user_id;
		
		//pr($event->data['entity']);die();
		
		return $event;
	}
	
	
	public function findIds($order_id){
		$OrderItems = TableRegistry::get("OrderItems");
		$ids = $OrderItems->find()
		->select([
			'id',
			'product_id',
		])
		->where(['order_id'=>$order_id,'kos'=>0,'done'=>0])
		->combine('product_id','id')
		->toArray();
		//pr($order_id);
		
		$order_ids = [];
		//foreach($ids AS $k=>$i){
			//$order_ids[] = (($i==1)?'d':'').$k;
			//$order_ids[$] = (($i==1)?'d':'').$k;
		//}
		$order_ids = $ids;
		
		//pr($order_ids);
		return($order_ids);	
	}
	
	public function validationDefault(Validator $validator){
    
	$validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
		//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
		//->notEmpty('name',__("Musíte vyplnit název"))
		;
    return $validator;
  }

  
}
