<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Cache\Cache;

use Cake\Network\Session;

class TableGroupsTable extends Table{

	public function initialize(array $config){
		parent::initialize($config);
		$this->addBehavior('Timestamp');
	}
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//pr($query->_repository);
		$query->where([$event->subject()->alias().'.system_id' => $system_id]);
	}
 

	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
	  
		return $event;
	}
	
	
	public function listGroupAll(){
		if (($table_group_list = Cache::read('table_group_list')) === false) {
				
			$table_group_list = $this->find('list',['keyField' => 'id','valueField' => 'name'])
			->toArray();
			Cache::write('table_group_list', $table_group_list);
		}
		return($table_group_list);	
	}
	
	
	
  
}
