<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

use Cake\Network\Session;

class ProductsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->addBehavior('Timestamp');
	
	$this->hasMany('ProductRecepturas');
	$this->hasMany('ProductConnAddons');
	$this->hasMany('ProductConnects');
  }
  
	public function beforeFind($event, $query, $options, $primary){
		
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		
		return $event;
	}
	
	public function productList(){
		$query = $this->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'
		])
		->where(['kos'=>0])
		->order('name ASC');
		$data = $query->toArray();
		//pr($data);
		return $data;
			
	}
	
	public function groupList(){
		$ProductConnects = TableRegistry::get("ProductConnects");
		$query = $ProductConnects->find('list', [
			'keyField' => 'product_id',
			'valueField' => 'product_group_id'
		]);
		$data = $query->toArray();
		//pr($data);
		return $data;
			
	}
	
	public function ListAll(){
		$Products = TableRegistry::get("Products");
		//if (($product_list = Cache::read('product_list')) === false) {
				
			$product_list_load = $Products->find()
			->select([
				'id',
				'name',
				'price',
				'price_tax_id',
				//'product_group_id',
				'num',
				'code',
			])
			->where(['kos'=>0])
			->autoFields(false)
			->contain(['ProductConnects' => function($q) {
					return $q->select([
						'ProductConnects.id',
						'ProductConnects.product_id',
						'ProductConnects.product_group_id',
						'ProductConnects.price',
						'ProductConnects.price2',
						'ProductConnects.price3',
						'ProductConnects.price4',
					]);
				}])
			->contain(['ProductConnAddons' => function($q) {
					return $q->select([
						'ProductConnAddons.id',
						'ProductConnAddons.product_id',
						//'ProductConnAddons.product_addon_id',
					])->contain([
						'ProductAddons' => function($q) {
							return $q
								->select([
									'ProductAddons.id', 'ProductAddons.name','ProductAddons.price',
								])
								->where([
									'kos = 0',
								]);
						}
					]);
				}]
			)
			->hydrate(false)
			->toArray();
			
			//pr($product_list_load);die();
			
			$product_list = [];
			if (count($product_list_load)>0){
				
				foreach($product_list_load AS $k=>$p){
					//$product_list[$p['product_group_id']][] = $p;
					if (isset($p['product_connects']) && !empty($p['product_connects'])){
						foreach($p['product_connects'] AS $pc){
							$p['price'] = $pc['price'];
							$p['price2'] = $pc['price2'];
							$p['price3'] = $pc['price3'];
							$p['price4'] = $pc['price4'];
							if ($pc['product_id'] == 9){
							//pr($pc);
							//pr($p);	
							}
							$product_list[$pc['product_group_id']][] = $p;
						}
					}
				}
			}
			Cache::write('product_list', $product_list);
				
		//}
		
		//pr($product_list);die();
		return($product_list);	
	}
	
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			
			->notEmpty('code',__("Musíte vyplnit kód"))
			->notEmpty('price',__("Musíte vyplnit cenu"))
			
		;
		return $validator;
	}

  
}
