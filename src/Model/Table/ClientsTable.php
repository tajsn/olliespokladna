<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;


class ClientsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	
	$this->addBehavior('Timestamp');
    //$this->hasOne("Projects");
    //$this->belongsTo("Users");
    $this->table('clients');
  }
 
  
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['Clients.system_id' => $system_id]);
	}
 
  public function beforeSave($event){
    $session = new Session();
	$system_id = $session->read('System.system_id');
	$event->data['entity']["system_id"] = $system_id;
	
	if (isset($event->data['entity']["prijmeni"])){
		$event->data['entity']["name"] = $event->data['entity']["prijmeni"].' '.$event->data['entity']["jmeno"];
	}
    
    return $event;
  }
  
 

  public function validationDefault(Validator $validator){
    
	$validator
      //->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
		->requirePresence('jmeno', 'create',   __("Musíte vyplnit jméno zákazníka"))
		->notEmpty('jmeno',__("Musíte vyplnit jméno zákazníka"))
		
		->requirePresence('prijmeni', 'create',   __("Musíte vyplnit příjmení zákazníka"))
		->notEmpty('prijmeni',__("Musíte vyplnit příjmení zákazníka"))
		
		->requirePresence('telefon', 'create',   __("Musíte vyplnit telefon zákazníka"))
		->notEmpty('telefon',__("Musíte vyplnit telefon zákazníka"))
		->add('telefon', [
			'length' => [
				'rule' => ['minLength', 9],
				'message' => 'Telefon musí mít 9 znaků',
			]
		])
		
		->allowEmpty('email')
		->add('email', 'validFormat', [
			'rule' => 'email',
			'message' => __('Email zákazníka není ve správném formátu')
		])
		
		//->notEmpty('ic',__("Musíte vyplnit IČ"))
		//->add("ic", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Toto IČO je již použito")])
		
		
	;	

    return $validator;
  }
}
