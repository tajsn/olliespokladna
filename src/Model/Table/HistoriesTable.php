<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Utility\Hash;

use Cake\Network\Session;

class HistoriesTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->addBehavior('Timestamp');
	
  }
  
	public function beforeFind($event, $query, $options, $primary){
		
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['Histories.system_id' => $system_id]);
		//pr($event->data);
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//$user_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		$event->data['entity']["user_id"] = $_SESSION['Auth']['User']['id'];
		
		
		return $event;
	}
	
  
}
