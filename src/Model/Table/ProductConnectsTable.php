<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Database\Expression\QueryExpression;
use Cake\Network\Session;

class ProductConnectsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->addBehavior('Timestamp');
    
  
  }
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//pr($query->_repository);
		$query->where([$event->subject()->alias().'.system_id' => $system_id]);
	}
	
   public function beforeSave($event){
    $session = new Session();
	$system_id = $session->read('System.system_id');
	$event->data['entity']["system_id"] = $system_id;
	
    return $event;
  }
  
  public function getList(){
		$ProductConnects = TableRegistry::get("ProductConnects");
		$group_list = $ProductConnects->find()
		->select([
			'product_id',
			'product_group_id',
		])
		->combine('id','name')
		->toArray();
		return($group_list);	
	}
  

  
}
