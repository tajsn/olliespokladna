<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class GroupsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
    $this->belongsTo("Users");
    $this->hasMany("GroupUsers");
  }

  public function validationDefault(Validator $validator)
  {
    return $validator
      ->requirePresence("name", __("Musí být zadán název skupiny"))
      ->notEmpty("name",  __("Název skupiny musí být vyplněn"));
  }

  public function findGroups($query, $options = null){

    return $query
      ->join(["GroupUsers" => 'group_users'])
      ->Contain("Users")
      ->where($options["where"]);

  }

  public function invate($invations){
    if(!empty($invations)){

      $Users = TableRegistry::get("Users");

      foreach($invations as $inv){
        $usr = $Users->find()->where(["username" => $inv])->first();
        if($usr){
          //$this->UserGroups->
        }
        else{
          echo "ne";
        }
      }
    }

    return false;
  }


}
