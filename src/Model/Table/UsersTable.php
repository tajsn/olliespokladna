<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Network\Session;
use Cake\Cache\Cache;

/**
 * Users Model
 *
 */
class UsersTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('users');

  }
	
	public function ridicList(){
		$Users = TableRegistry::get("Users");
		$ridic_list = $Users->find("all")
		  ->where(['group_id IN'=>[5],'kos'=>0])
		  ->select([
			"id", 
			"name",
			])
			->order("name ASC")
			->combine('id','name')
			->toArray()
			
			;
		return $ridic_list;
	}
	
	public function dispecerList(){
		$Users = TableRegistry::get("Users");
		$dispecer_list = $Users->find("all")
		  ->where(['group_id IN'=>[2,3],'kos'=>0])
		  ->select([
			"id", 
			"name",
			])
			->order("name ASC")
			->combine('id','name')
			->toArray()
			
			;
		return $dispecer_list;
	}
	
	public function userList(){
		$Users = TableRegistry::get("Users");
		if (($user_list = Cache::read('user_list')) === false) {
				
			$user_list = $Users->find("all")
			  ->where(['kos'=>0])
			  ->select([
				"id", 
				"name",
				])
				->order("name ASC")
				->combine('id','name')
				->toArray()
				
				;
			$user_list[-1] = 'Superadmin';
			$user_list[0] = 'Superadmin';
			
			Cache::write('user_list', $user_list);
		}
		
		return $user_list;
	}
	
  public function editAuth($id, $data){
    $data = serialize($data);

    $userAuthTable = TableRegistry::get("UserAuths");
    $auth = $userAuthTable->get($id);
    $auth->auth = $data;

    if($userAuthTable->save($auth)){
      return true;
    }
  }

  public function validationDefault(Validator $validator)
  {
    $validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
      ->requirePresence('username', 'create',   __("Musíte vyplnit Váš email"))
      ->notEmpty('username',__("Musíte vyplnit email"))
      ->add("username", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Tuto emailovou adresu již někdo používá")])
	
      ->requirePresence("code", "create", __("Kod musíte vyplnit"))
      ->notEmpty("code", __("Kod musíte vyplnit"))
      ->add('code', 'valid',['last' => true, 'rule' => ['custom' , '/^[0-9]{4,}$/i'], 'message'=> __('Kod musí být 4 znaky dlouhý')])
	  ->add("code", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Tento kod již někdo používá")])
	  
	;
    
    $validator
      ->requirePresence('password', 'create',  __("Musíte vyplnit heslo"))
      ->notEmpty('password',__("Musíte vyplnit heslo"));

    $validator->add('password', 'passwordsEqual', [
      'rule' => function ($value, $context) {
        return
          isset($context['data']['password2']) &&
          $context['data']['password2'] === $value;
      },
      'message' => __("Hesla se navzájem neshodují")
    ]);

    //$validator
      //->requirePresence('name', 'create',   __("Vyplňte prosím Vaše jméno a příjmení"))
      //->notEmpty('name',__("Musíte vyplnit jméno a příjmení"));

    return $validator;
  }

  public function beforeSave($event){
    $session = new Session();
	$system_id = $session->read('System.system_id');
	$event->data['entity']["system_id"] = $system_id;
	
	if (isset($event->data['entity']['prijmeni']))
	$event->data['entity']["name"] = $event->data['entity']['prijmeni'].' '.$event->data['entity']['jmeno'];
		
    return $event;
  }
  
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['Users.system_id' => $system_id]);
		//pr($event->data);
	}

}
