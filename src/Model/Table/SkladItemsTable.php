<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

use Cake\Network\Session;

class SkladItemsTable extends Table
{

	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->addBehavior('Timestamp');
		$this->hasOne('SkladStavItems',['order' => 'SkladStavItems.id DESC']);
		$this->hasOne('Sklads',['order' => 'Sklads.id DESC']);
	
	}
  
	public function beforeFind($event, $query, $options, $primary){
		
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		
		return $event;
	}
	
	
	public function findLastIdSkladItems(){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		
		$find = $this->find()
		->select(['id',])
		//->where(['system_id'=>$system_id])
		->order('id DESC')
		->first();
		//pr($find);
		return($find->id);	
	}
	
	public function skladItemData($id){
		
		$find = $this->find()
		->select(['id','tax_id','jednotka_id'])
		->where(['id'=>$id])
		->first();
		return($find);	
	}
	
	public function checkMakroSkladItem($data,$price_tax_list_makro){
		$find = $this->find()
			->where(['kos'=>0,'ean'=>$data[2]])
			->select([
				'id',
				'name',
				'jednotka_id',
			])
			//->combine('id','name')
			->first();
		if ($find){
			return $find;
		} else {
			//pr($data);
				
			$save_new = [
				'name'=>trim(strtr($data[13],['  '=>''])),
				'ean'=>$data[2],
				'dodavatel_id'=>1,
				'tax_id'=>$price_tax_list_makro[$data[12]],
			];
			
			$save_item = $this->newEntity($save_new);
			
			$find = $this->save($save_item);
			return $find;
			
			//pr($save_item);
			//die('neni');
		}
		
	}
	public function checkBidFoodSkladItem($data,$price_tax_list_makro){
		$find = $this->find()
			->where(['kos'=>0,'bidfood_id'=>$data[5]])
			->select([
				'id',
				'name',
				'jednotka_id',
			])
			//->combine('id','name')
			->first();
		if ($find){
			return $find;
		} else {
			//pr($data);
				
			$save_new = [
				'name'=>trim(strtr($data[6],['  '=>''])),
				'bidfood_id'=>$data[5],
				'dodavatel_id'=>2,
				'jednotka_id_dodavatel'=>$data[9],
				'tax_id'=>$price_tax_list_makro[$data[11]],
			];
			
			$save_item = $this->newEntity($save_new);
			//pr($save_item);die();
			
			//pr($save_item);die();
			$find = $this->save($save_item);
			//pr($find);die();
			return $find;
			
			//pr($save_item);
			//die('neni');
		}
		
	}
	
	public function skladItemsList($dodavatel_id=null){
		
		//if (($dodavatel_list = Cache::read('dodavatel_list')) === false) {
		$conditions = ['kos'=>0];
		if ($dodavatel_id != null){
			//$conditions['dodavatel_id']= $dodavatel_id;
			$conditions['group_id']= $dodavatel_id;
		}
			$items_list_load = $this->find()
			->where($conditions)
			->select([
				'id',
				'name',
				'jednotka_id',
			])
			//->combine('id','name')
			->toArray();
			
			//Cache::write('dodavatel_list', $dodavatel_list);
		//}
		//pr($group_list);
		//pr($items_list);	
		
		$sklad_jednotka_list = [
			1=>'ks',
			2=>'l',
			3=>'g',
			4=>'kg',
		];
		$items_list = [];
		foreach($items_list_load AS $i){
			//pr($i);
			$items_list[$i->id] = $i->name.(($i->jednotka_id>0)?' ('.$sklad_jednotka_list[$i->jednotka_id].')':'');
		}
		return($items_list);	
	}
	
	public function skladItems(){
		
		//if (($dodavatel_list = Cache::read('dodavatel_list')) === false) {
				
		$items_list_load = $this->find()
			->where(['kos'=>0])
			->select([
				'id',
				'name',
				'min',
				'max',
				'tax_id',
				'jednotka_id',
			])
			
			->toArray();
			
			//Cache::write('dodavatel_list', $dodavatel_list);
		//}
		$items_list = Hash::combine($items_list_load, '{n}.id','{n}');
		
		//pr($items_list);	
		return($items_list);	
	}
	
	
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			
			->notEmpty('code',__("Musíte vyplnit kód"))
			->notEmpty('price',__("Musíte vyplnit cenu"))
			
		;
		return $validator;
	}

  
}
