<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;


class DriversTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->table('users');
   $this->addBehavior('Timestamp');
  }
	
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['Drivers.system_id' => $system_id]);
		//pr($event->data);
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		if (isset($event->data['entity']['prijmeni']))
		$event->data['entity']["name"] = $event->data['entity']['prijmeni'].' '.$event->data['entity']['jmeno'];
		
		
		return $event;
	}
  
	public function get_driver_detail($driver_id){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$Drivers = TableRegistry::get("Drivers");
		
		$driver_detail = $Drivers->find()
		  ->where(['kos'=>0,'system_id'=>$system_id,'id'=>$driver_id])
		  ->select([
			'id',
			'tel_firemni',
			'name',
			'prijmeni',
			'jmeno',
		  ])
		  ->order('name ASC')
		  ->hydrate(false)
		  ->first();
		  
		return $driver_detail;  
	}
	
	
  public function validationDefault(Validator $validator){
    
	$validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
		//->requirePresence('jmeno', 'create',   __("Musíte vyplnit jméno"))
		->notEmpty('jmeno',__("Musíte vyplnit jméno"))
		
		//->requirePresence('prijmeni', 'create',   __("Musíte vyplnit příjmení"))
		->notEmpty('prijmeni',__("Musíte vyplnit příjmení"))
		
		//->requirePresence('number', 'create',   __("Musíte vyplnit číslo"))
		->notEmpty('number',__("Musíte vyplnit číslo"))
		->add("number", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Toto číslo již někdo používá")])

		
	;	

    return $validator;
  }
  
}
