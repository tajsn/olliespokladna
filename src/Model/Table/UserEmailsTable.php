<?php
namespace App\Model\Table;

use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Users Model
 *
 */
class UserEmailsTable extends Table
{
  public function initialize(array $config)
  {
    parent::initialize($config);
  }

  public function beforeSave($event){
    if(isset($event->data['entity']["password"])){
      $event->data['entity']["password"] =  Security::encrypt($event->data['entity']["password"], Security::salt());
    }
  }

  public function readPassword($pass){
    return Security::decrypt($pass, Security::salt());
    die();
  }
}
