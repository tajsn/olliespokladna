<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Cache\Cache;
use Cake\Core\Configure;

use Cake\Utility\Hash;

use Cake\Network\Session;

class TrzbasTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->table('orders');
	$this->addBehavior('Timestamp');
	$this->belongsTo("Clients");
	$this->belongsTo("Tables");
	$this->hasMany("TrzbaItems");
  }
  
	public function beforeFind($event, $query, $options, $primary){
		
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['Trzbas.system_id' => $system_id]);
		
		$user_id = $session->read('System.user_id');
		$event->data['entity']["user_id"] = $user_id;
		
		//pr($event->data);
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		//pr($event->data['entity']);die();
		
		return $event;
	}
	
	public function checkOpenTable(){
		$Orders = TableRegistry::get("Orders");
		$table_list_load = $Orders->find()
		->select([
			'id',
			'table_id',
			'total_price',
		])
		->where(['done'=>0])
		->group(['table_id'])
		->combine('table_id','total_price')
		->toArray();
		return($table_list_load);	
	}
	
	public function findTableData($table_id){
		//if (($table = Cache::read('table_id_'.$table_id)) === false) {
			$OrderItems = TableRegistry::get("OrderItems");
		
			$table = $OrderItems->find()
				->where(['table_id'=>$table_id,'done'=>0,'printed'=>0])
				->select([
					'id',
					'order_id',
					'done',
					'num',
					'product_id',
					'product_group_id',
					'name',
					'order_id',
					'price',
					'price_default',
					'price_tax_id',
					'code',
					'ks',
					'note',
					'addon',
				])
				->toArray();
			/*
			$Orders = TableRegistry::get("Orders");
		
			$table = $Orders->find()
				->where(['table_id'=>$table_id,'done'=>0])
				->contain(['OrderItems'=>['conditions'=>['kos'=>0,'printed'=>0],'fields'=>[
					'id',
					'order_id',
					'done',
					'num',
					'product_id',
					'product_group_id',
					'name',
					'order_id',
					'price',
					'price_default',
					'price_tax_id',
					'code',
					'ks',
					'note',
					'addon', 
				]]])
				->select([
					'id',
					'total_price',
					'done',
					'table_id',
				])
				->first()
			;
			*/
			Cache::write('table_id_'.$table_id, $table);
			
		//}
		//pr($table);
		return $table;
	}
	
	
	
	
	public function validationDefault(Validator $validator){
    
	$validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
		//->requirePresence('name', 'create',   __("Mus�te vyplnit n�zev"))
		//->notEmpty('name',__("Mus�te vyplnit n�zev"))
		;
    return $validator;
  }

  
}
