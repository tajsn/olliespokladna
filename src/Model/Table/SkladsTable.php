<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

use Cake\Network\Session;

class SkladsTable extends Table
{

	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->belongsTo('SkladItems');
		$this->addBehavior('Timestamp');
	
	}
  
	public function beforeFind($event, $query, $options, $primary){
		
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		
		return $event;
	}
	
	
	
	
	public function getSkladValues($from_id){
		$sklad_values = [];
		$sklad_data = $this->find();
		
		$fields = ['Sklads.id','Sklads.sklad_item_id','sklad_type_id','sum_value' => $sklad_data->func()->sum('Sklads.value')];
		
		$sklad_data
		->select($fields)
		->where(['id >'=>$from_id])
		->group(['sklad_item_id','sklad_type_id']);
		$data = $sklad_data->toArray();
		//pr($data);
		
		foreach($data AS $d){
			if (!isset($sklad_values[$d->sklad_item_id])){
				$sklad_values[$d->sklad_item_id] = [
					'sklad_item_id'=>$d->sklad_item_id,
					'sum'=>0
				];
			}
			if (in_array($d->sklad_type_id,[1,5])){
				$sklad_values[$d->sklad_item_id]['sum'] += $d->sum_value;
			} else {
				$sklad_values[$d->sklad_item_id]['sum'] -= $d->sum_value;
				
			}
			//pr($d);
		}
		
		//$sklad_values = ;
		return $sklad_values;
	}
	
	
	
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			
			->notEmpty('code',__("Musíte vyplnit kód"))
			->notEmpty('price',__("Musíte vyplnit cenu"))
			
		;
		return $validator;
	}

  
}
