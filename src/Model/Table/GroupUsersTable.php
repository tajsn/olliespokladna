<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class GroupUsersTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
    $this->belongsTo("Users");
    $this->belongsTo("Groups");
  }
}
