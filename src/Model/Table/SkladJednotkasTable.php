<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

use Cake\Network\Session;

class SkladJednotkasTable extends Table
{

	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->addBehavior('Timestamp');
	
	}
  
	public function beforeFind($event, $query, $options, $primary){
		
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		
		return $event;
	}
	
	
	public function getSkladJednotkas(){
		$sklad_jednotkas = [];
		
		$fields = ['jednotka_id','dodavatel_jednotka','prevod'];
		
		$jednotka_load = $this->find()
		->select($fields)
		->where(['kos'=>0])
		;
		$data = $jednotka_load->toArray();
		//pr($data);
		foreach($data AS $d){
			$sklad_jednotkas[$d->dodavatel_jednotka] = [
				'prevod'=>$d->prevod,
				'jednotka_id'=>$d->jednotka_id,
			];
		}
		return $sklad_jednotkas;
	}
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Mus�te vyplnit n�zev"))
			->notEmpty('name',__("Mus�te vyplnit n�zev"))
			
			->notEmpty('code',__("Mus�te vyplnit k�d"))
			->notEmpty('price',__("Mus�te vyplnit cenu"))
			
		;
		return $validator;
	}

  
}
