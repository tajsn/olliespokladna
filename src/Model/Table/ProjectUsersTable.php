<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Users Model
 *
 */
class ProjectUsersTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->hasOne("Projects");

    $this->table('project_users');
  }




}
