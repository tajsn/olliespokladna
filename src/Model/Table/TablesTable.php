<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Cache\Cache;

use Cake\Network\Session;

class TablesTable extends Table{

	public function initialize(array $config){
		parent::initialize($config);
		$this->addBehavior('Timestamp');
	}
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//pr($query->_repository);
		//pr($system_id);die();
		$query->where([$event->subject()->alias().'.system_id' => $system_id]);
	}
 

	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
	  
		return $event;
	}
	
	
	public function loadTableData($table_id){
			$table_data = $this->find()
			->where(['id'=>$table_id])
			->select([
				'id',
				'name',
				'data',
			])
			->map(function($r){
				$r->data = json_decode($r->data,true);
				return $r;
			})
			->first();
			
			
		return($table_data);	
	}
	
	public function checkOpenTable(){
			$table_data = $this->find()
			->where(['date_open >'=>0])
			->select([
				'id',
				'total_price',
				'name',
			])
			->map(function($r){
				$r->data = json_decode($r->data,true);
				return $r;
			})
			->first();
			
			
		return($table_data);	
	}
	public function listAll(){
		//$Tables = TableRegistry::get("Tables");
		//if (($table_list = Cache::read('table_list')) === false) {
				
			$table_list_load = $this->find()
			->select([
				'id',
				'name',
				'group_id',
				'on_map',
				'data',
				'total_price',
				'terminal_id',
			])
			//->combine('id','name')
			->toArray();
			//pr(configSqlDb);die();
			//pr($table_list_load);die();
			$table_list = [];
			foreach($table_list_load AS $t){
				$table_list[$t->group_id][] = $t;
			}
			//Cache::write('table_list', $table_list);
		//}
		return($table_list);	
	}
	
	public function listAllSum(){
		//$Tables = TableRegistry::get("Tables");
		//if (($table_list = Cache::read('table_list')) === false) {
				
			$table_list_load = $this->find()
			->select([
				'id',
				'total_price',
				'terminal_id',
			])
			//->combine('id','name')
			->toArray();
			
			//Cache::write('table_list', $table_list);
		//}
		//pr($table_list_load);
		$table_list = [];
		//pr($_SESSION['Auth']['User']);
		foreach($table_list_load AS $value){
			if ($value['total_price'] > 0){
				$tableId = $value['id'];
				$table_list[$tableId] = [
					'total_price'=>$value['total_price'],
					'terminal_id'=>$value['terminal_id'],
					'table_class'=>(($value['terminal_id'] == $_SESSION['Auth']['User']['terminal_id'])?'active':'').' '.(($value['terminal_id']>0 && $value['terminal_id'] != $_SESSION['Auth']['User']['terminal_id'])?'active_other':''),
				];
			}
		}
		return($table_list);	
	}
	public function tableNameList(){
		$Tables = TableRegistry::get("Tables");
		if (($table_list_load = Cache::read('table_list_load')) === false) {
				
			$table_list_load = $Tables->find()
			->select([
				'id',
				'name',
			])
			->combine('id','name')
			->toArray();
			Cache::write('table_list_load', $table_list_load);
		}
		$table_list_load[0] = 'Volný prodej';
		return($table_list_load);	
	}
	
	
	
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			->notEmpty('num',__("Musíte vyplnit číslo"))
			
		;
		return $validator;
	}
  
	
  
}
