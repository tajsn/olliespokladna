<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\Network\Session;



class ZakazkasTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->addBehavior('Timestamp');
	
    $this->belongsTo('Clients');
    $this->hasOne('ZakazkaConnects');
    $this->belongsTo('AdresaNalozeni',['className'=>'Adresas','foreignKey'=>'adresa_nalozeni_id']);
	$this->belongsTo('AdresaVylozeni',['className'=>'Adresas','foreignKey'=>'adresa_vylozeni_id']);
    //$this->hasMany("ZakazkaConnects");
  }
  
  
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['Zakazkas.system_id' => $system_id]);
		
	}
	
  public function beforeSave($event){
    $session = new Session();
	$system_id = $session->read('System.system_id');
	$user_id = $session->read('Auth.User.id');
	if (!isset($event->data['entity']["user_id"]) || empty($event->data['entity']["user_id"])){
		$event->data['entity']["user_id"] = $user_id;
	
	}
	$event->data['entity']["system_id"] = $system_id;
	
	if (isset($event->data['entity']['client']['name']))
	$event->data['entity']["client_name"] = $event->data['entity']['client']['name'];
	//$event->data['entity']["adresa_nalozeni"] = $event->data['entity']['adr']['ulice1'].' '.$event->data['entity']['adr']['mesto1'].' '.$event->data['entity']['adr']['psc1'];
	//$event->data['entity']["adresa_vylozeni"] = $event->data['entity']['adr']['ulice2'].' '.$event->data['entity']['adr']['mesto2'].' '.$event->data['entity']['adr']['psc2'];
	//pr($event->data['entity']);
    
    return $event;
  }
  
	public function findAdresa($query, $options){
		return $query->join(
			[
			"table" => "group_users",
			"alias" => "GroupUsers",
			'type' => 'left',
			'conditions' => 'Projects.group_id = GroupUsers.group_id'
			]
		);
		
    }
	
	public function get_zakazka_driver($zakazka_id){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$ZakazkaConnects = TableRegistry::get("ZakazkaConnects");
		
		$zakazka_detail = $ZakazkaConnects->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['ZakazkaConnects.system_id'=>$system_id,'ZakazkaConnects.zakazka_id'=>$zakazka_id])
		  ->select([
			'driver_id',
			'car_id',
		  ])
		  ->hydrate(false)
		  ->first();
		//pr($zakazka_detail);  
		return $zakazka_detail;  
	}	
		
	public function get_zakazka_detail($zakazka_id){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$Zakazkas = TableRegistry::get("Zakazkas");
		
		$zakazka_detail = $Zakazkas->find()
		  ->contain(['Clients','AdresaNalozeni', 'AdresaVylozeni'])
		  ->where(['Zakazkas.kos'=>0,'Zakazkas.system_id'=>$system_id,'Zakazkas.id'=>$zakazka_id])
		  ->select([
			'id',
			'size',
			'weight',
			'code_int',
			'sklad_id',
			'AdresaNalozeni.name',
			'AdresaNalozeni.ulice',
			'AdresaNalozeni.mesto',
			'AdresaNalozeni.psc',
			'AdresaNalozeni.stat',
			'AdresaNalozeni.firma',
			'AdresaNalozeni.lat',
			'AdresaNalozeni.lng',
			
			'AdresaVylozeni.name',
			'AdresaVylozeni.ulice',
			'AdresaVylozeni.mesto',
			'AdresaVylozeni.psc',
			'AdresaVylozeni.stat',
			'AdresaVylozeni.firma',
			'AdresaVylozeni.lat',
			'AdresaVylozeni.lng',
			
			'ZakazkaConnects.id',
			'ZakazkaConnects.car_id',
			'ZakazkaConnects.driver_id',
			//'adresa_nalozeni',
			
		  ])
		  ->contain(["ZakazkaConnects"])
		  ->hydrate(false)
		  ->first();
		  
		return $zakazka_detail;  
	}
	
	
	public function validationDefault(Validator $validator){
    //pr($validator);
	$validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
		//->requirePresence('name', 'create',   __("Musíte vyplnit číslo zakázky"))
		//->notEmpty('name',__("Musíte vyplnit číslo zakázky"))
		//->add("name", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Tuto číslo zakázky je již použito")])
		
		->notEmpty('code_int',__("Musíte vyplnit Interní číslo zakázky"))
		->add("code_int", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Tuto interní číslo zakázky je již použito")])
		
		//->requirePresence('adresa_nalozeni_id', 'create',   __("Musíte vybrat adresu naložení"))
		->notEmpty('adresa_nalozeni_id',__("Musíte vybrat adresu naložení"))
		
		//->requirePresence('adresa_vylozeni_id', 'create',   __("Musíte vybrat adresu vyložení"))
		->notEmpty('adresa_vylozeni_id',__("Musíte vybrat adresu vyložení"))
		
		//->requirePresence('weight', 'create',   __("Musíte vyplnit hmotnost"))
		->notEmpty('weight',__("Musíte vyplnit hmotnost"))
		
		
		//->requirePresence('date_nalozeni', 'create',   __("Musíte vyplnit datum naložení"))
		->notEmpty('date_nalozeni',__("Musíte vyplnit datum naložení"))
		->add('date_nalozeni', 'validFormat', [
			'rule' => array('date', 'dmy'),
			'message' => 'Neznámý formát pro datum naložení'
		])
		
		//->requirePresence('size', 'create',   __("Musíte vyplnit počet palet"))
		->notEmpty('size',__("Musíte vyplnit počet palet"))
	 ;
     
	  
	  
      
    
    
    return $validator;
  }
	public function validationOnlyCheck($validator) {
        //pr($validator);
		$validator->remove('code_int');

        return $validator;
	}

  
}
