<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Cache\Cache;

use Cake\Network\Session;

class SettingsTable extends Table{

	public function initialize(array $config){
		parent::initialize($config);
		$this->addBehavior('Timestamp');
	}
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//pr($query->_repository);
		$query->where([$event->subject()->alias().'.system_id' => $system_id]);
	}
 

	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		return $event;
	}
	
	public function settingTableMap(){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$Settings = TableRegistry::get("Settings");
		if (($setting_table_map = Cache::read('setting_table_map')) === false) {
				
			$setting_table_map = $Settings->find()
			->select([
				'id',
				'data',
			])
			->where(['system_id'=>$system_id,'type'=>2])
			->map(function($r){
				
				$r->data = unserialize($r->data);
				return $r;
			})
			->first();
			//pr($setting_table_map->data);die();
			Cache::write('setting_table_map', $setting_table_map);
		}
		return($setting_table_map);	
	}
	public function settingGlobal(){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		
		$Settings = TableRegistry::get("Settings");
		//if (($setting_global = Cache::read('setting_global')) === false) {
				
			$setting_global = $this->find()
			->select([
				'id',
				'data',
			])
			->where(['system_id'=>$system_id,'type'=>1])
			->map(function($r){
				
				$r->data = unserialize($r->data);
				return $r;
			})
			->first();
			//pr($setting_table_map->data);die();
			Cache::write('setting_global', $setting_global);
		//}
		return($setting_global);	
	}
	
	
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			->notEmpty('num',__("Musíte vyplnit císlo"))
			
		;
		return $validator;
	}
  
	
  
}
