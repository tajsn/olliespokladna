<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

use Cake\Network\Session;

class SkladStavsTable extends Table
{

	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->hasMany('SkladStavItems');
		$this->addBehavior('Timestamp');
	
	}
  
	public function beforeFind($event, $query, $options, $primary){
		
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		
		return $event;
	}
	
	public function findLastId(){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		
		$find = $this->find()
		->select(['id','to_sklad_id'])
		->where(['system_id'=>$system_id])
		->order('id DESC')
		->first();
		if (!isset($find->to_sklad_id) || $find->to_sklad_id == ''){
			$last_id = 0;
		}  else {
			$last_id = $find->to_sklad_id;
		}
		//pr($find);
		return($last_id);	
	}
	
	
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			
			->notEmpty('code',__("Musíte vyplnit kód"))
			->notEmpty('price',__("Musíte vyplnit cenu"))
			
		;
		return $validator;
	}

  
}
