<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Utility\Hash;

use Cake\Network\Session;

class DokladsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->addBehavior('Timestamp');
	$this->belongsTo("Orders");
  }
  
	public function beforeFind($event, $query, $options, $primary){
		
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$query->where(['Doklads.system_id' => $system_id]);
		
		//pr($event->data);
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		//$user_id = $session->read('System.user_id');
		//$event->data['entity']["user_id"] = $user_id;
		
		//pr($event->data['entity']);die();
		
		return $event;
	}
	
	
	
	public function validationDefault(Validator $validator){
    
	$validator
      ->add('id', 'valid', ['rule' => 'numeric'])
      ->allowEmpty('id', 'create');

    $validator
		//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
		//->notEmpty('name',__("Musíte vyplnit název"))
		;
    return $validator;
  }

  
}
