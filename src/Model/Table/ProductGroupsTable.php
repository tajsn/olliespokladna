<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Cake\Utility\Hash;

use Cake\Network\Session;

class ProductGroupsTable extends Table{

	public function initialize(array $config){
		parent::initialize($config);
		$this->addBehavior('Timestamp');
	}
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//pr($query->_repository);
		$query->where([$event->subject()->alias().'.system_id' => $system_id]);
	}
 

	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
	  
		return $event;
	}
	
	function group_list($group_list,$data,$level=1){
				foreach($data AS $k=>$g){
					if ($g['level'] == $level && $level == 1){
						$group_list[$g['id']] = $g;
					}
					if ($g['level'] == $level && $level == 2){
						$group_list[$g['parent_id']]['children'][$g['id']] = $g;
					}
					if ($g['level'] == $level && $level == 3){
						foreach($group_list AS $gk=>$gg){
							if (isset($group_list[$gk]['children'][$g['parent_id']])){
								$group_list[$gk]['children'][$g['parent_id']]['children'][$g['id']] = $g;
							}
						}
					}
					
				}
				//pr($group_list);
				return $group_list;
	}
	
	
	
	function groupListParent(){
		$ProductGroups = TableRegistry::get("ProductGroups");
		$group_list_load = $ProductGroups->find()
			->where(['kos'=>0])
			->select([
				'id',
				'name',
				'level',
				'parent_id',
				'color',
				'kos',
			])
			->hydrate(false)
			//->combine('id','name')
			->toArray();
		//pr($group_list_load);
		$list = [];
		foreach($group_list_load AS $g){
			if (!isset($list[$g['parent_id']])){
				$list[$g['parent_id']] = [];
			}
			$list[$g['parent_id']][] = $g['id'];
			
			
		}
		foreach($group_list_load AS $k=>$g){
			foreach($list AS $key=>$ll){
				if (in_array($g['parent_id'],$ll)){
					$list[$key][] = $g['id'];
					//pr($ll);
				}
				
			}
			//pr($list);
		}
		//pr($list);
		//die('a');
		//pr($list3);
		//pr($list);
		return $list;
		//pr($list);die();
	}
	
	public function groupListAll(){
		/*
		$ProductGroups = TableRegistry::get("ProductGroups");
		
		if (($group_list_all = Cache::read('group_list_all')) === false) {
				
			$group_list_load = $ProductGroups->find()
			->where(['kos'=>0])
			->select([
				'id',
				'name',
				'parent_id',
				'color',
				'level',
				'kos',
			])
			->hydrate(false)
			->toArray();
			//pr($group_list_load);die();
			$group_list_all = [];
			
			$group_list_all = $this->group_list($group_list_all,$group_list_load);
			$group_list_all = $this->group_list($group_list_all,$group_list_load,2);
			$group_list_all = $this->group_list($group_list_all,$group_list_load,3);
			//pr($group_list_all);
			Cache::write('group_list_all', $group_list_all);
		}	
		//pr($group_list_all);die();	
		return($group_list_all);	
		*/
		if (($group_list_all = Cache::read('group_list_all')) === false) {
		
			$ProductGroups = TableRegistry::get("MenuItems");
			$group_list_all = $ProductGroups->find('threaded')
				->hydrate(false)
				->toArray();
			Cache::write('group_list_all', $group_list_all);
		}
	
		return($group_list_all);	
		
	}
	
	
  
	public function groupListName(){
		if (($group_list_all_name = Cache::read('group_list_all_name')) === false) {
		
			$ProductGroups = TableRegistry::get("MenuItems");
			$group_list_all_name = $ProductGroups->find('list',['key'=>'id','value'=>'name'])
				->select(['id','name'])
				->hydrate(false)
				->toArray();
			Cache::write('group_list_all_name', $group_list_all_name);
		}
		//pr($group_list_all_name);die();
		return($group_list_all_name);	
	
	}
	
	public function groupList(){
		
		$ProductGroups = TableRegistry::get("ProductGroups");
		if (($group_list = Cache::read('group_list')) === false) {
				
			$group_list_load = $ProductGroups->find()
			->where(['kos'=>0])
			->select([
				'id',
				'name',
				'level',
				'parent_id',
				'color',
				'kos',
			])
			->hydrate(false)
			//->combine('id','name')
			->toArray();
			
			/*
			foreach($group_list_all_load AS $k=>$g){
				$pref = '';
				if ($g->level == 1){
					$pref = '&nbsp;&nbsp;';
				}
				if ($g->level == 2){
					$pref = '&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				if ($g->level == 3){
					$pref = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				//pr($g);
				$group_list_all[$k] = $pref.$g->name;
			}
			*/
			$group_list_load = $this->groupListAll();
			$group_list = [];
			$this->groupNameList($group_list,$group_list_load,0);
				
			
			Cache::write('group_list', $group_list);
		}
		//pr($group_list);
			
		return($group_list);	
	}
	
	public function gnameList($product_group_list){
		$gname_list = [];
		function gName($glist,$gname_list,$name=null){
			foreach($glist AS $k=>$g){
				$gname_list[$g['id']] = $name.$g['name'];
				if (isset($g['children']) && !empty($g['children'])){
					//pr($g['children']);
					$gname_list = gName($g['children'],$gname_list,$name.$g['name'].' - ');
				}
			}
			
			return $gname_list;
		}
		$gname_list = gName($product_group_list,$gname_list);
		return $gname_list;
	}
	
	public function groupListLevel(){
		
		$ProductGroups = TableRegistry::get("ProductGroups");
		//if (($group_list = Cache::read('group_list')) === false) {
				
			$group_list_load = $ProductGroups->find()
			->where(['kos'=>0])
			->select([
				'id',
				'name',
				'level',
				'parent_id',
				'color',
				'kos',
			])
			->hydrate(false)
			//->combine('id','name')
			->toArray();
			
			/*
			foreach($group_list_all_load AS $k=>$g){
				$pref = '';
				if ($g->level == 1){
					$pref = '&nbsp;&nbsp;';
				}
				if ($g->level == 2){
					$pref = '&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				if ($g->level == 3){
					$pref = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				//pr($g);
				$group_list_all[$k] = $pref.$g->name;
			}
			*/
			$group_list_load = $this->groupListAll();
			$group_list_level = [];
			$this->groupNameListLevel($group_list_level,$group_list_load,0);
			
			//pr($group_list);
			Cache::write('group_list_level', $group_list_level);
		//}
		//pr($group_list_level);
		return($group_list_level);	
	}
	
	private function groupNameList(&$group_list,$data,$level){
		//$group_list = [];
		foreach($data AS $k=>$d){
			$pref = ' - ';
			$group_list[$k] = str_repeat($pref,$d['level']-1).$d['name'];
			
			if (isset($d['children']) && !empty($d['children'])){
				//pr($d['children']);
				
				$this->groupNameList($group_list,$d['children'],$d['level']);
			}
		}
		
		
		//return $group_list;
	}
	private function groupNameListLevel(&$group_list_level,$data,$level){
		//$group_list = [];
		foreach($data AS $k=>$d){
			$pref = ' - ';
			//pr($group_list_level);
			
			$group_list_level[$k] = [
				'name'=>$d['name'],
				'parent_id'=>(!empty($d['parent_id'])?$d['parent_id']:0),
				'level'=>(!empty($d['level'])?$d['level']:0),
			];
			
			if (isset($d['children']) && !empty($d['children'])){
				//pr($d['children']);
				
				$this->groupNameListLevel($group_list_level,$d['children'],$d['level']);
			}
		}
		
		
		//return $group_list;
	}
	
	public function groupList2(){
		$ProductGroups = TableRegistry::get("ProductGroups");
		if (($group_list_all2 = Cache::read('group_list_all2')) === false) {
				
			$group_list_all2_load = $ProductGroups->find()
			->where(['kos'=>0])
			->select([
			])
			->toArray();
			$group_list_all2 = [];
			foreach($group_list_all2_load AS $g){
				$group_list_all2[$g['id']] = $g;
			}
			Cache::write('group_list_all2', $group_list_all2);
		}
		return($group_list_all2);	
	}
	
	public function printerList(){
		$ProductGroups = TableRegistry::get("ProductGroups");
		$printer_list = $ProductGroups->find()
		->select([
			'id',
			'printer',
		])
		->combine('id','printer')
		->toArray();
		return($printer_list);	
	}
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Musíte vyplnit název"))
			->notEmpty('name',__("Musíte vyplnit název"))
			
		;
		return $validator;
	}
  
	
  
}
