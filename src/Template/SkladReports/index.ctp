<div class="layout">
<?php 
//pr($this->request->query);
echo '<div id="top_action">';
	echo $this->Html->link('Tisk', ['action' => 'tisk?'.http_build_query($this->request->query)], ['class' => 'button pdf']);
echo '</div>';

echo '<div id="filtration">';

	echo '<div class="input text">'; 
		echo $this->Form->select('select_sklad',[0=>__('Vyberte sklad')]+$sklad_type[$system_id],['class'=>'select_params','data-type'=>'sklad_id','default'=>(isset($this->request->query['sklad_id'])?$this->request->query['sklad_id']:'')]); 
	echo '</div>';
	
	echo '<div class="input text">';
		echo $this->Form->select('select_car',[0=>__('Vyberte auto')]+$car_list,['class'=>'select_params','data-type'=>'car_id','default'=>(isset($this->request->query['car_id'])?$this->request->query['car_id']:'')]); 
	echo '</div>';
echo '</div>';

?>
<?php
//pr($data);
if (isset($data) && count($data)>0){
	echo '<div class="table_response">';
	echo '<table class="table-formated">';
	echo '<tr>';
		echo '<th>'.__('Možnosti').'</th>';
		echo '<th>'.__('Typ').'</th>';
		echo '<th>'.__('Auto').'</th>';
		echo '<th>'.__('Naložení').'</th>';
		echo '<th>'.__('Vyložení').'</th>';
		echo '<th>'.__('Sklad').'</th>';
		echo '<th>'.__('Palet').'</th>';
		echo '<th>'.__('Váha').'</th>';
		echo '<th>'.__('Code').'</th>';
		echo '<th>'.__('SPZ').'</th>';
		echo '<th>'.__('Řidič').'</th>';
		
	echo '</tr>';
	foreach($data AS $key=>$d){
		$adresa_nalozeni = unserialize($d['adresa_nalozeni']);
		$adresa_vylozeni = unserialize($d['adresa_vylozeni']);
		//pr($adresa_nalozeni);
		echo '<tr class="item-row '.(($key++ % 2) ? "even" : "odd").'">';
			echo '<td>';
				if (in_array($d->type,[1,3])){
					echo $this->Html->link(__('Potvrdit vyložení'),'/sklad-reports/save_data/unload/'.$d->id.'/'.$d->zakazka_id,array('title'=>__('Potvrdit vyložení'),'class'=>'button small confirm driver_req'));
					
				}
				
				if (in_array($d->type,[2,4])){
					echo $this->Html->link(__('Potvrdit naložení'),'/sklad-reports/save_data/load/'.$d->id.'/'.$d->zakazka_id,array('title'=>__('Potvrdit naložení'),'class'=>'button small confirm driver_req'));
					
				}
				
				//if ($d->driver_unload == 0)
				//echo $this->Html->link(__('Vyložil jsem'),'/driver-orders/driver_save/unload/'.$d->id.'/'.$d->zakazka_id,array('title'=>__('Vyložil jsem'),'class'=>'button small vylozeno '.(($d->driver_load == 0)?'none':'').' driver_req'));
			echo '</td>';
			echo '<td>'.$sklad_type_report[$d->type].'</td>';
			echo '<td>'.$car_list[$d->car_id].'</td>';
			echo '<td>'.$adresa_nalozeni['firma'].' '.$adresa_nalozeni['ulice'].' '.$adresa_nalozeni['mesto'].' '.$adresa_nalozeni['psc'].' '.$adresa_nalozeni['stat'].'</td>';
			echo '<td>'.$adresa_vylozeni['firma'].' '.$adresa_vylozeni['ulice'].' '.$adresa_vylozeni['mesto'].' '.$adresa_vylozeni['psc'].' '.$adresa_vylozeni['stat'].'</td>';
			echo '<td>'.(isset($sklad_type[$system_id][$d->sklad_id])?$sklad_type[$system_id][$d->sklad_id]:'').'</td>';
			echo '<td>'.$d->size.'</td>';
			echo '<td>'.$d->weight.'</td>';
			echo '<td>'.$d->code.'</td>';
			echo '<td>'.$d->spz.(!empty($d->spz2)?'<br />'.$d->spz2:'').'</td>';
			echo '<td>'.$d->ridic.' ('.$d->telefon.')'.(!empty($d->ridic2)?'<br />'.$d->ridic2.' ('.$d->telefon2.')':'').'</td>';
		echo '</tr>';
	}
	echo '</table>';
	echo '</div>';
} else {
	echo '<p class="noresult">'.__('Nenalezeny žádné přiřazené zakázky').'</p>';
}
?>
</div>
<script type="text/javascript">
//<![CDATA[
function moje_zakazky(){
	window.addEvent('domready',function(){
		if ($$('select_params')){
			$$('.select_params').addEvent('change',function(e){
				var params = '?';
				$$('.select_params').each(function(item){
					if (item.value > 0){
						params += item.get('data-type') + '='+item.value+'&';
					}
				});
				//console.log(params);
				//alert('');
				window.location = '/sklad-reports/'+params;
			});
		}
		$$('.driver_req').addEvent('click',function(e){
			e.stop();
			var parent = e.target.getParent('tr');
			if (e.target.hasClass('nalozeno')){
				var type = 'nalozeno';
			}
			if (e.target.hasClass('confirm')){
				var type = 'confirm';
			}
			if (e.target.hasClass('vylozeno')){
				var type = 'vylozeno';
			}
			save_load_req = new Request.JSON({
				url:e.target.href,
				onComplete :(function(json){
					if (type == 'confirm'){
						if (parent.getElement('.confirm')) parent.getElement('.confirm').addClass('none');
						if (parent.getElement('.nalozeno')) parent.getElement('.nalozeno').removeClass('none');
						parent.destroy();
					}
					if (type == 'nalozeno'){
						if (parent.getElement('.confirm')) parent.getElement('.confirm').addClass('none');
						if (parent.getElement('.nalozeno')) parent.getElement('.nalozeno').addClass('none');
						parent.destroy();
					}
					if (type == 'vylozeno'){
						if (parent.getElement('.confirm')) parent.getElement('.confirm').addClass('none');
						if (parent.getElement('.nalozeno')) parent.getElement('.nalozeno').addClass('none');
						if (parent.getElement('.vylozeno')) parent.getElement('.vylozeno').addClass('none');
						parent.destroy();
					}
				}).bind(this)
			});
			save_load_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			save_load_req.send();
		
		});
		
		
		
		
	});
}
moje_zakazky();
//]]>
</script>		