<?php
echo $this->Form->create($sklads);
echo $this->Form->input("id");
//echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);
//echo 'a';
echo '<div style="min-height:700px">';
echo $this->element('modal',['params'=>['zakladni'=>__('Základní'),'import'=>__('Import')]]);
echo $this->element('../Sklads/elements/table');
echo '</div>';

echo $this->Form->button(__('Uložit'), ['class' => 'btn','id'=>'SaveModal']);

echo $this->Form->end();

echo $this->Html->script("fst_system/modal_window");