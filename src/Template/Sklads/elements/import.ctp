<fieldset width="100%" class="clearFix">
		
		<?php echo $this->Form->input("makro_file", ['label' => __("Makro"),'class'=>'','type'=>'file']); ?>
		<?php echo $this->Form->input("bidfood_file", ['label' => __("BidFood"),'class'=>'','type'=>'file']); ?>
		<script type="text/javascript">
		//<![CDATA[
		   $('makro-file').addEvent('change',function(e){
				var file = $('makro-file').files[0];
				var reader = new FileReader();
				//console.log(reader);
					reader.onload = (function(theFile) {
						//console.log(reader.result);
						jsonData = JSON.encode(reader.result);
						//console.log(jsonData);
						//console.log(btoa(reader.result));
						new Request.JSON({
							url:'loadMakro',	
							data:{'jsonData':jsonData},
							onComplete:function(json){
								if (json.r == true){
									window.data_products = json.data;
									$('sklad_items').set('text',JSON.encode(json.sklad_items_list));
									//console.log(window.data_products);
									listProducts();
								} else {
									FstError(json.m);
								}
							}
						}).post($('send_error_form'));
						//console.log(JSON.encode(reader.result));
						//console.log(theFile);
					});
					reader.readAsText(file,'windows-1250');
					//reader.readAsText(file,'iso-8859-1');
		   });
		   $('bidfood-file').addEvent('change',function(e){
				var file = $('bidfood-file').files[0];
				var reader = new FileReader();
				//console.log(reader);
					reader.onload = (function(theFile) {
						//console.log(reader.result);
						jsonData = JSON.encode(reader.result);
						//console.log(jsonData);
						//console.log(btoa(reader.result));
						new Request.JSON({
							url:'loadBidFood',	
							data:{'jsonData':jsonData},
							onComplete:function(json){
								if (json.r == true){
									window.data_products = json.data;
									$('sklad_items').set('text',JSON.encode(json.sklad_items_list));
									//console.log(window.data_products);
									listProducts();
								} else {
									FstError(json.m);
								}
							}
						}).post($('send_error_form'));
						//console.log(JSON.encode(reader.result));
						//console.log(theFile);
					});
					reader.readAsText(file,'windows-1250');
					//reader.readAsText(file,'iso-8859-1');
		   });
		//]]>
		</script>
		</fieldset>