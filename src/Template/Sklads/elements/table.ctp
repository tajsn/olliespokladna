<div class="clear"></div>
	<table class="table-formated" id="sklad_products_list">
		<thead>
			<tr>
				<th>Položka</th>
				<th>Produkt</th>
				<th>DPH</th>
				<th class="price_right">Množ.</th>
				<th class="price_right">Cena jed. bez DPH</th>
				<th class="price_right">Cena jed. s DPH</th>
				<th class="price_right">Cena cel. bez DPH</th>
				<th class="price_right">Cena cel. s DPH</th>
				<th>Jednotka</th>
				<th>Poznámka</th>
				<th>Smazat</th>
			</tr>
				</thead>
	
			<tbody id="products_list"></tbody>
			<tfoot>
				<tr>
					<td>Celkem:</td>
					<td></td>
					<td></td>
					<td></td>
					<td id="t_nakup_price" class="price_right">0</td>
					<td id="t_nakup_price_vat" class="price_right">0</td>
					<td id="t_nakup_price_total" class="price_right">0</td>
					<td id="t_nakup_price_total_vat" class="price_right">0</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tfoot>
	</table>
	<?php //pr($sklads_list); ?>
	<div class="none">
	<div id="sklads_list"><?php echo(isset($sklads_list)?json_encode($sklads_list):'');?></div>
	<?php echo $this->Form->input("save_data", ['label' => false,'class'=>'','type'=>'textarea']); ?>
	</div>

<script type="text/javascript">
//<![CDATA[

	function listProducts(){
		if (window.data_products){
			product_items = JSON.decode($('product_items').get('text'));
			sklad_items = JSON.decode($('sklad_items').get('text'));
			tax_items = JSON.decode($('tax_items').get('text'));
			jednotka_items = JSON.decode($('jednotka_items').get('text'));
			$('products_list').empty();
			//console.log(window.data_products);
			//console.log(sklad_items);
			var t_nakup_price = 0;
			var t_nakup_price_vat = 0;
			var t_nakup_price_total = 0;
			var t_nakup_price_total_vat = 0;
			
			window.data_products.each(function(item,key){
			//console.log(sklad_items[item.sklad_item_id]);
			//console.log(item);
				
				t_nakup_price = t_nakup_price+item.nakup_price.toFloat();
				t_nakup_price_vat = t_nakup_price_vat+item.nakup_price_vat.toFloat();
				t_nakup_price_total = t_nakup_price_total+item.nakup_price_total.toFloat();
				t_nakup_price_total_vat = t_nakup_price_total_vat+item.nakup_price_total_vat.toFloat();
				
				tr = new Element('tr').inject($('products_list'));
				//console.log(item.sklad_item_id);
				td1 = new Element('td').inject(tr);
				link = new Element('a',{'href':'/sklad-items/edit/'+item.sklad_item_id}).set('text',((sklad_items[item.sklad_item_id])?sklad_items[item.sklad_item_id].name:'')).inject(td1);
				//.set('text',((sklad_items[item.sklad_item_id])?sklad_items[item.sklad_item_id].name:'')).inject(tr);
				link.addEvent('click',function(e){
					e.stop();
					var win = window.open(e.target.href, '_blank');
					win.focus();
				});
				
				td2 = new Element('td').set('text',product_items[item.sklad_item_product_id]).inject(tr);
				td3 = new Element('td').set('text',tax_items[item.tax_id]).inject(tr);
				td4 = new Element('td',{'class':'price_right'}).set('text',item.value).inject(tr);
				td5 = new Element('td',{'class':'price_right'}).set('text',item.nakup_price).inject(tr);
				td6 = new Element('td',{'class':'price_right'}).set('text',item.nakup_price_vat).inject(tr);
				td7 = new Element('td',{'class':'price_right'}).set('text',item.nakup_price_total).inject(tr);
				td8 = new Element('td',{'class':'price_right'}).set('text',item.nakup_price_total_vat).inject(tr);
				td9 = new Element('td').set('text',jednotka_items[item.jednotka_id]).inject(tr);
				td10 = new Element('td').set('text',item.note).inject(tr);
				td11 = new Element('td').inject(tr);
				deleteButton = new Element('input',{'type':'button','class':'button small','value':'Smazat'}).inject(td11);
				deleteButton.addEvent('click',function(e){
					if (confirm('Opravdu smazat?')){
						e.stop();
						//delete window.data_products[key];
						window.data_products.splice(key, 1)
						listProducts();
					}
				});
			});
			if(window.data_products.length>0){
				$('save-data').value = JSON.encode(window.data_products);
			} else {
				$('save-data').value = '';
			}
			$('t_nakup_price').set('text',price(t_nakup_price.toFixed(3)));
			$('t_nakup_price_vat').set('text',price(t_nakup_price_vat.toFixed(3)));
			$('t_nakup_price_total').set('text',price(t_nakup_price_total.toFixed(3)));
			$('t_nakup_price_total_vat').set('text',price(t_nakup_price_total_vat.toFixed(3)));
		}
	}
	

window.addEvent('domready',function(){
	dph_list_conf = {
		2:0.1736, // 21
		1:0.1304, // 15
		3:0,	// 0
		4:0.0909 // 10
	};
	dph_list = {
		2:1.12, // 21
		1:1.15, // 15
		3:0,	// 0
		4:1.10 // 10
	};
	//sklad_lists = [];
	sklad_lists = JSON.decode($('sklads_list').get('text'));
	//console.log(sklad_lists);
	if (sklad_lists != null && sklad_lists.length > 0){
		window.data_products = sklad_lists;
		listProducts();
		$$('.clear_save').each(function(item){
			item.value = '';
		});
		
	}
	// 
	if ($('sklad-type-id').value == 1){
		$('SaveModal').set('text','Uložit celý příjem');
	}
	if ($('sklad-type-id').value == 3){
		$('hide_price').addClass('none');
	}
	$('AddProduct').addEvent('click',function(e){
		if ($('sklad-type-id').value != 3 && $('nakup-price').value == ''){
			FstError('Zadejte cenu');
			return false;
		}
		if ($('sklad-item-id').value == '' && $('sklad-item-product-id').value == ''){
			FstError('Vyberte produkt nebo položku');
			return false;
		}
		if (!window.data_products){
			window.data_products = []
		}
		
		data = {}
		$$('.save_item').each(function(item){
			data[item.get('name').replace('[]','')] = item.value;
		});
		window.data_products.push(data);
		console.log(window.data_products);
		listProducts();
		$$('.clear_save').each(function(item){
			item.value = '';
		});
		$('sklad-item-id').focus();
	});
	
	$$('.price').addEvent('change',function(e){
		ks = $('value').value.toFloat();
		tax_id = $('tax-id').value;
		
		if(isNaN(ks)){
			FstError('Nejprve zadejte počet ks');
			e.target.value = '';
			$('value').focus();
			return false;
		}
		
		type = e.target.get('data-type');
		
		nakup_price = $('nakup-price').value.toFloat();
		nakup_price_vat = $('nakup-price-vat').value.toFloat();
		nakup_price_total = $('nakup-price-total').value.toFloat();
		nakup_price_total_vat = $('nakup-price-total-vat').value.toFloat();
		
		if(isNaN(nakup_price)){
			nakup_price = 0;
		}
		
		if(isNaN(nakup_price_vat)){
			nakup_price_vat = 0;
		}
		
		if(isNaN(nakup_price_total)){
			nakup_price_total = 0;
		}
		
		if(isNaN(nakup_price_total_vat)){
			nakup_price_total_vat = 0;
		}
		if (type == 'nakup_price_total') {
			nakup_price_total_vat = nakup_price_total * dph_list[tax_id];
			nakup_price = nakup_price_total / ks;
			nakup_price_vat = nakup_price_total_vat / ks;
		}
		if (type == 'nakup_price_total_vat') {
			nakup_price_total = nakup_price_total_vat - (nakup_price_total_vat * dph_list_conf[tax_id]);
			nakup_price = nakup_price_total / ks;
			nakup_price_vat = nakup_price_total_vat / ks;
		}
		
		if (type != 'nakup_price') nakup_price = nakup_price_vat - (nakup_price_vat * dph_list_conf[tax_id]);
		if (type != 'nakup_price_vat') nakup_price_vat = nakup_price * dph_list[tax_id];
		if (type != 'nakup_price_total') nakup_price_total = nakup_price * ks;
		if (type != 'nakup_price_total_vat') nakup_price_total_vat = nakup_price_vat * ks;
		
		$('nakup-price').value =  nakup_price.toFixed(3);
		$('nakup-price-vat').value = nakup_price_vat.toFixed(3);
		$('nakup-price-total').value = nakup_price_total.toFixed(3);
		$('nakup-price-total-vat').value = nakup_price_total_vat.toFixed(3);
		
		//if (e.hasClass('price_nakup')){
		
		//}
	});
	
	$('sklad-item-id').addEvent('change',function(e){
		sklad_items = JSON.decode($('sklad_items').get('text'));
		if (sklad_items[e.target.value]){
			(function(){
			$('value').focus();
			}).delay(500);
		}
	});
	$('sklad-item-product-id').addEvent('change',function(e){
		if (e){
			$('sklad-item-id').value = '';
		}
	});
	$('sklad-item-id').addEvent('change',function(e){
		if (e){
			$('sklad-item-product-id').value = '';
		}
	});
	/*
	$('value').addEvent('change',function(e){
		sklad_items = JSON.decode($('sklad_items').get('text'));
		if (sklad_items[$('sklad-item-id').value]){
			$('nakup-price').value = sklad_items[$('sklad-item-id').value].nakup_price;
			$('tax-id').value = sklad_items[$('sklad-item-id').value].tax_id;
			$('nakup-price-total').value = sklad_items[$('sklad-item-id').value].nakup_price * $('value').value.toInt();
		}
	});
	*/
});
//]]>
</script>