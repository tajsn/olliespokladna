<style>
.full select.bigheight {
	width:100%;
	height:300px;

}
.price_right  {
	text-align:right;
}
.clearFix {
	clear:both;
}
</style>
<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		
		
		<div class="sll full"> 
		<?php //pr($sklad_group_list); ?>
		<?php echo $this->Form->input("group_id", ['label' => __("Skupina"),'options'=>$sklad_group_list,'class'=>'search_selecta clear_save']); ?>
		<?php //echo $this->Form->input("dodavatel_id", ['label' => __("Dodavatel"),'options'=>$dodavatel_list,'class'=>'search_selecta clear_save']); ?>
		<div class="clear" ></div>
		<label>Položka</label>
		<?php echo $this->Form->input("sklad_item_id", ['label' => __("Položka"),'options'=>$sklad_items_list,'empty'=>true,'class'=>'search_selecta save_item clear_save bigheight','multiple'=>true,'label'=>false]); ?>
		</div>
		<div class="slr full none"> 
		<label>Produkt</label>
		<?php echo $this->Form->input("sklad_item_product_id", ['label' => __("Produkt"),'options'=>$product_list,'empty'=>true,'class'=>'search_selecta save_item clear_save bigheight','multiple'=>true,'label'=>false]); ?>
		
		</div>
		<div class="none">
		<?php echo $this->Form->input("sklad_type_id", ['label' => __("Typ"),'options'=>$sklad_type_list,'empty'=>true,'class'=>' save_item','value'=>(isset($this->request->query['type-id'])?$this->request->query['type-id']:$sklads->sklad_type_id)]); ?>
		</div>
		
		<div id="tax_items" class="none"><?php echo json_encode($price_tax_list);?></div>
		<div id="jednotka_items" class="none"><?php echo json_encode($sklad_jednotka_list);?></div>
		<div id="product_items" class="none"><?php echo json_encode($product_list);?></div>
		<div id="sklad_items" class="none"><?php echo json_encode($sklad_items);?></div>
		
		
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("tax_id", ['label' => __("DPH"),'class'=>' save_item','options'=>$price_tax_list]); ?>
		<?php echo $this->Form->input("value", ['label' => __("Množství"),'class'=>'float price_ks save_item clear_save']); ?>
		<div id="hide_price">
		<?php echo $this->Form->input("nakup_price", ['label' => __("Cena jed. bez DPH"),'class'=>'float price save_item clear_save','data-type'=>'nakup_price']); ?>
		<?php echo $this->Form->input("nakup_price_vat", ['label' => __("Cena jed. s DPH"),'class'=>'float price  save_item clear_save','data-type'=>'nakup_price_vat']); ?>
		<?php echo $this->Form->input("nakup_price_total", ['label' => __("Cena celkem bez DPH"),'class'=>'float price  save_item clear_save','data-type'=>'nakup_price_total']); ?>
		<?php echo $this->Form->input("nakup_price_total_vat", ['label' => __("Cena celkem s DPH"),'class'=>'float price save_item clear_save','data-type'=>'nakup_price_total_vat']); ?>
		</div>
		<?php echo $this->Form->input("jednotka_id", ['label' => __("Jednotka"),'class'=>' save_item','options'=>$sklad_jednotka_list]); ?>
		<?php echo $this->Form->input("faktura", ['label' => __("Faktura"),'class'=>' save_item']); ?>
		<?php echo $this->Form->input("note", ['label' => __("Poznámka"),'class'=>' save_item clear_save','type'=>'textarea']); ?>
		<?php echo $this->Form->input("Přidat", ['label' => false,'class'=>'','type'=>'button','id'=>'AddProduct']); ?>
	
	</div>
	
</fieldset>
<?php /* ?>
<fieldset>
	<legend><?php echo __("Nákupní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("nakup_price", ['label' => __("Nákupní cena"),'class'=>'float']); ?>
		<?php echo $this->Form->input("tax_id", ['label' => __("DPH"),'class'=>'','options'=>$price_tax_list]); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("nakup_prumer", ['label' => __("Nákupní průměr"),'class'=>'float']); ?>
		
	</div>
</fieldset>
<?php */ ?>
<script type="text/javascript">
//<![CDATA[
   $('group-id').addEvent('change',function(e){
		new Request.JSON({
			url:'getSkladItems/'+$('group-id').value,
			onComplete:function(json){
				if (json.data){
					$('sklad-item-id').empty();
					Object.each(json.data,function(item,k){
						new Element('option',{'value':k}).set('text',item).inject($('sklad-item-id'));
					});
				}
			}
		}).send();
		
   });
   $('group-id').fireEvent('change');
//]]>
</script>
