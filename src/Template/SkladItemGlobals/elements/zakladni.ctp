<fieldset id="sklad_item">
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php //echo $this->Form->input("dodavatel_id", ['label' => __("Dodavatel"),'options'=>$dodavatel_list,'empty'=>true,'class'=>'fst_select','data-url'=>'/skladItems/dodavatelAdd/']); ?>
		<?php echo $this->Form->input("name", ['label' => __("Název")]); ?>
		<div class="show_dodavatel dodavatel1">
		<?php //echo $this->Form->input("ean", ['label' => __("Ean")]); ?>
		</div>
		<div class="show_dodavatel dodavatel2">
		<?php //echo $this->Form->input("bidfood_id", ['label' => __("BidFood ID"),'type'=>'text']); ?>
		</div>
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("jednotka_id", ['label' => __("Jednotka"),'options'=>$sklad_jednotka_list]); ?>
		<?php //echo $this->Form->input("jednotka_id_dodavatel", ['label' => __("Jednotka dodavatel")]); ?>
		<?php //echo $this->Form->input("jednotka_prevod", ['label' => __("Jednotka prevod"),'type'=>'text']); ?>
		<?php echo $this->Form->input("group_id", ['label' => __("Skupina"),'options'=>$sklad_group_list,'empty'=>true,'class'=>'fst_select','data-url'=>'/skladItems/skladGroupAdd/']); ?>
		
	</div>
</fieldset>

<fieldset>
	<legend><?php echo __("Nákupní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("min", ['label' => __("Min množství"),'class'=>'float']); ?>
		<?php echo $this->Form->input("max", ['label' => __("Max množství"),'class'=>'float']); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("tax_id", ['label' => __("DPH"),'class'=>'','options'=>$price_tax_list]); ?>
		
	</div>
</fieldset>
<script type="text/javascript">
//<![CDATA[

/*
$('dodavatel-id').addEvent('change',function(e){
	
	$('sklad_item').getElements('.show_dodavatel').each(function(item){
		item.addClass('none');
		if (item.hasClass('dodavatel'+$('dodavatel-id').value)){
			item.removeClass('none');
		}
		//$('sklad_item').getElement('.dodavatel'+e.target.value).removeClass('none');
	});
	
});
$('dodavatel-id').fireEvent('change');
*/
//]]>
</script>
