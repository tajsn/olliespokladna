<style>
#print_ucet#print_order_part,
#print_order_part#print_order_part,
.print_data#print_order_part {
  left: 300px;
  left: -500px;
}
#print_ucet .header,
#print_order_part .header,
.print_data .header,
#print_ucet .footer,
#print_order_part .footer,
.print_data .footer {
  background: none;
  color: #000;
  text-align: center;
  font-size: 12px;
  line-height: 11px;
}
#print_ucet label,
#print_order_part label,
.print_data label {
  display: inline-block;
  width: 33%;
  padding: 0;
  margin: 0;
  float: none;
  line-height: 13px;
  font-size: 12px;
}
#print_ucet span,
#print_order_part span,
.print_data span {
  font-size: 12px;
}
#print_ucet table,
#print_order_part table,
.print_data table {
  width: 50%;
  margin: 0;
}
#print_ucet table tr th,
#print_order_part table tr th,
.print_data table tr th,
#print_ucet table tr td,
#print_order_part table tr td,
.print_data table tr td {
  padding: 2px;
  background: #fff;
  font-size: 12px;
  border: 0;
}
#print_ucet table tr th.bold,
#print_order_part table tr th.bold,
.print_data table tr th.bold {
  font-weight: bold;
}
#print_ucet table.total tr th,
#print_order_part table.total tr th,
.print_data table.total tr th {
  font-weight: bold;
}
#print_ucet .copyright,
#print_order_part .copyright,
.print_data .copyright {
  font-size: 6px;
}
#print_ucet tr.group td,
#print_order_part tr.group td,
.print_data tr.group td {
  border-bottom: 1px solid #000;
}
#print_ucet .group_name,
#print_order_part .group_name,
.print_data .group_name {
  font-weight: bold;
  font-size: 12px;
}
#print_ucet .group_name.group1,
#print_order_part .group_name.group1,
.print_data .group_name.group1 {
  font-weight: bold;
}
#print_ucet .group_name.group2,
#print_order_part .group_name.group2,
.print_data .group_name.group2 {
  font-weight: bold;
  font-size: 12px;
  padding-top: 5px;
}
#print_ucet .dph_line td,
#print_order_part .dph_line td,
.print_data .dph_line td {
  border-bottom: 1px solid #ccc;
}
#print_ucet .price,
#print_order_part .price,
.print_data .price {
  text-align: right;
}
table tr th {text-align:left;}

</style>
<h2>Vygenerována uzávěrka</h2>
<?php echo $data ?>