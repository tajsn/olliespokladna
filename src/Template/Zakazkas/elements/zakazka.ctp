<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<label><?php echo __("Datum / čas naložení")?></label>
		<?php echo $this->Form->input("date_nalozeni", ['label' => false,'type'=>'text','dateFormat' => 'D.M.Y','class'=>'date half2','placeholder'=>__('První dat.')]); ?>
		<?php echo $this->Form->input("date_nalozeni2", ['label' => false,'type'=>'text','dateFormat' => 'D.M.Y','class'=>'date half2','placeholder'=>__('Druhé dat.')]); ?>
		<?php echo $this->Form->input("time_nalozeni", ['label' => false,'class'=>'half2','placeholder'=>__('Čas')]); ?>
		<label><?php echo __("Datum / čas vyložení");?></label>
		<?php echo $this->Form->input("date_vylozeni", ['label' => false,'class'=>'date half2','type'=>'text','dateFormat' => 'D.M.Y','placeholder'=>__('První dat.')]); ?>
		<?php echo $this->Form->input("date_vylozeni2", ['label' => false,'class'=>'date half2','type'=>'text','dateFormat' => 'D.M.Y','placeholder'=>__('Druhé dat.')]); ?>
		<?php echo $this->Form->input("time_vylozeni", ['label' => false,'class'=>'half2','type'=>'text','placeholder'=>__('Čas')]); ?>
		<label><?php echo __('Typ');?></label>
		<?php echo $this->Form->select("type",$zakazka_type, []); ?>
		<?php 
		/*
		if (isset($zakazka->id)){ 
			echo '<label>'.__('Kód pro scanování').'</label>';
			echo $this->Fastest->qr_code($zakazka->id,200);
			echo $this->Html->link(__('Tisk'),'/zakazkas/tisk_qr/'.$zakazka->id,['title'=>__('Tisk'),'class'=>'button','target'=>'_blank']);
		}
		*/
		?>
		
	</div>
	<div class="slr"> 
		<label><?php echo __('Hmot. (kg) / palet (ks)');?></label>
		<?php echo $this->Form->input("weight", ['label' => false,'class'=>'integer half','placeholder'=>__('hmotnost (kg)')]); ?>
		<?php echo $this->Form->input("size", ['label' => false,'class'=>'integer half','placeholder'=>__('palet (ks)')]); ?>
		<label><?php echo __('Stav');?></label>
		<?php echo $this->Form->select("stav",$stav_list, []); ?>
		<?php echo $this->Form->input("type_doklad", ['label'=>__('Typ dokladu'),'options'=>$type_doklad[$system_id]]); ?>
		<?php 
		if(isset($zakazka->driver_id)){
			echo '<label>'.__('Řidič').'</label><var>'.$dispecer_list[$zakazka->driver_id].'</var><div class="clear"></div>';
			echo '<label>'.__('Auto').'</label><var>'.$car_list[$zakazka->car_id].'</var><br class="clear" />';
		}
		?>
		
		
	</div>
	<div class="clear"></div>
	<?php echo $this->Form->input("anomalie", ['label' => __("Anomálie"),'class'=>'long']); ?>
		
	
	<div class="clear"></div>
</fieldset>
<fieldset id="change_price">
	<legend><?php echo __("Cena zakázky")?></legend>
	<div class="sll"> 
		<label><?php echo __('Typ');?></label>
		<?php echo $this->Form->select("price_type",$price_type[$system_id], ['class'=>'change_price','data-type'=>'price_type']); ?>
		<?php echo $this->Form->input("price", ['label' => __("Cena bez DPH"),'class'=>'decimal change_price','data-type'=>'price','placeholder'=>__('v Kč / EUR')]); ?>
		<?php //echo $this->Form->input("price_eu", ['label' => __("Cena EU bez DPH"),'class'=>'decimal change_price','data-type'=>'price_eu']); ?>
		<?php echo $this->Form->hidden("dph", ['class'=>'decimal change_price','value'=>dphType,'data-type'=>'dph']); ?>
		<?php echo $this->Form->hidden("dph2", ['class'=>'decimal change_price','value'=>dphType2,'data-type'=>'dph2']); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("price_kurz", ['value'=>(empty($zakazka->price_kurz)?$kurz_eu:$zakazka->price_kurz),'data-type'=>'price_kurz','class'=>'change_price']); ?>
		<?php echo $this->Form->input("price_with_tax", ['label' => __("Cena s DPH"),'class'=>'decimal change_price','data-type'=>'price_with_tax','placeholder'=>__('v Kč / EUR')]); ?>
		<?php //echo $this->Form->input("price_eu_with_tax", ['label' => __("Cena EU s DPH"),'class'=>'decimal change_price','data-type'=>'price_eu_with_tax']); ?>
	
	</div>
	<div class="clear"></div>
	
</fieldset>
<?php echo $this->element('../Zakazkas/elements/dispecer'); ?>

<script type="text/javascript">
//<![CDATA[
function change_price(el){
	var change_list = $(el).getElements('.change_price');
	change_list.addEvent('change',function(e){
		price = {}
		current_type = e.target.get('data-type');
		
		change_list.each(function(item){
			price[item.get('data-type')] = item.value;
			
		});
		if (current_type == 'price'){
			$('price-with-tax').value = e.target.value * price['dph'];
		}
		if (current_type == 'price_with_tax'){
			$('price').value = e.target.value - e.target.value * price['dph2'];
		}
		
		if (price['price_type'] == 1){
			if ($('price-eu-with-tax').value > 0){
				$('price-with-tax').value = $('price-eu-with-tax').value * price['price_kurz'];
				$('price').value = $('price-eu').value * price['price_kurz'];
			}
		}
		
		if (price['price_type'] == 2 || price['price_type'] == 3 || price['price_type'] == 4){
			$('price-eu-with-tax').value = $('price-with-tax').value / price['price_kurz'];
			$('price-eu').value = $('price').value / price['price_kurz'];
		}
		
		if (price['price_type'] == 3 || price['price_type'] == 4){
			$('price-with-tax').value = '';
			$('price-eu-with-tax').value = '';
		}
		//console.log(price);
		//console.log(change_list);
	});
	
}
change_price('change_price');
//]]>
</script>