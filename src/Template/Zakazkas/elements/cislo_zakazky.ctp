<fieldset>
<?php //pr(); ?>
	<legend><?php echo __("Čísla zakázky")?></legend>
	<div class="sll">
		<label><?php echo __('Čís. inter / kontrola')?></label>
		<?php echo $this->Form->input("code_int", ['label' => false,'class'=>'half','placeholder'=>__('Interní číslo zakázky')]); ?>
		<?php echo $this->Form->input("code_int_check", ['label' =>false,'class'=>'half','options'=>$ne_ano_duplicita]); ?>
		
	</div>
	<div class="slr"> 
		<?php //echo $this->Form->input("name", ['label' => __("Číslo zakázky")]); ?>
		<?php echo $this->Form->input("id_tmp", ['label' => __("Číslo zakázky"),'value'=>$zakazka->id,'disabled'=>true,'placeholder'=>__('Bude doplněno po uložení')]); ?>
		
	</div>
</fieldset>