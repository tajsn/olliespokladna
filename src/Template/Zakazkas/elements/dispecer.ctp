
<fieldset>
	<legend><?php echo __("Dispečer zadavatele")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("dispecer_name", ['label' => __("Jméno dispečera")]); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("dispecer_tel", ['label' => __("Telefon")]); ?>
		<?php echo $this->Form->input("dispecer_mobile", ['label' => __("Mobil")]); ?>
	
	</div>
	<div class="clear"></div>
</fieldset>