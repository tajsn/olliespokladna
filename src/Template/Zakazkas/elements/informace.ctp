<div id="readonly_div">
<fieldset>
	<legend><?php echo __("Informace o zakázce")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("helios", ['label' => __("Exp. Helios"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_load", ['label' => __("Naloženo"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_unload", ['label' => __("Vyloženo"),'options'=>$ano_ne,'empty'=>false,'class'=>'read']); ?>
		<?php echo $this->Form->input("driver_unload_sklad", ['label' => __("Vyloženo sklad"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_unload_another", ['label' => __("Vyloženo jiné"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("spz", ['label' => __("SPZ"),'class'=>'read','empty'=>false]); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("driver_send", ['label' => __("Posláno naložení"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_send2", ['label' => __("Posláno vyložení"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_confirm", ['label' => __("Potvrzeno naložení"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_confirm2", ['label' => __("Potvrzeno vyložení"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_confirm_sklad", ['label' => __("Potvrzeno sklad"),'options'=>$ano_ne,'empty'=>false]); ?>
		<?php echo $this->Form->input("driver_confirm_another", ['label' => __("Potvrzeno jiné"),'options'=>$ano_ne,'empty'=>false]); ?>
	
	</div>
	<div class="clear"></div>
</fieldset>
<fieldset>
	<legend><?php echo __("Data zakázky")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("driver_date_load", ['label' => __("Naložení"),'type'=>'text','class'=>'date']); ?>
		<?php echo $this->Form->input("driver_date_load_another", ['label' => __("Naložení ost."),'type'=>'text','class'=>'date']); ?>
		<?php echo $this->Form->input("driver_date_unload", ['label' => __("Vyložení"),'type'=>'text','class'=>'date_time read']); ?>
		<?php echo $this->Form->input("driver_date_unload_another", ['label' => __("Vyložení jiné"),'type'=>'text','class'=>'date']); ?>
		<?php echo $this->Form->input("sklad_date_unload", ['label' => __("Naložení sklad"),'type'=>'text','class'=>'date']); ?>
		<?php echo $this->Form->input("sklad_date_unload", ['label' => __("Vyložení sklad"),'type'=>'text','class'=>'date']); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("driver_date_confirm", ['label' => __("Potvrzení nal."),'type'=>'text','class'=>'date']); ?>
		<?php echo $this->Form->input("driver_date_confirm2", ['label' => __("Potvrzení vyl."),'type'=>'text','class'=>'date']); ?>
		<?php echo $this->Form->input("driver_date_confirm_sklad", ['label' => __("Potvrzení sklad"),'type'=>'text','class'=>'date']); ?>
		<?php echo $this->Form->input("driver_date_confirm_another", ['label' => __("Potvrzení jiné"),'type'=>'text','class'=>'date']); ?>
	</div>
	<div class="clear"></div>
</fieldset>
</div>
<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
$('readonly_div').getElements('input,select').each(function(item){
	//console.log(item);
	if (!item.hasClass('read'))
	item.setProperty('disabled','disabled');
});
});
//]]>
</script>