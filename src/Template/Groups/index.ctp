<?php
echo $this->Html->link(__("Vytvořit skupinu"), ['controller' => 'groups', 'action' => 'add'], ['class' => 'btn', 'ajax' => true]);
if(isset($groups)){
  echo '<div>';
  foreach($groups as $g){
    echo '<div>';
    echo '<h2>'.$g["name"].'</h2>';
    echo __('<strong>vlastník:</strong>').$g["user"]["name"];
    if(count($g["members"]) > 0) {
      echo __("<strong>členové:</strong>").'<br />';
      foreach ($g["members"] as $m) {
        echo $m["user"]["name"];
      }
    }
    echo $this->Html->link(__("Spravovat"), ["controller" => "groups", "action" => "view", $g["id"]], ["ajax" => true, "class" => "btn"]);
    echo '</div>';
  }
  echo '</div';
}
else{
  echo '<p>'.__("Vytvořte svou první skupinu a pozvěte své kolegy ke spolupráci").'</p>';
}
