<?php

echo $this->Form->create($group);
  //$this->Form->unlockField('invation');
  echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);
  echo $this->Form->input("name", ['label' => __("Název skupiny")]);

  echo '<h2>'.__("Pozvěte kolegy do skupiny").'</h2>';
  echo '<p>'.__("Pošlete kolegům pozvánku ke spolupráci, následně budou vyzváni k registraci nebo u existujících účtů ke spolupráci").'</p>';
  echo '<div id="invations">';
  echo $this->Form->input("invation.0", ['type' => 'text', 'label' => __('Zadejte email'), 'q' => 0]);
  echo '</div>';
  echo $this->Html->link(__("Pozvat další"), "#", ['id' => 'addInvation']);

echo $this->Form->button(__("Uložit"), ['class' => 'btn']);
echo $this->Form->end();

echo $this->Html->script("Groups/add");