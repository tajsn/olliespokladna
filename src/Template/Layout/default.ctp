<?php  
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
if ($_SERVER['REMOTE_ADDR'] == '89.102.102.21' && isset($current_url) && $current_url == 'order'){
	$manifest =  'manifest="/offline.manifest"';
	
}
?>
<!DOCTYPE html>
<html <?php //echo(isset($manifest)?$manifest:''); ?>> 
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="all,index,follow" />
	<meta name='author' content='Fastest Solution, 2016 All Rights Reserved url: http://www.fastest.cz' />
	<meta name='dcterms.rights' content='Fastest Solution, 2016 All Rights Reserved url: http://www.fastest.cz' />
	<link rel="manifest" href="/manifest.json">
	
    <title>
        <?= (isset($title)?$title:appName) ?>
    </title>
    <?= $this->Html->meta('icon') ?>
	<link rel="icon" href="/css/layout/favicon.png" />
    <link rel="stylesheet" type="text/css" href="/css/fonts/fontawesome-all.min.css"  media="screen" />
    <link rel="stylesheet/less" type="text/css" href="<?php echo $cssUri;?>"  media="screen" />
	
    <?= $this->Html->script("less"); ?>
	<?php /* ?>
    <link rel="stylesheet/less" type="text/css" href="/css/print_ucet.css"  media="screen" />
	<script>
	  less = {
		env: "development"
	  };
	</script> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
   <script src="http://scripts.fastesthost.cz/js/mootools1.4/core1.6.js"></script>
	<script src="http://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js"></script>
	
	<script src="/js/fst_pokladna/fst_modal.js"></script>
	<script src="/js/node/bundle.js"></script>
   <script src="/js/websocket/client.js"></script>
	*/?>

    
   <?= $this->Html->script($jsUri); ?>
    <script src="/js/FstEscPos/table_client.js"></script>
	<script src="/js/FstEscPos/EscPosClient.js"></script>
	
	
	<script src="/js/fst_pokladna/fst_uzaverka.js"></script>
	<script src="/js/fst_pokladna/fst_table.js"></script>
	<script src="/js/fst_pokladna/fst_print.js"></script>
	<script src="/js/fst_pokladna/fst_pay.js"></script>
	<script src="/js/fst_system/fst_system.js"></script>
   
	
	

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	<?php
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
	
	<?php if ($current_url == 'order' || $current_url == ''){ ?>
	<script src="/js/fst_pokladna/fst_pokladna.js"></script>
   <?php } ?>
	
</head>
<body id="body" class="<?php echo strtolower($this->request->controller).' '.$this->request->action;?>" data-controller="<?php echo strtolower($this->request->controller)?>">
    <?= $this->Flash->render() ?>
	<?php 
	if (isset($loggedUser)){ ?>
		<?php if(!isset($nolayout)) echo $this->element('top-menu');?>
		<?php echo $this->element('logged_info');?>
	<?php } ?>
    <section id="content">
            <?= $this->fetch('content') ?>
    </section>
	<div id="addon"></div>
	<div id="page_url" class="none"><?php echo 'http://'+$_SERVER['HTTP_HOST'];?></div>
	<?php echo $this->element('keyboard');?>
	<?php echo $this->element('send_error');?>
	<?php echo $this->Form->input('system_id',array('id'=>'SystemId','value'=>(isset($loggedUser['system_id'])?$loggedUser['system_id']:''),'type'=>'text','type'=>'hidden'));?>
	<div id="printer_list" class="none"></div>
	<div id="websocket_status"></div>
	
</body>
</html>
