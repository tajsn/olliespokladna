<!DOCTYPE html>
<html> 
<head>
    <?= $this->Html->charset() ?>
<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>
</head>
<body>
<?= $this->fetch('content') ?>
<?= $this->Flash->render() ?>
</body>
</html>