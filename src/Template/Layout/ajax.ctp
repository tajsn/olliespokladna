<!DOCTYPE html>
<html>
<head>
  <?= $this->Html->charset() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
    <?= $title ?>
  </title>
  <?php /* ?>
  <script type="text/javascript">less = { env: 'development' };</script>
  <?= $this->Html->css("ajax.css", ["rel" => "stylesheet/less"]); ?>
  <?php */ ?>
  <?= $this->Html->meta('icon') ?>
  
  <link rel="stylesheet/less" type="text/css" href="<?php echo $cssUri;?>"  media="screen" />
  <?php /*<?= $this->Html->css("default.css", ["rel" => "stylesheet/less"]); ?>*/?>

  
	<?= $this->Html->script("less"); ?>
	<?= $this->Html->script($jsUri); ?>
	<script src="/js/fst_pokladna/fst_table.js"></script>
	<script src="/js/fst_pokladna/fst_print.js"></script>
	<script src="/js/fst_pokladna/fst_pay.js"></script>
	<script src="/js/fst_system/fst_select.js"></script>
	<script src="/js/fst_system/fst_system.js"></script>
  
  <?php if ($current_url == 'order' || isset($fst_pokladna)){ ?>
	<script src="/js/fst_pokladna/fst_uzaverka.js"></script>
	<script src="/js/fst_pokladna/fst_pokladna.js"></script>
    <script src="/js/websocket/client.js"></script>
  <?php } ?>
  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>
  <?php
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
</head>
<body class="ajax <?= strtolower($this->request->controller."-".$this->request->action) ?>">
<div class="header"><h1><?= $title ?></h1></div>
<div class="container clearfix">
	<div id="modal_in">
		<?php //pr($current_url); ?>
		<?= $this->fetch('content') ?>
	</div>
</div>
<?php echo $this->Form->input('system_id',array('id'=>'SystemId','value'=>$loggedUser['system_id'],'type'=>'text','type'=>'hidden'));?>
<div id="printer_list" class="none"></div>
</body>
</html>