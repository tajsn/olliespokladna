<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $title ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet/less" type="text/css" href="<?php echo $cssUri;?>"  media="screen" />
    
    <?= $this->Html->script("less"); ?>
    <?= $this->Html->script($jsUri); ?>
    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body class="<?php echo strtolower($this->request->controller).' '.$this->request->action;?>">
    <div class="page-wrap">
        <header id="top-header">
            <div class="container clearfix">
                <?php echo $this->element('logo-block');?>
                <?php echo $this->element('user-menu');?>
            </div>
        </header>

        <nav id="top-menu">
            <div class="container clearfix">
                <?php echo $this->element('top-menu');?>
            </div>
        </nav>

        <div id="sub-header">
            <div class="container clearfix">
                <h1><?php echo isset($page_caption)?$page_caption:'';?></h1>
            </div>
        </div>

        <section id="content">
            <div class="container clearfix">
                <?php echo $this->fetch('content'); ?>
            </div>
        </section>
        <?php echo $this->Flash->render(); ?>
    </div>     
        
    <footer>
        <div id="top-footer" class="footer">
            <div class="container clearfix">
                <?php echo $this->element('top-footer');?>
            </div>    
        </div>
        <div id="sub-footer" class="footer">
            <div class="container clearfix">
                <?php echo $this->element('sub-footer');?>
            </div> 
        </div>
    </footer>

</body>
</html>
