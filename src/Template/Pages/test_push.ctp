<?php /* ?>
<script src="//messaging-public.realtime.co/js/2.1.0/ortc.js"></script>
<script src="/js/push/manager.js"></script>  
<?php */ ?>
<script type="text/javascript">
//<![CDATA[
/*
window.addEvent('load',function(){
	var chromePushManager = new ChromePushManager('./js/push/service-worker.js', function(error, registrationId){
		console.log(error);
		console.log(registrationId);
	});
	console.log(navigator.serviceWorker);

});
*/
loadOrtcFactory(IbtRealTimeSJType, function (factory, error) {
 if (error != null) {
  alert("Factory error: " + error.message);
 } else {
   if (factory != null) {
    // Create Cloud Messaging client
    var client = factory.createClient();
 console.log(client);
    // Set client properties
    client.setConnectionMetadata('Some connection metadata');
    client.setClusterUrl('http://ortc-developers.realtime.co/server/2.1/');
 
    client.onConnected = function (theClient) {
      // client theClient is connected
 
      theClient.subscribe('myChannel', true,
               function (theClient, channel, msg) {
                 console.log("Received message:", msg);                 
               });
    };
 
    client.onSubscribed = function (theClient, channel) {
       // Subscribed to the channel 'channel');
       // Send a message to the channel
       theClient.send(channel, 'Hello World');
    };
 
    client.connect('[Z2E0Nf]', 'myAuthenticationToken');
   }
 }
});
//]]>
</script>