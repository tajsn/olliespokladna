<div id="map_area">
	<div id="panel">
	<p class="tips">Nejprve vytvořte oblast a následně klikněte na Uložit oblasti.</p>
	<strong>Barva oblasti</strong><br />
	<div id="color-palette"></div>
	<br class="clear" />
	<br class="clear" />
	<div>
		<form action="/google_areas_save/" method="post" id="FormArea" class="form">
			<?php echo $this->Form->input('area_coords',['type'=>'hidden']); ?>
			<?php echo $this->Form->input('area_color',['type'=>'hidden']); ?>
			<strong>Název oblasti:</strong><br />
			<?php echo $this->Form->input('area_name',['class'=>'text','placeholder'=>'Název oblasti','style'=>'width:100%','label'=>false]); ?>
        </form>
		<div class="buttons">
			<button id="delete-button" class="button small">Smazat vybranou oblast</button>
			<button id="rename-button" class="button small">Přejmenovat oblast</button>
			<button id="gen_coords"  class="button small red">Uložit oblasti</button>
		</div>
     </div>
	
	<div id="coords" class="none">
		Nejprve vytvořte oblasti, nebo je upravte a následně klikněte na Uložit oblasti.
	</div>
	  
    </div>
	
	<div id="win_name" class="none">
		<div id="win_name_over"></div>
		<div id="win_name_inner">
			
        
		</div>
	</div>
	<div id="maparea_canvas"></div>
	
	<div id="marea_map_data_load" class="none">
	<?php /* ?>
	{"ic4hb81f":{"name":"aaa","color":"#1E90FF","coords":"}_otH}|jvAjbdB~nr@xs^e~}D"},"ic4hb81g":{"name":"vvfd","color":"#32CD32","coords":"qphqHmvmeBhfeBn}nD~{aAk}uG"}}
	<?php */ ?>
	<?php 
	if (isset($load->coords) && !empty($load->coords)){ 
		echo $load->coords;
	}
	?>
	</div>

</div>

<script type="text/javascript">
//<![CDATA[
   
window.addEvent('load', function () {
	loadScript();
	

});

//]]>
</script>