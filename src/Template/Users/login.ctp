
<div id="login_box">
<?php echo '<h1>'.appName.'</h1>';?>
<?= $this->Flash->render('auth') ?>
    <?= $this->Form->create(null,['url' => ['action' => 'login'],'class'=>'login','id'=>'LoginForm']) ?>
    <fieldset>
        <legend><?= __('Přihlašte se prosím') ?></legend>
        <ul id="login_ciselnik">
			<?php for ($i = 0; $i <= 9; $i++) { ?>
			<li data-id="<?php echo $i?>"><?php echo $i;?></li>
			<?php } ?>
		</ul>
		<?php //pr(configTerminalId); ?>
		<?php //pr($system_id); ?>
		<?php 
		if(isset($_GET['superadmin'])){
			$type = 'select';
		} else {
			$type = 'hidden';
		}
		?>
		<?= $this->Form->input('terminal_id', ['label' => __('Terminál'),'options'=>$terminal_list,'value'=>configTerminalId,'type'=>$type]); ?>
		<div id="hide_code">
		<?= $this->Form->input('code', ['label' => __('Váš kód'),'type'=>'password']); ?>
		</div>
		<div id="hide_password">
		<?= $this->Form->input('username', ['label' => __('Váš email')]) ?>
        <?= $this->Form->input('password', ['label' => __('Váše heslo')]) ?>
		</div>
    </fieldset>
    <?= $this->Form->button(__('Přihlásit se'),['id'=>'LoginButton']); ?>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
//<![CDATA[
function login(){
	window.addEvent('domready',function(){
		if ($('LoginButton').hasClass('preloader')){
			button_preloader($('LoginButton'));
		
		}
		
		$('LoginForm').addEvent('submit',function(){
			button_preloader($('LoginButton'));
		});
		$('code').value = '';
		$('login_ciselnik').getElements('li').addEvent('click',function(e){
			$('code').value += this.get('data-id'); 
			$('code').fireEvent('change',$('code'));
			$('code').focus();
		});
		
		$('username').addEvent('change',function(e){
			if (this.value != ''){
				$('hide_code').addClass('none');
				$('code').setProperty('disabled');
			} else {
				$('hide_code').removeClass('none');
				$('code').removeProperty('disabled');
			}
		});
		
		$('code').addEvent('change',function(e){
			if (this.value != ''){
				$('hide_password').addClass('none');
				$('username').setProperty('disabled');
				$('password').setProperty('disabled');
			} else {
				$('hide_password').removeClass('none');
				$('username').removeProperty('disabled');
				$('password').removeProperty('disabled');
			
			}
		});
	});
}   
login();
//]]>
</script>