<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input('username', ['label' => __('Uživ. jméno')]); ?>
		<?php //echo $this->Form->input(__("password_tmp"), ['type' => 'password','label'=>false]);?>
		<?php echo $this->Form->input(__("password"), ['type' => 'password', 'label' => 'Heslo','class'=>'password']);?>
		<?php echo $this->Form->input(__("password2"), ['type' => 'password', 'label' => 'Heslo znovu','class'=>'password2']);	?>
		
	    
	</div>
	<div class="slr"> 
		<label><?php echo __('Skupina');?></label>
		<?php echo $this->Form->select('group_id', $group_list,['empty' => false]); ?>
		<?php echo $this->Form->input('code', ['label' => __('Kód')]); ?>
		
	</div>
	
	<div class="clear"></div>
</fieldset>
<fieldset>
	<legend><?php echo __("Údaje uživatele")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input('prijmeni', ['label' => __('Příjmení')]); ?>
		
	    
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input('jmeno', ['label' => __('Jméno')]); ?>
		
	</div>
	
	<div class="clear"></div>
</fieldset>
