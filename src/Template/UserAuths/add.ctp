<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List User Auths'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="userAuths form large-9 medium-8 columns content">
    <?= $this->Form->create($userAuth) ?>
    <fieldset>
        <legend><?= __('Add User Auth') ?></legend>
        <?php
            echo $this->Form->input('user_id');
            echo $this->Form->input('auth');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
