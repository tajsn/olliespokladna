<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Auth'), ['action' => 'edit', $userAuth->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Auth'), ['action' => 'delete', $userAuth->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userAuth->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Auths'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Auth'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userAuths view large-9 medium-8 columns content">
    <h3><?= h($userAuth->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($userAuth->id) ?></td>
        </tr>
        <tr>
            <th><?= __('User Id') ?></th>
            <td><?= $this->Number->format($userAuth->user_id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Auth') ?></h4>
        <?= $this->Text->autoParagraph(h($userAuth->auth)); ?>
    </div>
</div>
