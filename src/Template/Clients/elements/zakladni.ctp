<?php  
if (isset($zakazka->client_id)){
	$real_id = $zakazka->client_id;
}  
if (isset($clients->id)){
	$real_id = $clients->id;
}
?>
<?php echo $this->Form->input((isset($zakazka)?'client.':'')."id",['class'=>'add_adr client_id client_adopt','data-type'=>'client_id','value'=>(isset($real_id)?$real_id:'tmp_'.time())]); ?>
<fieldset>
<?php //pr(); ?>
	<legend><?php echo __("Základní údaje zákazníka")?></legend>
	<div class="sll"> 
		<div class="relative">
			<?php echo $this->Form->input((isset($zakazka)?'client.':'').'name', ['label' => __("Společnost").':','class'=>'ares_firma','placeholder'=>__('Vyhledání názvu společnosti...')]); ?>
			<div id="auto_loading" class="auto_loading none"></div>
			<?php echo $this->Form->input((isset($zakazka)?'client.':'').'problem_text', ['label' => __("Problémový zák.").':','class'=>'pozor_problem client_adopt']); ?>
		
		</div>
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'ic', ['label' => __("IČ").':','class'=>'client_adopt find_ares ares_ico','maxlength'=>8]); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'dic', ['label' => __("DIČ").':','class'=>'client_adopt ares_dic find_dph']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'valid_dph', ['label' => __("DPH ověřeno").':','options'=>$ano_ne,'class'=>'ares_valid_dph client_adopt']); ?>

	</div>
</fieldset>
<fieldset class="<?php echo (isset($hide)?'hide_fieldset':'');?>">
	<legend><?php echo __("Fakturační adresa zákazníka")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'ulice', ['label' => __("Ulice").':','tabindex'=>20,'class'=>'client_adopt ares_ulice']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'psc', ['label' => __("PSČ").':','tabindex'=>22,'class'=>'client_adopt ares_psc']); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'mesto', ['label' => __("Město").':','tabindex'=>21,'class'=>'client_adopt ares_mesto']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'stat', ['label' => __("Stát").':','tabindex'=>24,'class'=>'client_adopt ares_stat']); ?>

	</div>
</fieldset>
<fieldset class="<?php echo (isset($hide)?'hide_fieldset':'');?>">
	<legend><?php echo __("Kontaktní údaje zákazníka")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'telefon', ['label' => __("Telefon").':','class'=>'client_adopt']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'telefon2', ['label' => __("Telefon 2").':','class'=>'client_adopt']); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'email', ['label' => __("Email").':','class'=>'client_adopt']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'zastupce', ['label' => __("Zástupce").':','class'=>'client_adopt']); ?>

	</div>
</fieldset>
<?php //pr($client); ?>