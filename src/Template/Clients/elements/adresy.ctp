
<fieldset class="field_adresa">
	<legend><?php echo __("Adresy nakládky zákazníka")?></legend>
	<div class="field_bg">
	<label><?php echo __('Vyberte adresu z uložených');?></label>
	<?php //pr($zakazka); 
	if (!isset($zakazka)) $zakazka = '';
	//pr($adresa_list_data);
		
	?>
	<select id="SelectAddress1" class="SelectAddress add_adr" data-type="1" data-adresa-type="1" name="adresa_nalozeni_id">
		<option></option>
	<?php 
		function create_option($d,$k,$zakazka,$type){
				$val = explode('|',$d);
				//pr($val);
				if ($type == 1){
					if (isset($zakazka['adresa_nalozeni_id']) && !empty($zakazka['adresa_nalozeni_id']) && $zakazka['adresa_nalozeni_id'] == $val[0]){
						$selected = 'selected=selected';
						//pr($selected);
					}
				}
				if ($type == 2){
					if (isset($zakazka['adresa_vylozeni_id']) && !empty($zakazka['adresa_vylozeni_id']) && $zakazka['adresa_vylozeni_id'] == $val[0]){
						$selected = 'selected=selected';
						//pr($selected);
					}
				}
				echo '<option 
				value="'.$val[0].'" 
				data-ulice="'.$val[2].'" 
				data-mesto="'.$val[3].'" 
				data-psc="'.$val[4].'" 
				data-stat="'.$val[5].'" 
				data-lat="'.$val[6].'" 
				data-lng="'.$val[7].'" 
				data-type="'.$val[8].'" 
				data-firma="'.$val[9].'" 
				data-zona="'.$val[10].'" 
				data-oblast_id="'.$val[11].'" 
				'.(isset($selected)?$selected:'').'
				">'.$val[1].'</option>';
		}
		if (isset($adresa_list_data[1]) && count($adresa_list_data[1])>0){
			foreach($adresa_list_data[1] AS $k=>$d){
				//if (isset($zakazka))
				create_option($d,$k,$zakazka,1);
			}
		}
	//echo $this->Form->select("adr/list", $adresa_list,['empty'=>true,'class'=>'SelectAddress']); 
	?>
	</select>
	</div>
	<?php echo $this->element('../Clients/elements/adresa',['type'=>1]); ?>
</fieldset>

<fieldset class="field_adresa">
	<legend><?php echo __("Adresy vykládky zákazníka")?></legend>
	<div class="field_bg">
	<label><?php echo __('Vyberte adresu z uložených');?></label>
	<?php //pr($zakazka); ?>
	<select id="SelectAddress2" class="SelectAddress add_adr" data-type="2" data-adresa-type="2"  name="adresa_vylozeni_id">
		<option></option>
	<?php 
		if (isset($adresa_list_data[2]) && count($adresa_list_data[2])>0){
			
			foreach($adresa_list_data[2] AS $k=>$d){
				//if (isset($zakazka))
				create_option($d,$k,$zakazka,2);
			}
		}
	//echo $this->Form->select("adr/list", $adresa_list,['empty'=>true,'class'=>'SelectAddress']); 
	?>
	</select>
	</div>
	<?php echo $this->element('../Clients/elements/adresa',['type'=>2]); ?>
</fieldset>

<div id="map_areas_load" class="none">
	<?php echo $map_areas_load; ?>
</div>

<script type="text/javascript">
//<![CDATA[
	function show_address(place,parent,inject){
		if (place){
				var latlng = place.geometry.location;
				
				var cp = '';
				var co = '';
				var ulice = '';
				var mesto = '';
				var psc = '';
				var stat = '';
				Object.each(place.address_components, address_each = function(item){
				switch(item.types[0]){
					case "street_address":
						
						ulice = item.short_name;
						break;
					case "route":
						ulice = item.short_name;
						break;
					case "premise":
						cp = ' '+item.short_name;
						break;
					case "street_number":
						co = '/'+item.short_name;
						break;
					case "postal_code":
						psc = item.short_name;
						break;
					case "country":
						stat = item.short_name;
						break;
					case "locality":
						mesto = item.short_name;
						break;                  
				}
				});
				
				if (inject == true){
					parent.getElements('.add_ins').each(function(item){
						
						if (item.get('data-type') == 'ulice') item.value = ulice + cp + co;
						if (item.get('data-type') == 'mesto') item.value = mesto;
						if (item.get('data-type') == 'psc') item.value = psc;
						if (item.get('data-type') == 'stat') item.value = stat;
						if (item.get('data-type') == 'lat') item.value = latlng.lat();
						if (item.get('data-type') == 'lng') item.value = latlng.lng();
					});
					/*
					$('adr-ulice').value = ulice + cp + co;
					$('adr-mesto').value = mesto;
					$('adr-psc').value = psc;
					$('adr-stat').value = stat;
					$('adr-lat').value = latlng.lat();
					$('adr-lng').value = latlng.lng();
					*/
				}
				
				parent.getElement('.ShowAddress').fireEvent('click',parent.getElement('.ShowAddress'));
				
				
				ulice = null;
				cp = null;
				co = null;
				mesto = null;
				psc = null;
				stat = null;
		}	
	}
	
	function initialize_google(){
		//var options = {componentRestrictions: {country: 'cz'}, radius:20};
		var options = {radius:20};
		
		var autocomplete = {};
		var autocomplete_change = {};
		$$('.autocomplete_ulice').each(function(item,k){
			//console.log(item);
			autocomplete[k] = new google.maps.places.Autocomplete(item,options);
			
			google.maps.event.addListener(autocomplete[k], 'place_changed', (autocomplete_change[k] = function() {
				var place = autocomplete[k].getPlace();
				
				if (!place.geometry) {
				  return;
				}
				show_address(place,item.getParent('.field_adresa'),true);
				place = null;
				
			}));
		});
		
		$$('.ShowAddress').addEvent('click',VarsModal.show_address_fce = function(e){
			if (typeof e.stop != 'undefined'){
				e.stop();
			}
			if (!e.target){
				e.target = e;
			}
			var fieldset = e.target.getParent('fieldset');
			
			if (fieldset.getElement('.adr_lng').value == ''){
				FstError('Není známa GPS poloha');
				return true;
			}
			
			$$('.show_address').addClass('none');
			var el = e.target.getNext('.show_address');
			el.removeClass('none');
			
			var close_map = new Element('div',{'class':'close_map button small'}).set('text','Zavřít mapu').inject(el,'after');
			close_map.addEvent('click',function(e){
				e.stop();
				el.addClass('none');
				this.destroy();
			});
			
			if (el){
				if ($('show_map')) $('show_map').destroy();
				
				var div_over = new Element('div',{'id':'show_map'}).inject(el);
				div_over.set('data-lat',fieldset.getElement('.adr_lat').value);
				div_over.set('data-lng',fieldset.getElement('.adr_lng').value);
				div_over.set('data-oblast',fieldset.getElement('.adr_oblast').getProperty('id'));
				init_google(true);
			}
		});
		
		$$('.DeleteAddress').addEvent('click',VarsModal.delete_address_fce = function(e){
			e.stop();
			if (confirm(lang['opravdu_smazat'])){
				var selectTag = e.target.getParent('.field_adresa').getElement('.SelectAddress');
				var parent = e.target.getParent('.field_adresa');
				
				if (selectTag.value == ''){
					FstError(lang['neni_vybrana_adresa']);
					return false;
				}
				
				
				VarsModal.del_adr_reg = new Request.JSON({
					url:'/clients/del_address/'+selectTag.value,	
					
					onComplete: (VarsModal.del_adr_reg_complete = function(json){
						if (json.r == true){
							parent.getElements('.add_ins').each(function(item){
								item.value = '';
							});
							var opt_list = selectTag.getElements('option');
							opt_list[selectTag.selectedIndex].destroy();
							selectTag.value = '';
						} else {
							FstError(json.m);
						}
					})
				});
				VarsModal.del_adr_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				VarsModal.del_adr_reg.send();
				}
		});
		
		
		$$('.AddAddress').addEvent('click',VarsModal.add_address_fce = function(e){
			e.stop();
			var params = '';
			var parent = e.target.getParent('.field_adresa');
			var selectTag = e.target.getParent('.field_adresa').getElement('.SelectAddress');
			if ($('adr-ulice1').value == ''){
				return true;
			}
			//selectTag.value = '';
			parent.getElements('.add_adr').each(function(item){
				//console.log(item);
				//console.log(item.value);
				params += item.get('data-type')+'='+item.value+'&';
			});
			
			params += $('modal_in').getElement('.client_id').get('data-type')+'='+$('modal_in').getElement('.client_id').value;
			
			//console.log(params); return false;
			
			VarsModal.add_adr_reg = new Request.JSON({
				url:'/clients/add_address/?p='+Base64.encode(JSON.encode(params)),	
				
				onComplete: (VarsModal.add_adr_reg_complete = function(json){
					if (json.r == true){
						FstAlert(json.m);
						new Element('option',json.option).set('text',json.option.name).inject(selectTag);
						parent.getElements('.add_ins').each(function(item){
							item.value = '';
						});
						//console.log(selectTag.getElements('option').getLast().value);
						selectTag.value = selectTag.getElements('option').getLast().value;
						selectTag.fireEvent('change',selectTag);
					} else {
						FstError(json.m);
					}
				})
			});
			VarsModal.add_adr_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.add_adr_reg.send();
			
		});
		
		$$('.SelectAddress').addEvent('change',VarsModal.select_address_fce = function(e){
			if (!e.target) e.target = e.getParent('select');
			
			var opt_list = e.target.getElements('option');
			
			var parent = e.target.getParent('.field_adresa');
			parent.getElements('.add_ins').each(function(item){
				item.value = opt_list[e.target.selectedIndex].get('data-'+item.get('data-type'));
				//if (item.get('data-type') == opt.get('data-
			});
			
		});
		
		// fire events if edit zakazka
		$$('.SelectAddress').each(function(select_adr){
			if (select_adr.value > 0){
				select_adr.fireEvent('change',select_adr);
			}
		});
	}
	
   function loadScriptMap(){
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&callback=initialize_google";
		document.body.appendChild(script);
   	}
	window.addEvent('domready',function(){
		loadScriptMap();
	});
	
//]]>
</script>