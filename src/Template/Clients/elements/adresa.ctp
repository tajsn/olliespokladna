
<fieldset>
	<legend><?php echo __("Přidat / editovat adresu")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("adr.ulice".$type, ['label' => __("Ulice").':','tabindex'=>$type*30,'class'=>'add_adr add_ins autocomplete_ulice','data-type'=>'ulice']); ?>
		<?php echo $this->Form->input("adr.zona".$type, ['label' => __("Zóna").':','tabindex'=>$type*32,'class'=>'add_adr add_ins','data-type'=>'zona']); ?>
		<?php echo $this->Form->input("adr.firma".$type, ['label' => __("Firma").':','tabindex'=>$type*34,'class'=>'add_adr add_ins','data-type'=>'firma']); ?>
		<?php echo $this->Form->button(__('Přidat'), ['class' => 'btn fleft AddAddress']);?>
		<?php echo $this->Form->button(__('Smazat'), ['class' => 'btn fleft DeleteAddress']);?>
		<?php echo $this->Form->button(__('Mapa'), ['class' => 'btn fleft ShowAddress']);?>
		<div class="show_address none">
		
		</div>
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("adr.mesto".$type, ['label' => __("Město").':','tabindex'=>$type*31,'class'=>'add_adr add_ins','data-type'=>'mesto']); ?>
		<?php echo $this->Form->input("adr.psc".$type, ['label' => __("PSČ").':','tabindex'=>$type*33,'class'=>'add_adr add_ins','data-type'=>'psc']); ?>
		<?php echo $this->Form->input("adr.stat".$type, ['label' => __("Stát").':','tabindex'=>$type*35,'class'=>'add_adr add_ins','data-type'=>'stat']); ?>
		<?php echo $this->Form->input("adr.lat".$type, ['id'=>'adr-lat','class'=>'add_adr add_ins adr_lat','data-type'=>'lat','label'=>__('GPS Lat').':']); ?>
		<?php echo $this->Form->input("adr.lng".$type, ['id'=>'adr-lng','class'=>'add_adr add_ins adr_lng','data-type'=>'lng','label'=>__('GPS Lng').':']); ?>
		<?php echo $this->Form->hidden("adr.type".$type, ['id'=>'adr-type','class'=>'add_adr add_ins','data-type'=>'type','value'=>(isset($type)?$type:'')]); ?>
		<?php echo $this->Form->input("adr.oblast_id".$type, ['options'=>$map_areas_list,'empty'=>true,'class'=>'add_adr add_ins adr_oblast','data-type'=>'oblast_id','label' => __("Oblast").':']); ?>
		
	</div>
</fieldset>