<?php
echo $this->Form->create($clients);

//echo "The time is " . date("H:i:sa");
//pr($clients);
//pr($clients->id);
//echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);

$params = ['zakladni'=>__('Základní'),'adresy'=>__('Adresy'),'jednani'=>__('Jednání')];
if (!isset($clients->id)){
	unset($params['adresy']);
}

echo $this->element('modal',['params'=>$params]);


echo $this->Form->button(__('Uložit'), ['class' => 'btn','id'=>'SaveModal']);

echo $this->Form->end();

echo $this->Html->script("fst_system/modal_window");