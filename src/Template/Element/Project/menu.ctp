<div class="menu">
  <ul>
  <?php
    echo '<li>'.$this->Html->link(__("Přehled"), ['action' => 'view', $project->id], ['class' => (($this->request->action == 'view')? 'active' : null)]).'</li>';
    echo '<li>'.$this->Html->link(__("Klient"), ['action' => 'view_client', $project->id], ['class' => (($this->request->action == 'viewClient')? 'active' : null)]).'</li>';
    echo '<li>'.$this->Html->link(__("Spolupráce"), ['action' => 'view_members', $project->id], ['class' => (($this->request->action == 'viewMembers')? 'active' : null)]).'</li>';
    echo '<li>'.$this->Html->link(__("Nastavení"), ['action' => 'view_members', $project->id], ['class' => (($this->request->action == 'viewMembers')? 'active' : null)]).'</li>';
  ?>
  </ul>
</div>