
<ul class="fright user-menu">
    <li class="user">
        <span class="fa fa-user"></span>
        <strong><?= $this->Html->link($this->request->Session()->read("Auth.User.name"), ['controller' => 'users', 'action' => 'edit', $this->request->Session()->read("Auth.User.id")]) ?></strong>
    </li>
    <li class="notify"><?= $this->Html->link('<span class="fa fa-bell-o"></span>', ['controller' => 'calendars'], ['escape'=>false, 'title'=>'notifikace']) ?>
    </li>
    <li class="cal"><?= $this->Html->link('<span class="fa fa-calendar"></span>', ['controller' => 'calendars'], ['escape'=>false, 'title'=>'kalendář']) ?>
    </li>
    <li class="logout"><?= $this->Html->link('<span class="fa fa-sign-out"></span>', ['controller' => 'users', 'action' => 'logout'], ['escape'=>false, 'title'=>'odhlásit']) ?>
    </li>
</ul>

