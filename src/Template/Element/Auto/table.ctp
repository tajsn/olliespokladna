<?php
  echo '<div class="layout" id="fst_history">';
  if(isset($items) && $items->count() > 0){

  echo '<table class="table-formated">';

    $first = $items->first()->toArray();
    //pr($first);
	//pr($viewIndex['th']);
	foreach($first as $key => $item){
    $th = $this->Autotable->th($key, $item);
    }
    echo '<tr>';
      echo $this->Autotable->tableth;
      echo '</tr>';

    foreach($items as $key => $item){
    echo '<tr class="item-row" click-id="'.$item->id.'">';
      echo $this->Autotable->td($item->toArray());
      echo '</tr>';
    }

    echo '</table>';
	echo '</div>';
  echo $this->element("pagination");
  }

  $paging_url = @key($this->request->paging);
?>

<script>
  var viewUrl = '<?= strtolower($paging_url."/view/") ?>'
</script>