<?php 
	//$loggedUser = '';
	//pr($loggedUser['disable_menu']);
	
	function gen_menu_items($html,$loggedUser,$menu_items,$class=null,$id=null){
		echo '<ul id="'.$id.'" class="'.$class.'">';
		foreach($menu_items AS $mkey=>$menu){ 
			//pr($loggedUser['disable_menu']);
			if (isset($loggedUser['disable_menu']) && in_array($mkey,$loggedUser['disable_menu'])){
				continue;
			}
			echo '<li>'.$html->link($menu['name'], $menu['url'],['class'=>$menu['class'],'escape' => false,'ajax'=>(isset($menu['ajax']))?$menu['ajax']:false]);
			if (isset($menu['child'])){
				gen_menu_items($html,$loggedUser,$menu['child']);
				echo '</li>';
		
			} else {
				echo '</li>';
			
			}
		}
		echo '</ul>';
		
	}
	gen_menu_items($this->Html,$loggedUser,$menu_items,'top_menu','top_menu');
	gen_menu_items($this->Html,$loggedUser,$menu_items2,'top_menu','top_menu2');
	//pr($loggedUser); 
	
	echo '<div id="menu_open" class="fa  fa-bars"></div>';
	
	?>
	

<?php //echo $this->Html->script("pages"); ?>