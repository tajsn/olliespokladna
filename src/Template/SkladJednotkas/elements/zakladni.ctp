<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("dodavatel_jednotka", ['label' => __("Jednotka dodavatel")]); ?>
		<?php echo $this->Form->input("jednotka_id",['label'=>__('Jednotka'),'options'=>$sklad_jednotka_list,'empty'=>false]); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("prevod", ['label' => __("Převod")]); ?>
		
	</div>
</fieldset>