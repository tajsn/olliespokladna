<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("name", ['label' => __("Název")]); ?>
		<?php echo $this->Form->input("group_id", ['label' => __("Skupina"),'options'=>$stoly_group_list[$loggedUser['system_id']]]); ?>

	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("num", ['label' => __("Číslo"),'class'=>'integer']); ?>

	</div>
</fieldset>
