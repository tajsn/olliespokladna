<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("jmeno", ['label' => __("Jméno"),'tabindex'=>1]); ?>
		<?php echo $this->Form->input("number", ['label' => __("Poř. číslo"),'tabindex'=>3]); ?>

	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("prijmeni", ['label' => __("Příjmení"),'tabindex'=>2]); ?>
		<?php echo $this->Form->input("datum_narozeni", ['label' => __("Datum narození"),'class'=>'date','dateFormat' => 'D.M.Y','type'=>'text','tabindex'=>4]); ?>
		
	</div>
</fieldset>
<fieldset>
	<legend><?php echo __("Telefoní kontakt")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("tel_firemni", ['label' => __("Firemní")]); ?>
		<?php echo $this->Form->input("tel_soukromy", ['label' => __("Soukromý")]); ?>

	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("tel_pribuzny", ['label' => __("Příbuzný")]); ?>
	</div>
</fieldset>
