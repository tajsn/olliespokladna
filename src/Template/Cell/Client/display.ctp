<?php


  echo '<p>'. __("Vyberte nebo vytvořte klienta") .'</p>';
  echo $this->Form->input("client_id", ["options" => $clients, "empty" => __("Vyberte klienta"), 'label' => __("Klient")]);
  echo $this->Html->link(__("Přidat nového klienta"), "#", ["id" => "addClientBtn"]);

  echo '<div id="add-client-container" style="display:none;">';
    echo $this->element("Client/add", ["client" => $client]);
  echo '</div>';

  echo $this->Html->script("Clients/add");


