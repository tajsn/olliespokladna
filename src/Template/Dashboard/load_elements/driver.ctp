<?php
if (isset($data) && count($data)>0){ 
echo '<ul class="sort_list2">';
foreach($data AS $k=>$d){
	echo '<li id="driver_'.$d->id.'"class="element_drag driver_list fst_drag '.(($k==0 && isset($modal_edit))?'is_ridic':'').'" data-id="'.$d->id.'" data-type="driver" data-drop="car" data-name="'.$d->name.'" >';
		echo $this->Form->input("driver_".$d->id, ['label' => false,'type'=>'checkbox','data-id'=>$d->id,'class'=>'checkbox']);
		echo '<div class="fa fa-user"></div>';
		echo '<span class="name2">'.$d->name.(($k==0 && isset($modal_edit))?' - '.__('je řidič'):'').'</span>';
		echo '<span class="number">#'.$d->number.'</span>';
		echo '<span class="sklad_id">'.(isset($sklad_type[$system_id][$d->sklad_id])?$sklad_type[$system_id][$d->sklad_id]:'').'</span>';
		if (isset($modal_edit)){
			echo '<div class="modal_fce">';
				echo $this->Html->link('', ['controller' => 'dashboard', 'action'=>'delete_driver/'.$d->id],['class'=>'delete_driver fa fa-trash','title'=>__("Smazat"),'data-id'=>$d->id,'data-car_id'=>$car_id]);
			echo '</div>';
		}
		echo '<div class="detail_hover none">';
			echo '<fieldset>';
				echo '<legend>'.__('Řidič').'</legend>';
				echo '<div class="sll">';
					echo '<label>'.__('Číslo').':</label>'.$d->number.'<br />';
					echo '<label>'.__('Věk').':</label>'.$d->vek.'<br />';
				echo '</div>';
				echo '<div class="slr">';
					echo '<label>'.__('Firemní').':</label>'.$d->tel_firemni.'<br />';
					echo '<label>'.__('Soukromý').':</label>'.$d->tel_sokromy.'<br />';
					echo '<label>'.__('Přibuzný').':</label>'.$d->tel_pribuzny.'<br />';
				echo '</div>';
			echo '</fieldset>';
			echo '<fieldset>';
				echo '<legend>'.__('Telefony').'</legend>';
				echo '<div class="sll">';
					echo '<label>'.__('Firemní').':</label>'.$d->tel_firemni.'<br />';
					echo '<label>'.__('Soukromý').':</label>'.$d->tel_sokromy.'<br />';
				echo '</div>';
				echo '<div class="slr">';
					echo '<label>'.__('Přibuzný').':</label>'.$d->tel_pribuzny.'<br />';
				echo '</div>';
			echo '</fieldset>';
		echo '</div>';
		
	echo '</li>';
	
} 
echo '</ul>';

} else {
	echo '<p class="noresult">'.__('Nenalezeny žádné záznamy').'</p>';
}
?>