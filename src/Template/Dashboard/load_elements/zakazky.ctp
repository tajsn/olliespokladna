<?php 
//pr($data);

if (isset($modal_edit)){
	echo '<h3>'.$car_data->name.'</h3>';
}
if (isset($data) && count($data)>0){




if (!isset($modal_edit)){
	
	
	echo '<div class="date_select">';
		echo '<div class="dsel" data-date="'.strtotime(date('Y-m-d')).'">'.__('Dnes').'</div>';
		echo '<div class="dsel" data-from="'.strtotime(date('Y-m-d',strtotime(date('Y-m-d'). "+1 day"))).'" data-to="'.strtotime(date('Y-m-d',strtotime(date('Y-m-d'). "+8 days"))).'">'.__('Tento týden').'</div>';
		echo '<div class="dsel" data-from="'.strtotime(date('Y-m-d',strtotime(date('Y-m-d'). "+8 days"))).'">'.__('Ostatní').'</div>';
	echo '</div>';
}

echo '<ul class="sort_list">';
foreach($data AS $k=>$d){
	echo '<li id="zakazka_'.$d->id.'" class="element_drag fst_drag zakazky_list '. (($k++ % 2) ? "even" : "odd").' '.(($k==0 && !isset($modal_edit))?'first':'').(($d->driver_load)?' loaded':'').(($d->driver_unload)?' unloaded':'').'" data-id="'.$d->id.'" data-date_nalozeni="'.strtotime($d->date_nalozeni).'" data-date_nalozeni2="'.strtotime($d->date_nalozeni2).'" data-type="zakazka" data-drop="car" data-size="'.$d->size.'" data-size2="'.$d->weight.'" '.(isset($modal_edit)?'data-car_id="'.$car_id.'"':'').'>';
		
		//echo '<div class="fa fa-database"></div>';
		echo '<div class="handle_sort fa fa-arrows-v" title="'.('Změnit pořadí').'"></div>';
		
		echo '<div class="clear line"></div>';
		
		echo '<div class="sll dates">';
			echo '<label>'.__('Naložení').':</label>'.$d->date_nalozeni.(($d->time_nalozeni>0)?' ('.$d->time_nalozeni.')':'').' '.(!empty($d->date_nalozeni2)?'-'.substr($d->date_nalozeni2,0,6):'').'<br />';
			echo '<label>'.__('Vyložení').':</label>'.$d->date_vylozeni.(($d->time_vylozeni>0)?' ('.$d->time_vylozeni.')':'').' '.(!empty($d->date_vylozeni2)?'-'.substr($d->date_vylozeni2,0,6):'').'<br />';
		echo '</div>';
		echo '<div class="slr address">';
			echo '<label>'.__('Naložení').':</label>'.(isset($d->adresa_nalozeni->psc)?$d->adresa_nalozeni->psc.' '.$d->adresa_nalozeni->stat:'').'<br />';
			echo '<label>'.__('Vyložení').':</label>'.(isset($d->adresa_vylozeni->psc)?$d->adresa_vylozeni->psc.' '.$d->adresa_vylozeni->stat:'').'<br />';
			echo '<div class="none">';
				echo '<div class="gps nalozeni">'.$d->adresa_nalozeni->lat.','.$d->adresa_nalozeni->lng.'</div>';
				
				echo '<div class="gps vylozeni">'.$d->adresa_vylozeni->lat.','.$d->adresa_vylozeni->lng.'</div>';
			echo '</div>';
		echo '</div>';
		echo '<div class="clear"></div>';
	
		echo '<div class="price">';
			echo '<label>'.__('Cena').':</label>'.$this->Fastest->price($d->price);
		echo '</div>';
		echo '<div class="name">';
			echo (isset($d->client->name)?$d->client->name:'');
		echo '</div>';
		echo '<div class="zakazka_id">';
			echo (isset($d->id)?$d->id:'');
		echo '</div>';
		if (!empty($d->anomalie)){
			echo '<div class="anomalie">';
				echo '<label>'.__('Anomalie').':</label>'.$d->anomalie;
			echo '</div>';
		}
		if (isset($modal_edit)){
			echo '<div class="modal_fce">';
				if ($d->driver_send == 1)
				echo '<div class="driver_confirm fa '.(($d->driver_confirm == 1)?'fa-calendar-check-o':'fa-calendar-times-o').'" title="'.(($d->driver_confirm == 1)?__('Řidič potvrdil přijetí naložení '.$d->driver_date_confirm):__('Řidič nepotvdil přijetí naložení')).'"></div>';
				
				echo $this->Html->link('', ['controller' => 'dashboard', 'action'=>'send_zakazka/driver/'.$car_id.'/'.$d->id],['class'=>'send_zakazka fa fa-send '.(($d->driver_send)?'sended':''),'title'=>__("Poslat řidiči k naložení"),'data-confirm'=>'ridic','data-car_id'=>$car_id,'data-size'=>$d->size, 'data-size2'=>$d->weight]);
				
				echo $this->Html->link('', ['controller' => 'dashboard', 'action'=>'#'],['class'=>'send_zakazka fa fa-archive','title'=>__("Předat do skladu"),'data-confirm'=>'sklad_show']);
				
				echo $this->Html->link('', ['controller' => 'dashboard', 'action'=>'delete_from_car/'.$car_id.'/'.$d->id],['class'=>'delete_zakazka fa fa-trash','title'=>__("Smazat"),'data-size'=>$d->size, 'data-size2'=>$d->weight,'data-car_id'=>$car_id,'data-id'=>$d->id]);
				
				
				echo '<div class="show_sklad none">';
					foreach($sklad_type[$system_id] AS $k=>$s){
						echo $this->Html->link('', ['controller' => 'dashboard', 'action'=>'send_zakazka/zakazka_to_sklad/'.$car_id.'/'.$d->id.'/'.$k],['class'=>'send_zakazka fa fa-archive sklad'.$k,'title'=>__("Předat do skladu ".$s),'data-confirm'=>'sklad','data-car_id'=>$car_id,'data-size'=>$d->size, 'data-size2'=>$d->weight]);
					
					}
					//echo $this->Form->select("sklad", [0=>__('Vyberte sklad')]+$sklad_type[$system_id],[]);
				echo '</div>';
				
				echo '<div class="right">';
					if ($d->driver_load == 0){
						echo '<div class="no_confirm_load" title="'.__('Nelze poslat k vyložení dokud není naloženo').'"></div>';
					}
					if ($d->driver_send2 == 1)
					echo '<div class="driver_confirm fa '.(($d->driver_confirm2 == 1)?'fa-calendar-check-o':'fa-calendar-times-o').'" title="'.(($d->driver_confirm2 == 1)?__('Řidič potvrdil přijetí vyložení '.$d->driver_date_confirm2):__('Řidič nepotvdil přijetí vyložení')).'"></div>';
					
					echo $this->Html->link('', ['controller' => 'dashboard', 'action'=>'send_zakazka/driver2/'.$car_id.'/'.$d->id],['class'=>'send_zakazka fa fa-send '.(($d->driver_send2)?'sended':''),'title'=>__("Poslat řidiči k vyložení"),'data-confirm'=>'ridic','data-car_id'=>$car_id,'data-size'=>$d->size, 'data-size2'=>$d->weight]);
					
					if ($d->done == 0)
					echo $this->Html->link('', ['controller' => 'dashboard', 'action'=>'done_zakazka/'.$car_id.'/'.$d->id],['class'=>'done_zakazka fa fa-check','title'=>__("Zakázku přesunout do hotových"),'data-size'=>$d->size, 'data-size2'=>$d->weight,'data-car_id'=>$car_id,'data-id'=>$d->id]);
			
				echo '</div>';
				
			echo '</div>';
		}
		
		
		
		echo '<div class="detail_hover none">';
			if (isset($modal_edit)){
				echo '<fieldset>';
					echo '<legend>'.__('Stav zakázky').'</legend>';
					echo '<label>'.__('Stav').':</label>'.$stav_list[$d->stav].'<br />';
					echo '<div class="sll">';
					echo '</div>';
					echo '<div class="slr">';
					echo '</div>';
				echo '</fieldset>';
			}
			echo '<fieldset>';
				echo '<legend>'.__('Zadavatel').'</legend>';
				echo '<div class="sll">';
					if (isset($d->client->name)){
					echo '<label>'.__('Společnost').':</label>'.$d->client->name.'<br />';
					echo '<label>'.__('Město').':</label>'.$d->client->mesto.'<br />';
					echo '<label>'.__('Stát').':</label>'.$d->client->stat.'<br />';
					} else {
						echo __('Není přiřazen klient');
					}
				echo '</div>';
				echo '<div class="slr">';
					echo '<label>'.__('Dispečer').':</label>'.$d->dispecer_name.'<br />';
					echo '<label>'.__('Telefon').':</label>'.$d->dispecer_tel.'<br />';
					echo '<label>'.__('Mobil').':</label>'.$d->dispecer_mobile.'<br />';
				echo '</div>';
			echo '</fieldset>';
			
			echo '<fieldset>';
				echo '<legend>'.__('Nakládka').'</legend>';
				echo '<div class="sll">';
					if (isset($d->adresa_nalozeni->ulice)){
					echo '<label>'.__('Ulice').':</label>'.$d->adresa_nalozeni->ulice.'<br />';
					echo '<label>'.__('Město').':</label>'.$d->adresa_nalozeni->mesto.'<br />';
					echo '<label>'.__('PSČ').':</label>'.$d->adresa_nalozeni->psc.'<br />';
					echo '<label>'.__('Stát').':</label>'.$d->adresa_nalozeni->stat.'<br />';
					if (!empty($d->adresa_nalozeni->lat))
					echo $this->Html->link(__('Mapa'), ['action' => '#'], ['class' => 'show_map button small','data-lat'=>$d->adresa_nalozeni->lat,'data-lng'=>$d->adresa_nalozeni->lng]);
					} else {
						echo __('Není definována adresa naložení');
					}
					
				echo '</div>';
				echo '<div class="slr">';
					echo '<label>'.__('Datum').':</label>'.$d->date_nalozeni.'<br />';
					echo '<label>'.__('Váha').':</label>'.$d->weight.'<br />';
					echo '<label>'.__('Počet').':</label>'.$d->size.'<br />';
					echo '<label>'.__('Cena').':</label>'.$this->Fastest->price($d->price).'<br />';
					echo '<label>'.__('Anomálie').':</label>'.$d->anomalie.'<br />';
				echo '</div>';
			echo '</fieldset>';
			
			echo '<fieldset>';
				echo '<legend>'.__('Vykládka').'</legend>';
				echo '<div class="sll">';
					if (isset($d->adresa_vylozeni->ulice)){
					echo '<label>'.__('Ulice').':</label>'.$d->adresa_vylozeni->ulice.'<br />';
					echo '<label>'.__('Město').':</label>'.$d->adresa_vylozeni->mesto.'<br />';
					echo '<label>'.__('PSČ').':</label>'.$d->adresa_vylozeni->psc.'<br />';
					echo '<label>'.__('Stát').':</label>'.$d->adresa_vylozeni->stat.'<br />';
					if (!empty($d->adresa_vylozeni->lat))
					echo $this->Html->link(__('Mapa'), ['action' => '#'], ['class' => 'show_map button small','data-lat'=>$d->adresa_vylozeni->lat,'data-lng'=>$d->adresa_vylozeni->lng]);
					} else {
						echo __('Není definována adresa vyložení');
					}
				echo '</div>';
				echo '<div class="slr">';
				echo '</div>';
			echo '</fieldset>';
			//pr($d);
		echo '</div>';
		
	echo '</li>';
	//pr($d->adresa_vylozeni);
}
echo '</ul>';
} else {
	echo '<p class="noresult">'.__('Nenalezeny žádné záznamy').'</p>';
}
?>