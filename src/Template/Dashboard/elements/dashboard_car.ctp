<?php 
$weight_load = 0;
$size_load = 0;
foreach($car->zakazka_connects AS $zk){
	$weight_load += $zk->weight;
	$size_load += $zk->size;
}

?>


<div class="car load_from_db fst_drop fst_drag car_<?php echo $car->id?>" 
	data-type="car" 
	data-drop="basket"  
	data-size_all="<?php echo $car->size_all; ?>" 
	data-size_all2="<?php echo $car->weight_all; ?>" 
	data-id="<?php echo $car->id; ?>" 
	data-weight_load="<?php echo $weight_load; ?>" 
	data-size_load="<?php echo $size_load; ?>" 
	data-spz="<?php echo $car->spz; ?>" 
	data-car_type="<?php echo $car_type_list[$car->car_type]; ?>"
	data-car_type_id="<?php echo $car->car_type; ?>"
	data-car_color="<?php echo $car_color_list[$car->car_type]; ?>"
	data-prives_id="<?php echo $car->prives_id; ?>"
	
	>
	<?php echo $this->Form->checkbox("car_check", ['class'=>'checkbox'])	; ?>
	<?php echo $car->name; ?>
	<?php echo '<span class="spz">'.$car->spz.'</span>'; ?>
	<?php echo '<span class="car_type">'.$car_type_list[$car->car_type].'</span>'; ?>
	<div class="zakazka_ids">
		
		<?php 
		if (isset($car['zakazka_connects']) && !empty($car['zakazka_connects'])){
			$zakazka_list = array();
			foreach($car['zakazka_connects'] AS $zc){
				
				$zakazka_list[] = $zc->zakazka_id;
			}
			echo implode('|',$zakazka_list).'|';
		}
		?>
	</div>
				
	<div class="progress_over">
		<div class="progress progr"><div class="pr"></div><span></span></div>
		<div class="progress2 progr"><div class="pr"></div><span></span></div>
	</div>
	<div class="driver_detail fa fa-user <?php echo (empty($car->driver_id)?'noload':''); ?>" data-id="<?php echo $car->driver_id; ?>" title="Řidič"></div>
	<div class="delete_car fa fa-trash" title="Smazat auto"></div>
	<div class="show_detail fa fa-list"></div>
	<div class="prelozeno fa fa-exclamation-triangle"></div>
	<?php if ($car->car_type == 7){ ?>
		<div class="car_join fa fa-chain"></div>
	<?php } ?>
	
	<?php 
	//if ($car->prives_id > 0){ 
	//	echo $this->element('../Dashboard/elements/dashboard_car',array('car'=>$car));
	//}
	?>
	
</div>
<?php 
//pr($zakazka_list);  
//pr($car['zakazka_connects']); ?>
