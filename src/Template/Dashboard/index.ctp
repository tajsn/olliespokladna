<div id="dashboard">
	<div id="load_map_side"></div>
	<div id="side_menu">
		<div id="side_drag"></div>
		<div id="dashboard_kos" class="fst_drop" data-type="basket"><?php echo __('Předat auto dispečerovi');?></div>
		<div id="select_dispecer" class="none">
			<div>
				<?php echo $this->Form->select('select_dispecer',[0=>__('Vyberte dispečera')]+$dispecer_list,['id'=>'SelectDispecer']); ?>
				<div class="close fa fa-close" id="CloseSelectDispecer"></div>
			</div>
		</div>
		<ul id="switch_content_menu">
			<li><a href="#" data-type="0" data-load="zakazky" class="active"><?php echo __('Nezpracované');?></a></li>
			<li><a href="#" data-type="3" data-load="sklad" ><?php echo __('Sklad');?></a></li>
			<li><a href="#" data-type="1" data-load="auta" id="side_menu_auta" ><?php echo __('Auta');?></a></li>
			<li><a href="#" data-type="2" data-load="driver" ><?php echo __('Řidiči');?></a></li>
			<li><a href="#" data-type="4" data-load="zakazky_done" ><?php echo __('Hotové zak.');?></a></li>
		</ul>
		<div id="switch_content">
			<?php //pr($loggedUser); ?>
			<div class="tabs">
				<?php 
				echo '<div class="filtrace" data-type="zakazky">';
					echo $this->Form->input("canvas/date_nalozeni",['id'=>'CanvasDateNalozeni','data-type'=>'date_nalozeni','class'=>'get_params date','label'=>false,'placeholder'=>__('Datum naložení')]);
					echo $this->Form->input("canvas/psc",['data-type'=>'psc','class'=>'get_params','label'=>false,'placeholder'=>__('PSČ naložení')]);
					echo $this->Form->input("canvas/psc2",['data-type'=>'psc2','class'=>'get_params','label'=>false,'placeholder'=>__('PSČ vyložení')]);
					if ($loggedUser['group_id'] == 2){$selected = 1;}
					if ($loggedUser['group_id'] == 3){$selected = 2;}
					//pr($selected);
					echo $this->Form->select("canvas/type",[0=>__('Typ zakázky')] + $zakazka_type, ['id'=>'CanvasType','data-type'=>'type','class'=>'get_params','data-value'=>true,'value'=>(isset($selected)?$selected:'')]);
					echo $this->Form->select("canvas/sort",[0=>__('Datum nakládky')] + $sort_psc, ['id'=>'CanvasType','data-type'=>'sort','class'=>'get_params']);
					//echo $this->Form->select("canvas/sklad_type",[0=>__('Typ skladu')] + $sklad_type[$system_id], ['id'=>'CanvasSkladType','data-type'=>'sklad_id','class'=>'get_params']);
					echo $this->Form->button("Mapa",['class'=>'button small show_side_map']);
				
				echo '</div>';
				?>
				<div id="load_zakazky" class="load_data overflow"></div>
			</div>
			<div class="tabs">
				<?php 
				echo '<div class="filtrace" data-type="auta">';
					echo $this->Form->select("canvas/sklad_type",[0=>__('Lokalita')] + $sklad_type[$system_id], ['id'=>'CarSkladId','data-type'=>'car_sklad_id','class'=>'get_params']);
					
				echo '</div>';
				?>
				<div id="load_auta" class="load_data overflow"></div>
			</div>
			<div class="tabs">
				<?php 
				echo '<div class="filtrace" data-type="driver">';
					echo $this->Form->select("canvas/sklad_type",[0=>__('Lokalita')] + $sklad_type[$system_id], ['id'=>'DriverSkladId','data-type'=>'driver_sklad_id','class'=>'get_params']);
					
				echo '</div>';
				?>
				<div id="load_driver" class="load_data overflow"></div>
			</div>
			<div class="tabs">
				<?php 
				echo '<div class="filtrace" data-type="sklad">';
					echo $this->Form->input("canvas/sklad_date_nalozeni",['id'=>'CanvasDateNalozeni','data-type'=>'date_nalozeni','class'=>'get_params date','label'=>false,'placeholder'=>__('Datum naložení')]);
					echo $this->Form->input("canvas/sklad_psc",['data-type'=>'sklad_psc','class'=>'get_params','label'=>false,'placeholder'=>__('PSČ naložení')]);
					echo $this->Form->input("canvas/sklad_psc2",['data-type'=>'sklad_psc2','class'=>'get_params','label'=>false,'placeholder'=>__('PSČ vyložení')]);
					echo $this->Form->select("canvas/sklad_type",[0=>__('Typ zakázky')] + $zakazka_type, ['id'=>'CanvasType','data-type'=>'sklad_type','class'=>'get_params']);
					echo $this->Form->select("canvas/sklad_sklad_type",[0=>__('Typ skladu')] + $sklad_type[$system_id], ['id'=>'CanvasSkladType','data-type'=>'sklad_id','class'=>'get_params']);
					echo $this->Form->button("Mapa",['class'=>'button small show_side_map']);
					
				echo '</div>';
				?>
				<div id="load_sklad" class="load_data overflow"></div>
			</div>
			<div class="tabs">
				<?php 
				echo '<div class="filtrace" data-type="zakazky_done">';
					
				echo '</div>';
				?>
				<div id="load_zakazky_done" class="load_data overflow"></div>
			</div>
		</div>
	</div>
	<div id="canvas_left">
		<div id="canvas_filtr" class="filtrace" data-type="canvas">
			<?php echo $this->Form->select("canvas/sklad_type",[0=>__('Auta mimo sklady')] + $sklad_type_auta[$system_id], ['id'=>'CanvasSkladType','data-type'=>'canvas_sklad_id','class'=>'get_params']); ?>
			<?php echo $this->Form->select("canvas/user_id",[0=>__('Všichni dispečeři')] + $dispecer_list, ['id'=>'CanvasUserId','data-type'=>'canvas_user_id','class'=>'get_params','default'=>$loggedUser['id']]); ?>
			
			<?php echo $this->Html->link('<span id="canvas_count" class="none">0</span>', '#',['title'=>__('Obnovit plochu'),'class'=>'refresh fa  fa-refresh','id'=>'refresh_canvas','escape' => false]);?>
			<?php echo $this->Html->link(__('Vybrat vše'), '#',['title'=>__('Vybrat vše'),'class'=>'select_checkboxs','data-type'=>'all','escape' => false]);?>
			<?php echo $this->Html->link(__('Odebrat vše'), '#',['title'=>__('Odebrat vše'),'class'=>'select_checkboxs ','data-type'=>'none','escape' => false]);?>
		</div>
		<div id="map_filtr" class="none">
			<?php echo $this->Form->input("map.adresa",['options'=>$adresa_type,'label'=>false]); ?>
			<?php echo '<div class="filtr_txt">Vybráno <span id="map_count">0</span> zakázek </div>';?>
			<?php echo $this->Form->input("map.car_filtr",['options'=>[],'label'=>false]); ?>
		
		</div>
		<div id="load_canvas" class="load_data fst_drop canvas" data-type="canvas"><ul class="ul_drag"></ul></div>
		
	</div>	
	<div id="sklad_list" class="none">
		<?php 
		if (isset($sklad_type[$system_id])){ 
			echo json_encode($sklad_type[$system_id]);
		}
		?>
	</div>
	
</div>
<?php echo $this->Html->script("fst_drag/fst_modal"); ?>
<?php echo $this->Html->script("fst_drag/fst_car"); ?>
<?php echo $this->Html->script("fst_drag/fst_drag"); ?>
