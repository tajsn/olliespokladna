<?php if (isset($params_viewIndex['filtr'])){ ?>
<div id="filtration">
	<strong class="title"><?php echo $title;?></strong>
	<?php 
	if (isset($params_viewIndex['top_action'])){
		echo '<ul id="top_action">';
		foreach($params_viewIndex['top_action'] AS $key=>$item){
			$fce = explode('|',$key);
			//pr($fce);
			echo $this->Html->link($item, ['action' => $fce[0]], ['class' => 'button '.(isset($fce[1])?$fce[1]:''), 'ajax' => (!isset($fce[1])?true:false)]);
		}
		echo '</ul>';
	}
	?>
	
	<?php 
	//pr($ano_ne);
	foreach($params_viewIndex['filtr'] AS $key=>$item){ 
		//pr($item);
		//list($name,$model,$type,$select_list) = explode('|',$item);
		$item = explode('|',$item);
		if (isset($item[0])) $name = $item[0];
		if (isset($item[1])) $model = $item[1];
		if (isset($item[2])) $type = $item[2];
		if (isset($item[3])) $select_list = $item[3];
		if (isset($item[4])) $url = $item[4];
		
		if ($type == 'text'){
			echo $this->Form->input($model, ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text fst_h','data-fparams'=>$model,'type'=>'text']);
		}
		if ($type == 'date'){
			echo $this->Form->input($model, ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text fst_h date','data-fparams'=>$model.'|date']);
		}
		if ($type == 'date_from'){
			echo $this->Form->input($model, ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text fst_h date','data-fparams'=>$model.'|date_from']);
		}
		if ($type == 'date_to'){
			echo $this->Form->input($model, ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text fst_h date','data-fparams'=>$model.'|date_to']);
		}
		if ($type == 'mesto_auto'){
			echo $this->Form->input($model.'S', ['class'=>'text fst_h none','label'=>false,'data-fparams'=>$model.'|mesto_auto']);
			echo $this->Form->input($model, ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text autocom','data-type'=>$select_list,'data-url'=>$url,]);
			echo '<div id="auto_loading_adresanalozeni-mesto" class="auto_loading none"></div>';
		}
		if ($type == 'text_like'){
			echo $this->Form->input($model.'|like', ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text fst_h','data-fparams'=>$model.'|like','data-like'=>true]);
		}
		if ($type == 'text_from'){
			echo $this->Form->input($model.'|from', ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text fst_h','data-fparams'=>$model.'|from']);
		}
		if ($type == 'text_to'){
			echo $this->Form->input($model.'|to', ['title' => $name,'placeholder' => $name,'label'=>false,'class'=>'text fst_h','data-fparams'=>$model.'|to']);
		}
		if ($type == 'select'){
			echo '<div class="input text">';
				//echo $this->Form->label($model, $name.':');
				if ($select_list == 'stoly_group_list'){
					$select_list = $stoly_group_list[$loggedUser['system_id']];
					echo $this->Form->select($model,[''=>$name]+$select_list, ['class'=>'text fst_h','data-fparams'=>$model,'empty'=>false]);
				} else {
					echo $this->Form->select($model,[''=>$name]+$$select_list, ['class'=>'text fst_h','data-fparams'=>$model,'empty'=>false]);
				}
			echo '</div>';
		}
	}
	echo $this->Form->button(__('Filtrovat'), ['class' => 'btn small','id'=>'FiltrRun']);
	echo $this->Form->button(__('Zrušit'), ['class' => 'btn small','id'=>'FiltrCancel']);
	?>
</div>
<?php } ?>