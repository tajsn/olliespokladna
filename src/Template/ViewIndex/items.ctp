<?php  
if (isset($items) && count($items)>0){	
//pr($params_viewIndex);
echo '<table class="table-formated">';
		$first = $items->first()->toArray();
		//pr($items->toArray());
		//pr($order_list);
		//pr($viewIndex['th']);
			if (isset($order_list)){
				$params_viewIndex['order_list'] = $order_list;
				foreach($order_list AS $o){
					foreach($first as $key => $item){
					//pr($item);
					if ($o == $key){
						$th = $this->Autotable->th($key, $item);
					}
					
					}
				}
			} else {
				foreach($first as $key => $item){
					//pr($key);
					$th = $this->Autotable->th($key, $item);
				}
			}
		
		echo '<thead>';
			/*
			if (isset($params_viewIndex['stats_data'])){
				echo '<tr>';
				$first = $items->first()->toArray();
				foreach($first AS $k=>$f){
					echo '<th class="stats_data fixed_top '.$k.'" >';
						if (isset($params_viewIndex['stats_data'][$k])){
							if ($k == 'price'){
								echo $this->Fastest->price($params_viewIndex['stats_data'][$k]);
							} else {
								echo $params_viewIndex['stats_data'][$k];
							
							}
						}
					echo '</th>';
				}				
				//pr($params_viewIndex['stats']);	
				echo '</tr>';
			}
			*/
			echo '<tr>';
				if (isset($params_viewIndex['checked'])){
					echo '<th class="fixed_top"></th>';
				}
				echo $this->Autotable->tableth;
				if (isset($params_viewIndex['posibility']) && !empty($params_viewIndex['posibility'])){
					echo '<th class="fixed_top">';
						echo __('Možnosti');
					echo '</th>';
				}
			echo '</tr>';
			
		echo '</thead>';
		
		echo '<tbody id="items">'; 
			foreach($items as $key => $item){
			
				
				if (isset($params_viewIndex['sum_list_col'])){
						$it = $item->toArray();
						
						
						foreach($it AS $itK=>$itV){
							if (isset($params_viewIndex['sum_list_col'][$itK])){
								$params_viewIndex['sum_list_col'][$itK] += $itV;
								
							}
							//pr($itK);
						}
				}
			
				if (isset($params_viewIndex['sum_list'])){
						$it = $item->toArray();
						
						if (isset($it['storno']) && $it['storno'] == 1){
							$is_storno = true;
						} else {
							$is_storno = false;
						
						}
						
						foreach($it AS $itK=>$itV){
							if ($itK == 'storno' && $itV == 1){
								$params_viewIndex['sum_list']['is_storno'] += $it['total_price'];
							}
							if (isset($params_viewIndex['sum_list'][$itK])){
								$params_viewIndex['sum_list'][$itK] += $itV;
								
							}
							//pr($itK);
						}
				}
				
				echo '<tr class="item-row '.(($key++ % 2) ? "even" : "odd").' '.((isset($is_storno) && $is_storno == true) ? "storno_line none" : "normal_line").'" click-id="'.$item['id'].'" ';
					if (isset($params_viewIndex['row_params'])){
						if (isset($params_viewIndex['row_params']['link'])){
							echo 'data-link="'.$params_viewIndex['row_params']['link'].$item['id'].'"';
						}
					}
				echo '>';
					if (isset($params_viewIndex['checked'])){
						echo '<td>'. $this->Form->input("checked".$item['id'], ['data-id'=>$item['id'],'label' => false,'type'=>'checkbox','class'=>'checked_list']).'</td>';
					}
					
					
					echo $this->Autotable->td($item->toArray(),$params_viewIndex);
					
					
					if (isset($params_viewIndex['posibility']) && !empty($params_viewIndex['posibility'])){
						echo '<td class="posibility">';
						foreach($params_viewIndex['posibility'] AS $key=>$pos){
							$pos_data = explode('|',$pos);
							$pos = $pos_data[0];
							
							if ($key == 'clone_el'){
								echo $this->Html->link($pos, ['action' => $key,$item['id']], ['class' => 'button clone small '.$key]);
							
							} else if ($key == 'trash'){
								echo $this->Html->link($pos, '/'.$key.'/'.(isset($params_viewIndex['model'])?$params_viewIndex['model']:$this->request->params['controller']).'/'.$item['id'], ['class' => 'button small trash_button','onclick'=>'return false;']);
						
							} else {
								echo $this->Html->link($pos, ['action' => $key,$item['id']], ['class' => 'button small '.$key.' '.(isset($pos_data[1])?$pos_data[1]:''), 'ajax' => (isset($pos_data[1])?false:true)]);
							
							}
						}
						echo '</td>';
					}
					
				echo '</tr>';
			}
			if (isset($params_viewIndex['sum_list_col'])){
				echo '<tr class="trzba_total">';
				foreach($params_viewIndex['order_list'] AS $col){
					if (in_array($col,['price','price_without_tax','price_sum','price_sum_without_tax','nakup_price','nakup_price_vat'])){
						$tdClass = 'price';
					}
					
					echo '<td class="'.(isset($tdClass)?$tdClass:'').'">';
						if (isset($params_viewIndex['sum_list_col'][$col])){
							echo $params_viewIndex['sum_list_col'][$col];
						}
					echo '</td>';
				}
				echo '</tr>';
				//pr($params_viewIndex['sum_list_col']);
			}
			
			//pr($params_viewIndex['sum_list']);
		if (isset($params_viewIndex['sum_list'])){
			$col = count($order_list);
			 //pr($params_viewIndex['stats_data']);
			$col = 5;
			echo '<tr class="trzba_total">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>Tržba celkem</td>';
				echo '<td>'.$params_viewIndex['sum_list']['total_price'].'</td>';
				echo '<td>'.$params_viewIndex['sum_list']['pay_hotovost'].'</td>';
				echo '<td>'.$params_viewIndex['sum_list']['pay_karta'].'</td>';
				echo '<td>'.$params_viewIndex['sum_list']['pay_poukazka'].'</td>';
				echo '<td>'.$params_viewIndex['sum_list']['pay_sleva'].'</td>';
				echo '<td>'.$params_viewIndex['sum_list']['pay_zam'].'</td>';
				echo '<td colspan="'.$col.'"></td>';
				//echo '<td colspan="'.$col.'">a</td>';
			
			echo '</tr>';
			$col = 10;
			echo '<tr class="trzba_yellow">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>Rozvoz</td>';
				echo '<td>0</td>';
				echo '<td colspan="'.$col.'"></td>';
			
			echo '</tr>';
			echo '<tr class="trzba_yellow">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>Vklad/výběr</td>';
				echo '<td>0</td>';
				echo '<td colspan="'.$col.'"></td>';
			
			echo '</tr>';
			echo '<tr class="trzba_yellow">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td><a href="#" class="show_storno">Storno</a></td>';
				echo '<td>'.$params_viewIndex['sum_list']['is_storno'].'</td>';
				echo '<td colspan="'.$col.'"></td>';
			
			echo '</tr>';
			echo '<tr class="trzba_yellow">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>Sleva</td>';
				echo '<td>0</td>';
				echo '<td colspan="'.$col.'"></td>';
			
			echo '</tr>';
			echo '<tr class="trzba_yellow">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>Vratka</td>';
				echo '<td>0</td>';
				echo '<td colspan="'.$col.'"></td>';
			
			echo '</tr>';
			echo '<tr class="trzba_yellow">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>Vráceno na sklad</td>';
				echo '<td>0</td>';
				echo '<td colspan="'.$col.'"></td>';
			
			echo '</tr>';
			echo '<tr class="trzba_yellow">';
				echo '<td></td>';
				echo '<td></td>';
				echo '<td>Přesuny na účtech</td>';
				echo '<td>0</td>';
				echo '<td colspan="'.$col.'"></td>';
			
			echo '</tr>';
		}
		echo '</tbody>';

echo '</table>';
//echo '</div>';
echo $this->element('pagination');
} else {
	echo '<div class="noresult">'.__('Nenalezeny žádné záznamy, zkuste upravit filtraci').'</div>';
}
//pr($this->request->params['controller']);
?>