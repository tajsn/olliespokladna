<div id="basket_products_obal">
<div id="notable_select" class="hide_basket">
	<?php echo __('Nejprve vyberte stůl');?>
</div>
<table id="basket_products" class="hide_basket none">
	<thead>
	<tr>
		<td colspan="4" id="tableName"></td>
	</tr>
	<tr class="header">
		<!--<th class="col_num"><?php echo __('Číslo');?></th>-->
		<th><?php echo __('Název');?></th>
		<th class="col_ks"><?php echo __('Ks');?></th>
		<th class="col_price"><?php echo __('Cena');?></th>
		<th class="col_pos"></th>
	</tr>
	</thead>
	<tbody id="basket_body">
		<tr data-template="data-template" data-if-tr_class="parser" id="line_id_{{line_id}}" data-order_item_id="line{{id}}" data-parent_id="{{parent_id}}"  data-line="{{line_id}}" class="{{tr_class}}">
			<td class="col_parse" data-order_item_id="{{opt.line_count}}" data-id="{{id}}" colspan="3">{{name}}</td>
			<td class="col_pos" ><input type="button" class="small button delete_button change_ks" value="X" data-line="{{line_id}}" /></td>
		</tr>
		<tr data-template="data-template" id="line_id_{{line_id}}" data-order_item_id="line{{id}}" data-parent_id="{{parent_id}}"  data-line="{{line_id}}" class="product_line {{tr_class}}">
			<td class="col_name" data-order_item_id="{{opt.line_count}}" data-id="{{id}}"><span class="detail">{{name}}</span>
				<div class="note none">
					<textarea type="text" class="note_area" id="Note{{line_id}}" data-line="{{line_id}}" >{{note}}</textarea>
					<span class="more">?</span>
					<div class="more_show none">
						<label>Vytvořeno: </label>{{created}}<br />
						<label>Terminál: </label>{{terminal_name}}<br />
						<label>Uživatel: </label>{{user_name}}<br />
					</div>
				</div>
			</td>
			<td class="col_ks detail {{parse_hide}}" ><input type="text" class="ks_input change_ks" id="KsLine{{line_id}}" value="{{count}}" data-line="{{line_id}}" /></td>
			<td class="col_price row_price {{parse_hide}}" >{{row_price}}</td>
			<td class="col_pos" ><input type="button" class="small button delete_button change_ks" value="X" data-line="{{line_id}}" /></td>
		</tr>
		<tr class="basket_empty" data-template-fallback="data-template-fallback"><td colspan="3"><?php echo __('Nejprve vyberte produkt z nabídky'); ?></td></tr>
	</tbody>
	<tfoot>
		<tr class="table_total">
			<th colspan="2"><?php echo __('Cena celkem')?>:</th>
			<td colspan="2" class="col_price" id="total_price">0.00</td>
		</tr>
	</tfoot>
</table>
<?php echo '<div id="loggedUser" data-id="'.$loggedUser['id'].'"></div>'; ?>
<?php echo '<div id="loggedTerminalId" data-id="'.$loggedUser['terminal_id'].'"></div>'; ?>
<?php echo '<div id="terminal_list" data-list=\''.json_encode($terminal_list).'\'></div>'; ?>
<?php echo '<div id="user_list" data-list=\''.json_encode($user_list).'\'></div>'; ?>

<div class="none">
<?php echo $this->Form->input("rozdelit",['class'=>'','id'=>'OrderRozdelit','type'=>'text']);?>
</div>
<div id="basket_total_price">
<?php //echo $this->Form->input("id"); ?>
<?php echo $this->Form->input("id",['class'=>'hide_price','id'=>'OrderId','type'=>'text','label'=>false]);?>
<?php echo $this->Form->input("table_id",['class'=>'hide_price','id'=>'TableId','label'=>false,'type'=>'text']); ?>
<?php echo $this->Form->input("total_price",['class'=>'hide_price','id'=>'TotalPrice','label'=>false,'type'=>'text']); ?>
<?php echo $this->Form->input("history_value",['id'=>'HistoryValue','label'=>false,'type'=>'text','class'=>'none']); ?>
<?php echo $this->Form->texarea("products_tmp",['class'=>'hide_price','id'=>'ProductsLoad','label'=>false]); ?>
</div>



<div id="basket_function" class="hide_basket none">
	<?php echo $this->Form->input('check_ukoncit',['type'=>'hidden','id'=>'CheckUkoncit']); ?>
	<ul>
		<li><?php echo $this->Form->input(__('Ukončit'),['class'=>'button basket_fce','data-fce-type'=>'ukoncit','class'=>'ukoncit','label'=>false,'type'=>'button']); ?></li>
		<li><?php echo $this->Form->input(__('Zaplatit'),['class'=>'button basket_fce','data-fce-type'=>'zaplatit','class'=>'zaplatit','label'=>false,'type'=>'button']); ?></li>
	</ul>
</div>

<div id="basket_pay" class="none">
		<div id="pay_top">
			<?php echo $this->Form->input(__('Zrušit'),['class'=>'button bill_fce black','data-type'=>'zpet','label'=>false,'type'=>'button','id'=>'billHide']); ?>
			<?php echo $this->Form->input(__('Zpět'),['class'=>'button bill_fce black none','data-fce-type'=>'bill_back','label'=>false,'type'=>'button','id'=>'bill_back']); ?>
		
		</div>
		<div id="pay_products" class="co">
			<?php echo $this->Form->input(__('Vybrat vše'),['class'=>'button basket_fce black','data-fce-type'=>'select_all','label'=>false,'type'=>'button']); ?>
			<div id="pay_products_list" class="product_list_bill"></div>
		</div>
		<div id="pay_bill"  class="co">
			<ul id="bill_products"></ul>
			<div id="bill_total">Celkem: <span id="bill_total_price"></span></div>
		</div>
		<div id="pay_fce"  class="co">
			<div id="pay_fce1" class="pay_fce_field">
				<?php echo $this->Form->input(__('Zaplatit'),['class'=>'button bill_fce','data-type'=>'done','label'=>false,'type'=>'button']); ?>
				<?php //echo $this->Form->input(__('Platba výběr'),['class'=>'button bill_fce','data-type'=>'pay_select','label'=>false,'type'=>'button']); ?>
				<?php //echo $this->Form->input(__('Platba část'),['class'=>'button bill_fce','data-type'=>'pay_part','label'=>false,'type'=>'button']); ?>
				<?php //echo $this->Form->input(__('Rozdělit'),['class'=>'button bill_fce','data-type'=>'rozdelit','label'=>false,'type'=>'button']); ?>
				<?php echo $this->Form->input(__('Členství'),['class'=>'button bill_fce','data-type'=>'clenstvi','label'=>false,'type'=>'button']); ?>
				<?php echo $this->Form->input(__('Storno'),['class'=>'button bill_fce','data-fce-type'=>'cancel','label'=>false,'type'=>'button']); ?>
			</div>
			<div id="pay_fce2" class="pay_fce_field none">
				<?php echo $this->Form->input(__('Hotově'),['class'=>'button pay_fce','data-platba_type_id'=>1,'data-type'=>'hotove','label'=>false,'type'=>'button']); ?>
				<?php echo $this->Form->input(__('Kartou'),['class'=>'button pay_fce','data-platba_type_id'=>2,'data-type'=>'kartou','label'=>false,'type'=>'button']); ?>
				<?php echo $this->Form->input(__('Tisk účtenky'),['class'=>'button pay_fce','data-type'=>'print_ucet','label'=>false,'type'=>'button']); ?>
				<?php echo $this->Form->input(__('Sleva'),['class'=>'button bill_fce','data-type'=>'sleva','label'=>false,'type'=>'button']); ?>
				<?php //echo $this->Form->input(__('Stravenka'),['class'=>'button pay_fce','data-type'=>'hotove','label'=>false,'type'=>'button']); ?>
			
			</div>
		</div>
		
</div>

<div id="stul_rozdelit" class="none">
		<div id="stul_top">
			<?php echo $this->Form->input(__('Zpět'),['class'=>'button stul_fce black nonae','data-fce-type'=>'stul_back','label'=>false,'type'=>'button','id'=>'stul_back']); ?>
		
		</div>
		<ul class="stul_col lft table_select" data-type="table_prod_left"  id="table_left"></ul>
		<div class="stul_col lft_product" id="table_prod_left" data-check="table_right" data-adopt="table_prod_right"><div class="itms product_list_bill"></div><div class="select_all">Vybrat vše</div></div>
		<div class="stul_col rght_product" id="table_prod_right" data-check="table_left" data-adopt="table_prod_left"><div class="itms product_list_bill"></div><div class="select_all">Vybrat vše</div></div>
		<ul class="stul_col rght table_select" data-type="table_prod_right"   id="table_right"></ul>
		<?php /* ?>
		<div id="stul_products" class="co">
			<?php echo $this->Form->input(__('Vybrat vše'),['class'=>'button stul_fce black','data-fce-type'=>'select_all','label'=>false,'type'=>'button','id'=>'vybrat-vse-stul']); ?>
			<div id="stul_products_list"></div>
		</div>
		<div id="stul_fce"  class="co">
			<div id="stul_fce1" class="pay_fce_field">
				<?php echo $this->Form->input(__('Přesunout'),['class'=>'button stul_fce','data-type'=>'move','label'=>false,'type'=>'button']); ?>
			</div>
		</div>
		<?php */ ?>
		
</div>

<div id="basket_product_detail" class="none">
	<div id="basket_product_detail_close">X</div>
	<?php  
		echo $this->Form->input("note",['type'=>'textarea','placeholder'=>__('Poznámka'),'label'=>false,'id'=>'OrderItemNote','class'=>'show_keyboard']);
		echo $this->Form->input("Uložit",['type'=>'button','label'=>false,'class'=>'button','id'=>'SaveNote']);
	?>
	
	<div id="basket_product_detail_txt"></div>

</div>

<div id="modal_sleva" class="pay_modal none">
	<div class="pay_modal_close">X</div>
	<?php echo $this->Form->input("sleva_value",['id'=>'SlevaValue','label'=>false,'type'=>'number','class'=>'text integer','placeholder'=>__('Sleva v částce')]); ?>
	<?php echo $this->Form->input("sleva_perc",['id'=>'SlevaPerc','label'=>false,'type'=>'number','class'=>'text integer','placeholder'=>__('Sleva v procentech')]); ?>
	<?php echo $this->Form->input(__('Přidat slevu'),['class'=>'button','id'=>'AddSleva','label'=>false,'type'=>'button']); ?>
	
</div>

<div id="modal_pay_part" class="pay_modal none">
	<div class="pay_modal_close">X</div>
	<?php echo $this->Form->input("pay_part_ks",['id'=>'PayPartValue','label'=>false,'type'=>'number','class'=>'text integer','placeholder'=>__('Zadejte částku')]); ?>
	<?php echo $this->Form->input(__('Zaplatit část'),['class'=>'button','id'=>'PayPartUcet','label'=>false,'type'=>'button']); ?>
	
</div>
<div id="modal_printer_setting" class="pay_modal none">
	<div class="pay_modal_close">X</div>
	<div id="printers_data" class="none"></div>
	<?php /**/ ?>
	<ul class="default_printer_list">
		<li>Vychozí tiskárna:
			<select class="printer_list" data-id="default" id="printer_default_select"></select>
		</li>
	</ul>
	<?php /**/?>
	<div id="printer_default" class="none"><?php echo $setting_global->data['default_printer'];?></div>
	<?php gen_group($product_group_list,'printer_prod_group');?>
	<?php echo $this->Form->input(__('Uložit nastavení tiskáren'),['class'=>'button','id'=>'SavePrinterSetting','label'=>false,'type'=>'button']); ?>
	
</div>



<div id="modal_rozdelit" class="pay_modal none">
	<div class="pay_modal_close">X</div>
	<?php echo $this->Form->input("rozdelit_ks",['id'=>'RozdelitValue','label'=>false,'type'=>'number','class'=>'text integer','placeholder'=>__('Na kolik částí rozdělit?')]); ?>
	<?php echo $this->Form->input(__('Rozdělit účet'),['class'=>'button','id'=>'RozdelitUcet','label'=>false,'type'=>'button']); ?>
	
</div>

<div id="basket_change_ks" class="none">
	<strong><?php echo __('Kolik ks upravit?'); ?></strong>
	<?php echo $this->Form->input("change_ks",['class'=>'integer show_keyboard_num','id'=>'ChangeKs','label'=>false,'type'=>'number']); ?>
	<?php //echo $this->Form->input(__('Zavřít'),['class'=>'button','id'=>'ChangeKsButton','label'=>false,'type'=>'button']); ?>
	<?php echo $this->Form->input(__('Upravit'),['class'=>'button','id'=>'ChangeKsButton','label'=>false,'type'=>'button']); ?>
</div>



<div id="rozdelit_window" class="order_modal none">
	<div class="order_modal_close">X</div>
	<h2><?php echo __('Rozdělit účet');?></h2>
	<div class="sll" id="rozdelit_products_table"> 
		<h3><?php echo __('Produkty na stole');?></h3>
	</div>
	<div class="slr"> 
		<h3><?php echo __('Produkty k úhradě');?></h3>
		
	</div>

</div>

</div>