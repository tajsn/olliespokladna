<fieldset id="client_data">
	<legend><?= __('Údaje zákazníka'); ?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("client.id",['label'=>false]); ?>
		<?php echo $this->Form->input("client.prijmeni",['label'=>__('Příjmení'),'tabindex'=>1]); ?>
		<?php echo $this->Form->input("client.tel_pref",['label'=>__('Telefon'),'options'=>$tel_pref,'empty'=>false,'class'=>'mobil_pref']); ?>
		<?php echo $this->Form->input("client.telefon",['label'=>false,'tabindex'=>3,'maxlength'=>9,'class'=>'integer mobil']); ?>
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("client.jmeno",['label'=>__('Jméno'),'tabindex'=>2]); ?>
		<?php echo $this->Form->input("client.email",['label'=>__('Email'),'tabindex'=>4]); ?>
	</div>
</fieldset>