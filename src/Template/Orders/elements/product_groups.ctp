<?php 
echo '<div class="products_group_side">'; 
//pr($product_group_list);die();
if (isset($product_group_list) && count($product_group_list)>0){ 
	function gen_group($data,$id=null,$level=1){
		//pr($id);die();
		if (!in_array($id,['printer_prod_group','printer_sub'])){
			$class = 'product_group_list_sub product_group_list level_'.$level;
		} else {
			$class = '';
			
		}
		//pr($id);
		echo '<ul  data-level="'.$level.'" class="'.$class.' ">';
		if ($level >= 3){
			echo '<li class="back none">'.__('Zpět').'</li>';
		}
		
		foreach($data AS $k=>$pg){
				
				echo '<li class="'.(!in_array($id,['printer_prod_group','printer_sub'])?'noselect product_group_li':'').'" data-id="'.$pg['id'].'" data-color="'.(isset($pg['color'])?$pg['color']:'').'">';
					if (in_array($id,['printer_prod_group','printer_sub'])){
						echo '<div>';
						echo $pg['name'];
						echo '<select class="printer_list" data-id="'.$pg['id'].'"></select>';
						echo '</div>';
					} else {
						echo $pg['name'];
						
					}
					if (isset($pg['children']) && !empty($pg['children'])){
						
						gen_group($pg['children'],((!in_array($id,['printer_prod_group','printer_sub']))?'product_group_list_sub':'printer_sub'),$pg['level']+1);
					}
				echo '</li>';
				//die();
			}
		echo '</ul>';
	}
	//pr($product_group_list);
	
	
	$i = 0;
	foreach($product_group_list AS $k=>$pg){
		
		echo '<div class="group_tabs '.(($i!=0)?'none':'').'" id="tab'.$k.'">';
			gen_group($product_group_list[$k]['children'],'product_group_list');	
		echo '</div>';
		$i++;
	}	
	
}
echo '</div>';

//pr($product_list);die();
if (isset($product_list) && count($product_list)>0){
	//echo '<ul class="product_group_list" id="product_group_list_sub"></ul>';
	echo '<div id="product_list">';
	foreach($product_list AS $k=>$p){
		echo '<div id="group_id'.$k.'" class="group_id_hide">';
		echo '<div class="product_group">'.$product_group_name[$k].'</div>';
		foreach($p AS $prod){
			$prod['cat_select'] = $k;
			//pr($prod);
			$keys = array_keys($prod);
			//pr($
			//pr($keys);
			echo '<div class="product noselect" ';
				echo 'data-product=\''.json_encode($prod).'\'';
				echo '>';
				echo '<span class="name">'.$prod['name'].'</span>';
				echo '<span class="num">'.$prod['num'].'</span>';
				echo '<span class="price">'.$this->Fastest->price($prod['price']).'</span>';
			
				if (isset($prod['product_conn_addons']) && !empty($prod['product_conn_addons'])){
					echo '<div class="product_addons none">';
						echo '<div class="product noselect back_product">';
							echo '<span class="name">'.__('Zpět').'</span>';
						
						echo '</div>';
						echo '<div class="product noselect without_addon">';
							echo '<span class="name">'.__('Bez přívlastku').'</span>';
						
						echo '</div>';
						//pr($prod);die();	
						foreach($prod['product_conn_addons'] AS $addon){
							echo '<div class="product noselect paddon" data-parent_id="'.$prod['id'].'" data-product=\''.json_encode($addon['product_addon']).'\' >';
								echo '<span class="name">'.$addon['product_addon']['name'].'</span>';
								echo '<span class="price">'.$this->Fastest->price($addon['product_addon']['price']).'</span>';
				 
							echo '</div>';
						}
					echo '</div>';
				}
			
			echo '</div>';
		}
		echo '</div>';
		//pr($p);
	}
	echo '<div class="noresult none" id="noresult_product">'.__('V této kategorii nejsou žádné produkty').'</div>';
	echo '</div>';
}
//pr($product_list);die();

?>