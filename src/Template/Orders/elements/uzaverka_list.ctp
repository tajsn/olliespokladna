<?php 
if (isset($uzaverka_list)){
	echo '<table class="table-formated">';
		echo '<tr>';
			echo '<th>Datum</th>';
			echo '<th>Od</th>';
			echo '<th>Do</th>';
			echo '<th>Datum otevření</th>';
			echo '<th>Datum uzavření</th>';
			echo '<th>Uživatel</th>';
			echo '<th>Možnosti</th>';
		echo '</tr>';
		foreach($uzaverka_list AS $u){
			echo '<tr>';
				echo '<td>'.$u->created.'</td>';
				echo '<td>'.$u->order_id_from.'</td>';
				echo '<td>'.$u->order_id_to.'</td>';
				echo '<td>'.$u->open_date.'</td>';
				echo '<td>'.$u->done_date.'</td>';
				echo '<td>'.$user_list[$u->user_id].'</td>';
				echo '<td>';
					echo $this->Form->button("Náhled",['type'=>'button','class'=>'button small UzaverkaShow','data-type'=>'nahled','data-id'=>$u->id]);
					echo $this->Form->button("Tisk",['type'=>'button','class'=>'button small UzaverkaShow','data-type'=>'nahled','data-id'=>$u->id,'data-print'=>true]);
				echo '</td>';
			echo '</tr>';
		}
	echo '</table>';
} else {
	echo '<p class="noresult">Nenalezeny žádné uzávěrky</p>';
}

?>