<div id="modal_uzaverkas" class="modal_win uzaverka none">
	<div class="over"></div>
	<div class="content">
		<div class="big_modal_close">X</div>
		<div class="content_in ">
			<div id="uzaverkas">
				<div class="sl_left"> 
					<div class="buttons">
						<?php echo $this->Form->button("Náhled uzávěrky",['class'=>'button btn2 UzaverkaShow','data-type'=>'nahled','data-confirm'=>'Opravdu zobrazit náhled']);?><br />
						<?php echo $this->Form->button("Uzavřít směnu",['class'=>'button btn2 UzaverkaShow','data-type'=>'done','data-print'=>true,'id'=>'UzavritSmenuButton','data-confirm'=>'Opravdu uzavřít směnu?']);?>
					</div>
					<div id="uzaverka_list" class="">
						
						<?php //echo $this->element('../Orders/elements/uzaverka_list'); ?>
					</div>
				</div>
				<div class="sl_right">
					<div class="relative">
						<div id="iframe_preloader" class="none"></div>
						<?php /* ?><iframe src="" id="uzaverkas_frame"></iframe> */?>
						<div id="uzaverkas_frame" class="print_data">
							<div class="txt">Nejprve zvolte Náhled <br />nebo Uzavření směny</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		
		
		</div>
	</div>
</div>
