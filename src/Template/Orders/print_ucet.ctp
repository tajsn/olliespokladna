<div id="ucet">
	
	<div class="header">
	<img src="logo.png" class="logo"><br />
	
	<?php echo $print_ucet_top;?></div>
	<table>
		<tr>
			<td></td>
			<td id="p_ucet_modified"></td>
		</tr>
		<tr>
			<td>Daňový doklad: <span id="p_ucet_doklad">Nebylo možné získat z internetu</span></td>
		</tr>
		<tr>
			<td>Tisk provedl:  <span id="p_ucet_user"><?php echo $loggedUser['name']?></span><span id="p_ucet_user_id" class="none"><?php echo $loggedUser['id']?></span></td>
		</tr>
	</table>
	<table class="products">
		<thead>
		<tr>
			<th>Položka</th>
			<th>Množ.</th>
			<th>DPH</th>
			<th>Kč/jed.</th>
			<th class="text_right">Cena Kč</th>
		</tr>
		</thead>
		<tbody id="print_ucet_products">
		
		</tbody>
		<?php 
		$total_price = [
			'with_tax'=>0,
			'without_tax'=>0,
			'tax'=>[
				1=>0,
				2=>0,
			],
		];		
		?>
		<?php //foreach($order->order_items AS $item){ ?>
			<?php 
			/*
			if (!empty($item->ks_rozdelit)){ 
				$item->ks = $item->ks_rozdelit;
			}
			<tr>
				<td><?php echo $item->name;?></td>
				<td><?php echo $item->ks;?></td>
				<td><?php echo $price_tax_list[$item->price_tax_id];?></td>
				<td class="text_right"><?php echo $this->Fastest->price($item->price);?></td>
				<td class="text_right"><?php echo $this->Fastest->price($item->price*$item->ks);?></td>
			</tr>
			*/
			?>
			<?php   
			//$total_price['with_tax'] += $item->price*$item->ks;
			//$total_price['without_tax'] += ($item->price - $item->price*$tax_value2[$item->price_tax_id]) * $item->ks;
			//$total_price['tax'][$item->price_tax_id] += ($item->price*$tax_value2[$item->price_tax_id]*$item->ks );
			//pr($item->price *$item->ks - $item->price*$tax_value2[$item->price_tax_id]*$item->ks);
			?>
		<?php //} ?>
		<tfoot>
		<tr class="bottom"><td colspan="5"></td></tr>
		</tfoot>
	</table>
	<?php //pr($order); ?>
	<?php //pr($total_price); ?>
	<table class="total">
		<tr>
			<th>CELKEM s DPH</th>
			<th class="text_right" id="p_ucet_price_with_tax"></th>
		</tr>
		<tr>
			<td>Platba:</td>
			<td class="text_right"  id="p_ucet_platba"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td></td>
		</tr>
		<?php /* ?>
		<?php foreach($total_price['tax'] AS $k_tax=>$tax){ ?>
		<?php if ($tax > 0){ ?>
		<tr>
			<td>DPH <?php echo $price_tax_list[$k_tax];?></td>
			<td class="text_right"><?php echo $this->Fastest->price($tax); ?></td>
		</tr>
		<tr>
			<td>Základ DPH <?php echo $price_tax_list[$k_tax];?></td>
			<td class="text_right"><?php echo $this->Fastest->price($total_price['without_tax']); ?></td>
		</tr>
		<?php } ?>
		<?php } ?>
		<?php */ ?>
	</table>
	<div class="footer">
		<?php echo $print_ucet_bottom;?>
		<div class="copyright">
		<br />-----------------------------------------------------------<br />
		www.fastest.cz
		</div>
	</div>
</div>
<?php //pr($order); ?>