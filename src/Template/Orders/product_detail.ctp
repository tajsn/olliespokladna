<?php /* ?>
<table class="product_detail">
	<tr>
		<th><?php echo __('Produkt')?>:</th><td><?php echo $product->name; ?></td>
		<th><?php echo __('Počet ks')?>:</th><td><?php echo $product->ks ?></td>
	</tr>
	<tr>
		<th><?php echo __('Vytvořeno')?>:</th><td><?php echo $product->created; ?></td>
		<th><?php echo __('Cena')?>:</th><td><?php echo $product->price ?></td>	
	</tr>
</table>
<br />
<?php */ ?>

<?php 

echo $this->Form->create($product_details,['id'=>'OrderFormDetail']);
//pr($zakazka);
//pr($product);
//echo $this->Form->hidden("id",['value'=>(isset($product->id)?$product->id:''),'id'=>'OrderItemId']);
echo $this->Form->hidden("original_id",['id'=>'OrderItemOriginalId','value'=>$original_id,'type'=>'text']);
echo $this->Form->input("note",['value'=>(isset($product->note)?$product->note:''),'type'=>'textarea','placeholder'=>__('Poznámka'),'label'=>false,'id'=>'OrderItemNote','class'=>'show_keyboard']);
echo $this->Form->input("Uložit",['type'=>'button','label'=>false,'class'=>'button','id'=>'SaveNote']);
echo $this->Form->end();

if (isset($history_list) && count($history_list)>0){ 
	echo '<table class="product_detail">';
		echo '<tr>';
			echo '<th>'.__('Vytvořeno').'</th>';
			echo '<th>'.__('Typ').'</th>';
			echo '<th>'.__('Ks').'</th>';
			echo '<th>'.__('Uživatel').'</th>';
		echo '</tr>';
	foreach($history_list AS $h){
		echo '<tr>';
			echo '<td>'.$h->created.'</td>';
			echo '<td>'.$h->name.'</td>';
			echo '<td>'.$h->value.'</td>';
			echo '<td>'.(isset($user_list[$h->user_id])?$user_list[$h->user_id]:'').'</td>';
		echo '</tr>';
	}
	echo '</table>';
	//pr($user_list);
}
?>