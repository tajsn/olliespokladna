<?php  
function table_line($data,$price_tax_list,$products_list,$level=0){
	$out = '';
	//pr($data);
	if ($level == 1) $skupina_name = 'Skupina:';
	if ($level == 2) $skupina_name = '';
	
	$out .= '<tr>
		<td colspan="3" class="group_name group'.$level.'">'.(($level > 0)?$skupina_name:'').' '.$data['name'].'</td>
	</tr>';
	
	$out .='
	<tr><td>&nbsp;</td><td>DPH 0%</td><td class="price">0</td></tr>
	<tr><td>&nbsp;</td><td>DPH 10%</td><td class="price">0</td></tr>
	<tr><td>Sebou</td><td>DPH 15%</td><td class="price">0</td></tr>
	<tr><td>Tady</td><td>DPH 21%</td><td class="price">0</td></tr>
	';
	
	
	if (isset($data['children'])){
		$level ++;
		
		foreach($data['children'] AS $dd){
			//if ($level < 2)
			$out .= table_line($dd,$price_tax_list,$products_list,$level);
		}
	}
	
	/*
	$out .= '<tr>
		<td>'.$data['name'].'</td>
		<td>'.$price_tax_list[$data['price_tax_id']].'</td>
	</tr>';
	*/
	return $out;
}
?>
<div class="print_data">
<table>
	<tr>
		<th class="bold">Uzávěrka (<?php echo $uzaverka_name;?>) - <?php echo $uzaverka_date;?></th>
	</tr>
</table>
<?php //pr($product_group_list); ?>
<?php //pr($price_tax_list); ?>
<?php //pr($products_list); ?>
<table>
	<?php 
	$total = [
		1=>0,
		2=>0,
		3=>0,
		4=>0,
		'price'=>0,
	];
	foreach($platba_list AS $platba_id=>$platba){
		$total['platba'][$platba_id] = 0;
	}
	
	if(isset($products_data)){
	
		foreach($products_data AS $k=>$p){
			//pr($p);
			echo '<tr class="group"><td colspan="2" class="group_name group'.$p['level'].'">'.$p['name'].'</td><td class="price group_name">'.((count($p['items'])>0)?'('.count($p['items']).'ks)':'&nbsp;').'</td></tr>';
				
				echo '<tr class="dph_line"><td>&nbsp;</td><td>DPH 0%</td><td class="price">'.$this->Fastest->price($p['data'][3],['symbol_after'=>'']).'</td></tr>';  // 3
				echo '<tr class="dph_line"><td>&nbsp;</td><td>DPH 10%</td><td class="price">'.$this->Fastest->price($p['data'][4],['symbol_after'=>'']).'</td></tr>'; // 4
				echo '<tr class="dph_line"><td>Sebou</td><td>DPH 15%</td><td class="price">'.$this->Fastest->price($p['data'][1],['symbol_after'=>'']).'</td></tr>'; // 1
				echo '<tr class="dph_line"><td>Tady</td><td>DPH 21%</td><td class="price">'.$this->Fastest->price($p['data'][2],['symbol_after'=>'']).'</td></tr>'; // 2
				echo '<tr class="dph_line"><td>Celkem s DPH</td><td>&nbsp;</td><td class="price">'.$this->Fastest->price($p['total'],['symbol_after'=>'']).'</td></tr>'; // total

			
			$total[3] += $p['data'][3];
			$total[4] += $p['data'][4];
			$total[1] += $p['data'][1];
			$total[2] += $p['data'][2];
			
			$total['price'] += $p['data'][1]+$p['data'][2]+$p['data'][3]+$p['data'][4];
			
			
			// platba sum
			if (isset($p['platba_list'])){
				foreach($p['platba_list'] AS $platba_key => $platba_data){
					foreach($platba_data AS $platba_price){
						$total['platba'][$platba_key] += $platba_price; 
					}
				}
			}
		}
		
		echo '<tr><td colspan="4">&nbsp;</td></tr>';
		
		echo '<tr><td>&nbsp;</td><td>DPH 0%</td><td class="price">'.$this->Fastest->price($total[3],['symbol_after'=>'']).'</td></tr>';
		echo '<tr><td>&nbsp;</td><td>DPH 10%</td><td class="price">'.$this->Fastest->price($total[4],['symbol_after'=>'']).'</td></tr>';
		echo '<tr><td>&nbsp;</td><td>DPH 15%</td><td class="price">'.$this->Fastest->price($total[1],['symbol_after'=>'']).'</td></tr>';
		echo '<tr><td>&nbsp;</td><td>DPH 21%</td><td class="price">'.$this->Fastest->price($total[2],['symbol_after'=>'']).'</td></tr>';
		echo '<tr><td>Celkem:</td><td>&nbsp;</td><td class="price">'.$this->Fastest->price($total['price'],['symbol_after'=>'']).'</td></tr>';
		
		echo '<tr><td colspan="4">&nbsp;</td></tr>';
		
		echo '<tr><td colspan="3" class="group_name group1">Způsob úhrady</td></tr>';
		foreach($platba_list AS $platba_key=>$platba){
			echo '<tr><td>'.$platba.'</td><td>&nbsp;</td><td class="price">'.$this->Fastest->price($total['platba'][$platba_key],['symbol_after'=>'']).'</td></tr>';
		}
		
		echo '<tr><td colspan="4">&nbsp;</td></tr>';
		
		echo '<tr><td colspan="3" class="group_name group1">DPH</td></tr>';
		echo '<tr><td>Základ DPH 0%</td><td>&nbsp;</td><td class="price">'.$this->Fastest->price($total[3]-($total[3]*$dph_conf[3]),['symbol_after'=>'']).'</td></tr>';
		echo '<tr><td>Základ DPH 10%</td><td>&nbsp;</td><td class="price">'.$this->Fastest->price($total[4],['symbol_after'=>'']).'</td></tr>';
		echo '<tr><td>Základ DPH 15%</td><td>&nbsp;</td><td class="price">'.$this->Fastest->price($total[1],['symbol_after'=>'']).'</td></tr>';
		echo '<tr><td>Základ DPH 21%</td><td>&nbsp;</td><td class="price">'.$this->Fastest->price($total[2]-($total[2]*$dph_conf[2]),['symbol_after'=>'']).'</td></tr>';
		
		echo '<tr><td colspan="4">&nbsp;</td></tr>';
		
		echo '<tr><td colspan="3">Tisk provedl '.$loggedUser['name'].'</td></tr>';
		echo '<tr><td colspan="3">Zahájení: '.$zahajeni.'</td></tr>';
		echo '<tr><td colspan="3">Ukončení: '.$ukonceni.'</td></tr>';
		
		
		
		
	}
	//pr($products_data);
	//pr($product_group_list);
	//die();
	
	
	?>
</table>
</div>
<?php 
//pr($total);
	
?>