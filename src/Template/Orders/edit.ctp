<?php
echo $this->Form->create($orders,['id'=>'OrderForm']);
//pr($zakazka);
//pr($loggedUser);
echo $this->Form->input("user_id",['value'=>$loggedUser['id'],'type'=>'hidden']);
echo $this->Form->input("user_group_id",['value'=>$loggedUser['group_id'],'type'=>'hidden']);

//echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);
//pr($product_list);
?>
<style>
#canvas {
	border:1px solid red;
	
}
</style>
<?php /* ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script type="text/javascript" src="/js/html2canvas/html2canvas.js"></script>
<?php */ ?>
<div id="isUpdate" class="none">
    <div class="content">Pokladna se aktualizovala, proveďte ctrl + F5</div>
</div>
<div id="orders_over">
	<?php echo $this->element('../Orders/elements/top_menu'); ?>
	
	<div class="col first"> 
		<?php //echo $this->element('../Orders/elements/clients'); ?>
		<?php echo $this->element('../Orders/elements/tables'); ?>
		
	</div>
	<div class="col second">
		
		<?php echo $this->element('../Orders/elements/product_groups'); ?>
		<?php echo $this->element('../Orders/elements/product_parser'); ?>
		
	</div> 
	<div class="col third"> 
		<?php echo $this->element('../Orders/elements/basket_products'); ?>
	</div>
	<?php echo $this->element('../Orders/elements/map_table'); ?>
	
	
</div>
<div class="clear"></div>
<div id="order_footer">
	<ul class="m_left">
		<li><?php echo $this->Html->link(__('Výběr stolů'),'/',['title'=>__('Výběr stolů'),'class'=>'button','id'=>'show_map_table'],null,false); ?></li>
		<li id="clear_data"><a>Vymazat</a></li>
	</ul>
	<?php echo $this->Html->link(__('Nastavení'),'#',['title'=>__('Nastavení'),'class'=>'button','id'=>'showFooterMenu'],null,false); ?>
	<ul id="footer_order_menu" class="none">
		<li><?php //echo $this->Html->link(__('Uzávěrky'),'#',['title'=>__('Uzávěrky'),'class'=>'button','id'=>'UzaverkasModalOpen'],null,false); ?></li>
		<li><?php //echo $this->Html->link(__('Tržby směny'),'#',['title'=>__('Tržby směny'),'class'=>'button','id'=>'OrdersModalOpen'],null,false); ?></li>
		<li><?php echo $this->Html->link(__('Uzavřít směnu'),'#',['title'=>__('Uzavřít směnu'),'class'=>'button','id'=>'UzaverkasModalOpen'],null,false); ?></li>
		<li><?php echo $this->Html->link(__('Nastavení tiskárny'),'#',['title'=>__('Nastavení tiskárny'),'class'=>'button','id'=>'SettingPrinters'],null,false); ?></li>
		<li><?php echo $this->Html->link(__('Nastavení systém'),'/',['title'=>__('Nastavení'),'class'=>'button'],null,false); ?></li>
	</ul>
</div>

<div id="print_ucet" class="nonea">
	<?php //echo $this->element('../Orders/print_ucet'); ?>
</div>
<?php echo $this->element('../Orders/elements/modal_uzaverka'); ?>
<?php /* ?>
<div id="print_order_part" class="nonea">
	<?php //echo $this->element('../Orders/print_order_part'); ?>
</div> 

<div id="modal_orders" class="big_modal none">
	<div class="over"></div>
	<div class="content">
		<div class="big_modal_close">X</div>
		<div class="filtr">
			<?php echo $this->Form->input('TrzbaDate',['label'=>__('Datum tržby'),'id'=>'TrzbaDate','type'=>'select','options'=>[]]); ?>
		</div>
		<div class="content_in_template none"><?php echo $this->element('../Orders/json/order_item');?></div>
		<div class="content_in "></div>
	</div>
</div>
*/
?>
<?php echo $this->element('../Orders/elements/modal_open_smena');?>
<?php //echo $this->element('../Orders/elements/modal_uzaverka');?>

<?php
//echo $this->element('modal',['params'=>['client'=>__('Zákazník'),'zakazka'=>__('Zakázka'),'historie'=>__('Historie'),'informace'=>__('Informace'),]]);
/*
echo '<div class="buttons">';
	echo $this->Form->button(__('Zavřít objednávku'), ['type'=>'button','class' => 'btn btn2 modal_close','id'=>'CloseModalOrder']);
	echo $this->Form->button(__('Uložit objednávku'), ['class' => 'btn btn2','id'=>'SaveModalOrder']);
echo '</div>';
*/
echo $this->Form->end();
?>
<?php if (isset($request_direct)){ ?>
<script type="text/javascript">
//<![CDATA[
   window.addEvent('domready',function(){
	//window.fstPokladna.init_after_modal_load();
   });
//]]>
</script>
<?php } 
//echo $this->Html->script("fst_system/modal_window");

?>


