
<table>
<thead>
<tr>
	<td colspan="14">
	<table>
		
		<tr>
			<th>Datum</th>
			<td class="adopt_total text_right" data-type="datum"></td>
			
			<th>Tržba celkem</th>
			<td class="adopt_total text_right" data-type="price"></td>
			
			<th>Sleva</th>
			<td class="adopt_total text_right" data-type="sleva"></td>
		</tr>
		<tr>
			<th>Platba hotově</th>
			<td class="adopt_total text_right" data-type="hotove"></td>
			<th>Platba kartou</th>
			<td class="adopt_total text_right" data-type="kartou"></td>
			<th></th>
			<td></td>
		</tr>
	</table>

	</td>
</tr>
<tr>
	<td colspan="14">&nbsp;</td>
</tr>
<tr>
	<th>Otevření účtu</th>
	<th>Zaplacení účtu</th>
	<th>Název účtu</th>
	<th>Celkem tržba</th>
	<th>Hotovost</th>
	<th>Platební karta</th>
	<th>Poukázka</th>
	<th>Bonus</th>
	<th>ZAM</th>
	<th>Sleva</th>
	<th>Vytiskl</th>
	<th class="text_center">Položek</th>
	<th>ID</th>
	<th class="text_right"></th>
</tr>
</thead>
<tbody class="tbody">
<tr data-template id="Order_id{{id}}">
	<td>{{created}}
		<table class="none">
			<tr>
				<th>Produkt</th>
				<th>Ks</th>
				<th>Cena</th>
				<th>Přidal</th>
			</tr>
			<tr data-template-for="order_items">
				<td>{{name}}</td>
				<td>{{ks}}</td>
				<td>{{price}}</td>
				<td>{{username}}</td>
			</tr>
		</table>
	
	</td>
	<td>{{done_date}}</td>
	<td>{{table_name}}</td>
	<td class="price">{{total_price}}</td>
	<td>{{pay_hotovost}}</td>
	<td>{{pay_karta}}</td>
	<td>{{pay_bonus}}</td>
	<td>{{pay_poukazka}}</td>
	<td>{{pay_zam}}</td>
	<td>{{pay_sleva}}</td>
	<td>{{print_username}}</td>
	<td class="text_center">{{count_items}} ks</td>
	<td>{{id}}</td>
	<td class="text_right">
		<input type="button" class="button small show_items" value="Položky" />
		<input data-type="order" type="button" class="button small storno_items"  value="Storno" />
	</td>
</tr>
<tr class="orders_empty" data-template-fallback="data-template-fallback"><td colspan="12"><?php echo __('Nenalezeny žádné účty pro tento den'); ?></td></tr>
</tbody>
</table>
