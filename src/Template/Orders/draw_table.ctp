<div class="header_top"></div>
<div id="draw_canvas">
	<p><?php echo __('Nejprve vyberte skupinu stolů');?></p>
	<?php foreach($stoly_group_list[$loggedUser['system_id']] AS $k=>$s){ ?>
		
		<div data-id="<?php echo $k?>" class="canvas_group canvas_group<?php echo $k?> none">
			<div class="label"><?php echo $s;?></div>
			
			<?php 
			$setting_table_map['data'] = json_decode(json_encode($setting_table_map['data']), true);
			
			if (isset($setting_table_map['data'][$k])){ 
				foreach($setting_table_map['data'][$k] AS $d){
					
					echo '<div class="table" data-name="'.$d['name'].'" data-id="'.$d['id'].'" style="width:'.$d['width'].';height:'.$d['height'].';top:'.$d['top'].'px;left:'.$d['left'].'px;">';
						echo '<div class="name">'.$d['name'].'</div>';
						echo '<div class="move"></div>';
						echo '<div class="res"></div>';
						echo '<div class="del">&#xf00d;</div>';
					echo '</div>';
	
				}
			} 
			
			?>
			
		</div>
	<?php } ?>
	<?php /* ?>
	<div class="table">
		<div class="name">Tble 3</div>
		<div class="move"></div>
		<div class="res"></div>
	</div>
	<?php */ ?>
</div>
<div id="canvas_list">
	<ul id="group_select">
	<?php foreach($stoly_group_list[$loggedUser['system_id']] AS $k=>$s){ ?>
		
		<?php echo '<li data-id="'.$k.'">'.$s.'</li>'; ?>
	<?php } ?>
	</ul>
	<div id="table_groups">
		<?php foreach($stoly_group_list[$loggedUser['system_id']] AS $k=>$s){ ?>
			<div class="none table_group table_group<?php echo $k?>">
				<?php foreach($table_list[$k] AS $table){ ?>
					<?php if ($table->on_map == 0){ ?>
					<div class="table" data-id="<?php echo $table->id?>" data-name="<?php echo $table->name?>">
						<div class="name"><?php echo $table->name?></div>
						<div class="move"></div>
						<div class="res"></div>
						<div class="del">&#xf00d;</div>
					</div>
					<?php } ?>
					
				<?php } ?>
			
			</div>
		<?php } ?>
	</div>
	<?php //pr($table_list);
	/*
	?>
	<div class="table"><div class="name">Table 1</div><div class="move"></div><div class="res"></div></div>
	<div class="table"><div class="name">Table 2</div><div class="move"></div><div class="res"></div></div>
	<div class="table"><div class="name">Table 3</div><div class="move"></div><div class="res"></div></div>
	<div class="table"><div class="name">Table 4</div><div class="move"></div><div class="res"></div></div>
	<div class="table"><div class="name">Table 5</div><div class="move"></div><div class="res"></div></div>
	<div class="table"><div class="name">Table 6</div><div class="move"></div><div class="res"></div></div>
	<?php */ ?>
</div>
<script type="text/javascript" src='/js/fst_table/fst_table.js' />
