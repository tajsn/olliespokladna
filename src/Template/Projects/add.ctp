<?php
  echo $this->Flash->render("project");
  if(!isset($ok)) {
    echo $this->Form->create($project);
    echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);
    echo $this->Form->input("name", ['label' => __("Název projektu")]);
    echo $this->Form->input("project_id", ['label' => __("Skupina"), "options" => $groups, "empty" => __("Nepřiřazeno")]);
    echo $this->Form->input("description", ['label' => __("Popis projektu"), 'type' => 'textarea']);
    echo $this->Form->button(__("Uložit"));
    echo $this->Form->end();
  }
?>