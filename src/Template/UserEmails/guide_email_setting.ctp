<?php

echo $this->Form->create($userEmail, ['class' => 'col half mcenter', 'url' => ['controller' => 'user_emails', 'action' => 'guide_email_setting']]);
  echo '<p class="legend">'.__("Před prvním použitím je třeba něco málo nastavit.").'</p>';
  echo $this->Form->input("email", ['label' => __("emailová adresa")]);
  echo $this->Form->input("password", ['label' => __("heslo")]);
  echo $this->Form->input("imap_server", ['label' => __("imap server")]);
  echo $this->Form->input("smtp_server", ['label' => __("smtp server")]);
  echo $this->Form->button(__("Nastavit"), ['class' => 'btn btn-success']);
echo $this->Form->end();

echo $this->Html->script("UserEmails/guide_setting");