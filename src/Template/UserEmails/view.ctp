<?php

if(isset($messages)){

    echo '<table class="table-formated">';

    echo 'počet zpráv: '.$countMessages = count($messages);

    foreach($messages as $message){
        echo '<tr>';
            echo '<td>'.$message->getFrom().'</td>';
            echo '<td>'. ((strlen($message->getSubject()) > 20)? substr($message->getSubject(), 0, 20)."..." : $message->getSubject()) .'</td>';
            echo '<td>'.substr(strip_tags($message->getBodyText()), 0, 100).'</td>';
            echo '<td>'.$message->getDate()->format("d.n.Y").'</td>';
        echo '</tr>';
    }

    echo '</table>';
}

?>