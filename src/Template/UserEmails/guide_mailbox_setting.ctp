<?php

echo $this->Form->create($userEmail, ['class' => 'col half mcenter', 'url' => ['controller' => 'user_emails', 'action' => 'guide_email_setting']]);
  echo '<p class="legend">'.__("Emailová schránka obsahuje několik mailboxů. Vyberte prosím, které složky si přejete sledovat").'</p>';

  foreach($mailboxes as $box){
    echo $this->Form->input($box->getName(), ['type' => 'checkbox']);
  }

echo $this->Form->end();

echo $this->Html->script("UserEmails/guide_setting");