<style>
.price,.pref {
	width:100px !important;
}
label {
	width:300px !important;
}
.checkbox {
	width:25px !important;
}
#menu_items .group {
	background:#efefef;
}
</style>
<ul id="menu_items">
	<?php 
	//pr($menu_list);
	foreach($menu_list AS $pid=>$m){
		$class= '';
		if (strpos($m,'--') !== false && strpos($m,'---') === false){
			$class='group';
		}
		echo '<li class="'.$class.'">';
			echo $this->Form->input('product_connects.'.$pid.'.checked',['type'=>'checkbox','class'=>'checkbox','label'=>$m,'checked'=>(isset($products_group) && in_array($pid,$products_group)?'checked':false)]);
			echo $this->Form->input('product_connects.'.$pid.'.price',['type'=>'text','class'=>'price','label'=>false,'placeholder'=>'Cena']);
			echo $this->Form->input('product_connects.'.$pid.'.price2',['type'=>'text','class'=>'price','label'=>false,'placeholder'=>'Cena zam',]);
			echo $this->Form->input('product_connects.'.$pid.'.price3',['type'=>'text','class'=>'price','label'=>false,'placeholder'=>'Cena 3']);
			echo $this->Form->input('product_connects.'.$pid.'.price4',['type'=>'text','class'=>'price','label'=>false,'placeholder'=>'Cena 4']);
			echo $this->Form->input('product_connects.'.$pid.'.pref',['type'=>'text','class'=>'pref','label'=>false,'placeholder'=>'Prefix']);
		echo '</li>';
	}?>
</ul>