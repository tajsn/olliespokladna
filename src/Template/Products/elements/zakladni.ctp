<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("name", ['label' => __("Název")]); ?>
		
		<?php //echo $this->Form->input("price", ['label' => __("Cena s DPH")]); ?>
		<?php //echo $this->Form->input("price_zam", ['label' => __("Zam. Cena s DPH ")]); ?>
		<?php echo $this->Form->input("price_tax_id",['label'=>__('DPH'),'options'=>$price_tax_list,'empty'=>false]); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("num", ['label' => __("Číslo")]); ?>
		<?php echo $this->Form->input("code", ['label' => __("Kód")]); ?>
		<?php //pr($products_group[0]); ?>
		<?php //echo $this->Form->input("product_connects.product_group_id[]",['label'=>__('Skupina'),'class'=>'level_adopt','options'=>$group_list,'empty'=>true,'id'=>'select_cat1','data-select'=>'select_cat2','data-level'=>1,'value'=>(isset($products_group[0])?$products_group[0]:'')]); ?>
		<?php //echo $this->Form->input("product_connects.product_group_id[]",['label'=>__('Skupina 2'),'class'=>'level_adopt','options'=>$group_list,'empty'=>true,'id'=>'select_cat2','data-select'=>'select_cat3','data-level'=>2,'value'=>(isset($products_group[1])?$products_group[1]:'')]); ?>
		<?php //echo $this->Form->input("product_connects.product_group_id[]",['label'=>__('Skupina 3'),'class'=>'level_adopt','options'=>$group_list,'empty'=>true,'id'=>'select_cat3','data-level'=>3,'value'=>(isset($products_group[2])?$products_group[2]:'')]); ?>
		<?php echo $this->Form->input("group_trzba_id",['label'=>__('Skupina tržby'),'options'=>$group_trzba_list,'empty'=>true]); ?>
		<?php echo $this->Form->input("Kopírovat", ['label' => false,'class'=>'button','id'=>'CloneButton','type'=>'button']); ?>
		
	</div>
</fieldset>
<div id="group_list_level" class="none"><?php echo json_encode($group_list_level)?></div>
<?php echo $this->element('../Products/elements/product_addons'); ?>
<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
	$('CloneButton').addEvent('click',function(e){
		if (confirm('Opravdu kopírovat?')){
			//console.log(window.parent.document.getElementById('modalWindow'));
			//return false
			VarsModal.load_table = new Request.JSON({
				url:'/products/cloneEl/'+$('id').value,
				onComplete: VarsModal.compl_fce = (function(json){
					if (json.result == true){
						window.parent.document.getElementById('modalWindow').src = '/products/edit/'+json.id;
						
						
					} else {
					}
				}.bind(this))
			});
			VarsModal.load_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.load_table.send();
			
		}
	});

/*
   data = JSON.decode($('group_list_level').get('text'));
   selects = $$('.level_adopt');
  // console.log(selects);
   Object.each(selects,function(select){
		if (select.nodeName == 'SELECT'){
		
		select.getElements('option').each(function(option){
			//console.log(data[option.value]);
			
			if (data[option.value]){
				option.addClass('show_parent parent_'+data[option.value].parent_id);
				option.addClass('level_'+data[option.value].level);
				if (data[option.value].level != select.get('data-level').toInt()){
					option.addClass('none');
				}
			}
			
		});
		
		}
		
   });
   /*
   $$('.level_adopt').addEvent('change',function(e){
		if (e.nodeName == 'OPTION'){
			e = e.getParent('select');
			noclear = true;
		}
		//console.log(e.nodeName);
		//console.log(e);
		if (!e.target) e.target = e;
		level = e.target.get('data-level');
		parent_id = e.target.value;
		select = e.target.get('data-select');
		//console.log($(select));
		if (!noclear){
			$(select).value = '';
		}
		$(select).getElements('.show_parent').each(function(item){
			if (!item.hasClass('parent_'+parent_id) && item.hasClass('level_'+$(select).get('data-level'))){
				//console.log(item);
				item.addClass('none');
			} else {
				if (item.hasClass('level_'+$(select).get('data-level'))){
					item.removeClass('none');
				}
			}
		});
   });
   */
   //$('select_cat1').fireEvent('change',$('select_cat1'));
   //$('select_cat2').fireEvent('change',$('select_cat2'));
}); 
//]]>
</script>