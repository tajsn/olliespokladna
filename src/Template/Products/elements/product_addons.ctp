<fieldset>
	<legend><?php echo __("Přívlastky produktu")?></legend>
	<?php 
	if (isset($privlastky_list) && count($privlastky_list)>0){ 
		foreach($privlastky_list AS $kgroup=>$group){
			echo '<fieldset class="checkbox_list">';
			echo '<legend>'.$addon_list[$kgroup];
				echo $this->Html->link(__('Vybrat vše'),'#',array('title'=>__('Vybrat vše'),'class'=>'select_all_fieldset'),null,false);
			echo '</legend>';

			foreach($group AS $k=>$p){
				echo $this->Form->input('addon_list.addon_id.'.$p->id,array('label'=>$p->name,'type'=>'checkbox','class'=>'checkbox','checked'=>(isset($addon_list_load) && in_array($p->id,$addon_list_load)?true:false)));
			}
			
			echo '</fieldset>';
		}
	}
	?>
</fieldset>
