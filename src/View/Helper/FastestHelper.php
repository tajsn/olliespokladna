<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class FastestHelper extends Helper
{

  public $helpers = [];
  public $tableth = "";
  private $cname = [];

	public function qr_code($id,$size=300){
		$link = 'http://p-pospiech.fastest.cz/scan_load/'.$this->encode_long_url($id);
		//pr($link);
		return '<img src="https://chart.googleapis.com/chart?chs='.$size.'x'.$size.'&cht=qr&chl='.$link.'&choe=UTF-8" title="Potvrzení naložení / vyložení" />';
	}
	
	public function encode_long_url($data){
         return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
    }
    
	/*** DECODE LONG URL **/	
	public function decode_long_url($data){
    	$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
    }
	
	public function price($price,$mena_suf = array()){
		
		//pr($mena_suf);
		$symbol_before = '';
		$kurz = null;
		$count = 1;
		$decimal = 2;
		$symbol_after = '';
		extract($mena_suf);
		
    	$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbol_before.number_format(strtr($price,$white), $decimal, '.', ' ').$symbol_after;
		else
			return $symbol_before.number_format(strtr(0.00,$white), $decimal, '.', ' ').$symbol_after;	
	}
	
	

}