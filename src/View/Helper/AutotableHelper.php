<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class AutotableHelper extends Helper
{

  public $helpers = ['Paginator','Fastest'];
  public $tableth = "";
  private $cname = [];

  public function th($ckey, $cvalue){
    if(!is_array($cvalue)){
      $this->tableth .= '<th class="fixed_top">'.$this->Paginator->sort($ckey, self::translateCol($ckey)).'</th>';
    }
    else{
      foreach($cvalue as $k => $v) {
        self::th($ckey.".".$k, $v);
      }
    }
  }

  public function td($cvalue,$params_viewIndex=null){
    //pr($params_viewIndex['list']);
	if(!is_array($cvalue)){
     if (isset($this->format)){
		echo '<td class="'.$this->format.'">'.$this->Fastest->price($cvalue).'</td>';
	  } else {
		 echo '<td>'.$cvalue.'</td>'; 
      }
	  
    }
    else{
      if (isset($params_viewIndex['order_list'])){
		foreach($params_viewIndex['order_list'] AS $k){
			$v = $cvalue[$k];
			$this->each_data($v,$k,$params_viewIndex);
		}  
      } else {
		
	  foreach($cvalue as $k=>$v) {
		$this->each_data($v,$k,$params_viewIndex); 
		 
      }  
      }
    }
  }
  
  private function each_data($v,$k,$params_viewIndex){
			if (isset($params_viewIndex['list'][$k])){
				if ($k == 'user_id'){
					$params_viewIndex['list']['user_id'][1000] = 'Superadmin';
					
				}
				if (isset($params_viewIndex['list'][$k][$v])){
					
					$v = $params_viewIndex['list'][$k][$v];
					
				} else {
					$v = __('Nevybráno');
				}
			}
			if (in_array($k,['price','price_without_tax','price_sum','price_sum_without_tax','nakup_price','nakup_price_vat'])){
				$this->format = 'price';
			} else {
				if (isset($this->format)){
					unset($this->format);
				}
			}
			self::td($v);
  }


  // překládá název sloupce pokud je překlad vyplněn v config/autotable_translate.php kde je to děláno skrze get text.
  // může tam být jak překlad sloupce všeobecně, tak překlad sloupce konkrétně pro nějaký Model
  // Pokud ani to nepomůže, můžeš si ty překlady přepsat ve volání helperu, ale musíš si to trochu poupravit.
  // tzn. nastavíš si třeba proměnnou v controlleru a přidáš si ji jako parametr do volání fce th();
  private function translateCol($ckey)
  {
    if ($this->config("colTranslate." . @key($this->request->paging).".".$ckey)) {
      return $this->config("colTranslate." . @key($this->request->paging).".".$ckey);
    }
    else if($this->config("colTranslate.".$ckey)){
      return $this->config("colTranslate." . $ckey);
    }
    else{
      return $ckey;
    }
  }


}