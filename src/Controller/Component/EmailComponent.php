<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;


class EmailComponent extends Component
{
  public $fromEmail = "noreply@fastest.cz";
  public $from = "Ollies Pokladna";


  public function send($to, array $options = null){
    $email = new Email();
	//pr($options);die();
	
    $email->from($this->fromEmail, $this->from);
    $email->to($to)->emailFormat('html');
	$email->subject($options['subject']);
	//pr($options);
	if (isset($options['data']['SendErrorText'])){
	$email->viewVars(['data'=>$options['data']['SendErrorText']]);
    	
	} else {
	$email->viewVars(['data'=>$options['data']]);
    	
	}
	
	if(isset($options["template"])){
      $email->template($options["template"]);
    }

    $email->send();


  }




}