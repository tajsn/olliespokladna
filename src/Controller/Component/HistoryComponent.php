<?php
namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;


class HistoryComponent extends Component{
   
	public function saveH($opt){
		$controller = $this->_registry->getController();
		$controller->loadModel('Histories');
		//pr($opt);
		
		$save_history = $controller->Histories->newEntity($opt);
		//pr($save_history);
		$controller->Histories->save($save_history);
		
		return true;
	}
	
   
}