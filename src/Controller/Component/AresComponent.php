<?php
namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;


class AresComponent extends Component{
   var $ares_url = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=';
   //var $ares_url = 'http://www.fastest.cz';
   
	public function search($ic=28591232){
	   $this->ic = $ic;
	   $this->getData();
	   $this->parseData();
	   
	   return $this->result;
	}
	
	private function getData(){
		
		if ($curl = curl_init($this->ares_url.$this->ic)) {
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$content = curl_exec($curl);
			//$info = curl_getinfo($curl);
			curl_close($curl);
			$this->data = $content;
			//pr($content);
			
		}
		
	
	}
	
	private function parseData(){
		$result = array();
		if (isset($this->data)) {
			$xml = @simplexml_load_string($this->data);
		
			$ns = $xml->getDocNamespaces();
			
			$data = $xml->children($ns['are']);
			$el = $data->children($ns['D'])->VBAS;
			//pr($el);
			if (strval($el->ICO) == $this->ic) {
				$result['ico'] 	= strval($el->ICO);
				$result['dic'] 	= strval($el->DIC);
				$result['firma'] = strval($el->OF);
				$result['ulice']	= strval($el->AA->NU).' '.strval($el->AA->CD).'/'.strval($el->AA->CO);
				$result['mesto']	= strval($el->AA->N).'-'.strval($el->AA->NCO);
				$result['psc']	= strval($el->AA->PSC);
				$result['stat']	= strval($el->AA->NS);
				$result['result'] 	= true;
			} else {
				$result['result'] 	= false;
				$result['message'] 	= 'IČ firmy nebylo nalezeno';
			}
		} else {
			$result['result'] 	= false;
			$result['message'] 	= 'Databáze ARES není dostupná';
		}
		$this->result = $result;
	
	}
   
}