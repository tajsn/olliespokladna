<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;


class ViewIndexComponent extends Component
{
	
	function load($params){
		//pr($params);
		$table = TableRegistry::get($params['table']);
		$data = $table->find()
		  ->where($params['conditions'])
		  //->contain("Users")
		  ->select($params['fields']);
		
		return $data;
		//pr($data);
		//pr($params);
		/*
		if ($this->request->is('ajax')) { 
			$this->controller->render('ViewIndex/items');
		} else {
			
			$this->controller->render('ViewIndex/index');
		}
		*/
	}
	
	public function get_car_type(){
		$controller = $this->_registry->getController();
		//pr($controller->viewVars['car_type']);
		//pr($controller->viewVars['system_id']);
		$this->car_type_list = Hash::combine($controller->viewVars['car_type'][$controller->viewVars['system_id']], '{n}.id', '{n}.name');
		$controller->viewVars['car_type_list'] = $this->car_type_list;
		$controller->set('car_type_list',$this->car_type_list);
		//pr($this->car_type_list);
		//pr($controller->viewVars);
	}
	
	public function save_history($options){
		$controller = $this->_registry->getController();
		$controller->loadModel('Histories');
		$controller->loadModel('Zakazkas');
		
		//pr($controller->history_type);
		$options['name'] = $controller->history_type[$options['type']];
		$options['user_id'] = $controller->loggedUser['id'];
		
		if (isset($options['zakazka_id'])){
			$zakazka = $controller->Zakazkas->get_zakazka_driver($options['zakazka_id']);
			if (isset($zakazka['car_id'])) $options['car_id'] = $zakazka['car_id'];
			if (isset($zakazka['driver_id'])) $options['driver_id'] = $zakazka['driver_id'];
		}
		//pr($options);
		$save = $controller->Histories->newEntity($options);
		$controller->Histories->save($save);
		//die('a'); 
	}
	
	
	public function send_to_sklad($options){
		$controller = $this->_registry->getController();
		$controller->loadModel('SkladReports');
		$controller->loadModel('Zakazkas');
		$controller->loadModel('Cars');
		$controller->loadModel('Drivers');
		
		$zakazka_detail = $controller->Zakazkas->get_zakazka_detail($options['zakazka_id']);
		$car_detail = $controller->Cars->get_car_detail($zakazka_detail['zakazka_connect']['car_id']);
		$driver_detail = $controller->Drivers->get_driver_detail($zakazka_detail['zakazka_connect']['driver_id']);
		
		
		$save = [
			'sklad_id'=>$options['sklad_id'],
			'type'=>$options['type'],
			'message'=>$options['message'],
			'zakazka_id'=>$zakazka_detail['id'],
			'driver_id'=>$zakazka_detail['zakazka_connect']['driver_id'],
			'car_id'=>$zakazka_detail['zakazka_connect']['car_id'],
			'spz'=>$car_detail['spz'],
			'telefon'=>$driver_detail['tel_firemni'],
			'ridic'=>$driver_detail['name'],
			'adresa_nalozeni'=>serialize($zakazka_detail['adresa_nalozeni']),
			'adresa_vylozeni'=>serialize($zakazka_detail['adresa_vylozeni']),
			'size'=>$zakazka_detail['size'],
			'weight'=>$zakazka_detail['weight'],
			'code'=>$zakazka_detail['code_int'],
		];
		//pr($save);die();
		$save = $controller->SkladReports->newEntity($save);
		$controller->SkladReports->save($save);
		
		//pr($driver_detail);
		/*
		pr($save);	
		pr($zakazka_detail);
		pr($car_detail);
		pr($driver_detail);
		pr($options);
		*/
	}
	
	public function send_to_driver($options){
		$controller = $this->_registry->getController();
		$controller->loadModel('DriverReports');
		$controller->loadModel('Drivers');
		$controller->loadModel('Zakazkas');
		$controller->loadModel('Cars');
		
		$driver_id = $controller->Cars->carDriverId($options['car_id']);
		$car_detail = $controller->Cars->get_car_detail($options['car_id']);
		$driver_detail = $controller->Drivers->get_driver_detail($driver_id);
		$zakazka_detail = $controller->Zakazkas->get_zakazka_detail($options['zakazka_id']);
		if (isset($options['car_id2'])){
			$driver_id2 = $controller->Cars->carDriverId($options['car_id2']);
			$car_detail2 = $controller->Cars->get_car_detail($options['car_id2']);
			$driver_detail2 = $controller->Drivers->get_driver_detail($driver_id2);
			
			$options['spz2'] = $car_detail2['spz'];
			$options['ridic2'] = $driver_detail2['prijmeni'].' '.$driver_detail2['jmeno'];
			$options['telefon2'] = $driver_detail2['tel_firemni'];
					
		}
		//pr($car_detail);
		//pr($driver_detail);
		//pr($zakazka_detail);
		$options['spz'] = $car_detail['spz'];
		$options['ridic'] = $driver_detail['prijmeni'].' '.$driver_detail['jmeno'];
		$options['telefon'] = $driver_detail['tel_firemni'];
		$options['adresa_nalozeni'] = serialize($zakazka_detail['adresa_nalozeni']);
		$options['adresa_vylozeni'] = serialize($zakazka_detail['adresa_vylozeni']);
		$options['gps_nalozeni'] = $zakazka_detail['adresa_nalozeni']['lat'].','.$zakazka_detail['adresa_nalozeni']['lng'];
		$options['gps_vylozeni'] = $zakazka_detail['adresa_vylozeni']['lat'].','.$zakazka_detail['adresa_vylozeni']['lng'];
		$options['size'] = $zakazka_detail['size'];
		$options['weight'] = $zakazka_detail['weight'];
		$options['code'] = $zakazka_detail['code_int'];
		$options['driver_id'] = $driver_id;
		//$options['sklad_id'] = $options['sklad_id'];
		//pr($options);die('bb');
		
		$controller->PushMessage->send_message($driver_id,true);
		
		$save = $controller->DriverReports->newEntity($options);
		
		$controller->DriverReports->save($save);
		
	}
	
	public function client_adresa_list($client_id){
		// load adresa list
		$controller = $this->_registry->getController();
		$controller->loadModel('Adresas');
		
		$adresa_list_load = $controller->Adresas->find()
		  ->where(['client_id'=>$client_id])
		  ->select(['id','name','ulice','mesto','psc','stat','lat','lng','type','firma','zona','oblast_id'])
		  ->hydrate(false)
		  ->toArray();
		  $adresa_list_data = [];
		  foreach($adresa_list_load AS $k=>$a){
			$adresa_list_data[$a['type']][$a['id']] = implode('|',$a);   
		  }
		  //pr($adresa_list_load);
		  //pr($adresa_list_data);
		$controller->set('adresa_list_data',$adresa_list_data);
		return $adresa_list_data;
	}




}