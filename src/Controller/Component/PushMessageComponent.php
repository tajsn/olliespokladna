<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;


class PushMessageComponent extends Component
{

  public function send_message($user_id = null,$direct = false){
    $controller = $this->_registry->getController();
	$controller->loadModel('PushMessages');
		
		$conditions = [];
		if ($user_id != null){
			$conditions['user_id'] = $user_id;
		}
		$data = $controller->PushMessages->find()
		->where($conditions)
		->select([
			'id',
			'hash',
			'auth',
			'p256dh',
		])
		//->hydrate(false)
		->map(function($r){
			//pr($r);
			return $r;
		})
		->toArray()
		;
		//pr($data);
		// API access key from Google API's Console
		
		define( 'API_ACCESS_KEY', 'AIzaSyA1l7ncBTQEBgdtiCarAkuxKVDLfOkHVpQ' );
		$count = 0;
		if (isset($data) && count($data)>0){
			foreach($data AS $client){
				$reg_id = $client->hash;
				$msg = array
				(
					'message' 	=> 'nova zprava z api push',
					'title'		=> 'Pospiech Transport',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'icon'	=> 'http://www.fastest.cz/css/fastest/layout/logo.png',
					 
				);
				$fields = array
				(
					'registration_ids' 	=> [$reg_id],
					'data'			=> $msg,
					
				);
				$salt = mcrypt_create_iv(50, MCRYPT_DEV_URANDOM);
				//pr($fields);
				$headers = array
				(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json',
					'Encryption: '.$salt,
					'Encryption-Key: '.$client->p256dh,
					'TTL: 60',
					
				);
				 
				$ch = curl_init();
				
				
				
				curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
				//curl_setopt( $ch,CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				
				$info = curl_getinfo($ch);
				//pr($info);
				
				$result = curl_exec($ch );
				curl_close( $ch );
				//pr($client->p256dh);	
				$result = json_decode($result);
				//pr($result);
				if($result->failure == 1){
					$controller->PushMessages->deleteAll(['id' => $client->id]);
					//pr($client->id);
				} else {
					$count ++;
				}
				//echo $result;	
			}
			
		}
		//pr($direct);
		if ($direct == false){
			die(json_encode(['r'=>true,'m'=>__('Odesláno '.$count.' zpráv')]));
			
		} else {
			return true;
		}
  }




}