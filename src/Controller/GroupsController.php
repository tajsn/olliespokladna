<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\View\View;

class GroupsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->set("title", "Skupiny");
  }

  public function index()
  {
    $groups = $this->Groups->find("groups", ['where' => ["GroupUsers.user_id" => $this->Auth->user("id")]])
      ->select(["Users.id", "Users.name", "Groups.id", "Groups.name", "Groups.user_id"])
      ->map(function($r){
        $q = TableRegistry::get("GroupUsers");
        $r["members"] = $q->find()->where(["group_id" => $r["id"], "user_id !=" => $r["user"]["id"]])->contain("Users");
        return $r;
      });

    $this->set("groups", $groups->toArray());

  }

  public function add()
  {
    $this->loadModel("GroupUsers");
    $this->viewBuilder()->layout("ajax");

    $group = $this->Groups->newEntity();

    if($this->request->is("post")){
      $data = $this->request->data();

      $this->Groups->patchEntity($group, $data);
      if($this->Groups->save($group)){
        if(!empty($data["invation"][0])){
          $this->Groups->invate($data["invation"], $group->id);
        }
        $gu = $this->GroupUsers->newEntity(['group_id' => $group->id, 'user_id' => $this->Auth->user("id")]);
        $this->GroupUsers->save($gu);

        $this->redirect(["action" => 'view', $group->id]);
      }
    }

    $this->set(compact(['group']));
  }

  public function view($group_id)
  {
    $this->viewBuilder()->layout("ajax");
    $main = $this->Groups->get($group_id);

    $this->loadModel("Projects");
    $projects = $this->Projects->find("projects", ["user_id" => $this->Auth->user("id")])->where(["Projects.group_id" => $main->id]);
    $title = __("Skupiny - ").$main->name;

      $this->set(compact(["main", "title", "projects"]));
  }




}
