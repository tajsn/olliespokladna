<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\View\View;

use App\View\Helper\FastestHelper;

class DashboardController extends AppController
{
	
	public function initialize(){
		parent::initialize();
		$this->set("title", __('Nástěnka dispečera'));
	}

	public function index(){
		
		$this->loadModel('Users');
		$this->set('dispecer_list',$this->dispecer_list = $this->Users->dispecerList());
		
		//pr($dispecer_list);  
		//pr($this->loggedUser);
		
		
	}
	
	private function save_connect_driver_id($type,$driver_id,$save_value=null){
		if ($type == 'remove_driver'){
			$conditions = [
				'ZakazkaConnects.system_id'=>$this->system_id,
				'ZakazkaConnects.driver_id'=>$driver_id
			];
		}
		if ($type == 'add_driver'){
			$conditions = [
				'ZakazkaConnects.system_id'=>$this->system_id,
				'ZakazkaConnects.car_id'=>$driver_id
			];
		}
		$this->loadModel('ZakazkaConnects');
		$connect_ids = $this->ZakazkaConnects->find()
		->where($conditions)
		->select([
			'id',
			'car_id',
		])
		//->hydrate(false)
		->map(function($r){
			//pr($r);
			return $r->id;
		})
		->toArray()
		;
			
		if (isset($connect_ids) && !empty($connect_ids)){
			foreach($connect_ids AS $cid){
				$save_driver_id = $this->ZakazkaConnects->newEntity([
					'id'=>$cid,
					'driver_id'=>$save_value,
				]);
				//pr($save_driver_id);
				$this->ZakazkaConnects->save($save_driver_id);
			}
		}
	}
	
	
	// save data after drop
	public function saveData($type,$car_id,$zakazka_id=null,$weight=null,$size=null){
		// move car to sklad
		
		
		// car to sklad from cannvas
		if ($type == 'car_to_sklad'){
			$car_ids = explode(',',$car_id);
			$this->loadModel('Cars');
			
			foreach($car_ids AS $carId){
				
				// send to sklad report
				$zakazka_list = $this->Cars->zakazkaList($carId);
				//pr($zakazka_list);
				//pr($car);
				//pr($zakazka_id);
				if (isset($zakazka_list) && !empty($zakazka_list)){
					foreach($zakazka_list AS $zid){
						$opt = [
							'zakazka_id'=>$zid,
							'message'=>'Příjem auta na sklad',
							'sklad_id'=>$zakazka_id,
							'type'=>3,
						];
						$this->ViewIndex->send_to_sklad($opt);
					}
				}
				
				//die('a');
				
				// save history
				$this->ViewIndex->save_history(['type'=>'car_to_sklad','car_id'=>$carId,'sklad_id'=>$zakazka_id]);
			
				$save_car = $this->Cars->newEntity([
					'id'=>$carId,
					'on_dashboard'=>1,
					'sklad_id'=>$zakazka_id,
					'user_id'=>$this->loggedUser['id'],
				]);
				//pr($save_car);
				$this->Cars->save($save_car);
			}
		}
		
		// car zakazky to sklad from cannvas
		if ($type == 'car_zakazky_to_sklad'){
			$car_ids = explode(',',$car_id);
			foreach($car_ids AS $carId){
			
				//pr($carId);
				//pr($zakazka_id);
				
				$this->loadModel('ZakazkaConnects');
				$zakazka_list = $this->ZakazkaConnects->find()
				->where([
					"ZakazkaConnects.car_id" => $carId
				])
				->map(function($r){
					return $r->zakazka_id;
				})
				->toArray();
				//pr($zakazka_list);
				//die('a');
				
				
				$this->loadModel('Zakazkas');
				if (isset($zakazka_list) && count($zakazka_list)>0){
					
					foreach($zakazka_list AS $z){
						// save history
						$this->ViewIndex->save_history(['type'=>'car_zakazky_to_sklad','car_id'=>$carId,'sklad_id'=>$zakazka_id,'zakazka_id'=>$z]);
			
						
						$this->ZakazkaConnects->deleteAll(['zakazka_id'=>$z,'system_id'=>$this->system_id]);
						
						$save_zakazka = $this->Zakazkas->newEntity([
							'id'=>$z,
							'sklad_id'=>$zakazka_id,
							'on_car'=>0,
						]);
						//pr($save_zakazka);
						$this->Zakazkas->save($save_zakazka);
					
						
						
						// send to sklad report
						$opt = [
							'zakazka_id'=>$z,
							'message'=>'Příjem zakazky na sklad',
							'sklad_id'=>$zakazka_id,
							'type'=>1,
						];
						$this->ViewIndex->send_to_sklad($opt);
					
					}
				}
				$clear_zakazka = true;
			}
		}
		
		// move car to canvas
		if ($type == 'car_to_canvas'){
			$this->loadModel('Cars');
			//pr($car_id);
			
			$find_prives = $this->Cars->find()
			->where([
				'Cars.parent_id'=>$car_id
			])
			->select([
				"Cars.id"])
			->toArray();
			//pr($find_prives[0]->id);
			//die('a');
			
			// save history
			$this->ViewIndex->save_history(['type'=>'car_to_canvas','car_id'=>$car_id]);
		
			$save_car = $this->Cars->newEntity([
				'id'=>$car_id,
				'on_dashboard'=>1,
				'user_id'=>$this->loggedUser['id'],
			]);
			//pr($save_car);
			$this->Cars->save($save_car);
			
			if (isset($find_prives) && !empty($find_prives)){
				$prives_id = $find_prives[0]->id;
				//pr($prives_id);
				// save history
				$this->ViewIndex->save_history(['type'=>'car_to_canvas','car_id'=>$prives_id]);
			
				$save_prives = $this->Cars->newEntity([
					'id'=>$prives_id,
					'on_dashboard'=>1,
					'user_id'=>$this->loggedUser['id'],
				]);
				//pr($save_car);
				$this->Cars->save($save_prives);
					
			}
			
			
		}
		// driver to list from car
		if ($type == 'driver_to_list'){
			// save history
			$this->ViewIndex->save_history(['type'=>'driver_to_list','car_id'=>$car_id]);
			
			
			// remove driver from connect
			$this->save_connect_driver_id('remove_driver',$zakazka_id);
			//die('stop');
			
			$this->loadModel('Drivers');
			$save_driver = $this->Drivers->newEntity([
				'id'=>$zakazka_id,
				'on_dashboard'=>0,
			]);
			$this->Drivers->save($save_driver);
			
			
			
			$this->loadModel('ZakazkaConnectDrivers');
			$this->ZakazkaConnectDrivers->deleteAll(['car_id' => $car_id,'driver_id'=>$zakazka_id,'system_id'=>$this->system_id]);
			
			
		}
		
		// driver to car
		if ($type == 'driver_to_car'){
			$this->loadModel('ZakazkaConnectDrivers');
			$this->ZakazkaConnectDrivers->deleteAll(['driver_id'=>$zakazka_id,'system_id'=>$this->system_id]);
			
			
			// save driver ID
			$this->save_connect_driver_id('add_driver',$car_id,$zakazka_id);
			/*
			$this->loadModel('ZakazkaConnects');
			$connect_ids = $this->ZakazkaConnects->find()
			->where(['ZakazkaConnects.system_id'=>$this->system_id,'ZakazkaConnects.car_id'=>$car_id])
			  ->select([
				'id',
				'car_id',
			  ])
			//->hydrate(false)
			->map(function($r){
				//pr($r);
				return $r->id;
			})
			->toArray()
			;
			
			if (isset($connect_ids) && !empty($connect_ids)){
				foreach($connect_ids AS $cid){
					$save_driver_id = $this->ZakazkaConnects->newEntity([
						'id'=>$cid,
						'driver_id'=>$zakazka_id,
					]);
					//pr($save_driver_id);
					$this->ZakazkaConnects->save($save_driver_id);
				}
			}
			*/
			//pr($connect_ids);die();
			
			
			$this->loadModel('Drivers');
			$save_driver = $this->Drivers->newEntity([
				'id'=>$zakazka_id,
				'on_dashboard'=>1,
			]);
			$this->Drivers->save($save_driver);
			
			/*
			$entities = $splits->newEntities($this->request->data());
			foreach ($entities as $entity) {
				$splits->save($entity);
			}
			*/
			
			$this->loadModel('ZakazkaConnectDrivers');
			$save_connect = $this->ZakazkaConnectDrivers->newEntity([
				'driver_id'=>$zakazka_id,
				'car_id'=>$car_id,
			]);
			
			$this->ZakazkaConnectDrivers->save($save_connect);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'driver_to_car','car_id'=>$car_id,'zakazka_id'=>$zakazka_id]);
		
		}
		
		// driver to sklad
		if ($type == 'driver_to_sklad'){
			$driver_ids = explode(',',$car_id);
			foreach($driver_ids AS $driver_id){
				
				$this->loadModel('Drivers');
				$save_driver = $this->Drivers->newEntity([
					'id'=>$driver_id,
					'sklad_id'=>$zakazka_id,
				]);
				$this->Drivers->save($save_driver);
				
				
				// save history
				$this->ViewIndex->save_history(['type'=>'driver_to_sklad','driver_id'=>$driver_id,'sklad_id'=>$zakazka_id]);
			}
		}
		
		// move car from canvas
		if ($type == 'car_from_canvas'){
			$this->loadModel('Cars');
			$save_car = $this->Cars->newEntity([
				'id'=>$car_id,
				'on_dashboard'=>0,
				
				'predano'=>0,
				'user_id'=>null,
			]);
			$this->Cars->save($save_car);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'car_from_canvas','car_id'=>$car_id,'zakazka_id'=>$zakazka_id]);
			
		}
		
		// move zakazka to car
		if ($type == 'zakazka_to_car'){
			$this->loadModel('Zakazkas');
			$this->loadModel('ZakazkaConnects');
			$this->loadModel('ZakazkaConnectDrivers');
			
			// connect zakazka 
			$find_connect = $this->ZakazkaConnects->find()
			->where([
				"ZakazkaConnects.zakazka_id" => $zakazka_id
			])
			->map(function($r){
				return $r->car_id;
				
			})
			->toArray();
			
			// connect zakazka driver
			if (isset($find_connect[0])){
				$find_connect_driver = $this->ZakazkaConnectDrivers->find()
				->where([
					"ZakazkaConnectDrivers.car_id" => $find_connect[0]
				])
				->map(function($r){
					return $r->driver_id;
					
				})
				->toArray();
			}
			
			if (isset($find_connect[0])){
				$this->loadModel('Cars');
				$driver_id = $this->Cars->carDriverId($car_id);
			}
			
			
			//pr($find_connect);
			//pr($find_connect_driver);
			
			
			if (isset($find_connect) && !empty($find_connect)){
				$this->ZakazkaConnects->deleteAll(['zakazka_id'=>$zakazka_id,'system_id'=>$this->system_id,'weight'=>$weight,'size'=>$size]);
			
				// save history
				$car_id2 = $find_connect[0];
				if (isset($find_connect_driver[0])) $driver_id2 = $find_connect_driver[0];
				
				$this->ViewIndex->save_history(['type'=>'zakazka_to_another_car','car_id'=>$car_id,'zakazka_id'=>$zakazka_id,'car_id2'=>$car_id2,'driver_id2'=>(isset($find_connect_driver[0])?$find_connect_driver[0]:''),'driver_id'=>(isset($driver_id)?$driver_id:null)]);
				
				//die('a');
				
				
				
				$opt = [
					'car_id'=>$car_id2,
					'car_id2'=>$car_id,
					'zakazka_id'=>$zakazka_id,
					'message'=>'Přeložení na jiné auto',
					'type'=>3,
					'type_cesta'=>3,
					'type_name'=>'zakazka_to_another_car',
				];
				//pr($opt);
				$this->ViewIndex->send_to_driver($opt);
				
				$opt = [
					'car_id'=>$car_id,
					'car_id2'=>$car_id2,
					'zakazka_id'=>$zakazka_id,
					'message'=>'Přeložení z jiného auto',
					'type'=>5,
					'type_cesta'=>5,
					'type_name'=>'zakazka_from_another_car',
				];
				//pr($opt);
				$this->ViewIndex->send_to_driver($opt);
				
				/*	
				// save connect zakazka	second driver
				$save_connect = $this->ZakazkaConnects->newEntity([
					'zakazka_id'=>$zakazka_id,
					'car_id'=>$car_id2,
					'weight'=>$weight,
					'size'=>$size,
					'driver_id'=>(isset($driver_id)?$driver_id:null),
				]);
				$this->ZakazkaConnects->save($save_connect);
				*/
				
			} else {
				//die('aa');
				// save history
				$this->ViewIndex->save_history(['type'=>'zakazka_to_car','car_id'=>$car_id,'zakazka_id'=>$zakazka_id]);
				
				$this->ZakazkaConnects->deleteAll(['zakazka_id'=>$zakazka_id,'system_id'=>$this->system_id,'weight'=>$weight,'size'=>$size]);
					
			}
			
			$zakazka_detail = $this->Zakazkas->get_zakazka_detail($zakazka_id);
			//pr($zakazka_detail);die('eee');
			
			
			//die('a');
			
			// save zakazka on_car 
			$save_zakazka = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'on_car'=>1,
				'preposlano'=>(isset($car_id2)?1:0),
				'user_id2'=>(isset($user_id2)?$user_id2:null),
			]);
			$this->Zakazkas->save($save_zakazka);
			
			$this->loadModel('Cars');
			$driver_id = $this->Cars->carDriverId($car_id);
				
			// save connect zakazka	
			$save_connect = $this->ZakazkaConnects->newEntity([
				'zakazka_id'=>$zakazka_id,
				'car_id'=>$car_id,
				'weight'=>$weight,
				'size'=>$size,
				'driver_id'=>(isset($driver_id)?$driver_id:null),
			]);
			$this->ZakazkaConnects->save($save_connect);
			
			// send to sklad reports
			$opt = [
				'zakazka_id'=>$zakazka_id,
				'message'=>'Výdej zakazky na auto',
				'sklad_id'=>$zakazka_detail['sklad_id'],
				'type'=>2,
			];
			$this->ViewIndex->send_to_sklad($opt);
			
		}
		// delete zakazka from car
		if ($type == 'delete_from_car'){
			$this->loadModel('Zakazkas');
			
			$save_zakazka = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'on_car'=>0,
				'user_id'=>null,
			]);
			$this->Zakazkas->save($save_zakazka);
				
			$this->loadModel('ZakazkaConnects');
			$this->ZakazkaConnects->deleteAll(['car_id' => $car_id,'zakazka_id'=>$zakazka_id,'system_id'=>$this->system_id]);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'delete_from_car','car_id'=>$car_id,'zakazka_id'=>$zakazka_id]);
			
		}
		
		// done zakazka
		if ($type == 'done_zakazka'){
			$this->loadModel('Zakazkas');
			
			$save_zakazka = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'done'=>1,
				'user_id'=>null,
			]);
			$this->Zakazkas->save($save_zakazka);
				
			$this->loadModel('ZakazkaConnects');
			$this->ZakazkaConnects->deleteAll(['car_id' => $car_id,'zakazka_id'=>$zakazka_id,'system_id'=>$this->system_id]);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'done_zakazka','car_id'=>$car_id,'zakazka_id'=>$zakazka_id]);
			
		}
		
		// basket
		if ($type == 'car_to_basket'){
			
			$this->loadModel('Cars');
			$save_car = $this->Cars->newEntity([
				'id'=>$car_id,
				'user_id'=>$zakazka_id,
				'predano'=>1,
			]);
			$this->Cars->save($save_car);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'car_to_basket','car_id'=>$car_id,'user_id'=>$zakazka_id]);
		
		}
		
		// save join prives
		if ($type == 'join_prives'){
			$car_ids = explode(',',$car_id);
			$this->loadModel('Cars');
			if (count($car_ids) != 2){
				die(json_encode(['result'=>false,'message'=>__('Musíte vybrat 2 auta pro spojení')]));
			}
			$car_data = $this->Cars->find()
			->where([
				'Cars.id IN'=>$car_ids
			])
			->select([
				"Cars.id", "Cars.car_type"])
			->toArray();
			
			$is_prives = false;
			foreach($car_data AS $c){
				if ($c->car_type == 7){
					$is_prives = true;
					$prives_id = $c->id;
				} else {
					$car_id = $c->id;
				}
			}
			if($is_prives == false){
				die(json_encode(['result'=>false,'message'=>__('Musíte vybrat auto a jeden přívěs')]));
			}
			//pr($car_data);
			//pr($car_id);
			//pr($prives_id);
			//pr($car_ids);
			//die('a');
			$save_car = $this->Cars->newEntity([
				'id'=>$prives_id,
				'parent_id'=>$car_id,
			]);
			$this->Cars->save($save_car);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'save_prives','car_id'=>$car_id,'prives_id'=>$prives_id]);
			
		}
		// save unjoin prives
		if ($type == 'unjoin_prives'){
			$car_ids = explode(',',$car_id);
			$this->loadModel('Cars');
			
			foreach($car_ids AS $car_id){
			
				$save_car = $this->Cars->newEntity([
					'id'=>$car_id,
					'parent_id'=>null,
				]);
				$this->Cars->save($save_car);
				// save history
				$this->ViewIndex->save_history(['type'=>'remove_prives','car_id'=>$car_id]);
			}
			
		}
		/*
		// save prives
		if ($type == 'save_prives'){
			
			$this->loadModel('Cars');
			$save_car = $this->Cars->newEntity([
				'id'=>$zakazka_id,
				'parent_id'=>$car_id,
			]);
			
			$this->Cars->save($save_car);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'save_prives','car_id'=>$car_id,'prives_id'=>$zakazka_id]);
		
		}
		// remove prives
		if ($type == 'remove_prives'){
			
			$this->loadModel('Cars');
			$save_car = $this->Cars->newEntity([
				'id'=>$zakazka_id,
				'parent_id'=>null,
			]);
			$this->Cars->save($save_car);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'remove_prives','car_id'=>$car_id]);
		}
		*/
		die(json_encode(array('result'=>true)));
	}	
	
	// save data after resize
	public function saveOrder($type,$ids){
		if ($type == 'zakazka'){
			$this->loadModel('Zakazkas');
			
			$save_all = [];
			
			$ids = explode(',',$ids);
			
			foreach($ids AS $k=>$id){
				$save_all[] = [
					'id'=>$id,
					'sort'=>$k+1,
				];
			}
			//pr($save_all);			
			$entities = $this->Zakazkas->newEntities($save_all);
			//pr($entities);die();
			foreach ($entities as $entity) {
				//pr($entity->id);
				$this->Zakazkas->save($entity);
				//die('a');
			}

		}
		
		die(json_encode(array('r'=>true)));
	}
	
	// load modal
	public function loadModal($type,$car_id,$driver_id=null){
		
		$this->set('modal_edit',true);
		// ridic
		if ($type == 'zakazky'){
			$render = 'zakazky';
			$this->loadModel('ZakazkaConnects'); 
			$this->loadModel('Zakazkas');
			$this->loadModel('Cars');
			
			
			$this->set('car_data',$car_data = $this->Cars->get($car_id));
			
			$this->set('car_id',$car_id);
			// con list load
			$data_con_list =
				$this->ZakazkaConnects->find()
			->where([
				"ZakazkaConnects.car_id" => $car_id
			])
			->map(function($r){
				return $r->zakazka_id;
			})
			->toArray();


			//->hydrate(false)

			// data load
			if (isset($data_con_list) && !empty($data_con_list)){
			
				$data = $this->Zakazkas->find() 
				->where([
					"Zakazkas.kos" => 0,
					"Zakazkas.id IN" => $data_con_list 
				]
				)
				->contain(['Clients','AdresaNalozeni', 'AdresaVylozeni'])
				->select([])
				//->hydrate(false)
				->order('driver_load ASC, sort ASC , date_nalozeni ASC')
			
				->toArray();
			}
			
		}
		
		// ridic
		if ($type == 'driver'){
			$render = 'driver';
			
			$this->loadModel('ZakazkaConnectDrivers');
			$driver_ids = $this->ZakazkaConnectDrivers->find()
				->where([
					"ZakazkaConnectDrivers.car_id" => $car_id
				])->map(function($r){
					return $r->driver_id;
				})->toArray();
				
			//pr($driver_ids);
			if (!empty($driver_ids)){
			
			$this->loadModel('Drivers');
			$data = $this->Drivers->find()
			->where([
				"Drivers.id IN" => $driver_ids
			])
			->select(["Drivers.id", "Drivers.name", "Drivers.datum_narozeni", "Drivers.number", "Drivers.tel_firemni"])
			->toArray();
			}
		}
		//pr($data);
		if (isset($data)) $this->set('data',$data);
		$this->set('car_id',$car_id);
		
		$this->viewBuilder()->layout(false);
		$response = $this->render('load_elements/'.$render);
		
		$html = $response->body();
		
		if ($this->request->is('ajax')) { 
			die(json_encode(['result'=>true, "html" => $html]));
		}
	}	
	
	
	// load data to element
	public function loadData($type,$params=null){
		$params = json_decode($params,true);
		// zakazky
		if (isset($params['sklad_date_nalozeni'])) $params['date_nalozeni'] = $params['sklad_date_nalozeni'];
		if (isset($params['sklad_psc']) && $params['sklad_psc'] > 0) $params['psc'] = $params['sklad_psc'];
		if (isset($params['sklad_psc2']) && $params['sklad_psc2'] > 0) $params['psc2'] = $params['sklad_psc2'];
		if (isset($params['sklad_type']) && $params['sklad_type'] > 0) $params['type'] = $params['sklad_type'];
			
		
		if ($type == 'zakazky' || $type == 'zakazky_done' || $type == 'sklad'){
			$this->loadModel('Zakazkas');
			$conditions = [
				"Zakazkas.sklad_id IS" => null,
				"Zakazkas.on_car" => 0,
				"Zakazkas.done" => 0,
				"Zakazkas.kos" => 0,
			];
			
			$order = 'sort ASC , date_nalozeni ASC';
			
			// is sklad
			if ($type == 'sklad'){
				if (empty($params)){
					unset($conditions['Zakazkas.sklad_id IS']);
					$conditions['Zakazkas.sklad_id IS NOT'] = null;
				} else {
					unset($conditions['Zakazkas.sklad_id IS']);
					if ($params['sklad_id'] > 0){
						$conditions['Zakazkas.sklad_id'] = $params['sklad_id'];
					} else {
						$conditions['Zakazkas.sklad_id IS NOT'] = null;
						
					}
						
				}
			}
			
			// is done
			if ($type == 'zakazky_done'){
				unset($conditions['Zakazkas.on_car']);
				$conditions['Zakazkas.done'] = 1;
				
			}
			
			if (isset($params['type']) && $params['type'] > 0){
				$conditions['Zakazkas.type'] = $params['type'];
			}
			
			
			
			if (isset($params['date_nalozeni']) && $params['date_nalozeni'] != ''){
				$value_tmp = explode('.',$params['date_nalozeni']);
				$date = $value_tmp[2].'-'.$value_tmp[1].'-'.$value_tmp[0];
				$conditions['OR'] = [
					'Zakazkas.date_nalozeni' => $date,
					'Zakazkas.date_nalozeni2' => $date,
				];
			}
			if (isset($params['psc']) && $params['psc'] != ''){
				$conditions['AdresaNalozeni.psc'] = $params['psc'];
			}
			if (isset($params['psc2']) && $params['psc2'] != ''){
				$conditions['AdresaVylozeni.psc'] = $params['psc2'];
			}
			
			if (isset($params['sort']) && $params['sort'] != '0'){
				$order = $params['sort'];
			}
			//pr($order);
			//pr($conditions);
			$data = $this->Zakazkas->find()
			->where($conditions)
			->select([
				"Zakazkas.id", 
				"Zakazkas.name",
				"Zakazkas.done",
				"Zakazkas.sklad_id",
				"Zakazkas.size",
				"Zakazkas.weight",
				"Zakazkas.date_nalozeni",
				"Zakazkas.date_nalozeni2",
				"Zakazkas.date_vylozeni",
				"Zakazkas.date_vylozeni2",
				"Zakazkas.time_nalozeni",
				"Zakazkas.time_vylozeni",
				"Zakazkas.anomalie",
				"Zakazkas.price",
				"Zakazkas.adresa_nalozeni_id",
				"Zakazkas.adresa_vylozeni_id",
				"Zakazkas.dispecer_name",
				"Zakazkas.dispecer_tel",
				"Zakazkas.dispecer_mobile",
				
				"AdresaNalozeni.psc",
				"AdresaNalozeni.mesto",
				"AdresaNalozeni.ulice",
				"AdresaNalozeni.stat",
				"AdresaNalozeni.lat",
				"AdresaNalozeni.lng",
				
				"AdresaVylozeni.psc",
				"AdresaVylozeni.mesto",
				"AdresaVylozeni.ulice",
				"AdresaVylozeni.stat",
				"AdresaVylozeni.lat",
				"AdresaVylozeni.lng",
				
				"Clients.name",
				"Clients.mesto",
				"Clients.stat",
				
			])
			->order($order)
			->contain(['Clients','AdresaNalozeni', 'AdresaVylozeni'])
			->toArray(); 
			
		}
		// auta
		if ($type == 'auta'){
			
			$this->loadModel('Cars');
			$this->set('car_type_list',$this->car_type_list = $this->Cars->get_car_type());
			$this->set('car_color_list',$this->car_color_list = $this->Cars->get_car_color());
		
			$this->loadModel('Cars');
			$conditions = [
				"Cars.system_id" => $this->system_id,
				"Cars.on_dashboard" => 0,
				//"Cars.parent_id IS" => null
			];
			
			if (isset($params['car_sklad_id']) && $params['car_sklad_id'] > 0){
				$conditions['Cars.sklad2_id'] = $params['car_sklad_id'];
			}
			
			$data = $this->Cars->find()
			->where($conditions)
			->select([
				"Prives.id", 
				'Prives.name',
				'Prives.size_all',
				'Prives.weight_all',
				'Prives.spz',
				'Prives.car_type',
				'Prives.sklad2_id',
			"Cars.id", "Cars.name", "Cars.size_all", "Cars.weight_all",'Cars.spz','Cars.car_type', "Cars.sklad2_id",'Cars.parent_id'])
			->join(['Prives' => [
			  'table' => 'cars',
			  'type' => 'left',
			  'conditions' => 'Prives.parent_id = Cars.id'
			]])
			/*
			->map(function($row){
				if($row->parent_id != null){
					$prives = TableRegistry::get('Cars');
					$row->prives = $prives->find()
					->where(['id'=>$row->parent_id])
					->toArray();
					
				}
				return $row;
			})
			*/
			->toArray();
			
		
		}
		// ridici
		if ($type == 'driver'){
			
			$this->loadModel('Drivers');
			$conditions = [ 
				"Drivers.group_id" => 5,
				"Drivers.on_dashboard" => 0
			];
			
			if (isset($params['driver_sklad_id']) && $params['driver_sklad_id'] > 0){
				$conditions['Drivers.sklad_id'] = $params['driver_sklad_id'];
			}
			
			$data = $this->Drivers->find()
			->where($conditions)
			->select(["Drivers.id", "Drivers.name", "Drivers.datum_narozeni", "Drivers.number", "Drivers.tel_firemni", "Drivers.sklad_id"])
			->toArray();
		}
		
		// canvas
		if ($type == 'canvas'){
			$this->loadModel('Cars');
			//$this->set('car_type_list',$this->car_type_list = $this->Cars->get_car_type());
			//$this->set('car_color_list',$this->car_color_list = $this->Cars->get_car_color());
			//pr($this->car_color_list);
			
			$conditions = [
				"Cars.system_id" => $this->system_id,
				"Cars.on_dashboard" => 1,
			];
			if (isset($this->loggedUser)){
				$conditions['Cars.user_id'] = $this->loggedUser['id'];
			}
			if (isset($params['canvas_sklad_id']) && $params['canvas_sklad_id'] > 0){
				$conditions['Cars.sklad_id'] = $params['canvas_sklad_id'];
			
			} else {
				$conditions['Cars.sklad_id IS'] = null;
			}
			
			if (isset($params['canvas_user_id']) && $params['canvas_user_id'] > 0){
				$conditions['Cars.user_id'] = $params['canvas_user_id'];
			}
			//pr($conditions);
			$this->loadModel('Cars');
			$mapper = function ($data, $key, $mapReduce) {
				//pr($data);
				//pr($data->id);
				if (!empty($data->zakazka_connect_drivers)){
					$data->driver_id = $data->zakazka_connect_drivers[0]->id;
				
					$data->driver_count = count($data->zakazka_connect_drivers);
				} else {
					$data->driver_count = 0;
				}
				if (!empty($data->zakazka_connects)){
					$weight_load = 0;
					$size_load = 0;
					$zakazka_count = 0;
					$zakazka_list = [];
					foreach($data->zakazka_connects AS $c){
						//pr($c);
						$zakazka_list[] = $c->id;
						$weight_load += $c->weight;
						$size_load += $c->size;
						$zakazka_count += 1;
					}
					$data->weight_load = $weight_load;
					$data->size_load = $size_load;
					//pr($data->zakazka_connects);
					$data->zakazka_count = count($data->zakazka_connects);
					//$data->driver_id = $data->zakazka_connect_drivers[0]->id;
				
				} else {
					
					$data->weight_load = 0;
					$data->size_load = 0;
					$data->zakazka_count = 0;
				}
				$list = $this->Cars->get_car_data();
				//pr($list);
				$data->color = (isset($list[$data->car_type])?$list[$data->car_type]['color']:'');
				$data->car_type_name = (isset($list[$data->car_type])?$list[$data->car_type]['name']:'');
				
				
				//pr($zakazka_list);die();
				
				$zakazkas = TableRegistry::get('Zakazkas');
				if (isset($zakazka_list) && !empty($zakazka_list))
				$zakazkas_data = $zakazkas->find()
				->select([
					'AdresaNalozeni.id',
					'AdresaNalozeni.ulice',
					'AdresaNalozeni.mesto',
					'AdresaNalozeni.psc',
					'AdresaNalozeni.stat',
					
					'AdresaVylozeni.id',
					'AdresaVylozeni.ulice',
					'AdresaVylozeni.mesto',
					'AdresaVylozeni.psc',
					'AdresaVylozeni.stat',
				])
				->where(['Zakazkas.id IN'=>$zakazka_list,'Zakazkas.driver_load'=>1])
				->order('Zakazkas.modified DESC')
				->contain(["AdresaNalozeni","AdresaVylozeni"])
				->hydrate(false)
				->first()
				
				;
				//->toArray();
				//pr($zakazka_list);
				//pr($zakazkas_data);die();
				if (isset($zakazkas_data) && count($zakazkas_data)>0){
					//pr($zakazkas_data);
					//foreach($zakazkas_data AS $zakazka){
						//pr($zakazka->AdresaNalozeni);
					if (isset($zakazkas_data['AdresaNalozeni']) && !empty($zakazkas_data['AdresaNalozeni'])) $data->adresa_nalozeni= $zakazkas_data['AdresaNalozeni'];
					if (isset($zakazkas_data['AdresaVylozeni']) && !empty($zakazkas_data['AdresaVylozeni'])) $data->adresa_vylozeni= $zakazkas_data['AdresaVylozeni'];
						
					//}
				}
				//pr($data->color);
				//pr($data);	
				//pr($data->zakazka_connect_drivers);
				$mapReduce->emit($data);
			};
			
			
			$load_cars = $this->Cars->find("all")
			  ->where($conditions)
			  ->select([
				"Cars.id", 
				"Cars.name",
				"Cars.size_all",
				"Cars.weight_all",
				"Cars.spz",
				"Cars.car_type",
				"Cars.car_type",
				"Cars.parent_id",
				"Cars.predano",
				//"Cars.car_type_name",
				//"Cars.color",
				])
				->contain(["ZakazkaConnects","ZakazkaConnectDrivers"])
				->mapReduce($mapper)
				->order("Cars.order ASC")
				//->hydrate()
				->toArray()
				
			;
			
			/*
			$this->loadModel('Zakazkas');
			$zakazkas_data = $this->Zakazkas->find()
			->where([
				//'Zakazkas.id IN'=>$zakazka_list
			])
			->select(["Zakazkas.id", "Zakazkas.adresa_nalozeni_id", "Zakazkas.adresa_vylozeni_id"])
			->toArray();
			pr($zakazkas_data);
			*/

			//pr($load_cars);  
			die(json_encode(['result'=>true, "data" => $load_cars]));
			$this->set('load_cars',$load_cars);
		}
		
		
		//pr($data);
		
		if (isset($data)) $this->set('data',$data);
		$this->viewBuilder()->layout(false);
		if ($type == 'sklad') $type = 'zakazky';
		if ($type == 'zakazky_done') $type = 'zakazky';
		
		$response = $this->render('load_elements/'.$type);
		$html = $response->body();
		
		if ($this->request->is('ajax')) {
			die(json_encode(['result'=>true, "html" => $html]));
		}
	}
	
	// send zakazka
	public function sendZakazka($type,$car_id,$zakazka_id,$sklad_id=null){
		// send zakazka to driver
		if ($type == 'driver'){
			$this->loadModel('Zakazkas');
			$message = __('Posláno řidiči na naložení');
			
			
			
			
			$opt = [
				'car_id'=>$car_id,
				'zakazka_id'=>$zakazka_id,
				'message'=>$message,
				'type'=>1,
				'type_cesta'=>1,
				'type_name'=>'zakazka_to_driver',
			];
			//pr($opt);
			$this->ViewIndex->send_to_driver($opt);
			
			$save_sklad = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'driver_send'=>1,
			]);
			
			$this->Zakazkas->save($save_sklad);
			$message = __('Posláno řidiči na naložení');
			$delete = false;
			
			// save history
			$this->ViewIndex->save_history(['type'=>'zakazka_to_driver','zakazka_id'=>$zakazka_id,'car_id'=>$car_id]);
			
		}
		
		if ($type == 'driver2'){
			$this->loadModel('Zakazkas');
			$save_sklad = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'driver_send2'=>1,
			]);
			
			$this->Zakazkas->save($save_sklad);
			$message = __('Posláno řidiči na vyložení');
			$delete = false;
			
			$opt = [
				'car_id'=>$car_id,
				'zakazka_id'=>$zakazka_id,
				'message'=>$message,
				'type'=>2,
				'type_cesta'=>2,
				'type_name'=>'zakazka_to_driver2',
			];
			//pr($opt);
			$this->ViewIndex->send_to_driver($opt);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'zakazka_to_driver2','zakazka_id'=>$zakazka_id,'car_id'=>$car_id]);
			
		}
		
		// send zakazka to sklad
		if ($type == 'zakazka-to-sklad'){
			$message = __('Posláno do skladu');
			$delete = true;
			
			
			$opt = [
				'zakazka_id'=>$zakazka_id,
				'message'=>'Příjem zakázky na sklad',
				'sklad_id'=>$sklad_id,
				'type'=>1,
			];
			$this->ViewIndex->send_to_sklad($opt);
			//die('stop');
			
			$this->loadModel('Zakazkas');
			$save_sklad = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'sklad_id'=>$sklad_id,
				'on_car'=>0,
				'user_id'=>null,
			]);
			
			$this->Zakazkas->save($save_sklad);
			
			
			
			$opt = [
				'car_id'=>$car_id,
				'zakazka_id'=>$zakazka_id,
				'message'=>$message,
				'type'=>4,
				'type_cesta'=>4,
				'type_name'=>'zakazka_to_sklad',
				'sklad_id'=>$sklad_id,
			];
			//pr($opt);
			$this->ViewIndex->send_to_driver($opt);
			
			$this->loadModel('ZakazkaConnects');
			$this->ZakazkaConnects->deleteAll(['zakazka_id'=>$zakazka_id,'system_id'=>$this->system_id]);
			
			
			
			// save history
			$this->ViewIndex->save_history(['type'=>'zakazka_to_sklad','car_id'=>$car_id,'zakazka_id'=>$zakazka_id,'sklad_id'=>$sklad_id]);
		
			
		}
		die(json_encode(['r'=>true,'m'=>$message,'delete'=>$delete]));
	}
	
	public function test(){
		$this->loadModel('Zakazkas');
		$this->Zakazkas->get_zakazka_driver(9);
		die();
	}
	
	public function clear(){
		$this->loadModel('Zakazkas');
		$conn = ConnectionManager::get('default');
		$stmt = $conn->query('TRUNCATE zakazka_connect_drivers');
		$stmt = $conn->query('TRUNCATE zakazka_connects');
		$stmt = $conn->query('UPDATE `users` SET `on_dashboard` = 0');
		$stmt = $conn->query('UPDATE `cars` SET `on_dashboard` = 0');
		$stmt = $conn->query('UPDATE `zakazkas` SET `preposlano` = 0');
		$stmt = $conn->query('UPDATE `zakazkas` SET `done` = 0');
		$stmt = $conn->query('UPDATE `zakazkas` SET `on_car` = 0');
		$stmt = $conn->query('UPDATE `zakazkas` SET `sklad_id` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `sort` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_send` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_load` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_unload` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_confirm` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_confirm_sklad` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_confirm_another` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_date_load` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_date_unload` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_date_load_another` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_date_confirm` = NULL');
		$stmt = $conn->query('UPDATE `zakazkas` SET `driver_date_confirm2` = NULL');
		die('end');
	}
	
	public function countCanvas(){
		$this->loadModel('Cars');
		$data = $this->Cars->find()
		->where([
			"Cars.system_id" => $this->system_id,
			"Cars.on_dashboard" => 1,
			"Cars.sklad_id IS" => null,
			"Cars.user_id" => $this->loggedUser['id'],
		])
		->select(["Cars.id"])
		->toArray();
		
		die(json_encode(['r'=>true,'count'=>count($data)]));
	}
}
