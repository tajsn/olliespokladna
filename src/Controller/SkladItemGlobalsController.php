<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use Cake\Utility\Hash;

use Cake\Cache\Cache;

use App\View\Helper\FastestHelper;

class SkladItemGlobalsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index()
  {
		
	$this->set("title", __("Správa skladové položky globální"));
	
	
	$this->loadModel('SkladCiselniks');
	//$this->set('dodavatel_list',$this->dodavatel_list = $this->SkladCiselniks->dodavatelList());
	$this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
	$conditions = $this->convert_conditions(['SkladItemGlobals.kos'=>0]);
	
	$this->sortList = [
		'id',
		'name',
		'stav_real',
		'jednotka_id',
		'ean',
		'bidfood_id',
		'group_id',
		'dodavatel_id',
		'min',
		'max',
		'tax_id',
		'created',
	];
	
	
	$mapper = function($r, $key, $mapReduce){
		/*
		$r->stav_real = (isset($r->sklad_stav_item->stav_real)?$r->sklad_stav_item->stav_real:0);
		$currentData = clone $r;
		
		unset($r->sklad_stav_item);
		foreach($this->sortList AS $s){
			unset($r->$s);
			$r->$s = $currentData->$s;
		}
		//pr($currentData);
		//pr($r);
		*/
		$mapReduce->emit($r);
	};
	
	$data = $this->SkladItemGlobals->find()
		->where($conditions)
		//->contain(['SkladStavItems'])
		->select([
			'SkladItemGlobals.id', 
			'SkladItemGlobals.name',
			'SkladItemGlobals.jednotka_id',
			'SkladItemGlobals.ean',
			'SkladItemGlobals.bidfood_id',
			'SkladItemGlobals.group_id',
			'SkladItemGlobals.dodavatel_id',
			'SkladItemGlobals.min',
			'SkladItemGlobals.max',
			'SkladItemGlobals.tax_id',
			'SkladItemGlobals.created',
			//'SkladStavItems.stav_real',
		])
		->order('SkladItemGlobals.id DESC')
		->group('SkladItemGlobals.id')
		->mapReduce($mapper)
		/*
		->map(function($r){
			$r->stav_real = (isset($r->sklad_stav_item->stav_real)?$r->sklad_stav_item->stav_real:0);
			unset($r->sklad_stav_item);
			
			//pr($r);die('a');
			return $r;
		})
		*/
		
	  	
	  //->toArray()
	  ;
	  //$data->order = 'id DESC';
	//pr($data->toArray());	die();
	//die();
	$params = array(
		'top_action'=>array(
			'edit'=>__('Pridat'),
		),
		'filtr'=>array(
			'name'=>__('Název').'|SkladItemGlobals__name|text_like',
			'group_id'=>__('Skupina').'|SkladItemGlobals__group_id|select|sklad_group_list',
		//	'dodavatel_id'=>__('Dodavatel').'|SkladItemGlobals__dodavatel_id|select|dodavatel_list',
		),
		'list'=>array(
			'jednotka_id'=>$this->sklad_jednotka_list,
			'dodavatel_id'=>$this->dodavatel_list,
			'group_id'=>$this->sklad_group_list,
			'tax_id'=>$this->price_tax_list,
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			//'clone_el'=>__('Kopírovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }

  
  public function edit($id=null){
    $this->set("title", __("Editace skladové položky"));
    $this->viewBuilder()->layout("ajax");
	
	//$this->set('scripts','/js/fst_system/fst_select.js');
	$this->loadModel('SkladCiselniks');
	$this->set('dodavatel_list',$this->dodavatel_list = $this->SkladCiselniks->dodavatelList());
	$this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
	
    $products = $this->SkladItemGlobals->newEntity();
	
	
	if ($id != null){
		$products = $this->SkladItemGlobals->find()
		  ->where(['SkladItemGlobals.id'=>$id])
		  ->first();
		  //pr($products);
	}
	//pr($this->request);
    if ($this->request->is("ajax")){
     //pr($this->request->data());
	  $products = $this->SkladItemGlobals->patchEntity($products, $this->request->data());
	  //pr($products);
	  $this->check_error($products);
	  
	  if ($result = $this->SkladItemGlobals->save($products)) {
    	
		//pr($this->request->data['product_connects'])
		//pr($result->id);
		Cache::clear(false);
		die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$products->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("products"));
  }
  
  
  function dodavatelAdd($value){
		$save = [
			'name'=>$value,
			'type'=>1,
		];
		$this->loadModel('SkladCiselniks');
		$save_entity = $this->SkladCiselniks->newEntity($save);
		$result = $this->SkladCiselniks->save($save_entity);
		
		die(json_encode(['r'=>true,'id'=>$result->id,'name'=>$result->name]));
  }
  
  function skladGroupAdd($value){
		$save = [
			'name'=>$value,
			'type'=>2,
		];
		$this->loadModel('SkladCiselniks');
		$save_entity = $this->SkladCiselniks->newEntity($save);
		$result = $this->SkladCiselniks->save($save_entity);
		
		die(json_encode(['r'=>true,'id'=>$result->id,'name'=>$result->name]));
  }


}
