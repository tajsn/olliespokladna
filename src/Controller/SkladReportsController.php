<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Component\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\I18n\Time;

class SkladReportsController extends AppController
{
	
	
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }
	
	 
	
  public function index($driver_id = null)
  {
	//pr($this->request->query);	 
	$this->set("title", __("Zakázky na skladě"));
	
	$this->loadModel('Cars');
	$car_list_tmp = $this->Cars->carsList();		
	//pr($car_list_tmp);
	
		
		$this->loadModel('SkladReports');
		if (isset($this->request->query['sklad_id']) && $this->request->query['sklad_id'] > 0){
			$sklad_id = $this->request->query['sklad_id'];
		} else {
			//$sklad_id = $this->loggedUser['id'];
		}

		$conditions = [
			'SkladReports.kos'=>0,
			'SkladReports.done'=>0,
		
		];
		if (isset($sklad_id)){
			$conditions['SkladReports.sklad_id'] = $sklad_id;
		}
		if (isset($this->request->query['car_id']) && $this->request->query['car_id'] > 0){
			$conditions['SkladReports.car_id'] = $this->request->query['car_id'];
			
		}
		//pr($conditions);
		$data = $this->SkladReports->find()
		->where($conditions)
		->select([
			
		])
		->order('id DESC')
		->toArray()
		;
		//pr($data);
		$car_list = [];
		if (isset($data) && !empty($data)){
			foreach($data AS $d){
			
				$car_list[$d->car_id] = $car_list_tmp[$d->car_id];
			}
		}
		//pr($car_list);
		$this->set('car_list',$car_list);
		$this->set('data',$data);
	//}
	
  }
  
  public function saveData($type,$report_id,$zakazka_id){
		if ($type == 'unload' || $type == 'load'){
			
			$this->loadModel('Zakazkas');
			$save = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'sklad_date_'.$type=>new Time(),
			]);
			$this->Zakazkas->save($save);
			
			$save_done = $this->Zakazkas->newEntity([
				'id'=>$report_id,
				'done'=>1,
			]);
			$this->SkladReports->save($save_done);
			
			$report_data = $this->SkladReports->find()
			->where(['id'=>$report_id])
			->select([
				
			])
			->order('id DESC')
			->first()
			;
			// save history
			$this->ViewIndex->save_history(['type'=>'sklad_confirm_'.$type,'zakazka_id'=>$zakazka_id,'type_history'=>2,'sklad_id'=>$report_data['sklad_id']]);
			
			
		}
		
		die(json_encode(['r'=>true]));
  }
  
	private function save_report($type,$report_id){
		
		$this->loadModel('DriverReports');
		$save = [
			'id'=>$report_id,
			$type=>1,
		];
		if ($type == 'loaded'){
			$save['done']=1;
		}
		//pr($save);
		$save_report = $this->DriverReports->newEntity($save);
		$this->DriverReports->save($save_report);
			
		
	}
	
	function tisk(){
		//pr($this->request->query);
		
		$conditions = [
			'SkladReports.kos'=>0,
			'SkladReports.done'=>0,
		
		];
		if (isset($this->request->query) && !empty($this->request->query)){
			foreach($this->request->query AS $k=>$val){
				$conditions[strtr($k,['-'=>'_'])] = $val;
				
			}
		}
		//pr($conditions);die();
		
		$data = $this->SkladReports->find()
		->where($conditions)
		->select([
			
		])
		->order('SkladReports.id DESC')
		->contain(['Cars','Drivers'])
		->toArray()
		;
		//pr($data);
		$data_group = [];
		
		$file_name = 'tisk';
		$cars = [];
		if (isset($data) && count($data)>0){
			foreach($data AS $k=>$d){
				
				$data_group[$d->car_id][] = $d;
			}
			//pr($data_group);die();
			foreach($data_group AS $car_id=>$car){
				$orders = '';
				//$orders .= '<tr><td>';	
				
				$orders .= '<table class="order" width="100%">';	
				
				$orders .= '<tr>';	
					$orders .= '<th>Vykládka</th>';	
					$orders .= '<th>Nakládka</th>';	
					$orders .= '<th>Váha</th>';	
					$orders .= '<th>Rozměr</th>';	
					
				$orders .= '</tr>';	
				
				foreach($car AS $k=>$d){
					$vykladka = unserialize($d->adresa_vylozeni);
					$nakladka = unserialize($d->adresa_nalozeni);
					
					
					$orders .= '<tr>';
						$orders.='<td>'.$vykladka['name'].' '.$vykladka['mesto'].' '.$vykladka['psc'].' '.$vykladka['stat'].'</td>';
						$orders.='<td>'.$nakladka['name'].' '.$nakladka['mesto'].' '.$nakladka['psc'].' '.$nakladka['stat'].'</td>';
						$orders.='<td>'.$d->weight.'</td>';
						$orders.='<td>'.$d->size.'</td>';
					$orders .= '</tr>';
					
					
				}
				$orders.= '</table>';
				
				$cars[$car_id] = array(
						'car.SPZ'=>$d->car->spz,
						'car.RIDIC'=>$d->driver->name,
						'car.CAS_VYJEZDU'=>'cas',
						'car.ORDERS'=>$orders,
				);
				
				//$orders.= '</td></tr>';
				//$orders = 'a'; 
			}
		}
			$pdf_generate_link = "http://scripts.fastesthost.cz/mpdf/";
			$post_data = array(
				// PDF settings - require values
				'SECOND_PAGE' => true,
				'PDF_ARRAY' => false,
				'PDF_FILE' => $file_name,
				'PDF_ARRAY_FILE' => $file_name,
				'PDF_TITLE' => $file_name,
				'PDF_SUBJECT' => 'Tisk',
				'PDF_TEMPLATE' => 'http://p-pospiech.fastest.cz/uploaded/sklad.html',
				
				// variable constants
				'CARS' => $cars,
				'DATE_PRINT' => date('d.m.Y H:i:s'),
			);
			
			//pr($post_data);die();
			$post_url = $this->encode_long_url($post_data);
			//Header("Location: ".$pdf_generate_link."?params=".$post_url."");
			$url_open = $pdf_generate_link."?params=".$post_url;		
			//pr($url_open);
			$this->redirect($url_open);
		//die();
	}

  

}
