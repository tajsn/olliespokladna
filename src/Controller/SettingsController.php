<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

use Cake\Cache\Cache;

class SettingsController extends AppController
{
  public function initialize()
  {
    parent::initialize();
  }

  public function edit($id=null){
    $this->set("title", __("Editace nastavení"));
    $this->set("fst_pokladna",true);
    $this->viewBuilder()->layout("ajax");
    $settings = $this->Settings->newEntity();
	
	if ($id != null){
		
		$settings = $this->Settings->find()
		->where(['system_id'=>$this->system_id,'type'=>1])
		->first();
		
		$settings['data'] = unserialize($settings['data']);
		//pr($settings);
	}
	
	if ($this->request->is("ajax")){
   // pr($settings);
		$this->request->data['data'] = serialize($this->request->data['data']);
		$this->Settings->patchEntity($settings, $this->request->data());
	  
	  $this->check_error($settings);
	  
	  //pr($settings);
	  
	  if ($result = $this->Settings->save($settings)) {
        Cache::clear(false);
		die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$settings->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("settings"));
  }


}
