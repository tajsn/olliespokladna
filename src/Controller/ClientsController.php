<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Component\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;

class ClientsController extends AppController
{
	
	var $client_fields = ['id','name','ic','dic','ulice','mesto','psc','stat','problem_text','telefon','email','valid_dph','telefon2','zastupce'];
	
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }
	
	
	
  public function index()
  {
		
	$this->set("title", __("Zákaznící"));
	
	
	$conditions = $this->convert_conditions(['Clients.kos'=>0]);
	//pr($conditions);
	//die();
	$data = $this->Clients->find()
      ->where($conditions)
	  ->order('name ASC')
      ->select(['Clients.id', 'Clients.name', 'Clients.ulice', 'Clients.mesto']);
		
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
		),
		'filtr'=>array(
			'id'=>__('ID').'|Clients__id_tmp|text',
			'name'=>__('Název').'|Clients__name|text_like',
			'ic'=>__('IČO').'|Clients__ic|text_like',
			//'car_type'=>__('Typ').'|Cars__car_type|select|car_type_list',
		),
		'list'=>array(
			//'car_type'=>$this->car_type_list,
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }

  public function edit($id=null){
    $this->set("title", __("Editace zákazníka"));
    $this->viewBuilder()->layout("ajax");
    $clients = $this->Clients->newEntity();
	if ($id != null){
		
		$clients = $this->Clients->get($id);
		$this->ViewIndex->client_adresa_list($id);
		
	}
	
	if ($this->request->is("ajax")){
     
	  $this->Clients->patchEntity($clients, $this->request->data());
	  $find = $this->Clients->find()
		->where([
			"Clients.system_id" => $this->system_id,
			"Clients.ic" => (string) $this->request->data['ic'],
		])
		->select(["Clients.id"])
		->first();
		if (isset($find) && !empty($find)){
		  die(json_encode(['r'=>false,'m'=>__('Toto IČ je již použito')]));
	  
		}
		
	  
	  $this->check_error($clients);
		
					
		
	  if ($result = $this->Clients->save($clients)) {
        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$clients->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("clients"));
  }
  
	public function delAddress($id){
		$this->loadModel('Adresas');
		$entity = $this->Adresas->get($id);
		$result = $this->Adresas->delete($entity);
		
		die(json_encode(['r'=>true,'m'=>__('Adresa smazána')]));
		
	}
	
	
	  
  public function addAddress(){
	  // http://p-pospiech.fastest.cz/clients/add_address/?adr-ulice=Pelclova%202500/5&adr-psc=702%2000&adr-mesto=Ostrava&adr-stat=CZ&adr-lat=49.8481683&adr-lng=18.28562510000006&
	  //pr($this->request->query);
	  if (isset($this->request->query['p'])){
		  $data = explode('&',rtrim(json_decode(base64_decode($this->request->query['p'])),'&'));
		  $this->loadModel('Adresas');
		  //pr($data);
		  $save_adr = $this->Adresas->newEntity();
		  
		  $option = [];
		  foreach($data AS $k=>$p){
			  
			  $p = explode('=',$p);
			  
			  if (!empty($p[1]))
			  $save_adr->$p[0] = $p[1];
			  $option['data-'.$p[0]] = $p[1];
		  }
		  //pr($option);
		  //pr($save_adr);die();
		  if ($result = $this->Adresas->save($save_adr)) {
			  //pr($save_adr->id);die();
			  $option['value'] = $save_adr->id;
			  $option['name'] = $save_adr->name;
			  die(json_encode(['r'=>true,'m'=>__('Adresa uložena'),'option'=>$option]));
		  }
	  }
	  
	  die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
  }
  
  
	// autocomplete search client
	
	function autocomplete(){
		if (!isset($this->request->data['search']))
		$this->request->data['search'] = 'klient';
	
		$search = $this->request->data['search'];
		//pr($this->request->data['search']);
		$search_list_load = $this->Clients->find()
		  ->where(['name LIKE'=>'%'.$search.'%'])
		  ->select($this->client_fields)
		  ->order('name ASC')
		  ->limit(20)
		  ->hydrate(false)
		  ->toArray();
		  
		  $search_list = [];
		  foreach($search_list_load AS $k=>$c){
			
			$adresa_list = $this->ViewIndex->client_adresa_list($c['id']);
			$c['adresa_list'] = $adresa_list;
			$search_list[json_encode($c)] = $c['name'].'<var> ('.$c['mesto'].', '.$c['ulice'].')</var>';   
		  }
		  //pr($search_list_load);
		  //pr($adresa_list);
		
		die(json_encode($search_list));
	}

}
