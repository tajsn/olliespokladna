<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use Cake\Utility\Hash;

use Cake\Cache\Cache;

use App\View\Helper\FastestHelper;

class TablesController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index()
  {
		
	$this->set("title", __("Seznam stolů"));
	
	
	$this->loadModel('ProductGroups');
	$this->set('group_list',$this->group_list = $this->ProductGroups->groupList());
	
	$conditions = $this->convert_conditions(['Tables.kos'=>0]);
	
	$data = $this->Tables->find()
      ->where($conditions)
      ->select(['Tables.id', 'Tables.num','Tables.name','Tables.group_id']);
		
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
			'drawTable|redirect'=>__('Mapa stolů'),
		),
		'filtr'=>array(
			'name'=>__('Název').'|Tables__name|text_like',
			'num'=>__('Číslo').'|Tables__num|text_like',
			'group_id'=>__('Skupina').'|Tables__group_id|select|stoly_group_list',
		),
		'list'=>array(
			'group_id'=>$this->stoly_group_list[$this->loggedUser['system_id']],
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }
  
  

  public function drawTable($id=null){
	$this->redirect('/orders/drawTable');
  }
  
  public function edit($id=null){
    $this->set("title", __("Editace stolu"));
    $this->viewBuilder()->layout("ajax");
	
    $tables = $this->Tables->newEntity();
	
	$this->loadModel('ProductGroups');
	$this->set('group_list',$this->group_list = $this->ProductGroups->groupList());
	
	
	if ($id != null){
		$tables = $this->Tables->get($id);
	
	}
	//pr($this->request);
    if ($this->request->is("ajax")){
     
	  $tables = $this->Tables->patchEntity($tables, $this->request->data());
	  $this->check_error($tables);
	  
	  if ($result = $this->Tables->save($tables)) {
        Cache::clear(false);
		die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$tables->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("tables"));
  }
  
	
	

}
