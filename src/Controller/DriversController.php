<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Component\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;

class DriversController extends AppController
{
	
	
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }
	
	
	
  public function index()
  {
		
	$this->set("title", __("Řidiči"));
	
	
	$conditions = $this->convert_conditions(['Drivers.kos'=>0]);
	//pr($conditions);
	//die();
	$data = $this->Drivers->find()
      ->where($conditions)
      ->select(['Drivers.id', 'Drivers.number', 'Drivers.name']);
		
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
		),
		'filtr'=>array(
			'name'=>__('Jméno').'|Drivers__name|text_like',
			'number'=>__('Číslo').'|Drivers__number|text',
			//'size'=>__('Palet').'|Cars__size|text',
			//'car_type'=>__('Typ').'|Cars__car_type|select|car_type_list',
		),
		'list'=>array(
			//'car_type'=>$this->car_type_list,
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }

  public function edit($id=null){
    $this->set("title", __("Editace řidiče"));
    $this->viewBuilder()->layout("ajax");
    $drivers = $this->Drivers->newEntity();
	if ($id != null){
		
		$drivers = $this->Drivers->get($id);
		$this->ViewIndex->client_adresa_list($id);
		
	}
	
	if ($this->request->is("ajax")){
     
	  $this->Drivers->patchEntity($drivers, $this->request->data());
	  $this->check_error($drivers);
	  
	  if ($result = $this->Drivers->save($drivers)) {
        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$drivers->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("drivers"));
  }
  
  public function ownIndex(){
	  
  }
  

}
