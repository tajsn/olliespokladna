<?php
namespace App\Controller;
use App\Controller\AppController;
use App\Component\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use mPDF;

class StatisticsController extends AppController
{
	var $sum_list = [
		'count'=>0,
		'price'=>0,
		'price_without_tax'=>0,
		'price_sum'=>0,
		'price_sum_without_tax'=>0,
		
	];
		
	public function index($uzaverka_id=null){
		
		// load user list
		$this->loadModel('Users');
		$this->set('user_list',$this->user_list = $this->Users->userList());
		
		$this->loadModel('ProductGroups');
		$this->set('product_group_list',$this->product_group_list = $this->ProductGroups->groupList());
	
		
		//pr($uzaverka_data);
		$this->set("title", __("Statistiky prodejů"));
		
		$conditions = $this->convert_conditions([
			//'Statistics.kos'=>0,
			
			//'date(Statistics.open_date)'=>'2017-02-07',
			
		]);
		//pr($this->dph_conf[1]);die();
		$mapper = function ($data, $key, $mapReduce) {
			
			$data->price_without_tax = round($data->price - ($data->price*$this->dph_conf[$data->price_tax_id]),2);
			$data->price_sum = $data->price * $data->count;
			$data->price_sum_without_tax = $data->price_without_tax * $data->count;
			
			$nakup_price = 0;
			$nakup_price_vat = 0;
				
			if (isset($data->product_recepturas) && !empty($data->product_recepturas)){
				//pr(json_encode($data->product_receptura));die('a');
				
				foreach($data->product_recepturas AS $res){
					if (isset($res->sklad_item) && isset($res->sklad_item->sklad)){
						$nakup_price += $res->sklad_item->sklad->nakup_price;
						$nakup_price_vat += $res->sklad_item->sklad->nakup_price_vat;
						unset($data->product_recepturas);
					}
				}
			}
			$data->nakup_price = $nakup_price;
			$data->nakup_price_vat = $nakup_price_vat;
			
			$mapReduce->emit($data);
		};
		$data = $this->Statistics->find();
		//die();
		//$conditions['Statistics.product_id']=255;
		//$conditions['Statistics.product_id']=124;
		$data 
		  ->contain(
			[
				
				'ProductRecepturas'=>function($q){
                	$q
					->group('ProductRecepturas.id')
					->select(['id','value','product_id','sklad_item_global_id']);
					return $q;
                },
				
				'ProductRecepturas.SkladItems'=>function($q){
                    $q->select(['id','name','sklad_item_global_id']);
					return $q;
                },
				'ProductRecepturas.SkladItems.Sklads' => function($q){
                    $q->select(['id','nakup_price','nakup_price_vat','sklad_item_id']);
					return $q;
                },
				
				
				
			]
			)
		  ->where($conditions)
		  
		  ->select([
			
			'Statistics.product_id', 
			'Statistics.name', 
			'Statistics.storno', 
			'Statistics.count', 
			'Statistics.price', 
			'Statistics.price_tax_id', 
			'Statistics.user_id', 
			'Statistics.created', 
			'count' => $data->func()->sum('count') 
			
			//'Statistics.price_without_tax', 
		  ])
		  ->group('Statistics.product_id')
		  ->order('Statistics.id DESC')
		  ->mapReduce($mapper)
		 
		  ;
		//pr($data);
		//$data->order['Statistics.id'] =  'DESC';  
		//pr($conditions);
		//pr($data->toArray());die();
		
		$this->set('order_list',$order_list = [
			'product_id',
			'name',
			'storno',
			'count',
			'price',
			'price_tax_id',
			'price_without_tax',
			'price_sum',
			'price_sum_without_tax',
			'nakup_price',
			'nakup_price_vat',
			'user_id',
			'created',
		]);
		
		
			
		$params = array(
			'top_action'=>array(
				/*
				'bill_back|selected_items'=>__('Vrátit účet zpět'),
				'show_storno|show_storno'=>__('Zobrazit storna'),
				'bill_storno|selected_items storno_bill'=>__('Stornovat účet'),
				'export|selected_items'=>__('Export'),
				'print_part|selected_items'=>__('Tisk meziuzávěrky'),
				'print_bill|selected_items print_bill'=>__('Tisk účtenky'),
				'client_select|selected_items'=>__('Výběr zákazníka'),
				'change_platba|selected_items'=>__('Změna platby'),
				*/
			),
			'filtr'=>array(
				'created_date'=>__('Datum od').'|created|date_from',
				'created_date2'=>__('Datum do').'|created|date_to',
				'product_group_id'=>__('Skupina').'|product_group_id|select|product_group_list',
				'user_id'=>__('Pracovník').'|user_id|select|user_list',
				'price_tax_id'=>__('DPH').'|price_tax_id|select|price_tax_list',
				'storno'=>__('Storno').'|storno|select|ano_ne',
			
			),
			'list'=>array(
				'storno'=>$this->ano_ne,
				'user_id'=>$this->user_list,
				'price_tax_id'=>$this->price_tax_list,
			),
			'posibility'=>array(
				//'edit'=>__('Editovat').'|open_modal_pokl',
				//'trash'=>__('Smazat'),
			),
			'sum_list_col'=>$this->sum_list,
			'data'=>$data,
			'data_sum_col'=>true,
			'order_list'=>$order_list,
		);
		
		$this->renderView($params);
	
	}
	
	
	
}

