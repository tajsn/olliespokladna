<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use Cake\Utility\Hash;

use Cake\Cache\Cache;

use App\View\Helper\FastestHelper;

class LogsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index()
  {
		
	$this->set("title", __("Logace"));
	
	
	
	$conditions = $this->convert_conditions(['Logs.kos'=>0]);
	
	$this->loadModel('Users');
	$this->set('user_list',$this->user_list = $this->Users->userList());
		
	$data = $this->Logs->find()
      ->order('id DESC')
	  ->where($conditions)
      ->select(['Logs.id', 'Logs.controller','Logs.action','Logs.value','Logs.user_id','Logs.system_id']);
	//$data->order['id'] = 'DESC';
	//pr($this->request);
	$params = array(
		'top_action'=>array(
			//'edit'=>__('Pridat'),
			//'drawTable|redirect'=>__('Mapa stolu'),
		),
		'filtr'=>array(
			'controller'=>__('Modul').'|Logs__controller|text_like',
			'action'=>__('Akce').'|Logs__action|text_like',
			'value'=>__('Hodnota').'|Logs__value|text_like',
			'user_id'=>__('Uživatel').'|Logs__user_id|select|user_list',
			'system_id'=>__('System ID').'|Logs__system_id|select|system_list',
		),
		'list'=>array(
			'user_id'=>$this->user_list,
			'system_id'=>$this->system_list,
		),
		'posibility'=>array(
			//'edit'=>__('Editovat'),
			//'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }
  
  

  public function drawTable($id=null){
	$this->redirect('/orders/drawTable');
  }
  
  public function edit($id=null){
    $this->set("title", __("Editace stolu"));
    $this->viewBuilder()->layout("ajax");
	
    $tables = $this->Tables->newEntity();
	
	$this->loadModel('ProductGroups');
	$this->set('group_list',$this->group_list = $this->ProductGroups->groupList());
	
	
	if ($id != null){
		$tables = $this->Tables->get($id);
	
	}
	//pr($this->request);
    if ($this->request->is("ajax")){
     
	  $tables = $this->Tables->patchEntity($tables, $this->request->data());
	  $this->check_error($tables);
	  
	  if ($result = $this->Tables->save($tables)) {
        Cache::clear(false);
		die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$tables->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("tables"));
  }
  
	
	

}
