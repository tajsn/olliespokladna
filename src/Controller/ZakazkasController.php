<?php
namespace App\Controller;

use App\Controller\AppController;
//use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use App\View\Helper\FastestHelper;

use App\Component\ViewIndexComponent;

class ZakazkasController extends AppController
{

	public function initialize(){
		parent::initialize();
		$this->loadComponent('RequestHandler');
	}

	public function index(){
			
		$this->set("title", __("Zakázky"));
		
		$this->loadModel('Users');
		$this->set('user_list',$this->user_list = $this->Users->userList());
		
		$conditions = $this->convert_conditions(['Zakazkas.kos'=>0]);
		//pr($conditions);
		//die();
		$data = $this->Zakazkas->find()
		  ->where($conditions)
		  ->contain(['Clients','AdresaNalozeni', 'AdresaVylozeni'])
		  ->order('Zakazkas.id DESC')
		  ->select(['Zakazkas.id', 'Zakazkas.date_nalozeni','Zakazkas.code_int','Zakazkas.size','Zakazkas.on_car','Zakazkas.type','Zakazkas.stav','Zakazkas.price','Zakazkas.type_doklad','Zakazkas.user_id','Zakazkas.created',]);
			
		$params = array(
			'top_action'=>array(
				'edit'=>__('Přidat'),
				'export_checked|export_checked'=>__('Export'),
			),
			'filtr'=>array(
				'name'=>__('Číslo zakázky').'|Zakazkas__name|text_like',
				'code_int'=>__('Číslo interní').'|Zakazkas__code_int|text_like',
				'client_name'=>__('Zákazník').'|Zakazkas__client_name|text_like',
				'size'=>__('Palet').'|Zakazkas__size|text',
				'weight'=>__('Váha').'|Zakazkas__vaha|text',
				'date_nalozeni'=>__('Datum naložení').'|Zakazkas__date_nalozeni|date',
				'date_vylozeni'=>__('Datum vyložení').'|Zakazkas__date_vylozeni|date',
				'nalozeni_mesto'=>__('Naložení město').'|AdresaNalozeni__mesto|mesto_auto|1|zakazkas/autocomplete_mesto',
				'vylozeni_mesto'=>__('Vyložení město').'|AdresaVylozeni__mesto|mesto_auto|2|zakazkas/autocomplete_mesto',
				'on_car'=>__('Přiřazeno').'|Zakazkas__on_car|select|ano_ne',
				'stav'=>__('Stav').'|Zakazkas__stav|select|stav_list',
				'user_id'=>__('Vytvořil').'|Zakazkas__user_id|select|user_list',
				'type'=>__('Typ').'|Zakazkas__type|select|zakazka_type',
			),
			'list'=>array(
				'on_car'=>$this->ano_ne,
				'stav'=>$this->stav_list,
				'type'=>$this->zakazka_type,
				'user_id'=>$this->user_list,
				'type_doklad'=>$this->type_doklad[$this->loggedUser['system_id']],
			),
			'checked'=>true,
			'posibility'=>array(
				'edit'=>__('Editovat'),
				'trash'=>__('Smazat'),
				'clone_el'=>__('Kopie'),
			),
			'data'=>$data,
		);
		
		$this->renderView($params);
		
	}

	public function edit($id=null){
		$this->set("title", __("Přidání zakázka"));
			
		$this->viewBuilder()->layout("ajax");
		/*
		$this->set("scripts",[
			'/js/autocompleter/autocomplete.js',
		]);
		*/
		$zakazka = $this->Zakazkas->newEntity();
		
		
		if ($id != null){
			
			$this->set("title", __("Editace zakázka"));
			
			$zakazka = $this->Zakazkas->find()
			->where(['Zakazkas.id'=>$id,'Zakazkas.system_id'=>$this->system_id])
			->contain([
				'Clients'
			]);
			$zakazka = $zakazka->first();
			$this->loadModel('Cars');
			$zakazka->car_id = $this->Cars->carCarId($zakazka->id);
			$zakazka->driver_id = $this->Cars->carDriverId($zakazka->car_id);
			
			
			if (isset($zakazka->client_id) && !empty($zakazka->client_id)){
				$this->ViewIndex->client_adresa_list($zakazka->client_id);
			}
			$this->load_historie($id);
		}
		
		if ($this->request->is("ajax")) {
			//pr($this->request->data);
			if (empty($this->request->data['adr']['lat1'])){
				die(json_encode(['r'=>false,'m'=>__('Není zadána GPS poloha naložení')]));
			}
			if (empty($this->request->data['adr']['lat2'])){
				die(json_encode(['r'=>false,'m'=>__('Není zadána GPS poloha vyložení')]));
			}
			$this->loadModel('Clients');
			
			$client_id_tmp = $this->request->data['client']['id'];
			if (substr($client_id_tmp,0,4) == 'tmp_'){
				$this->request->data['client']['id'] = '';
				//pr($client_id_tmp);
			}
			
			//$this->Zakazkas->patchEntity($zakazka, $this->request->data());
			$cfg = [
				'associated' => ['Clients']
			];
			if ($this->request->data['code_int_check'] == 0){
				$cfg['validate'] = 'OnlyCheck';
			}
			$this->Zakazkas->patchEntity($zakazka, $this->request->data(),$cfg);
			
			//pr($zakazka);
			//pr($this->request->data());
			//die();
			$this->check_error($zakazka);
		 
		  
		  if ($this->Zakazkas->save($zakazka)) {
			$this->save_adresa_client_id($client_id_tmp,$zakazka->client_id);
			  
			die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$zakazka->id]));
		  } else {
			die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
		  }
		}

		$this->set(compact("zakazka"));
	}
	
	public function cloneEl($id){
		$fields = [
			'name',
			'code_int',
			'type',
			'stav',
			'client_id',
			'client_name',
			//'adresa_nalozeni_id',
			//'adresa_vylozeni_id',
			'anomalie',
		];
		
		$zakazka_load = $this->Zakazkas->find()
		->where(['Zakazkas.id'=>$id,'Zakazkas.system_id'=>$this->system_id])
		->select($fields)
		->first()
		->toArray();
		
		
		//pr($zakazka_load);
		$cfg = [];
		$cfg['validate'] = 'OnlyCheck';
		$save_clone = $this->Zakazkas->newEntity($zakazka_load,$cfg);
		//pr($save_clone);
		$this->Zakazkas->save($save_clone);
		
		die(json_encode(['result'=>true]));
	}
	
	private function load_historie($zakazka_id){
		$this->loadModel('Users');
		$this->loadModel('Cars');
		$this->set('dispecer_list',$this->dispecer_list = $this->Users->userList());
		$this->set('car_list',$this->car_list = $this->Cars->carsList());
		
		
		$this->loadModel('Histories');
		$historie_list = $this->Histories->find()
		->where([
			"Histories.system_id" => $this->system_id,
			"Histories.zakazka_id" => $zakazka_id
		])
		->select([])
		->order('created DESC')
		->toArray();
		//pr($historie_list);
		$this->set('historie_list',$historie_list);
	}
	
	
	// convert adresa tmp client id pokud je novy zakaznik
	private function save_adresa_client_id($client_id_tmp,$client_id){
		$this->loadModel('Adresas');
		$this->Adresas->updateAll(
			['client_id' => $client_id], // fields
			['client_id_tmp' => $client_id_tmp]
		);
	  
	}
	
	public function test($car_id=null){
		$this->loadModel('Cars');
		$this->Cars->carDriverId($car_id);
		die();
		
	}
	public function autocompleteMesto($type=null){
		if (!isset($this->request->data['search']))
		$this->request->data['search'] = 'Ostrava,70200,CZ';
		$this->loadModel('Adresas');
		$search = $this->request->data['search'];
		$search_array = explode(',',$search);
		//pr($search_array);
		
		$conditions = [];
		$conditions['mesto LIKE']= '%'.trim($search_array[0]).'%';
		
		if (isset($search_array[1]))
		$conditions['psc LIKE']= '%'.trim($search_array[1]).'%';
		
		if (isset($search_array[2]))
		$conditions['stat LIKE']= '%'.trim($search_array[2]).'%';
		//pr($conditions);
		
		$conditions['type'] = $type;
		
		$search_list_load = $this->Adresas->find()
		  ->where($conditions)
		  ->select([
			'name',
			'id',
			'mesto',
		  ])
		  ->order('name ASC')
		  ->group('name')
		  ->limit(20)
		  ->hydrate(false)
		  ->toArray();
		  
		  $search_list = [];
		  foreach($search_list_load AS $k=>$c){
			$search_list[json_encode($c)] = $c['name'];   
		  }
		  //pr($search_list_load);
		  //pr($search_list);
		
		die(json_encode($search_list));
	}

	public function tiskQr($id){
		$this->viewBuilder()->layout('print');
		$this->set("title", __("Tisk"));
		$this->set("id", $id);
		
	}
	
	private function xmlPartner($zak){
		//pr($zak);
		$xml_p = '';
		$xml_p .= "<partner>\n";
		$xml_p .= "\t<cislo>".$zak->client->id."</cislo>\n";
		$xml_p .= "\t<nazev>".$zak->client->name."</nazev>\n";
		$xml_p .= "\t<ulice>".$zak->client->ulice."</ulice>\n";
		$xml_p .= "\t<obec>".$zak->client->mesto."</obec>\n";
		$xml_p .= "\t<psc>".$zak->client->psc."</psc>\n";
		$xml_p .= "\t<stat>".$zak->client->stat."</stat>\n";
		$xml_p .= "\t<ico>".$zak->client->ic."</ico>\n";
		$xml_p .= "\t<dic>".$zak->client->dic."</dic>\n";
		$xml_p .= "\t<e_mail>".$zak->client->email."</e_mail>\n";
		$xml_p .= "\t<mobil>".$zak->client->telefon."</mobil>\n";
		$xml_p .= "\t<zastupce>".$zak->client->zastupce."</zastupce>\n";
		$xml_p .= "\t<typ>2</typ>\n";
		$xml_p .= "</partner>\n";
		
		return $xml_p;
	}
	
	private function xmlItem($zak,$polozky_items,$datum_plneni){
		if (empty($zak->spz)){
			$spz_load = $this->Histories->getCarSpz($zak->id);
			$spz = $spz_load['car']['spz'];
		} else {
			$spz = $zak->spz;
		}
		
		if (in_array($zak->type_doklad,['O02','O03','O04'])){
			$mena = 'EUR';
			$mena_cizi = true;
		} else {
			$mena = '';
		}
		
		
		
		$xml_item = '';
		$xml_item .= "<objednavka>\n";
			$xml_item .= '<termin>'.$datum_plneni->format('Y-m-d')."</termin>\n";
			$xml_item .= '<cislo>'.substr($zak->id,4,20)."</cislo>\n";
			//$xml_item .= '<cislo>'.$zak->id."</cislo>\n";
			$xml_item .= "<rada>05</rada>\n";
			$xml_item .= '<doklad>'.$zak->id."</doklad>\n";
			$xml_item .= '<ex_id_dokl>'.$zak->id."</ex_id_dokl>\n";
			$xml_item .= '<typ>'.$zak->type_doklad."</typ>\n";
			$xml_item .= '<objednavka>'.$zak->code_int."</objednavka>\n";
			$xml_item .= '<stred>'.(isset($spz)?$spz:'')."</stred>\n";
			$xml_item .= '<stred2>'.$this->convert_helios_zakazky[$zak->type]."</stred2>\n";
			if (isset($mena_cizi)){
				$xml_item .= '<kurs>'.$zak->price_kurz."</kurs>\n";
				$xml_item .= "<mena>".$mena."</mena>\n";
			}
			$xml_item .= "<stav>2</stav>\n";
			$xml_item .= "<vystavil>Jana Střebáková</vystavil>\n";
			$xml_item .= '<vystaveno>'.$zak->created->format('Y-m-d')."</vystaveno>\n";
			$xml_item .= '<odberatel>'.$zak->client_id."</odberatel>\n";
			$xml_item .= '<nazev1>'.$zak->client->name."</nazev1>\n";
			$xml_item .= '<ulice>'.$zak->client->ulice."</ulice>\n";
			$xml_item .= '<obec>'.$zak->client->mesto."</obec>\n";
			$xml_item .= '<psc>'.$zak->client->psc."</psc>\n";
			$xml_item .= '<ico>'.$zak->client->ic."</ico>\n";
			$xml_item .= '<dic>'.$zak->client->dic."</dic>\n";
			
			$xml_item .= "\t<POLOZKY>\n";
				foreach($polozky_items[$zak->code_int] AS $pol){
					$xml_item .= "\t<polozky>\n";
						$xml_item .= "\t\t<sklad>".$pol['sklad']."</sklad>\n";
						$xml_item .= "\t\t<cislo1>".$pol['cislo']."</cislo1>\n";
						$xml_item .= "\t\t<text>".$pol['text']."</text>\n";
						$xml_item .= "\t\t<typ>I</typ>\n";
						$xml_item .= "\t\t<mnozstvi>".$pol['mnozstvi']."</mnozstvi>\n";
						$xml_item .= "\t\t<mj>".$pol['mj']."</mj>\n";
						$xml_item .= "\t\t<dph>".(($zak->type_doklad == 'O01')?$pol['dph']:'')."</dph>\n";
						$xml_item .= "\t\t<stav>".$pol['stav']."</stav>\n";
						$xml_item .= "\t\t<cena_mj>".$pol['cena_mj']."</cena_mj>\n";
						if (isset($mena_cizi))
							$xml_item .= "\t\t<cena_cizi>".$pol['cena_mj']."</cena_cizi>\n";
					$xml_item .= "\t</polozky>\n";
				}
			
			$xml_item .= "\t</POLOZKY>\n";
		
		$xml_item .= "</objednavka>\n";
		/*
		<POLOZKY><polozky><sklad>01</sklad><cislo1>1</cislo1><text>Karta 1</text><typ>O</typ><mnozstvi>5</mnozstvi><mj>m2</mj><cena_mj>35</cena_mj><dph>21</dph><stav>2</stav><cena_fix>0</cena_fix></polozky><polozky><sklad>01</sklad><cislo1>2</cislo1><text>Karta2</text><typ>O</typ><mnozstvi>3</mnozstvi><mj>ks</mj><cena_mj>18.2</cena_mj><dph>21</dph><stav>2</stav><cena_fix>0</cena_fix></polozky></POLOZKY></objednavka></OBJEDNAVKA><PARTNER><partner><cislo>257</cislo><nazev>Novák Jan</nazev><ulice>Karlovarská 48</ulice><obec>Horní Bučkovice</obec><psc>10000</psc><ico>12345678</ico><dic>CZ12345678</dic><typ>2</typ></partner>
		*/
		return $xml_item;
	}
	
	private function genXmlText($adresa,$adresa2){
		$text = $adresa->stat.'_'.$adresa->psc.' '.$adresa->mesto.' * '.$adresa2->stat.'_'.$adresa2->psc.' '.$adresa2->mesto;
		return $text;
	}
	
	public function generateXml($ids=null){
		header('Content-Type: text/html; charset=utf-8');
		
		if (file_exists('./uploaded/helios/orders.red')){
			die('Helios stahuje objednávky');
		}
		
		$conditions2 = [
			//'Zakazkas.id IN'=>[201600003,201600001],
			//'Zakazkas.created >'=> date('Y-m-d'),
			//'Zakazkas.helios'=> 0,
			'Zakazkas.code_int !='=> '',
			'Zakazkas.stav NOT IN'=>[20],
			'Zakazkas.system_id'=>$this->system_id
		];
		
		if ($ids != null){
			$conditions2['id IN'] = $ids;
		}
		
		$select_codes = $this->Zakazkas->find()
		->where($conditions2)
		->select(['code_int','driver_unload','id','driver_date_unload'])
		->toArray()
		;
		//pr($conditions2);
		//pr($select_codes);
		
		if (isset($select_codes) && count($select_codes)>0){
			$code_list = [];
			foreach($select_codes AS $c){
				if (!isset($code_list[$c->code_int])){
					$code_list[$c->code_int] = [
						'count'=>1,
						'unload'=>0+$c->driver_unload,
					];
				} else {
					if (isset($code_list[$c->code_int])){
						$code_list[$c->code_int]['count'] ++;
						$code_list[$c->code_int]['unload'] = $code_list[$c->code_int]['unload']+$c->driver_unload;
						//unset($code_list[$c->code_int]);	
					}
				}
			}
			
			foreach($code_list AS $code_int=>$c){
				if ($c['count'] != $c['unload']){
					unset($code_list[$code_int]);
				} else {
					$code_list[$code_int] = $code_int;
				}
			}
			
		}
		
		//pr($code_list);die();
		
		if (!isset($code_list) || empty($code_list)){
			die('Neni co generovat');
		}
		
		$conditions = [
			'Zakazkas.code_int IN'=>$code_list,
			'Zakazkas.stav NOT IN'=>[20,25],
			//'Zakazkas.driver_unload'=>1,
			'Zakazkas.system_id'=>$this->system_id
		];
		
		
		$zakazkas_load = $this->Zakazkas->find()
		->where($conditions)
		->contain([
			'Clients'
		])
		//->group('code_int HAVING (count(code_int) = 1)')
		->toArray();
		
		//pr($zakazkas_load);
		
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$xml .= "<PROD_OBJ Generovano=\"".date('d. m. Y H:i:s')."\">\n";
		$xml .= "<OBJEDNAVKA>\n";
		
		$polozky_items_load = $this->Zakazkas->find()
		->where($conditions)
		->order('driver_date_unload DESC')
		->toArray();
		
		//pr($polozky_items_load);
		
		$polozky_items = [];
		$this->loadModel('Histories');
		$this->loadModel('Adresas');
		if (isset($polozky_items_load) && count($polozky_items_load)>0){
			
			foreach($polozky_items_load AS $p){
				
				
				
				$adresa_nalozeni = $this->Adresas->getAddress($p->adresa_nalozeni_id);
				$adresa_vylozeni = $this->Adresas->getAddress($p->adresa_vylozeni_id);
				$text = $this->genXmlText($adresa_nalozeni,$adresa_vylozeni);
				
				if (!isset($datum_plneni))
				$datum_plneni = $p->driver_date_unload;
				
				$polozky_items[$p->code_int][] = [
					'sklad'=>001,
					'text'=>$text,
					'cislo'=>$p->code_int,
					'mnozstvi'=>1,
					'mj'=>'ks',
					'typ'=>'O',
					'cena_mj'=>$p->price,
					'dph'=>21,
					'stav'=>2,
				];
				
				$save_load_helios = $this->Zakazkas->newEntity([
					'id'=>$p->id,
					'helios'=>1,
				]);
				//pr($save_driver_id);
				$this->Zakazkas->save($save_load_helios);
				
				//pr($adresa_nalozeni);
				//$polozky_items
			}
		}
		//pr($polozky_items_load);
		
		if (isset($zakazkas_load) && count($zakazkas_load)>0){
			foreach($zakazkas_load AS $zak){
				$xml .= $this->xmlItem($zak,$polozky_items,$datum_plneni);
			}
		}
		
		$xml .= "</OBJEDNAVKA>\n";
		$xml .= "<PARTNER>\n";
			$xml .= $this->xmlPartner($zak);
		
		$xml .= "</PARTNER>\n";
		$xml .= '</PROD_OBJ>';
		echo $xml;
		//die();
		
		// save file
		if ($ids == null){
			$file = './uploaded/helios/orders.xml';
			file_put_contents($file, $xml);
		} else {
			header("Content-Type: application/octet-stream");
			header("Content-Transfer-Encoding: Binary");
			//header("Content-disposition: attachment; filename=\"orders_".date('d_m_Y_H_i').".xml\""); 
			header("Content-disposition: attachment; filename=\"orders_tmp.xml\""); 
		}
		die();
	}
	
	public function exportChecked(){
		$ids = explode(',',$this->request->query['ids']);
		if (isset($ids) && count($ids)>0){
			$this->generateXml($ids);
		}
		die();
	}

	public function importPartneri(){
		header('Content-Type: text/html; charset=utf-8');
		$xml = simplexml_load_file('./uploaded/partneri.xml');
		if ($xml === false) {
			echo "Failed loading XML: ";
		} else {
			$this->loadModel('Clients');
			$count_load = count($xml->KARTA->karta);
			$count_import = 0;
			//pr($xml);
			
			foreach($xml->KARTA->karta AS $k){
				$find = $this->Clients->find()
				->where([
					"Clients.system_id" => $this->system_id,
					"Clients.ic" => (string) $k->ico,
				])
				->select(["Clients.id"])
				->first();
				//pr($find);
				if (isset($find) && !empty($find)){
					//continue;
				}
				
				$jednani = '';
				if (isset($k->JEDNANI->jednani))
				foreach($k->JEDNANI->jednani AS $j){
					$jednani .= (string) $j->datum.': Osoba:'. (string) $j->osoba.' Způsob: '. (string) $j->zpusob.' Text: '.(string) $j->text."\n";
				}
				
				$save = [
					'id'=>(string) $k->cislo,
					'name'=>(string) $k->nazev,
					'ic'=>(string) $k->ico, 
					'dic'=>(string) $k->dic,
					'zastupce'=>(string) $k->zastupce,
					'ulice'=>(isset($k->FAKADRESA->fakadresa->ulice)?(string) $k->FAKADRESA->fakadresa->ulice:''),
					'mesto'=>(isset($k->FAKADRESA->fakadresa->obec)?(string) $k->FAKADRESA->fakadresa->obec:''),
					'psc'=>(isset($k->FAKADRESA->fakadresa->psc)?(string) $k->FAKADRESA->fakadresa->psc:''),
					'stat'=>(isset($k->FAKADRESA->fakadresa->stat)?(string) $k->FAKADRESA->fakadresa->stat:''),
					'telefon'=>strtr((string) $k->mobil,[' '=>'']),
					'telefon2'=>strtr((string) $k->telefon,[' '=>'']),
					'email'=>(string) $k->e_mail,
					'jednani'=>$jednani,
				];
				if((string) $k->ico == '35838531'){
					//pr($k);
					//die('end');
				}
				$save_data = $this->Clients->newEntity($save);
				$this->Clients->save($save_data);
				//pr($save_data);die();
				
				$count_import ++ ;
				/*
				if (isset($find) && !empty($find)){
					$save['id'] = $find->id;
				}
				*/
				
				
				//pr($save_data);die();
			}
		}
		die('Načteno: '.$count_load. ', Importovano: '.$count_import);
	}
}
