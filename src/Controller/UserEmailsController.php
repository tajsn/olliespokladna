<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserEmails Controller
 *
 * @property \App\Model\Table\UserEmailsTable $UserEmails
 */
class UserEmailsController extends AppController
{

    public function initialize()
    {
        $this->loadComponent('Hmail');
        parent::initialize();
    }

    public function index()
    {
        $this->set('userEmails', $this->paginate($this->UserEmails));
        $this->set('_serialize', ['userEmails']);
    }

    public function view($email = null)
    {
        $queryConfig = $this->UserEmails->findByUserId($this->Auth->user("id"));

        if(isset($email)){
            $queryConfig->where(['email' => $email]);
        }

        if($queryConfig->count()){
            $conf = $queryConfig->first();
            $this->Hmail->connect($this->UserEmails->readPassword($conf->password), $conf);
            $this->Hmail->getInbox();
            $this->Hmail->getMessages($this->Hmail->inbox, ['dateFrom' => '2016-01-15']);

            $this->set("messages", $this->Hmail->messages);
        }
        else{
            $userEmail = $this->UserEmails->newEntity();
            $this->viewBuilder()->template("guide_email_setting");
            $this->set("userEmail", $userEmail);
        }
    }

    public function guideEmailSetting()
    {

        $data = $this->request->data();
        $data["user_id"] = $this->Auth->user("id");

        $ue = $this->UserEmails->newEntity($data);
        if($this->UserEmails->save($ue)){
            $this->Flash->set(__('Nastavení uloženo'));
            $this->redirect(['action' => 'view']);
        }
    }

    public function guideMailboxSetting()
    {

    }

}
