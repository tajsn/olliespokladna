<?php
namespace App\Controller;
use App\Controller\AppController;
use App\Component\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Cake\Datasource\ConnectionManager;
use mPDF;

class OrdersController extends AppController
{
	public function index(){
	$this->set("title", __("Správa objednávek"));
	
	$this->set('scripts',[
		'/js/fst_pokladna/fst_modal.js',
		'/js/fst_pokladna/fst_pokladna.js',
	]);
	
	$conditions = $this->convert_conditions(['Orders.kos'=>0]);
	
	$data = $this->Orders->find()
      ->where($conditions)
      ->select(['Orders.id', 'Orders.total_price']);
		
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
		),
		'filtr'=>array(
			'name'=>__('Název').'|Orders__name|text_like',
		),
		'list'=>array(
		),
		'posibility'=>array(
			'edit'=>__('Editovat').'|open_modal_pokl',
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }
	
	public function reloadTables(){
		$this->viewBuilder()->autoLayout(false);
			
		// load table list
		$this->loadModel('Tables');
		$this->set('table_list',$table_list = $this->Tables->listAllSum());
		if (!empty($table_list)){
			die(json_encode(['r'=>true,'data'=>$table_list,'count'=>count($table_list)]));
		} else {
			die(json_encode(['r'=>false,'data'=>$table_list]));
	
		}
	}
	
	public function edit($id=null){
		//$this->drawTable();
		//pr($this->loggedUser['terminal_id']);		
		// load user list
		$this->loadModel('Users');
		$this->set('user_list',$user_list = $this->Users->userList());
		
		// load table group
		$this->loadModel('TableGroups');
		$this->set('table_group_list',$table_group_list = $this->TableGroups->listGroupAll());
		
		
		// load table list
		$this->loadModel('Tables');
		$this->set('table_list',$table_list = $this->Tables->listAll());
		//pr($table_list);
		$orders = $this->Orders->newEntity();
		 
		if (!$this->request->is("ajax")){
			$this->set('scripts',[
				'/js/fst_pokladna/fst_modal.js',
				'/js/fst_pokladna/fst_uzaverka.js',
				'/js/fst_pokladna/fst_table.js',
				//'/js/fst_pokladna/fst_pokladna.js',
				//'/js/websocket/client.js',
			]);
			$this->set('request_direct',true);
		}
		
		
		
		
		if (empty($this->request->data)){
			
			$open_table = $this->Orders->checkOpenTable();
			$this->set('open_table',$open_table);
			//pr($open_table);
			
			if ($id != null){
				$orders = $this->Orders->find()
				->where(['Orders.id'=>$id])
				->contain(['Clients'])
				->first()
				;
				//$orders = $this->Orders->get($id);
				//$orders->products_tmp = json_encode(unserialize($orders->products));
				//$orders->products = unserialize($orders->products);
				//pr($orders);
			}
			
			// load product groups list
			$this->loadModel('ProductGroups');
			$product_group_list = $this->ProductGroups->groupListAll();
			//pr($product_group_list);
			$this->set('product_group_list',$product_group_list);
			
			
			// load product list
			$this->loadModel('Products');
			$product_list = $this->Products->ListAll();
			$this->set('product_list',$product_list);
			
			// product group name list
			$product_group_name = $this->ProductGroups->groupListName();
			//pr($product_group_name);
			$this->set('product_group_name',$product_group_name);
			
			
			if ($this->request->is("ajax")){
				$view = new View($this->request);
				$view->autoLayout(false);
				
				$view->set(compact('orders', $orders));
				$view->set(compact('product_list', $product_list));
				$view->set(compact('product_group_name', $product_group_name));
				$view->set(compact('product_group_list', $product_group_list));
				foreach($this->select_config AS $k=>$item){
					//pr($k);
					$view->set($k,$item);
				}
		
				
				$html = $view->render('Orders/edit');
				die(json_encode(['r'=>true,'html'=>$html]));
				
			} else {
				if (isset($this->request->query['show']) || (!$this->request->is("ajax")))
					$this->set('nolayout',true);
				else
					$this->viewBuilder()->autoLayout(false);
			}
			
		} else {
			$this->autoRender = false;
			// convert product from json
			if (isset($this->request->data['products']) && count($this->request->data['products'])>0){
				$products = $this->request->data['products'];
				//pr($products);
				$this->request->data['products'] = [];
				foreach($products AS $k=>$p){
					$p = json_decode($p,true);
					//pr($p);
					$p['kos'] = 0;
					$p['id'] = strtr($p['id'],['d'=>'']);
					if (!isset($p['load'])) unset($p['id']);
					
					$p['user_id'] = $this->request->data['user_id'];
					
					$this->request->data['products'][] = $p;
					
					//pr($p);
					
				}
				//$this->request->data['products'] = serialize($this->request->data['products']);
				
				
			} else {
				$this->request->data['products'] = [];
				//die(json_encode(['r'=>false,'m'=>__('Nemáte přidány žádné produkty')]));
				
			}
			if (!empty($this->request->data['products'])){
				$this->request->data['order_items'] = $this->request->data['products'];
			}
			
			
				
			//pr($this->request->data['order_items']);die();
			
			// delete all items
			$this->loadModel('OrderItems');
			if ($this->request->data['id'] > 0){
				
				$query = $this->OrderItems->query();
				$query->update()
					->set(['kos' => 1])
					->where(['order_id' => $this->request->data['id'],'printed'=>0])
					->execute();
			}
			//pr($this->request->data);
			
			$cfg = [
				'associated' => ['Clients','OrderItems']
			];
			
			if (isset($this->request->data['client'])){
				$this->request->data['name'] = $this->request->data['client']['prijmeni'].' '.$this->request->data['client']['jmeno'];
			
			}
			if (isset($this->request->data['products_tmp']) && empty($this->request->data['products_tmp'])) unset($this->request->data['products_tmp']);
			
			//pr($this->request->data);die();
			
			$this->Orders->patchEntity($orders, $this->request->data(),$cfg);
			
			//pr($orders);
			//die();
			$this->check_error($orders);
			
			if ($result = $this->Orders->save($orders)) {
				$ids = $this->OrderItems->findIds($orders->id);
				if (!empty($this->request->data['history_value'])){
					$history_data = json_decode($this->request->data['history_value']);
					//pr($ids);
					//pr($history_data);
					if (!empty($history_data)){
						foreach($history_data AS $history){
							if ($history->product_id){
							
							$this->History->saveH([
								'created'=>new Time($history->created),
								'type'=>$history->type,
								'name'=>$this->history_type[$history->type],
								'value'=>$history->value,
								'product_id'=>$history->product_id,
								'order_id'=>$history->order_id,
								'order_item_id'=>(isset($ids[$history->order_item_id])?$ids[$history->order_item_id]:$history->order_item_id),
							]);
							
							}
						}
					}
					/*
					$this->History->saveH([
						'type'=>$history->type,
						'name'=>$this->history_type[$history->type],
						'value'=>$history->value,
						'product_id'=>$history->product_id,
						'order_id'=>$history->order_id,
						'order_item_id'=>(isset($ids[$history->order_item_id])?$ids[$history->order_item_id]:''),
					]);
					*/
				
					
					//die();
				}
				die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$orders->id]));
			} else {
				die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
			} 
			
				
			
		}
		$this->set(compact("orders"));
	}
	
	public function loadTableData($table_id=null){
		$this->viewBuilder()->autoLayout(false);
		$this->loadModel('Tables');
		
		
		$this->Tables->updateAll(
			['terminal_id'=>null], // fields
			['terminal_id' => $this->loggedUser['terminal_id']]
		);
		$table_data = $this->Tables->find()
			->where(['id'=>$table_id])
			->select(['id','data','terminal_id'])
			->first();
		//pr($table_data);
		if ($table_data->terminal_id != null && $table_data->terminal_id != $this->loggedUser['terminal_id']){
			die(json_encode(['r'=>false,'m'=>__('Stůl používá jiný terminál')]));
		}
		
		$this->Tables->updateAll(
			['terminal_id'=>$this->loggedUser['terminal_id']], // fields
			['id' => $table_id]
		);
		$data = json_decode($table_data['data'],true);
		//pr($data);
		/*
		foreach($data['items'] AS $k=>$d){
			//pr($data['items'][$k]);
			$data['items'][$k]['created'] = date("Y-m-d H:i:s", strtotime($data['items'][$k]['created']));
		}
		*/
		//pr($table_data['data']);
		$table_data['data'] = json_encode($data);
		//pr($data);
		if (!isset($d['items']) || count($d['items']) == 0){
			$this->clearDateOpen($table_id);
		}
		
			
		die(json_encode(['r'=>true,'data'=>$table_data['data']]));
	}
	
	public function saveOrderData($table_id,$platba_id){
		$this->viewBuilder()->autoLayout(false);
		
		$this->loadModel('Tables');
		$table_data = $this->Tables->find()
			->where(['id'=>$table_id])
			->select(['id','date_open'])
			->first();
		//pr($table_data);die();
		$this->loadModel('Products');
		$group_list = $this->Products->groupList();
		//pr($this->request->data);
		$products = [];
		$total_price = 0;
		foreach($this->request->data AS $prod){
			$total_price += $prod['row_price'];
			$products[] = [
				'table_id'=>$table_id,
				'product_id'=>$prod['id'],
				'product_cat_select'=>$prod['cat_select'],
				'product_group_id'=>$group_list[$prod['id']],
				'name'=>$prod['name'],
				'code'=>$prod['code'],
				'note'=>(isset($prod['note'])?$prod['note']:''),
				'count'=>$prod['count'],
				'price'=>$prod['price'],
				'row_price'=>$prod['row_price'],
				'price_tax_id'=>$prod['price_tax_id'],
				'terminal_id'=>$prod['terminal_id'],
				'user_id'=>$prod['user_id'],
			];
		}
		$save_order = [
			'date_open'=>$table_data->date_open,
			'table_id'=>$table_id,
			'user_id'=>$table_data->user_id,
			'print_user_id'=>$this->loggedUser['id'],
			'order_items'=>$products,
			'total_price'=>$total_price,
			'platba_id'=>$platba_id,
		];
		switch($platba_id){
			// hotove
			case 1: $save_order['pay_hotovost'] = $total_price; break;
			// kartou
			case 2: $save_order['pay_karta'] = $total_price; break;
		}
		
		$save = $this->Orders->newEntity($save_order);
		$saved_data = $this->Orders->save($save);
		$print_order_data = $this->jsonToPrinter($saved_data);
		//pr($print_order_data);
		
		$this->loadModel('Tables');
		$table_data = $this->Tables->find()
			->where(['id'=>$table_id])
			->select(['id','data','terminal_id'])
			->map(function($r){
				$r->data = json_decode($r->data,true);
				return $r;
			})
			->first();
		//$table_data = json_decode($table_data,true);	
		
		
		$row_list = [];
		foreach($this->request->data AS $row_id=>$d){	
			//pr($d);
			$row_id = strtr($row_id,['d'=>'']);
			$row_list[$row_id] = $d['count'];
		}
		//pr($row_list);
		//pr($table_data);
		
		$total_price = 0;
		//pr($table_data->data);
		foreach($table_data->data['items'] AS $row=>$d){
			//pr($row_list);
			//pr($d);
			$row_original = $row;
			$row = strtr($row,['d'=>'']);
			if (isset($row_list[$row])){
				if ($row_list[$row] == $d['count']){
					//pr('a');
					unset($table_data->data['items'][$row_original]);
					//pr($table_data->data['items']);
				} else {
					$table_data->data['items'][$row_original]['count'] = $table_data->data['items'][$row_original]['count'] - $row_list[$row];
				}
			} else {
				$total_price += $d['row_price'];
			}
		}
		$table_data->data['total_price'] = $total_price;
		$table_data['total_price'] = $total_price;
		//pr($table_data->data);
		
		if (empty($table_data->data['items'])){
			$table_data->data['items'] = (object) $table_data->data['items'];
		}
		$data = json_encode($table_data->data);
		
		$save_table = true;
		if ($save_table){
		
		
			$this->Tables->updateAll(
				['data' => $data,'total_price'=>$table_data['total_price']], // fields
				['id' => $table_id]
			);
			
		}
		die(json_encode(['r'=>true,'print_data'=>$print_order_data,'table_id'=>$table_id]));
	}
	
	private function clearDateOpen($table_id){
		$this->Tables->updateAll(
			['date_open' => null,'user_id'=>null], // fields
			['id' => $table_id]
		);
		//pr('aa');
	}
	
	public function saveTableData(){
		$this->viewBuilder()->autoLayout(false);
		//pr($this->request->data);
		
		$this->loadModel('Tables');
		foreach($this->request->data AS $table_id=>$d){	
			
			$data = json_encode($d);	
			
			$table_data = $this->Tables->find()
			->where(['id'=>$table_id])
			->select(['date_open'])
			->first();
			//pr($data);die();
			if (empty($table_data->date_open)){
				$this->Tables->updateAll(
					['date_open' => new Time(),'user_id'=>$this->loggedUser['id'],'data' => $data,'total_price'=>$d['total_price']], // fields
					['id' => $table_id]
				);
				
			} else {
				$this->Tables->updateAll(
				['data' => $data,'total_price'=>$d['total_price']], // fields
				['id' => $table_id]
			);
			
			}
			//pr(count($d));
			if (!isset($d['items']) || count($d['items']) == 0){
				$this->clearDateOpen($table_id);
			}
			
			$table_data = $this->Tables->find()
			->where(['id'=>$table_id])
			->select(['id','data'])
			->first();
			//pr($table_data['data']);
			$part_print_json_data = $this->printPartData(json_decode($table_data['data'],true));
		}
		
		die(json_encode(['r'=>true,'print_data'=>$part_print_json_data]));
	}
	
	private function getPrinters(){
		
		// get printer list from cookie
		if (isset($_COOKIE['printers_setting'])){
			$printer_list = json_decode($_COOKIE['printers_setting'],true);
			$this->printer_list = $printer_list = json_decode($printer_list['data'],true);
		}
		
		// get default printer from cookie
		if (isset($_COOKIE['printer_default'])){
			$printer_default = json_decode($_COOKIE['printer_default'],true);
			$this->printer_default = $printer_default = json_decode($printer_default['data'],true);
		}
	}
	
	private function price_format($price){
			return number_format($price, 2, '.', ' ');
	}
		
	
	public function jsonToPrinter($data=null,$part_print=false){
		if ($data == 'null') $data = null;
		if ($data == null){
		
			$data = $this->Orders->find()
			->contain(['OrderItems'])
			->where(['id'=>34])
			->first();
		}
		
		// get printers from COOKIE
		$this->getPrinters();
		
		function price_format($price){
			return number_format($price, 2, '.', ' ');
		}
		
		// pokud je cast uctenky
		if ($part_print){
			//pr($data);
			$this->printPartData($data);
		}
		
		
		//pr($_COOKIE['printers_setting']);	
		//pr($printer_default);	
		//pr($printer_list);	
		//pr($data);
		$table_data = [];
		
		foreach($data['order_items'] AS $item){
			$table_data[] = [
				'format'=>'',
				'data'=>[
					'Produkt'=> $item['name'], 
					'Ks'=> $item['count'], 
					'Cena'=>price_format($item['price']),
					'Celkem'=>price_format($item['row_price']), 
				],
			];
		}
		$table_data[] = [
			'format'=>'bold',  
			'data'=>[
				'Produkt'=>'Celkem', 
				'Ks'=> '',
				'Cena'=>'',
				'Celkem'=>price_format($data['total_price']),
			],
		];
		$json_print_data = [
			'printer_name'=>$this->printer_default,
			'is_logo'=>true,
			'path_logo' => 'http://p-olliespokl.fastest.cz/css/logo/logo.png',
			'header'=>[
				[
					'font'=>'B',
					'align'=>'center',
					'format'=>'bold',
					'data'=>'Olliens Cukrárna',
				],
				[
					'font'=>'A',
					'align'=>'left',
					'format'=>'',
					'data'=>' ',
				],
				
			],
			'footer'=>[
				[
					'align'=>'left',
					'format'=>'normal',
					'data'=>'Přejeme dobrou chuť',
				],
			],
			'table_data'=>$table_data,
			'table_align'=>[
				'Produkt'=>'left',
				'Cena'=>'right',
				'Celkem'=>'right',	
			],
		];
		if (isset($this->request->query['debug'])){
			pr($json_print_data);
			die();
		} else {
		return $json_print_data;
			
		}
		//pr($json_print_data);die();
		/*
		$json_data = {
		  'printer_name' :'PRP-301', 
		  'is_logo' :true, 
		  'path_logo' :'http://p-olliespokl.fastest.cz/css/logo/logo.png',
		  'header':[
				{
					'font':'B',
					'align':'center',
					'format':'bold',
					'data':'Ollies Cukrárna',
				},
				{
					'font':'A',
					'align':'center',
					'format':'',
					'data':' ',
				},
		  ],
		  'footer':[
				{
					
					'align':'left',
					'format':'normal',
					'data':'Paticka textu',
				},
		  ],
		  'table_data':[
			  {
				'format':'',
				'data':{ Produkt: 'Dort malinový', Ks: '1', 'Cena': '1529.00','Celkem':'1529.00' },
			  },
			  {
				'format':'',  
				'data':{ Produkt: 'Dort červený', Ks: '2', 'Cena': '31.00' ,'Celkem':'62.00'},
			  },
			  {
				'format':'',  
				'data':{ Produkt: 'Zrcš cvjfdvhbfd', Ks: '1', 'Cena': '33.00','Celkem':'33.00' },
			  },
			  {
				//'font':'B',  
				'format':'bold',  
				'data':{ Produkt: 'Celkem', Ks: '', 'Cena': '','Celkem':'1833.00' },
			  },
			],
			'table_align':{
				'Produkt':'left',
				'Cena':'right',
				'Celkem':'right',	
			}
		};
		*/
	}
	
	
	// tisk uctenky pro vyrobu bez logo atd
	private function printPartData($data){
		// get printers from COOKIE
		$this->getPrinters();
		
		$part_json_print_data = [];
			//pr($data);die();
			if (!isset($data['order_items'])){
				$data['order_items'] = $data['items'];
			}
			//pr($data['order_items']);
			foreach($data['order_items'] AS $item){
				
				if (!isset($item['product_group_id'])){
					if (isset($item['product_connects']))
					$item['product_group_id'] = $item['product_connects'][0]['product_group_id'];
				}
				if (isset($item['product_group_id']))
				$part_data[$item['product_group_id']][] = [
					'name'=>$item['name'],
					'count'=>$item['count'],
					'printer_name'=>$this->printer_list[$item['product_group_id']],
				];
			}
			
			//pr($part_data);
			foreach($part_data AS $product_group_id=>$pitems){
				$table_data = [];
			
				foreach($pitems AS $pitem){
				
					$table_data[] = [
						'format'=>'',
						'data'=>[
							'Produkt'=> $pitem['name'], 
							'Ks'=> $pitem['count'], 
						],
					];	
				}
				$part_json_print_data[] = [
					'printer_name'=>$this->printer_default,
					'is_logo'=>false,
					'header'=>[
						[
							'font'=>'A',
							'align'=>'center',
							'format'=>'bold',
							'data'=>'Prosím vyrobit ',
						],
						[
							'font'=>'A',
							'align'=>'left',
							'format'=>'',
							'data'=>' ',
						],
						
					],
					'table_data'=>$table_data,
					'table_align'=>[
						'Produkt'=>'left',
						'Ks'=>'left',
					],
				];
			}
			return $part_json_print_data;
			//pr($part_json_print_data);
			//die();
	}
	
	// nacteni dat stolu pri presunu
	public function loadTable($table_id){
		$this->autoRender = false;
		$this->viewBuilder()->autoLayout(false);
		//$this->viewBuilder()->layout('debug');
		
		$this->loadModel('Tables');
		$table_data = $this->Tables->loadTableData($table_id);
		//pr($table_data);
		if (isset($table_data)){
			//$table->products = unserialize($table->products);
		
			die(json_encode(['r'=>true,'table_data'=>$table_data,'table_id'=>$table_id]));
		} else {
			die(json_encode(['r'=>false,'m'=>__('Stůl je prázdný'),'table_id'=>$table_id]));
			
		}
		
	}
	
	// change table data 
	public function changeTable($table_from_id,$table_to_id,$data_key){
		$this->loadModel('Tables');
		$table_data_from = $this->Tables->find()
			->where(['id'=>$table_from_id])
			->select([
				'id',
				'name',
				'data',
			])
			->hydrate(false)
			->map(function($r){
				$r['data'] = json_decode($r['data'],true);
				return $r;
			})
			
			->first();
		
		$table_data_to = $this->Tables->find()
			->where(['id'=>$table_to_id])
			->select([
				'id',
				'name',
				'data',
			])
			->hydrate(false)
			->map(function($r){
				$r['data'] = json_decode($r['data'],true);
				return $r;
			})
			->first();
		//pr($table_data_from);
		//pr($table_data_to);
		
		if (isset($table_data_from['data']['items'][$data_key])){
			$move = $table_data_from['data']['items'][$data_key];
			unset($table_data_from['data']['items'][$data_key]);
			
			if (!isset($table_data_to['data']['items'])){
				$table_data_to['data'] = [
					'total_price'=>0,
					'items'=>[],
				];
			}
			$table_data_to['data']['items'][$data_key] = $move;
			$table_data_from['data']['total_price'] = $table_data_from['data']['total_price']-$move['row_price'];
			$table_data_to['data']['total_price'] = $table_data_to['data']['total_price']+$move['row_price'];
			$table_data_from['total_price'] = $table_data_from['data']['total_price'];
			$table_data_to['total_price'] = $table_data_to['data']['total_price'];
		}
		
		$table_data_from['data'] = json_encode($table_data_from['data']);
		$table_data_to['data'] = json_encode($table_data_to['data']);
		
		$save_table_from = $this->Tables->newEntity($table_data_from);
		$save_table_to = $this->Tables->newEntity($table_data_to);
		if (!$this->Tables->save($save_table_from)){
			die(json_encode(['result'=>false]));
		}
		if (!$this->Tables->save($save_table_to)){
			die(json_encode(['result'=>false]));
		}
		//pr($save_table_from);
		//pr($save_table_to);
		die(json_encode(['result'=>true]));
	}
	
	
	// clear data from DB
	public function clearData(){
		$conn = ConnectionManager::get('default');
		$rs = $conn->query('TRUNCATE TABLE orders;');
		$rs = $conn->query('TRUNCATE TABLE order_items;');
		
		$this->loadModel('Tables');
		$this->Tables->updateAll(
			[
				'date_open'=>null,
				'data'=>null,
				'total_price'=>null,
				'terminal_id'=>null,
				'user_id'=>null,
			
			], // fields
			[]
		);
		die(json_encode(['r'=>true]));
	}
	
	public function checkOpenTables(){
		$this->loadModel('Tables');
		$open_table = $this->Tables->checkOpenTable();
		//pr($open_table);
		if (!$open_table){
			die(json_encode(['r'=>true]));
		} else {
			die(json_encode(['r'=>false,'table_name'=>$open_table->name]));
		}
	}
	
	
	private function uzaverkaPrintData($products_data,$uzaverka_data){
		//pr($products_data);
		$table_data = [];
		foreach($products_data AS $group_id=>$group){
			//pr($group);
			$total_group_count = 0;
			$group_products = [];
			foreach($group AS $g){
				$total_group_count += count($g);
				foreach($g AS $p){
					
					$group_products[] = [
						'name'=>$p['name'],
						'count'=>$p['count'],
						'row_price'=>$p['row_price'],
					];
				}
			}
			//pr($group_products);
			
			// nazev skupiny
			$table_data[] = [
				'format'=>'bold',  
				'data'=>[
					''=>(isset($this->gname_list[$group_id])?$this->gname_list[$group_id]:''),
					'ks'=>'',
					'Cena'=>$total_group_count.' ks',
				],
			];
			
			// produkty
			if (!empty($group_products)){
				foreach($group_products AS $gp){
					$table_data[] = [
						'format'=>'normal',  
						'data'=>[
							''=>$gp['name'], 
							'ks'=>$gp['count'],
							'Cena'=>$this->price_format($gp['row_price']),
						],
					];
					
				}
			}
			$table_data[] = [
				'format'=>'normal',  
				'data'=>[
					''=>'---------------', 
					'ks'=>'---',
					'Cena'=>'-------',
				],
			];
			// sumace dph
			
			foreach($this->price_tax_list AS $tax_id=>$tax_name){
				if (!isset($total_price[$tax_id])){
					$total_price[$tax_id] = 0;
				}
				if (isset($group[$tax_id])){
					
					foreach($group[$tax_id] AS $g){
						$total_price[$tax_id] += $g['row_price'];
					}
				} else {
					$total_price[$tax_id] = 0;
				}
				
			}
			$sum_total_price = 0;
			foreach($total_price AS $tax_id=>$tax){
				$sum_total_price += $tax;
				$table_data[] = [
					'format'=>'normal',  
					'data'=>[
						''=>$this->price_tax_list[$tax_id], 
						'ks'=>'',
						'Cena'=>$this->price_format($tax),
					],
				];
			}
			
			// sumace group
			$table_data[] = [
				'format'=>'bold',  
				'data'=>[
					''=>'Celkem', 
					'ks'=>'',
					'Cena'=>$this->price_format($sum_total_price),
				],
			];
			
			
		}
		
		$json_print_data = [
			'printer_name'=>$this->printer_default,
			'is_logo'=>true,
			'path_logo' => 'http://p-olliespokl.fastest.cz/css/logo/logo.png',
			'header'=>[
				[
					'font'=>'B',
					'align'=>'center',
					'format'=>'bold',
					'data'=>'Ollies Cukrárna',
				],
				[
					'font'=>'A',
					'align'=>'center',
					'format'=>'normal',
					'data'=>'Vytvořeno: '.$uzaverka_data->created,
				],
				
				
			],
			'footer'=>[
				[
					'font'=>'A',
					'align'=>'left',
					'format'=>'normal',
					'data'=>' ',
				],
				[
					'font'=>'A',
					'align'=>'left',
					'format'=>'normal',
					'data'=>'Vytisknuto: '.date('d.m.Y H:i:s'),
				],
				[
					'font'=>'A',
					'align'=>'left',
					'format'=>'normal',
					'data'=>'Vytvořil: '.$this->user_list[$uzaverka_data->user_id],
				],
			],
			'table_data'=>$table_data,
			'table_align'=>[
				''=>'left',
				'ks'=>'right',
				'Cena'=>'right',	
			],
		];
		//pr($table_data);
		$this->genJsonToHtml($json_print_data);
		if (isset($this->request->query['render'])){
			pr($this->html);
			pr($json_print_data);
		}
		
		return $json_print_data;
	}
	
	// generovani tiskoveho JSON do HTML
	public function genJsonToHtml($data){
		header('Content-Type: text/html; charset=utf-8');
		$this->html = '
		<style>
		#print_html {width:7cm}
		.print_font2 {font-size:15px;}
		.print_align_left {text-align:left;}
		.print_align_right {text-align:right;}
		.print_align_center {text-align:center;}
		.print_format_bold {font-weight:bold;}
		.print_table {width:100%; max-width:500px;margin:auto;}
		.print_table tr td{vertical-align:top;}
		
		
		</style>
		';
		$this->html .= '<div id="print_html">';
		
		// header
		if (isset($data['header']) && !empty($data['header'])){
			foreach($data['header'] AS $row){
				$this->jsonRowHtml($row);
			}
		}
		
		// table
		if (isset($data['table_data']) && !empty($data['table_data'])){
			$this->html .= '<table class="print_table">';
			foreach($data['table_data'] AS $row){
				$this->jsonRowHtml($row,'table',$data);
			}
			$this->html .= '</table>';
			
		} else {
			$this->html .= '<table class="print_table">';
			$row = [
				'data'=>[0=>'Nenalezeny žádné produkty']
			];
			
			$this->jsonRowHtml($row,'table',$data);
			
			$this->html .= '</table>';
		}
		
		// footer
		if (isset($data['footer']) && !empty($data['footer'])){
			foreach($data['footer'] AS $row){
				$this->jsonRowHtml($row);
			}
		}
		//pr($this->html);
		//pr($data);
	}
	
	private function jsonRowHtml($row,$type=null,$data=null){
		$html = '';
		// pokud je tabulka
		if ($type == 'table'){
			$html .= '<tr>';
			$td_class = '';
			if (isset($row['format'])){
				$td_class .= ' print_format_'.$row['format'];
			}
			foreach($row['data'] AS $k=>$col_value){
			
				if (isset($data['table_align']) && !empty($data['table_align'])){
					if (isset($data['table_align'][$k])){
						$td_class2 = 'print_align_'.$data['table_align'][$k];
					} else {
						$td_class2 = '';
					}
				}	
				$html .= '<td class="'.$td_class.' '.$td_class2.'">'.$col_value.'</td>';
			}
			$html .= '</tr>';
		
		} else {
		
		
		// font type
		if (isset($row['font']) && $row['font'] == 'B'){
			$html .= '<div class="print_font2">';
		}
		// font align
		if (isset($row['align'])){
			$html .= '<div class="print_align_'.$row['align'].'">';
		}
		// font format
		if (isset($row['format'])){
			$html .= '<div class="print_format_'.$row['format'].'">';
		}
		// data		
		if (isset($row['data'])){
			$html .= $row['data'];
		}
				
				
		if (isset($row['format'])){
			$html .= '</div>';
		}
		if (isset($row['align'])){
			$html .= '</div>';
		}
				
		if (isset($row['font']) && $row['font'] == 'B'){
			$html .= '</div>';
		}
		
		}
		$this->html .= $html;
	}
	
	public function genUzaverka($type='done',$uzaverka_id=null){
		
		$this->loadModel('Uzaverkas');
		
		$this->set('title','Tisk uzaverka');
		$this->viewBuilder()->layout('print');
		switch($type){
			case 'nahled': 
				$uzaverka_name = 'náhled';
			break;
			case 'done': 
				$uzaverka_name = 'uzavření';
				$done = true;
			break;
		}
		$this->loadModel('ProductGroups');
		
		
		$conditions = [
			'gen_uzaverka'=>0,
			'storno'=>0,
		];
		
		// load product groups list
		$this->loadModel('ProductGroups');
		$product_group_list = $this->ProductGroups->groupListAll(true);
		$this->gname_list = $this->ProductGroups->gnameList($product_group_list);
		
		// user list
		$this->loadModel('Users');
		$this->set('user_list',$this->user_list = $this->Users->userList());
		
		
		// pokud je nahled ulozene uzaverky
		if ($uzaverka_id != null){
			$uzaverka_data = $this->Uzaverkas->find()
				->where([
					'Uzaverkas.system_id'=>$this->loggedUser['system_id'],
					'Uzaverkas.id'=>$uzaverka_id,
				])
				->select([
					'user_id',
					'created',
					'order_id_from',
					'order_id_to',
				])
				->first()
			;
			//pr($uzaverka_data);
			$conditions['gen_uzaverka'] = 1;
			$conditions['id >='] = $uzaverka_data->order_id_from;
			$conditions['id <'] = $uzaverka_data->order_id_to;
			$uzaverka_date = $uzaverka_data->created;
		} else {
			$uzaverka_date = date('d.m.y H:i:s');
			$uzaverka_data = (object)[
				'user_id'=>$this->loggedUser['id'],
				'created'=>$uzaverka_date,
				'order_id_from'=>0,
				'order_id_to'=>0,
			];
		}
		
		
		//pr($conditions);
		
		
		$this->loadModel('Orders');
		$orders_list = $this->Orders->find()
			->where($conditions)
			->select([
			])
			->contain(['OrderItems'=>function ($q) {
					//pr($q);
					return $q->autoFields(false)
                        ->where([
				//			'kos'=>0
						 ])
                        ->select([
						'id',
						'name',
						'count',
						'row_price',
						'price',
						'price_tax_id',
						'product_group_id',
						'product_id',
						'order_id',
						])
                        ;
			}])
			->order('id ASC')
			->toArray()
		;
		
		
		
		//pr($conditions);
		if(!$orders_list){
			die(json_encode(['r'=>false,'m'=>'Není co generovat']));
		}
		//$it = $orders_list[0]->order_items[0];
		/*
		for ($i = 1; $i <= 55; $i++) {
			$orders_list[0]->order_items[] = $it;
		}
		*/
		//pr($orders_list[0]->order_items);
		//pr($orders_list);
		
		
		//pr($product_group_list);die();
		$products_list = [];
		if($orders_list){
			foreach($orders_list AS $order){
				
				foreach($order['order_items'] AS $pl){
					$pl->platba_id = $order->platba_id;
					$products_list[$pl->product_group_id][$pl->price_tax_id][] = $pl;
				}
			}
		}
		
		//pr($table_data);
		//pr($products_list);
		//pr($uzaverka_data);
		$json_print_data = $this->uzaverkaPrintData($products_list,$uzaverka_data);
		
		// pokud je uzaverneni smeny
		if (isset($done)){
			$this->sendUzaverkaEmail();
			
			
			$id_from = $orders_list[0]->id;
			$open_date = $orders_list[0]->created;
			$done_date = end($orders_list)->created;
			$id_to = end($orders_list)->id;
			//pr(end($orders_list));
			//pr($id_from);
			//pr($id_to);
			
			$this->Orders->updateAll(
				['gen_uzaverka' => 1], // fields
				['id >=' => $id_from,'id <='=>$id_to]
			);
			
			
			
			
			$save_uzaverka = $this->Uzaverkas->newEntity([
				'user_id'=>$this->loggedUser['id'],
				'order_id_from'=>$id_from,
				'order_id_to'=>$id_to + 1,
				'open_date'=>$open_date,
				'done_date'=>$done_date,
			]);
			//pr($json_print_data);
			//pr($save_uzaverka);die();
			$this->Uzaverkas->save($save_uzaverka);
			$table_data = true;
			
			
			//die('a');
		}
		//pr($this->html);
		die(json_encode(['r'=>true,'print_data'=>(!empty($table_data)?$json_print_data:''),'html'=>$this->html]));
	}
	
	
	
	
	
	
	
	
	
	
	
	public function loadOrders($date=null){
		
		// load table list
		$this->loadModel('Tables');
		$this->tableNameList = $this->Tables->tableNameList();
		//pr($tableNameList);	die();
		
		$this->loadModel('Users');
		$this->set('user_list',$this->user_list = $this->Users->userList());
		//pr($user_list);
		
		if ($date == null){
			$datum = date('Y-m-d');
			
		} else {
			$datum = $date;
		}
		$trzba_date_list = [date('Y-m-d')=>date('d.m.Y')];
		$orders_list = $this->Orders->find()
			->order('id DESC')
			->limit(2000)
			->select(['id','created'])
			->toArray();
		if ($orders_list){
			foreach($orders_list AS $o){
				$trzba_date_list[$o->created->format('Y-m-d')] = $o->created->format('d.m.Y');
			}
		}
		//pr($trzba_date_list);
		//pr($orders_list);
		
		$total = [
			'price'=>0,
			'sleva'=>0,
			'storno'=>0,
			'kartou'=>0,
			'hotove'=>0,
		];
		$total['datum'] = $datum;
		
		$conditions = [
			'kos'=>0,
			'DATE(created)'=>$datum,
		];
	//	pr($conditions);
		$orders = $this->Orders->find()
			->where($conditions)
			->contain(['OrderItems'=>['conditions'=>['kos'=>0]]])
			->order('id DESC')
			->limit(200)
			->select([])
			->map(function($row){
				//pr($user_list[$row->print_user_id]);
				$row->table_name = (isset($this->tableNameList[$row->table_id])?$this->tableNameList[$row->table_id]:'');
				$row->print_username = (isset($this->user_list[$row->print_user_id])?$this->user_list[$row->print_user_id]:'');
				$row->total_price_num = $row->total_price;
				$row->total_price = $this->price($row->total_price);
				$row->count_items = count($row->order_items);
				if (!empty($row->open_date))
				$row->created = $row->open_date->format('d.m.Y H:i:s');
				$row->done_date = $row->done_date->format('d.m.Y H:i:s');
				$row->modified = $row->modified->format('d.m.Y H:i:s');
				
				if (isset($row->order_items)){
					foreach($row->order_items AS $k=>$oi){
						$row->order_items[$k]->username = $this->user_list[$oi->user_id];
					}
				}
				//pr($row);
				return $row;
			})
			->toArray()
		;
		$data = $orders;
		//pr(count($data));
		foreach($data AS $d){
			//pr($d->pay_sleva);
				
			// storno
			if ($d->storno == 1){
				$total['storno'] += $d->total_price_num;
			} else {
				$total['price'] += $d->total_price_num;
			
			}
			// hotove
			if ($d->platba_id == 1){
				$total['hotove'] += $d->total_price_num;
				
			}
			// kreditka
			if ($d->platba_id == 2){
				$total['kartou'] += $d->total_price_num;
				
			}
			if (!empty($d->pay_sleva)){
				$total['sleva'] += $d->pay_sleva;
				
			}
		}
		//pr($total);
		if (!$this->request->is("ajax")){
			pr($data);
		}
		//pr($orders);
		die(json_encode(['r'=>true,'data'=>$data,'total'=>$total,'trzba_date_list'=>$trzba_date_list]));
	}
	
	public function printUcet($hash){
		header('Content-Type: text/plain; charset=utf-8');
		//pr(base64_decode($hash));die();
		//pr($this->request->data['data']);die();
		if (isset($this->request->data['data']))
		$hash = $this->request->data['data'];
		//$hash = strtr($hash,[' '=>'']);
		//pr($hash);
		//$hash = 'eyJjcmVhdGVkIjoiMjAxNi0xMS0yMiAwNTozMjoyNCIsImRvbmVfZGF0ZSI6IjIwMTYtMTEtMjIgMDY6MzI6MDQiLCJvcmRlcl9pdGVtcyI6W3siaWQiOiI5IiwicHJvZHVjdF9pZCI6IjkiLCJwcm9kdWN0X2dyb3VwX2lkIjo3NCwibnVtIjoiIiwiY29kZSI6IjUwMSIsIm5hbWUiOiJBbMWw61yc2vDoSAiLCJhZGRvbiI6MCwicHJpY2UiOiI1Ni4wMCIsInByaWNlX3RheF9pZCI6MiwidXNlcl9pZCI6MCwibGluZV9pZCI6ImxpbmU5Iiwia3MiOjEsInByaWNlX2RlZmF1bHQiOjU2LCJkb25lIjoxLCJzYXZlX2tzIjoiMSJ9LHsiaWQiOiIyIiwicGFyZW50X2lkIjoiOSIsInByb2R1Y3RfaWQiOiIyIiwibmFtZSI6IkJleiBrb2ZlaW51ICIsImFkZG9uIjoxLCJwcmljZSI6IjEwLjAwIiwidXNlcl9pZCI6MCwibGluZV9pZCI6ImxpbmUyXzkiLCJrcyI6MSwicHJpY2VfZGVmYXVsdCI6MTAsImRvbmUiOjEsInNhdmVfa3MiOiIxIn1dLCJ1c2VyX2lkIjowLCJ1c2VyIjoiU3VwZXJhZG1pbiIsInRhYmxlX2lkIjowLCJwbGF0YmFfaWQiOjEsInBsYXRiYV9uYW1lIjoiSG90b3bEmyIsInByaW50ZWQiOjEsImRvbmUiOjEsInByaW50X3VzZXJfaWQiOjAsImNvdW50X2l0ZW1zIjoyLCJ0YWJsZV9uYW1lIjoiVm9sbsO9IHByb2RlaiIsInRvdGFsX3ByaWNlIjoiNjYuMDAiLCJwYXlfaG90b3ZlIjo2Nn0=';
		//$hash = 'eyJjcmVhdGVkIjoiMjAxNi0xMS0yMiAwNTozMjoyNCIsImRvbmVfZGF0ZSI6IjIwMTYtMTEtMjIgMDY6MTg6MDUiLCJvcmRlcl9pdGVtcyI6W3siaWQiOiI5IiwicHJvZHVjdF9pZCI6IjkiLCJwcm9kdWN0X2dyb3VwX2lkIjo3NCwibnVtIjoiIiwiY29kZSI6IjUwMSIsIm5hbWUiOiJBbMW+w61yc2vDoSAiLCJhZGRvbiI6MCwicHJpY2UiOiI1Ni4wMCIsInByaWNlX3RheF9pZCI6MiwidXNlcl9pZCI6MCwibGluZV9pZCI6ImxpbmU5Iiwia3MiOjEsInByaWNlX2RlZmF1bHQiOjU2LCJkb25lIjoxLCJzYXZlX2tzIjoiMSJ9LHsiaWQiOiIyIiwicGFyZW50X2lkIjoiOSIsInByb2R1Y3RfaWQiOiIyIiwibmFtZSI6IkJleiBrb2ZlaW51ICIsImFkZG9uIjoxLCJwcmljZSI6IjEwLjAwIiwidXNlcl9pZCI6MCwibGluZV9pZCI6ImxpbmUyXzkiLCJrcyI6MSwicHJpY2VfZGVmYXVsdCI6MTAsImRvbmUiOjEsInNhdmVfa3MiOiIxIn1dLCJ1c2VyX2lkIjowLCJ1c2VyIjoiU3VwZXJhZG1pbiIsInRhYmxlX2lkIjowLCJwbGF0YmFfaWQiOjEsInBsYXRiYV9uYW1lIjoiSG90b3bEmyIsInByaW50ZWQiOjEsImRvbmUiOjEsInByaW50X3VzZXJfaWQiOjAsImNvdW50X2l0ZW1zIjoyLCJ0YWJsZV9uYW1lIjoiVm9sbsO9IHByb2RlaiIsInRvdGFsX3ByaWNlIjoiNjYuMDAiLCJwYXlfaG90b3ZlIjo2Nn0=';
		//$this->set('title','Tisk účet');
		$this->viewBuilder()->autoLayout(false);
		$params = json_decode(base64_decode($hash),true);
		$this->loadModel('ProductGroups');
		$printer_list = $this->ProductGroups->printerList();
		$this->set('printer_list',$printer_list);
		
		//pr($printer_list);die();
		if ($this->request->is("ajax")){
			//sleep(10);
		}
		
		$total_price = 0;
		foreach($params['order_items'] AS $k=>$i){
			unset($params['order_items'][$k]['id']);
			if (isset($i['save_ks'])){
				$params['order_items'][$k]['ks'] = $i['save_ks'];
				
			} else {
				$params['order_items'][$k]['ks'] = $i['ks'];
			
			}
			$params['order_items'][$k]['table_id'] = $params['table_id'];
			$params['order_items'][$k]['user_id'] = $params['user_id'];
			$params['order_items'][$k]['printed'] = $params['printed'];
			
			
			$total_price += $i['ks'] * $i['price'];
		}
		$params['open_date'] = new Time($params['created']);
		unset($params['created']);
		
		$params['done_date'] = new Time($params['done_date']);
		$params['total_price'] = $total_price;
		$params['done'] = 1;
		$params['print_user_id'] = $this->loggedUser['id'];
		//pr($params);die();
		
		//adie();
		$save_order = $this->Orders->newEntity($params);
		
		//pr($save_order);
		$result = $this->Orders->save($save_order);
		
		die(json_encode(['r'=>true,'ucet_id'=>$result->id]));
		
		
	}
	
	public function loadUzaverkaList(){
		$this->viewBuilder()->autoLayout(false);
		// load user list
		$this->loadModel('Users');
		$this->set('user_list',$user_list = $this->Users->userList());
		
		$this->uzaverkaList();
		$this->render('elements/uzaverka_list');
	}
	
	
	
	private function sendUzaverkaEmail(){
		$this->loadComponent('Email');
		$response = $this->render('gen_uzaverka');
		$html = $response->body();
		
		$options = [
			'template'=>'uzaverka',
			'subject'=>'Uzávěrka pokladna '.$this->system_list[$this->loggedUser['system_id']].' '.date('d.m.y H:i:s'),
			'data'=>$html,
		];
		//die('a');	
		$this->Email->send($this->email_list,$options);
	}
	
	private function uzaverkaList(){
		$this->loadModel('Uzaverkas');
		$uzaverka_list = $this->Uzaverkas->find()
			->where(['Uzaverkas.system_id'=>$this->loggedUser['system_id']])
			->select([])
			->order('id DESC')
			->toArray()
		;
		//pr($uzaverka_list);
		if ($uzaverka_list){
			$this->set('uzaverka_list',$uzaverka_list);
		}
	}
	
	public function drawTable(){
		$this->set('scripts',[
				'/js/fst_pokladna/fst_modal.js',
				'/js/fst_pokladna/fst_uzaverka.js',
				//'/js/fst_pokladna/fst_pokladna.js',
				//'/js/websocket/client.js',
		]);
		// load table list
		$this->loadModel('Tables');
		$this->set('table_list',$table_list = $this->Tables->listAll());
		//pr($table_list);die();
			
	}
	
	public function saveTable($data){
		$data = json_decode($data);
		// load table list
		$this->loadModel('Tables');
		$this->loadModel('Settings');
		$find_id = $this->Settings->find()
			->where([
				'type'=>2,
				'system_id'=>$this->loggedUser['system_id'],
			])
			//->combine('id','name')
			->first();
		//pr($find_id);die();
		
		$save_setting = $this->Settings->newEntity([
			'id'=>(isset($find_id->id)?$find_id->id:''),
			'type'=>2,
			'data'=>serialize($data),
		]);
		//pr($save_doklad);
		$ids = [];
		foreach($data AS $k=>$d){
			foreach($d AS $kk=>$dd){
			
				$ids[] = $kk;
			}
		}
		if ($ids){
		
		$query = $this->Tables->query();
			$query->update()
			->set(['on_map' => 1])
			->where(['id IN' => $ids])
			->execute();
		}
		
		$this->Settings->save($save_setting);
		Cache::clear(false);
		die(json_encode(['result'=>true]));
	}
	
	
	public function saveLocalOrders(){
		//$data = json_decode($data);
		//pr($this->request->data);
		if (isset($this->request->data)){
			
			$return_ids = [];
			foreach($this->request->data AS $date=>$data){
				foreach($data AS $k=>$d){
				
					$data[$k]['open_date'] = $data[$k]['created'] = new Time($d['created']);
					$data[$k]['done_date'] = new Time($d['done_date']);
				}
				$entities = $this->Orders->newEntities($data);
				
			}
			$return_ids[$date] = [];
			foreach ($entities as $row=>$entity) {
				//$return_ids[$date][$row] = 14;
				//pr($entity);
				$result = $this->Orders->save($entity);
				$return_ids[$date][$row] = $result->id;
			}
		}
		
		
		die(json_encode(['r'=>true,'return_ids'=>$return_ids]));
	}
	
	
	
	
	
	
	
	
	
	
	// format price
	public function price($price,$mena_suf = array()){
		
		//pr($mena_suf);
		$symbol_before = '';
		$kurz = null;
		$count = 1;
		$decimal = 0;
		$symbol_after = ',-';
		extract($mena_suf);
		
    	$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbol_before.number_format(strtr($price,$white), $decimal, '.', ' ').$symbol_after;
		else
			return $symbol_before.number_format(strtr(0.00,$white), $decimal, '.', ' ').$symbol_after;	
	}
	
	
	
	
	public function deleteTable($id){
		$this->loadModel('Tables');
		$query = $this->Tables->query();
			$query->update()
			->set(['on_map' => 0])
			->where(['id' => $id])
			->execute();
		Cache::clear(false);	
		die(json_encode(['result'=>true]));
		
	}
	
	
	public function findIds($order_id){
		$this->loadModel('OrderItems');
		$this->OrderItems->findIds($order_id);
		die('a');
	}
	
	public function printOrderPart($order_id){
		$this->set('title','Tisk částečně');
		$this->viewBuilder()->layout('print');
		
		$this->loadModel('ProductGroups');
		$printer_list = $this->ProductGroups->printerList();
		$this->set('printer_list',$printer_list);
		//pr($printer_list);
		
		// load table list
		$this->loadModel('Tables');
		$this->set('table_list_name',$table_list_name = $this->Tables->tableNameList());
		
		$order = $this->Orders->find()
			->where(['id'=>$order_id])
			->contain(['OrderItems'=>['conditions'=>['kos'=>0]]])
			->select([])
			->first()
		;
		if ($order){
		
			$printers = [];
			foreach($order->order_items AS $item){
				$printers[$item->product_group_id][] = $item;
			}
		
			//pr($printers);
			//pr($order);
			$this->set('order',$order);
		}
		
		$this->set('print_data',$printers);
		
		
		
		
	}
	
	public function printUcet2(){
		require_once(ROOT . '/vendor' . DS  . 'mpdf2' . DS . 'mpdf.php');
			
	


	

	$mpdf=new mPDF('c'); 

	$mpdf->SetDisplayMode('fullpage');

	// LOAD a stylesheet
	$stylesheet = file_get_contents('./css/print.css');
	$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
	
	$mpdf->WriteHTML($html);

	$mpdf->Output();

	exit;
		
	}
	
	
	private function saveDoklad($order,$hash){
		//pr($hash);
		//pr($order_data);
		
		$this->loadModel('Doklads');
		$total_price = 0;
		foreach($order->order_items AS $item){
			$total_price += $item->price * $item->ks;
		}
		$save_doklad = $this->Doklads->newEntity([
			'order_id'=>$order->id,
			'table_id'=>$order->table_id,
			'total_price'=>$total_price,
			'client_id'=>$order->client_id,
			'platba_id'=>$order->platba_id,
			'hash'=>serialize($hash),
			'order_items'=>serialize($order->order_items),
		]);
		//pr($save_doklad);
		$this->Doklads->save($save_doklad);
		$doklad_id = $save_doklad->id;
		//die('aa');
		return $doklad_id;
	}
	
	public function loadDoklad($id,$render=null){
		
		$this->loadModel('Doklads');
		$doklad = $this->Doklads->find()
			->where(['Doklads.id'=>$id])
			->contain(['Orders'=>['conditions'=>['Orders.kos'=>0]]])
			->select([])
			->map(function($r){
				$r->print_user_id = $r->order->print_user_id;
				$r->hash = unserialize($r->hash);
				$r->order_items = unserialize($r->order_items);
				return $r;
			})
			->first()
		;
		if ($render != null){
			$this->request->query[$render] = true;
		}
		$this->printUcet($doklad);
	}
	
	
	
	public function orderFce($type,$order_id,$table_id,$params=null){
		
		// ukoncit objednavku
		if ($type == 'ukoncit'){
			$this->loadModel('OrderItems');
			$product_list_to_print = [];
			$product_list = $this->OrderItems->find("all")
			  ->where(['kos'=>0,'order_id'=>$order_id])
			  ->select([
				"id", 
				"product_id",
				"done",
				"ks",
				"name",
				"price",
				"printed",
				])
				->order("done ASC")
				//->combine('id','product_id')
				->toArray()
				
			;
			$product_list_merge = [];
			$total_price = 0;
			$is_done = true;
			foreach($product_list AS $p){
				$total_price += $p->price;
				if ($p->printed == 0){
					
					$is_done = false;
				}
				if ($p->done == 0){
					$product_list_to_print[] = [
						'product_id'=>$p->product_id,
						'id'=>$p->id
					];
					
				}
				if (isset($product_list_merge[$p->product_id])){
					$product_list_merge[$p->product_id]['ks'] += $p->ks;
				} else {
					$product_list_merge[$p->product_id] = $p; 
					
				}
				
			}
			
			//pr($product_list_to_print);
			//pr($product_list_merge);
			//pr($product_list);
			
			
			$query = $this->OrderItems->query();
			$query->update()
			->set(['done' => 1])
			->where(['order_id' => $order_id,'kos'=>0])
			->execute();
			
			
			$this->loadModel('Orders');
			$query2 = $this->Orders->query();
			$query2->update()
			->set([
				'total_price' => $total_price,
				'done' => (($is_done == true)?1:0),
			])
			->where(['id' => $order_id,'kos'=>0])
			->execute();
			
			$this->updateCacheTable($table_id);
		}
		die(json_encode(['r'=>true,'close'=>(($is_done == true || $params == 'close')?true:false)]));
	}

	private function sum_total_price($order_id){
		$total_price = 0;
		$data = $this->OrderItems->find("all")
			->where(['kos'=>0,'order_id'=>$order_id])
			->select([
				"id", 
				"product_id",
				"done",
				"ks",
				"name",
				"price",
				"printed",
			])
			->order("done ASC")
			//->combine('id','product_id')
			->toArray();
		
		foreach($data AS $d){
			$total_price += $d->price;
		}
		$query = $this->Orders->query();
		$query->update()
		->set([
			'total_price' => $total_price,
		])
		->where(['id' => $order_id])
		->execute();
		
		//pr($total_price);
		return $total_price;
	}
	
	private function updateCacheTable($table_id){
		Cache::delete('table_id_'.$table_id);
		$table = $this->Orders->findTableData($table_id);
		
	}
	
	public function productDetail($id,$original_id=null){
		$this->loadModel('OrderItems');
		$product_details = $this->OrderItems->newEntity();
		//pr($product_details);  
		
		
		//$this->viewBuilder()->autoLayout(false);
		
		$this->loadModel('Users');
		
		$product = $this->OrderItems->find()
			->where(['id'=>$id])
			->select([
				'id',
				'note',
			])
			->first()
		;
		
		
		// load historie produktu
		if (in_array($this->loggedUser['group_id'],[1])){
		
		$this->loadModel('Histories');
		$history_list = $this->Histories->find()
			->where(['order_item_id'=>$id])
			->select([
				'type',
				'name',
				'value',
				'user_id', 
				'created', 
			])
			->order('id DESC')
			->toArray();
		//pr($history_list);
		} else {
			$history_list = [];
		}
		
		$view = new View($this->request);
		$view->autoLayout(false);
		$view->set('history_list',$history_list);
		$view->set('original_id',$original_id);
		$view->set('product',$product);
		$view->set('user_list',$user_list = $this->Users->userList());
		$view->set(compact("product_details"));
		
		
		if (isset($this->request->query['direct'])){
			$this->set(compact("product_details"));
		
			$this->set('history_list',$history_list);
			$this->set('product',$product);
			$this->set('user_list',$user_list);
		}
		
		$html = $view->render('Orders/product_detail');
		
		if (!isset($this->request->query['direct'])){
			die(json_encode(['r'=>true,'html'=>$html]));
		} else {
			$this->viewBuilder()->layout('debug');
			
		}
		//pr($product);
		
	}
}

