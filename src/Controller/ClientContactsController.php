<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class ClientContactsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
  }

  public function index()
  {
    $this->set('items', $this->paginate($this->Clients));
  }

  public function add()
  {
  }


}
