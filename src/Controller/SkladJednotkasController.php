<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use Cake\Utility\Hash;

use Cake\Cache\Cache;

use App\View\Helper\FastestHelper;
use Cake\Datasource\ConnectionManager;

class SkladJednotkasController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index()
  {
		
	$this->set("title", __("Nastavení jednotek skladu"));
	
	$conditions = $this->convert_conditions(['SkladJednotkas.kos'=>0]);
	
	$mapper = function($r, $key, $mapReduce){
		//pr($r);
		if (isset($r->product_connects) && !empty($r->product_connects)){
				foreach($r->product_connects AS $k=>$p){
					//pr($k);
					if ($k == 0) {
						$r->group_id1 = $p->product_group_id;
					}
					if ($k == 1) {
						$r->group_id2 = $p->product_group_id;
					}
					if ($k == 2) {
						$r->group_id3 = $p->product_group_id;
						
					}
				}
				unset($r->product_connects);
				
		}
		$mapReduce->emit($r);
			
	};
	$data = $this->SkladJednotkas->find()
      ->where($conditions)
	  //->contain(['ProductConnects'])
	  ->select(['SkladJednotkas.id', 'SkladJednotkas.jednotka_id', 'SkladJednotkas.dodavatel_jednotka','SkladJednotkas.prevod'])
	 
	  ->mapReduce($mapper)
	  	
	  //->toArray()
	  ;
	//pr($data);	die();
	//die();
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
		),
		'filtr'=>array(
			//'num'=>__('Číslo').'|SkladJednotkas__num|text_like',
			'dodavatel_jednotka'=>__('Dodavatel jed.').'|SkladJednotkas__dodavatel_jednotka|text'
		),
		'list'=>array(
			'jednotka_id'=>$this->sklad_jednotka_list,
			//'price_tax_id'=>$this->tax_value_con,
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }

  public function edit($id=null){
    $this->set("title", __("Editace jednotky"));
    $this->viewBuilder()->layout("ajax");
	$sklad_jednotkas = $this->SkladJednotkas->newEntity();
	
	
	if ($id != null){
		$sklad_jednotkas = $this->SkladJednotkas->find()
		  ->where(['SkladJednotkas.id'=>$id])
		  ->first();
		  //pr($sklad_jednotkas->product_connects);
	}
	//pr($this->request);
    if ($this->request->is("ajax")){
		if (!isset($this->request->data['product_recepturas'])) $this->request->data['product_recepturas'] = [];
	//pr($this->request->data());die();
	  $sklad_jednotkas = $this->SkladJednotkas->patchEntity($sklad_jednotkas, $this->request->data(),['associated' => ["ProductRecepturas"]]);
	  //pr($sklad_jednotkas);die();
	  $this->check_error($sklad_jednotkas);
	  
	  if ($result = $this->SkladJednotkas->save($sklad_jednotkas)) {
    	
		Cache::clear(false);
		die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$sklad_jednotkas->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("sklad_jednotkas"));
  }
  

}
