<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\Date;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

Date::$defaultLocale = 'cs-CZ';
Date::setToStringFormat('dd.MM.YYYY');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public function beforeFilter(Event $event)
    {
		
		
		//pr(Configure::read('App'));
		$this->loadComponent('History');
		$this->load_select_config();
		$this->load_system_id();
		if (in_array($this->request->action, [
			'sendPushMessage',
			'savePushHash',
			'saveGoogleAreas',
			'loadMakro',
			'loadBidFood',
			'getSkladItems',
		])) {
         $this->eventManager()->off($this->Csrf);
		}
		
		$this->Security->config('unlockedActions', [
			'autocomplete',
			'autocompleteMesto',
			'edit',
			'sendError',
			'sendPushMessage',
			'savePushHash',
			'saveGoogleAreas',
			'saveLocalOrders',
			'printUcet',
			'saveTableData',
			'saveOrderData',
			'menusSave',
			'editStav',
			'loadMakro',
			'loadBidFood',
		]);
		if(!$this->request->is("ajax")){		
			$this->load_js();
		}
		$this->check_logged();	
		$this->setting();	
		$this->saveLog();
		
		if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){
		//die('e');
		}
		//$this->map_areas_load();
        return parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();
		
		if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){
		//die('vv');
		}
        $this->loadComponent('Paginator',
          [
            'limit' => 1000,
            'maxLimit' => 1500,
          ]);
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Security');
        $this->loadComponent('Csrf');
		//pr($_SERVER);
		
		
        $this->loadComponent('ViewIndex');
        //$this->loadComponent('PushMessage');
        //pr(configSystemId);
		$this->loadComponent('Auth', [
          'unauthorizedRedirect' => false,
          'authError' => "Nemáte přístup do této stránky",
          'loginRedirect' => '/users/login',
          'authorize' => [
            'Controller'
          ]
        ]);
	
        //$this->Auth->allow(["edit",]);
        //$this->Auth->allow();
		$this->set('current_url',rtrim($this->request->url,'/'));
		
		
        if($this->request->is("ajax")){
            $this->viewBuilder()->layout("ajax");
        } else {
			
			$this->db_schema();
        }
		


    }
	
	
	function add_column_if_not_exist($db, $column, $column_attr = "VARCHAR( 255 ) NULL" ,$primary=null,$index=false){
		$conn = ConnectionManager::get('default');
		$exists = false;
		
		$columns = $conn->execute("show columns from $db");
		$results = $columns ->fetchAll('assoc');
		
		foreach($results AS $c){
		
			if($c['Field'] == $column){
				$exists = true;
				break;
			}
		}
		if(!$exists){
		  if ($primary){
			mysql_query("ALTER TABLE fastest__xmls DROP PRIMARY KEY");
		  }
		  $conn->execute("ALTER TABLE `$db` ADD `$column`  $column_attr");
		  if ($index)
		  $conn->execute("ALTER TABLE `$db` ADD INDEX ( `$column` )");
	  }
	}
	
	private function db_schema(){
		$this->add_column_if_not_exist('tables','terminal_id','INT(11) NULL ',null,true);
		/*
		$schema = new TableSchema('tables');
		pr($schema);
		$cols = $schema->columns();
		pr($cols);
		//pr($table);
		/*
		$schema->addColumn('terminal_id', [
		  'type' => 'integer',
		  'length' => 11,
		  'null' => true,
		  'default' => null,
		])
		->addIndex('terminal_id', [
		  'columns' => ['terminal_id'],
		  'type' => 'index'
		]);
		/*
		$db = ConnectionManager::get('default');
		$queries = $schema->createSql($db);
		foreach ($queries as $sql) {
			pr($sql);
		 // $db->execute($sql);
		}
		*/
		//pr($schema);
	}
	
	private function setting(){
		$this->loadModel('Settings');
		$this->set('setting_global',$this->setting_global = $this->Settings->settingGlobal());
		$this->set('setting_table_map',$this->setting_table_map = $this->Settings->settingTableMap());
		//pr($this->setting_global);
	}
	
	private function saveLog(){
		$this->loadModel('Logs');
		
		$save_log = [
			'user_id'=>$this->loggedUser['id'],
			'controller'=>$this->request->params['controller'],
			'action'=>$this->request->params['action'],
			'value'=>(isset($this->request->params['pass'])?implode(',',$this->request->params['pass']):''),
		];
		if ($save_log['controller'] != 'Orders' && isset($this->loggedUser['id'])){
			
		
		$save = $this->Logs->newEntity($save_log);
		$this->Logs->save($save);
		}
		//pr($save_log);
		//pr($this->request);
		//die();
	}
	
	private function map_areas_load(){
		if ($this->Auth->user()){
		$this->loadModel('Mapareas');
		$map_areas_load = $this->Mapareas->find()
		->where(['id'=>$this->system_id])
		->select(['coords'])
		->first()
		;
		//pr($this->system_id);
		
		$map_areas_load = $map_areas_load->coords;
		//pr($map_areas_load);
		$map_data = json_decode($map_areas_load,true);
		$map_areas_list = [];
		foreach($map_data AS $k=>$m){
			$map_areas_list[$k] = $m['name'];
		}
		//pr($map_areas_load);
		//die();
		$this->set('map_areas_list',$map_areas_list);
		$this->set('map_areas_load',$map_areas_load);
		}
	}

    public function isAuthorized($user = null){

        return true;

        $auth = $this->Auth->user("auth");
        $controller = strtolower($this->request->controller);
        $action = strtolower($this->request->action);

        if(isset($auth["all"]) && $auth["all"]["all"] == 1){
            return true;
        }
        else {
            if (isset($auth[$controller]) && isset($auth[$controller][$action]) && $auth[$controller][$action] == 1) {
                return true;
            } else {
                return false;
            }
        }
    }
	
	private function load_select_config(){
		$sc = Configure::read('select_config');
		$this->select_config = $sc;
		
		foreach($sc AS $k=>$item){
			$this->$k = $item;
			$this->set($k,$item);
		}
		//pr($sc);
		$kurz_eu = Configure::read('kurz_eu');
		$this->set('kurz_eu',$kurz_eu);
		//pr($kurz_eu);
		//die();
	}

	private function check_logged(){
		
		if ($this->Auth->user()){
			$this->set('loggedUser',$this->Auth->user());
			$this->loggedUser = $this->Auth->user();
			
			$controllers = [];
			if (isset($this->loggedUser['disable_menu'])){
				foreach($this->loggedUser['disable_menu'] AS $m){
					if (isset($this->menu_items[$m])){
						$controllers[] = strtr($this->menu_items[$m]['class'],['menu_'=>'']);
					}
					if (isset($this->menu_items2[$m])){
						$controllers[] = strtr($this->menu_items2[$m]['class'],['menu_'=>'']);
					}
				}
			}
			if (in_array(strTolower($this->request->params['controller']),$controllers)){
				$this->redirect('/');
			}
			//pr($this->request->params['controller']);
			//pr($controllers);
			//pr($this->loggedUser);
		}
	/*	
		$this->loggedUser = [
			'user_id'=> 1,
			'group_id'=> 1,
			'name'=> 'Superadmin',
		];
		//$this->request->session()->write('System.user_id', 1);
		$this->request->session()->write($this->loggedUser);
		*/
		//$this->set('loggedUser',$this->loggedUser);
		
	}
	
	private function load_system_id(){
		//pr($_SERVER);die();
		
		
		
		if ($this->request->session()->check('System.system_id')) {
			
			$this->request->session()->write('System.system_id', configSystemId);
			$this->system_id = $this->request->session()->read('System.system_id');
			//pr($this->system_id);
			//$this->system_id = configSystemId;
			
			$this->set('system_id',$this->system_id);
			
		} else {
			$this->request->session()->write('System.system_id', configSystemId);
			
		}
		//pr(configSystemId);die();
		
	}
	
	/*** ENCODE LONG URL **/	
	public function encode_long_url($data){
         return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
    }
    /*** DECODE LONG URL **/	
	public function decode_long_url($data){
    	$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(@gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
    }
	


    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
		
		
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
	
	
	
    
	
	private function find_ids($value,$array){
		if (isset($array[$value])){
			return array_keys($array[$value]);
		} else {
			foreach ($array AS $k=>$a){
				//$this->find_ids($)
			}
		}
	}
	
	public function convert_conditions($con){
		$conditions = array();
		$disable_keys = [
			'h',
			'sort',
			'direction',
			'page',
		];
			
		if (isset($this->request->query) && !empty($this->request->query)){
			//pr($this->request);
			
			foreach($this->request->query AS $key=>$value){
				$key = explode('|',$key);
				//pr($key);
				if (in_array($key[0],$disable_keys)){
					continue;
				}
				
				
				$key[0] = strtr($key[0],array('__'=>'.'));
				//pr($key);die();
				if (isset($key[0]) && $key[0] == 'Products.group_id'){
					$this->loadModel('MenuItems');
					$menu_ids = $this->MenuItems->menuList($value);
					$ids = [];
					foreach($menu_ids AS $mid=>$i){
						$ids[] = $mid;
					}
					//pr($ids);
					$this->loadModel('ProductConnects');
					$product_ids = $this->ProductConnects->find()
						->where(['product_group_id IN'=>$ids])
						->select([
							'id',
							'product_id',
						])
						->hydrate(false)
						->combine('id','product_id')
						->toArray();
					$conditions['Products.id IN'] = $product_ids;
					continue;
					//pr($conditions);
					//die();
					//pr($this->product_group_list);die();
				}
				
				if (isset($key[0]) && $key[0] == 'TrzbaItems.group_id'){
					$this->loadModel('ProductGroups');
					$this->product_group_parent = $this->ProductGroups->groupListParent();
					//$value = 7;
					//pr($this->product_group_parent);die();
					//pr($this->product_group_parent[$value]);
					$ids = [];
					if (isset($this->product_group_parent[$value])){
						$ids = $this->product_group_parent[$value];
					} else {
						$ids[] = $value;
					}
					
					//pr($ids);die();
					$this->loadModel('ProductConnects');
					$product_ids = $this->ProductConnects->find()
						->where(['product_group_id IN'=>$ids])
						->select([
							'id',
							'product_id',
						])
						->hydrate(false)
						->combine('id','product_id')
						->toArray();
					$conditions['TrzbaItems.product_id IN'] = $product_ids;
					//pr($conditions);die();
					continue;
					//pr($product_ids);
					//die('a');
				}
				//pr($key);die();
				
				// like conditions
				if (isset($key[1]) && $key[1] == 'like'){
					$conditions[$key[0].' '.$key[1]] = '%'.$value.'%';
			
				} else if (isset($key[0]) && $key[0] == 'id_tmp'){
					$conditions[strtr($key[0],['id_tmp'=>'id'])] = $value;
					//pr($conditions);
				} else if (isset($key[1]) && $key[1] == 'from'){
					$conditions[$key[0].' >= '] = $value;
					//pr($conditions);
				} else if (isset($key[1]) && $key[1] == 'date_from'){
					$value_tmp = explode('.',$value);
					$conditions['date('.$key[0].') >= '] = $value_tmp[2].'-'.$value_tmp[1].'-'.$value_tmp[0];;
					//pr($conditions);
				} else if (isset($key[1]) && $key[1] == 'to'){
					$conditions[$key[0].' <= '] = $value;
					//pr($conditions);
				} else if (isset($key[1]) && $key[1] == 'date_to'){
					$value_tmp = explode('.',$value);
					$conditions['date('.$key[0].') <= '] = $value_tmp[2].'-'.$value_tmp[1].'-'.$value_tmp[0];
					//pr($conditions);
				} else if (isset($key[1]) && $key[1] == 'date'){
					$value_tmp = explode('.',$value);
					$conditions[$key[0]] = $value_tmp[2].'-'.$value_tmp[1].'-'.$value_tmp[0];
					//pr($conditions);
				} else if (isset($key[1]) && $key[1] == 'mesto_auto'){
					$value_tmp = json_decode($value);
					//pr($value_tmp);
					$conditions[$key[0].' LIKE'] = '%'.$value_tmp->mesto.'%';
				} else {
				
					$conditions[$key[0]] = $value;
					
				}
			}
			//$conditions = ;
			
		}
		//pr($this->request->query);
		$conditions = $con+$conditions;
		//pr($conditions);
		//die();
		
		return $conditions;
	}
	
	public function check_error($entity=null){
		if ($entity != null && $entity->errors()){
			$message = '';
			$invalid_list = array();
			//pr($entity->errors());
			$convert_valid = [
				'date_'=>'date-',
			];
			
			foreach($entity->errors() AS $k=>$mm){
				foreach($mm AS $mk=>$m){
					if (is_array($m)){
						$invalid_list[] = $k.'-'.strtr($mk,$convert_valid);
						foreach($m AS $m2){
						$message .= $m2.'<br />';
						
						}
						
					} else {
						$message .= $m.'<br />';
						
					}
					
				}
				$invalid_list[] = strtr($k,$convert_valid);
			}
			die(json_encode(['r'=>false,'m'=>$message,'invalid'=>$invalid_list]));
		}
	}
	
	public function renderView($params){
		//pr($params);
		//$data = $this->paginate($params['data']);
		$data = $this->paginate($params['data']);
		$this->set('items', $data);
		if (isset($params['data2'])){
			$data2 = $this->paginate($params['data2']);
			$this->set('items2', $data2);
			
		}
		$this->set('params_viewIndex', $params);
		
		if ($this->request->is('ajax')) { 
			$this->viewBuilder()->layout(false);
			$this->render('../ViewIndex/items');
		} else {
			$this->render('../ViewIndex/index');
		}
	}
	
	
	
	
	
	private function load_js(){
		//pr($_SERVER);
		if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
			$pref = '';
		} else {
			$pref = '';
		}
		require_once($pref.'min/lib/Minify.php');
        require_once($pref.'min/lib/Minify/Source.php');
        require_once($pref.'min/utils.php');

        $jsUri = Minify_getUri('js'); // a key in groupsConfig.php
        $this->set('jsUri',$jsUri);		

        $cssUri = Minify_getUri('css'); // a key in groupsConfig.php
        $this->set('cssUri',$cssUri);		
    }
}
