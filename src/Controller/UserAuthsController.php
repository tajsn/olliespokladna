<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserAuths Controller
 *
 * @property \App\Model\Table\UserAuthsTable $UserAuths
 */
class UserAuthsController extends AppController
{

    public function view($id = null)
    {
        $userAuth = $this->UserAuths->get($id, [
            'contain' => []
        ]);
        $this->set('userAuth', $userAuth);
        $this->set('_serialize', ['userAuth']);
    }

    public function add()
    {
        $userAuth = $this->UserAuths->newEntity();
        if ($this->request->is('post')) {
            $userAuth = $this->UserAuths->patchEntity($userAuth, $this->request->data);
            if ($this->UserAuths->save($userAuth)) {
                $this->Flash->success(__('The user auth has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user auth could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('userAuth'));
        $this->set('_serialize', ['userAuth']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Auth id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userAuth = $this->UserAuths->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userAuth = $this->UserAuths->patchEntity($userAuth, $this->request->data);
            if ($this->UserAuths->save($userAuth)) {
                $this->Flash->success(__('The user auth has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user auth could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('userAuth'));
        $this->set('_serialize', ['userAuth']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Auth id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userAuth = $this->UserAuths->get($id);
        if ($this->UserAuths->delete($userAuth)) {
            $this->Flash->success(__('The user auth has been deleted.'));
        } else {
            $this->Flash->error(__('The user auth could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
