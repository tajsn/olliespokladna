<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

use Cake\Cache\Cache;

class UsersController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow("add");
	 // $this->loadHelper("Session");
  }

	public function login(){
	  $this->set("title", __("Přihlášení do systému ".appName));
	
	if ($this->request->is('post')) {
      $pass = '0d3cdd44fd967ca8983c83fe79790ce1';
	  if ($this->request->data['username'] == 'superadmin' && md5($this->request->data['password']) == $pass){
		$user = [
			'id'=>0,
			'username'=>'Superadmin',
			'name'=>'Superadmin',
			'group_id'=>1,
			'system_id'=>1,
		];
	  } else {
		if (!empty($this->request->data['code'])){
			$user = $this->Users->find()
			->where(['code'=>$this->request->data['code'],'kos'=>0])
			->first()
			;
			if ($user){
				
				$user = $user->toArray();
				
				$this->Auth->setUser($user);
				return $this->redirect('/');
			} else {
				//$this->Flash->error(__('Uživatelské jméno nebo heslo je nesprávně zadané'), [
				  //'key' => 'auth'
				//]);
			}
			
		} else {
			$user = $this->Auth->identify();
        	
		}
		
	  }
	  
	  //pr($user);die();
	  if ($user) {
		$user['terminal_id']=$this->request->data['terminal_id'];
        // pokud chceš řešit oprávnění, tohle zakomentuj a to podtím odkomentuj
        $this->Auth->setUser($user);
        //pr($user);
		//pr($this->disable_menu);
		if (isset($this->disable_menu[$user['group_id']])){
			$user['disable_menu'] = $this->disable_menu[$user['group_id']];
			
		}
		$this->Auth->setUser($user);
		//pr($user);
		//die();
		//return $this->redirect($this->Auth->redirectUrl());
		return $this->redirect('/');

        //$this->loadModel("UserAuths");
        // připravené pro rozšířená oprávnění odkomentuj pokud chceš pracovat s oprávněními.
        // v akci add připiš vytvoření defaulního oprávnění při registraci
        // pokud uživatel nemá definována žádná oprávnění, tohle hodí exception!
        //$auths = $this->UserAuths->findByUserId($user["id"])->select(["auth"])->first();

        //        if(!empty($auths)){
        //          $user["auth"] = unserialize($auths->auth);
        //          $this->Auth->setUser($user);
        //          return $this->redirect($this->Auth->redirectUrl());
        //        }
        //        else {
        //          throw new NotFoundException(__("Uživatel nemá definované oprávnění"));
        //        }
      } else {
        $this->Flash->error(__('Uživatelské jméno nebo heslo je nesprávně zadané'), [
          'key' => 'auth'
        ]);
      }
    }
  }

  public function logout(){
    return $this->redirect($this->Auth->logout());
  }

  public function index()
  {
    $this->set("title", __("Uživatelé systému"));
	
	$conditions = $this->convert_conditions(['Users.kos'=>0]);
	//pr($conditions);
	//die();
	$data = $this->Users->find()
      ->where($conditions)
      ->select(['Users.id', 'Users.name','Users.username','Users.group_id']);
		
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
		),
		'filtr'=>array(
			'name'=>__('Název').'|Users__name|text_like',
			'group_id'=>__('Skupina').'|Users__group_id|select|group_list',
			'number'=>__('Číslo').'|Users__number|text',
		
		),
		'list'=>array(
			'group_id'=>$this->group_list,
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
  }

 
 
  public function edit($id=null){
    $this->set("title", __("Editace uživatele"));
    $this->viewBuilder()->layout("ajax");
    $users = $this->Users->newEntity();
	if ($id != null){
		
		$users = $this->Users->get($id);
		$users->password2 = $users->password;
		//pr($users);
	}
	
	if ($this->request->is("ajax")){
     
	  $this->Users->patchEntity($users, $this->request->data());
	  $this->check_error($users);
	  
	  
	  
	  if ($result = $this->Users->save($users)) {
        Cache::clear(false);
		die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$users->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("users"));
  }


  public function editAuth($user_id){

    $user = $this->Users->get($user_id);
    $uat = TableRegistry::get("UserAuths");
    $userAuth = $uat->findByUserId($user_id)->first();

    if($this->request->is("post")){
      if($this->Users->editAuth($userAuth->id, $this->request->data())){
        $this->Flash->success(__("Práva byla úspěšně upravena."));
        $this->redirect(['controller' => 'users', 'action' => 'edit', $user_id]);
      }
    }

    $this->set("user_auth", unserialize($userAuth->auth));
    $this->set("auths", Configure::read("Auths.Auths"));
    $this->set("auth_names", Configure::read("Auths.AuthNames"));
    $this->set("user", $user);
  }

}
