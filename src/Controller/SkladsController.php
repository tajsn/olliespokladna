<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use Cake\Utility\Hash;

use Cake\Cache\Cache;

use App\View\Helper\FastestHelper;

class SkladsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index($type=null)
  {
		
	$this->set("title", __("Správa skladu"));
	
	
	$this->loadModel('SkladCiselniks');
	$this->loadModel('SkladItems');
	$this->set('sklad_items_list',$this->sklad_items_list = $this->SkladItems->skladItemsList());
	$this->set('dodavatel_list',$this->dodavatel_list = $this->SkladCiselniks->dodavatelList());
	$this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
	
	if (isset($this->request->query['type'])){
		$this->request->query['sklad_type_id'] = $this->request->query['type'];
		unset($this->request->query['type']);
	
		//$this->request->query['sklad_type_id'] = $this->request->query['sklad-type-id'];
		//$conditions['Sklads.sklad_type_id'] = $this->request->query['sklad_type_id'];
	}
	
	$conditions = $this->convert_conditions(['Sklads.kos'=>0]);
	
	
	
	
	$this->loadModel('Products');
	$this->set('product_list',$this->product_list = $this->Products->productList());
	
	
	$this->sort_list = [
		'id',
		'sklad_type_id',
		'sklad_item_id',
		'sum_value',
		'nakup_price',
		'nakup_price_total',
		'jednotka_id',
		'tax_id',
		'created',
		
	];
	
	$mapper = function ($d, $key, $mapReduce) {
		$old = clone $d;
		foreach($this->sort_list AS $s){
			unset($d->$s);
			$d->$s = $old->$s;
		}
		$mapReduce->emit($d);
	};
	
	
	$fields = [
		'Sklads.id', 
		'Sklads.sklad_type_id', 
		'Sklads.sklad_item_id', 
		'Sklads.sklad_item_product_id', 
		'Sklads.value', 
		'Sklads.nakup_price',
		'Sklads.nakup_price_total',
		'Sklads.jednotka_id',
		'Sklads.tax_id',
		'Sklads.faktura',
		'Sklads.created',
	];
	
	$data = $this->Sklads->find();
	
	switch($type){
		case 'group':
			unset($fields[3]);
			$select_fields = $fields+['sum_value' => $data->func()->sum('Sklads.value')];
			$data->group(['sklad_item_id','sklad_type_id']);
			$data->mapReduce($mapper);
		break;
		case 'faktura':
			unset($fields);
			$fields = [
				'Sklads.id',
				'Sklads.faktura',
				'Sklads.sklad_type_id',
				'Sklads.created',
			];
			$select_fields = $fields+['sum_value' => $data->func()->sum('Sklads.value')];
			
			$data->group(['faktura']);
			$this->is_faktura_list = true;
			//$data->mapReduce($mapper);
		break;
		default:
			$select_fields = $fields;
		
		break;
	}
	//pr($select_fields);
	//pr($conditions);
	$data
		//->select($fields+['sum_value' => $data->func()->sum('Sklads.value')])
		->select($select_fields)
		->where($conditions)
	//	->contain(['SkladItems'])
		->order('Sklads.id DESC')
		
	  	
	  ->toArray()
	  ;
	//  pr($select_fields);
	  //'sum_value' => $query->func()->count('value'),
	//pr($data->toArray());	
	//pr($data);	
	//die();
	//die();
	$params = array(
		'top_action'=>array(
			'edit?type_id=1'=>__('Příjem'),
			'edit?type_id=2'=>__('Převodka'),
			'edit?type_id=3'=>__('Odpis'),
			'index/group/|link'=>__('Seskupit'),
			'index/|link'=>__('Rozbalit'),
			'index/?type=1|link'=>__('Seznam příjemek'),
			'index/?type=2|link'=>__('Seznam převodek'),
			'index/?type=3|link'=>__('Seznam odpisů'),
			'index/?type=4|link'=>__('Seznam prodejů'),
			'index/faktura/|link'=>__('Seznam faktur'),
		),
		'filtr'=>array(
			//'sklad_type_id'=>__('Typ').'|Sklads__sklad_type_id|select|sklad_type_list',
			'sklad_item_id'=>__('Položka').'|Sklads__sklad_item_id|select|sklad_items_list',
			'created_date'=>__('Datum od').'|Sklads__created|date_from',
			'created_date2'=>__('Datum do').'|Sklads__created|date_to',
			
		),
		'list'=>array(
			'sklad_type_id'=>$this->sklad_type_list,
			'sklad_item_id'=>$this->sklad_items_list,
			'sklad_item_product_id'=>$this->product_list,
			'jednotka_id'=>$this->sklad_jednotka_list,
			'tax_id'=>$this->price_tax_list,
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			//'clone_el'=>__('Kopírovat'),
			//'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	if (isset($this->is_faktura_list)){
		$params['posibility'] = [
			'edit/faktura'=>'Editovat'
		];
	}
	
	$this->renderView($params);
	
  }

  
  public function edit($id=null,$id_faktura=null){
    $this->viewBuilder()->layout("ajax");
	$this->set('scripts',['/js/Searchable/MassSelect.js','/js/Searchable/selectize.js']);  
	$this->loadModel('SkladCiselniks');
	$this->set('dodavatel_list',$this->dodavatel_list = $this->SkladCiselniks->dodavatelList());
	$this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
	
	
	$this->loadModel('Products');
	$this->set('product_list',$this->product_list = $this->Products->productList());
	
	$this->loadModel('SkladItems');
	$this->loadModel('SkladItemGlobals');
	$this->set('sklad_items_list',$this->sklad_items_list = $this->SkladItemGlobals->skladItemsList());
	$this->set('sklad_items',$this->sklad_items = $this->SkladItems->skladItems());
	
    $sklads = $this->Sklads->newEntity();
	
	if (isset($this->request->query['type-id'])){
		switch($this->request->query['type-id']){
			case 1: $title = 'Příjem do skladu'; break;
			case 2: $title = 'Výdej ze skladu'; break;
			case 3: $title = 'Odpis ze skladu'; break;
		}
		
	} else {
		$title = 'Editace skladu';
	}
	
	$this->set("title", $title);
    
	if ($id != null){
		if ($id == 'faktura'){
			$conditions = [
				'Sklads.id'=>$id_faktura
			];
				
		} else {
			$conditions = ['Sklads.id'=>$id];
		}
	
		$sklads = $this->Sklads->find()
		  ->where($conditions)
		  ->first();
		
		if ($id == 'faktura'){
		
			$sklads_list_load = $this->Sklads->find()
				->hydrate(false)
				->where(['Sklads.faktura'=>$sklads->faktura])
				->toArray();
			//pr($sklads_list_load);
			$this->set('sklads_list',$sklads_list_load);
			//pr($sklads_list);
		}
		//pr($sklads);
	}
	
	
	if ($this->request->is("ajax")){
		if (isset($this->request->data['save_data']) && empty($this->request->data['save_data'])){
			die(json_encode(['r'=>false,'m'=>'Musíte přidat produkt nebo položku']));
		}
		
		
		if (isset($this->request->data['save_data'])){
			$save_data = json_decode($this->request->data['save_data'],true);
			//pr($save_data);
			$this->checkConstant($save_data);
			foreach($save_data AS $save){
				$save_entity = $this->Sklads->newEntity($save);
				//pr($save_entity);die();
				$this->Sklads->save($save_entity);
			}
			die(json_encode(['r'=>true,'m'=>__('Uloženo')]));
			
			//pr($save_data);die();
			
		} else {
		
		//pr($this->request->data);die();
    
		$sklads = $this->Sklads->patchEntity($sklads, $this->request->data());
		$this->check_error($sklads);
	  
		if ($result = $this->Sklads->save($sklads)) {
			
			//pr($this->request->data['product_connects'])
			//pr($result->id);
			Cache::clear(false);
			die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$sklads->id]));
			} else {
			die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
		}
		
		}
		
    }

    $this->set(compact("sklads"));
  }
  
  public function checkConstant($data){
	  //pr($data);die();
	  $ids = [];
	  foreach($data AS $d){
	  //pr($d);
	  $ids[] = $d['sklad_item_id']; 
	  }
	  //pr($ids);
	  $this->loadModel('SkladItems');
	  $conditions = ['id IN'=>$ids,'jednotka_prevod IS'=>null,'jednotka_id_dodavatel IS NOT'=>null,];
	  $skladItemsList = $this->SkladItems->find()
		->where($conditions)
		->toArray();
		//pr($skladItemsList);
	  //die();
	  if ($skladItemsList){
		  $message = "Nejsou napárovány: <br />";
		  foreach($skladItemsList AS $s){
			  $message .= $s->name.'<br />';
		  }
		  die(json_encode(['r'=>false,'list'=>$skladItemsList,'m'=>$message]));
	  }
	  //pr($skladItemsList);
	  //die();
  }
  
  public function getSkladItems($group_id){
	$this->loadModel('SkladItems');
	$this->loadModel('SkladItemGlobals');
	$items = $this->SkladItemGlobals->skladItemsListFromGlobal($group_id);
	die(json_encode(['data'=>$items]));
  }
  
  
  function dodavatelAdd($value){
		$save = [
			'name'=>$value,
			'type'=>1,
		];
		$this->loadModel('SkladCiselniks');
		$save_entity = $this->SkladCiselniks->newEntity($save);
		$result = $this->SkladCiselniks->save($save_entity);
		
		die(json_encode(['r'=>true,'id'=>$result->id,'name'=>$result->name]));
  }
  
  function skladGroupAdd($value){
		$save = [
			'name'=>$value,
			'type'=>2,
		];
		$this->loadModel('SkladCiselniks');
		$save_entity = $this->SkladCiselniks->newEntity($save);
		$result = $this->SkladCiselniks->save($save_entity);
		
		die(json_encode(['r'=>true,'id'=>$result->id,'name'=>$result->name]));
  }


  
  public function indexStavs($type=null){
		
	$this->set("title", __("Zrcadlo skladu"));
	
	
	$this->loadModel('SkladStavs');
	
	$conditions = $this->convert_conditions(['SkladStavs.kos'=>0]);
	
	
	
	
	$mapper = function ($d, $key, $mapReduce) {
		$old = clone $d;
		foreach($this->sort_list AS $s){
			unset($d->$s);
			$d->$s = $old->$s;
		}
		$mapReduce->emit($d);
	};
	
	
	$fields = [
		'SkladStavs.id', 
		'SkladStavs.created',
		'SkladStavs.from_sklad_id',
		'SkladStavs.to_sklad_id',
	];
	
	$data = $this->SkladStavs->find();
	$data
		->select($fields)
		->where($conditions)
	//	->contain(['SkladItems'])
		->order('SkladStavs.id DESC')
		
	  	
	  ->toArray()
	  ;
	$params = array(
		'top_action'=>array(
			'edit_stav'=>__('Nové zrcadlo'),
		),
		'filtr'=>array(
			//'sklad_type_id'=>__('Typ').'|Sklads__sklad_type_id|select|sklad_type_list',
			//'sklad_item_id'=>__('Položka').'|Sklads__sklad_item_id|select|sklad_items_list',
			'created_date'=>__('Datum od').'|Sklads__created|date_from',
			'created_date2'=>__('Datum do').'|Sklads__created|date_to',
			
		),
		'list'=>array(
			'tax_id'=>$this->price_tax_list,
		),
		'posibility'=>array(
			'edit_stav'=>__('Editovat'),
			'show_stav'=>__('Zobrazit'),
			//'clone_el'=>__('Kopírovat'),
			//'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	if (isset($this->is_faktura_list)){
		$params['posibility'] = [
			'edit/faktura'=>'Editovat'
		];
	}
	
	$this->renderView($params);
	
  }
  
   public function showStav($id=null,$render=false){
    $this->viewBuilder()->layout("ajax");
	
	$this->loadModel('SkladStavs');
	$this->loadModel('Sklads');
	$this->loadModel('SkladItems');
	
	$sklad_items_list = $this->SkladItems->skladItemsList();
	$last_sklad_item_id = $this->SkladItems->findLastIdSkladItems();
	$last_sklad_id = $this->SkladStavs->findLastId();
	//pr($last_sklad_id);
    $sklad_values = $this->Sklads->getSkladValues($last_sklad_id);
	if ($sklad_values)
	$this->set('sklad_values',$sklad_values);
	//pr($sklad_items_list);
	//pr($sklad_values);
	
	$skladStavs = $this->SkladStavs->newEntity();
	$skladStavs->from_sklad_id = $last_sklad_id;
	$skladStavs->to_sklad_id = $last_sklad_item_id;
	//pr($skladStavs);
	
	$this->set("sklad_items_list", $sklad_items_list);
	
	if ($id != null){
		$conditions = ['SkladStavs.id'=>$id];
		$mapper = function ($d, $key, $mapReduce) {
			foreach($d->sklad_stav_items AS $k=>$si){
				$d->sklad_stav_items[$si->sklad_item_id] = $si;
				unset($d->sklad_stav_items[$k]);
			}
			$mapReduce->emit($d);
		};
		$skladStavs = $this->SkladStavs->find()
			->contain(['SkladStavItems'])
			->where($conditions)
			->mapReduce($mapper)
			->first();
		
	}
	$this->set('title','Zobrazit stav zrcadla');
	$this->set(compact("skladStavs"));
	if ($render){
		$view = new View($this->request);
		$view->autoLayout(false);
		$view->set('skladStavs',$skladStavs);
		$view->set('sklad_items_list',$sklad_items_list);
		$html = $view->render('Sklads/show_stav');
		return $html;
	}
   }
  
  public function editStav($id=null){
    $this->viewBuilder()->layout("ajax");
	
	$this->loadModel('SkladStavs');
	$this->loadModel('Sklads');
	$this->loadModel('SkladItems');
	
	$sklad_items_list = $this->SkladItems->skladItemsList();
	$last_sklad_item_id = $this->SkladItems->findLastIdSkladItems();
	$last_sklad_id = $this->SkladStavs->findLastId();
	//pr($last_sklad_id);
    $sklad_values = $this->Sklads->getSkladValues($last_sklad_id);
	if ($sklad_values)
	$this->set('sklad_values',$sklad_values);
	//pr($sklad_items_list);
	//pr($sklad_values);
	
	$skladStavs = $this->SkladStavs->newEntity();
	$skladStavs->from_sklad_id = $last_sklad_id;
	$skladStavs->to_sklad_id = $last_sklad_item_id;
	//pr($skladStavs);
	
	
	if ($id == null)
		$title = 'Nové zrcadlo skladu';
	else
		$title = 'Editace zrcadla skladu';
	$this->set("sklad_items_list", $sklad_items_list);
	$this->set("title", $title);
    
	if ($id != null){
		$conditions = ['SkladStavs.id'=>$id];
		
		$mapper = function ($d, $key, $mapReduce) {
			foreach($d->sklad_stav_items AS $k=>$si){
				$d->sklad_stav_items[$si->sklad_item_id] = $si;
				unset($d->sklad_stav_items[$k]);
			}
			$mapReduce->emit($d);
		};
	
		$skladStavs = $this->SkladStavs->find()
			->contain(['SkladStavItems'])
			->where($conditions)
			->mapReduce($mapper)
			->first();
		
	}
	
	
	if ($this->request->is("ajax")){
		foreach($this->request->data['sklad_stav_items'] AS $d){
			if (isset($d['stav_real']) && $d['stav_real'] == ''){
				die(json_encode(['r'=>false,'m'=>'Musíte vyplnit všechny reálné stavy']));
			}
			//
		}
		
		
		$skladStavs = $this->SkladStavs->patchEntity($skladStavs, $this->request->data(),['associated' => ['SkladStavItems']]);
		//pr($skladStavs);die();
		$this->check_error($skladStavs);
	  
		if ($result = $this->SkladStavs->save($skladStavs)) {
			
			//pr($this->request->data['product_connects'])
			//pr($result);
			foreach($result->sklad_stav_items AS $save){
				$skladItemData = $this->SkladItems->skladItemData($save->sklad_item_id);
				//pr($skladItemData);
				$save_data = [
					'sklad_item_id'=>$save->sklad_item_id,
					'sklad_type_id'=>(($save->stav_count > 9)?5:6),
					'value'=>(($save->stav_count < 0)?$save->stav_count*-1:$save->stav_count),
					'jednotka_id'=>$skladItemData->jednotka_id,
					'tax_id'=>$skladItemData->tax_id,
				];
				//pr($save_data);die();
				$save_entity = $this->Sklads->newEntity($save_data);
				//pr($save_entity);die();
				$this->Sklads->save($save_entity);
			}
			$this->sendStavEmail($result->id,true);
			Cache::clear(false);
			die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$skladStavs->id]));
		} else {
			die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
		}
		
		
		
  }

    $this->set(compact("skladStavs"));
  }
  
	public function sendStavEmail($id,$inFce=false){
		if (!$inFce)
		$this->autoRender = false;
		$html = $this->showStav($id,true);
		//die();
		
		$this->loadComponent('Email');
		
		$options = [
			'template'=>'stav_skladu',
			'subject'=>'Stav skladu  '.date('d.m.y H:i:s'),
			'data'=>$html,
		];
		//die('a');	
		$this->Email->send($this->email_list,$options);
		if ($inFce){
			return true;
		}
	}
 

	public function loadMakro(){
		//$jsonFile = "HDR;347010932;24.10.2017;MAKRO OSTRAVA;MAKRO OSTRAVA;Místecká 280;720 00;OSTRAVA - HRABOVÁ;CZK;CZ26450691;CZ26450691;CZ26450691;2048930118/2600;2600;0308;3700329;28596030;CZ28596030;OLLIES DORTY S.R.O.;VÝSTAVNÍ 2968/108;703 00;OSTRAVA-VÍTKOVICE;INVOICE.TOT_INVOICE;0\r\nLIN;1;4333465030869;1;PC;2;346;0;346;51.9;51.9;397.9;15;HS CRÉME FRAICHE  38%    1KG;138064;173;2;4018905045725|4018905342930|4333465030869;;173;173;173\r\nLIN;2;8000430132564;3;BG;5;262.5;10.5;273;39.38;40.95;313.95;15;GALBANI MOZZARELLA 125G 3x      $;213413;18.2;15;8000430132564;A;145.6;140;18.2\r\nLIN;3;4002911122556;1;PC;1;339;-79;260;71.19;54.6;314.6;21;LATEX.RUK. MODRÉ 100KS \"\"\"M\"\"\"  $;209435;260;1;4002911122556;A;2.6;3.39;260\r\nLIN;4;8593807547103;1;BX;2;18;0;18;2.7;2.7;20.7;15;BIO-VIA NATUR BÍLÝ JOGURT  150G &;174401;9;2;8593807547103|8593807549107;;60;60;9\r\nLIN;5;5410126256109;4;BX;20;2580;0;2580;387;387;2967;15;LOTUS KARAMEL.SUŠ.250G 4x       $;130616;32.25;80;5410126256109;;129;129;32.25\r\nLIN;6;8594005012004;1;BT;10;1790;97.6;1887.6;375.9;396.4;2284;21;BOŽKOV ORIGINAL 37,5%1L         @;244569;188.76;10;8594005012004;A;188.76;179;188.76\r\nLIN;7;8595019690059;1;BX;6;2478;0;2478;371.7;371.7;2849.7;15;ČOKOLÁDOVÁ POLEVA 5KG           $;108914;413;6;8595019690059;;82.6;82.6;413\r\nLIN;8;4002911122570;1;PC;1;339;-79;260;71.19;54.6;314.6;21;LATEX.RUK. MODRÉ 100KS \"\"\"L\"\"\"  $;209436;260;1;4002911122570;A;2.6;3.39;260\r\nLIN;9;8584002002771;1;PC;5;545;0;545;81.75;81.75;626.75;15;DR.OETKER PUDING VANILKA 1KG    &;222573;109;5;8584002002771|8595072600576;;109;109;109\r\nLIN;10;8595072602174;1;PC;5;384.5;-83.4;301.1;57.68;45.17;346.27;15;DR.OETKER KYPŘÍCÍ PRÁŠEK 1KG    &;112320;60.22;5;8595072602174;A;60.22;76.9;60.22\r\nLIN;11;8595072603935;25;BX;10;2190;0;2190;328.5;328.5;2518.5;15;DR.OETKER DROŽDÍ 7G 25x         &;53499;8.76;250;8595072603935;;1251.43;1251.43;8.76\r\nLIN;12;8410522001942;1;BT;10;779;0;779;116.85;116.85;895.85;15;SP OLIVY 935G ČERNÉ KRÁJENÉ     &;184899;77.9;10;8410522001942;;83.32;83.32;77.9\r\nLIN;13;8026924001994;1;PC;6;672;-111;561;100.8;84.15;645.15;15;FL SUŠENÁ RAJČATA 660G;114986;93.5;6;8026924001994;A;141.67;169.7;93.5\r\nLIN;14;8594003670374;1;PC;10;519;-52;467;77.85;70.05;537.05;15;ARO MOUKA PŠENIČNÁ POLOHRUBÁ 5KG;213746;46.7;10;8594003670374;A;9.34;10.38;46.7\r\nLIN;15;8594003670480;1;PC;10;505;0;505;75.75;75.75;580.75;15;ARO MOUKA PŠENIČNÁ HLADKÁ 5KG;213759;50.5;10;8594003670480;;10.1;10.1;50.5\r\nLIN;16;8717755290045;1;BG;2;130;5.2;135.2;19.5;20.28;155.48;15;MRKEV KAROTKA SÍŤ 10KG;119374;67.6;2;8717755290045;A;6.76;6.5;67.6\r\nLIN;17;8436533230508;1;CA;1;179.5;7.18;186.68;26.93;28;214.68;15;PAPRIKA ČERVENÁ  80+I.KAR.5KG@;133625;186.68;1;8592373001484;A;37.34;35.9;186.68\r\nLIN;18;8595160101763;1;SW;1;19.9;.8;20.7;2.99;3.11;23.81;15;HS PETRŽEL  HLADKOL.VAN. 100G;195245;20.7;1;8595160101763;;207;199;20.7\r\nLIN;19;8595081585154;1;BG;2;3399;135.96;3534.96;509.85;530.24;4065.2;15;VLAŠSKÁ JÁDRA 5KG S;226543;1767.48;2;8595081585154;;353.5;339.9;1767.48\r\nLIN;20;8595160101756;1;PC;2;99.81;3.99;103.8;14.97;15.57;119.37;15;HS BAZALKA PRAVÁ VANIČKA 100G;672014;51.9;2;8595160101756;;519;499.05;51.9\r\nLIN;21;8011631006721;1;CA;1;199.9;8;207.9;29.99;31.19;239.09;15;ŠPENÁT BABY   NEPRANÝ   1 KG;248305;207.9;1;8011631006721;;207.9;199.9;207.9\r\nLIN;22;8595562908120;12;SW;2;189;0;189;39.69;39.69;228.69;21;ARO TP ECONOMIC 2VRS 68m 12ks;258428;7.88;24;8595562907192;;7.88;7.88;7.88\r\nLIN;23;8594156971809;1;CA;5;1228.51;49.14;1277.65;184.28;191.65;1469.3;15;JABLKA ŠAMPION 65+I.KART13KGS;263657;255.53;5;8594156971809|08594156971809;;19.66;18.9;255.53\r\nLIN;24;8718963000501;1;PC;2;91.81;3.67;95.48;13.77;14.32;109.8;15;HRÁŠEK PLOCH.LUSK.EAT ME 150G;316738;47.74;2;8718963000501;;318.27;306.03;47.74\r\nLIN;25;4337182010203;15;BT;3;1197;0;1197;179.55;179.55;1376.55;15;ARO SLUNEČNICOVÝ OLEJ 15X1L;317168;26.6;45;4337182122548;;26.6;26.6;26.6\r\nLIN;26;8718963089681;1;BG;15;598.56;23.94;622.5;89.78;93.38;715.88;15;BORŮVKY KANADSKÉ EAT ME 125G;320482;41.5;15;8718963089681|8592373044764;;332;319.23;41.5\r\nLIN;27;8595562910147;1;SW;10;1660;0;1660;348.6;348.6;2008.6;21;POTRAVINOVÁ FÓLIE 45cm x 300m;322371;166;10;8595562910147;;166;166;166\r\nLIN;28;8595121822454;12;PC;12;2268;0;2268;340.2;340.2;2608.2;15;ARO MLÉKO TRV.3,5% 1L 12x;322572;15.75;144;8595121822454;;15.75;15.75;15.75\r\nLIN;29;9001466221962;1;SW;4;740;0;740;111;111;851;15;GOLD PACK TŘTIN.CUKR DARK 4KG;336613;185;4;9001466221962;;46.25;46.25;185\r\nLIN;30;8594054331736;1;PC;3;59.71;2.39;62.1;8.96;9.32;71.42;15;HS SALÁT ŘÍMSKÝ 180+ I. KS;368315;20.7;3;8594054331736;;103.5;99.52;20.7\r\nLIN;31;8594023945018;1;BX;3;1137.4;45.5;1182.9;170.61;177.44;1360.34;15;BANÁNY 18+ KARTON 18.14KG @;197256;394.3;3;8594023945018;;21.74;20.9;394.3\r\nLIN;32;8595081571089;1;CA;10;559.04;22.36;581.4;83.86;87.21;668.61;15;ARO ROZINKY SUŠENÉ 1KG S        @;242615;58.14;10;8595081571089;;58.14;55.9;58.14\r\nLIN;33;;12;PC;5;1295;0;1295;194.25;194.25;1489.25;15;ZLATÉ VĚNEČKY 150G KAKAOVÉ 12x  $;373789;21.58;60;8593893902169;A;143.89;143.89;21.58\r\nVAT;15;3683.16;24554.37\r\nVAT;21;893.89;4256.6\r\nPAY;9;33388.02\r\n";
		/**/
		//pr($this->request->data['jsonData']);
		
		if (!isset($this->request->data['jsonData'])){
			die(json_encode(['r'=>false,'m'=>'Neni definovan soubor']));
			
		}
		$jsonFile = json_decode($this->request->data['jsonData']);
		if (empty($jsonFile)){
			die(json_encode(['r'=>false,'m'=>'Chyba parsovani souboru']));
		}
		
		//$jsonFile = $this->request->data['jsonData'];
		//$jsonFile = iconv('UTF-8', 'windows-1250', $jsonFile);
		//pr(mb_detect_encoding($jsonFile));
		//pr($jsonFile);die();
		$lines = explode("\n",$jsonFile);
		//pr($lines);die();
		//pr($jsonFile);
		/*
		$csv = array();
		$file = './uploaded/makro/makro.csv';
		
		$lines = file($file, FILE_IGNORE_NEW_LINES);
		*/
		//pr($lines);
		//die('a');
		//unset($lines[0]);
		//pr($lines);
		/*
		1 Pořadové číslo řádku faktury
		2 EAN kód artiklu
		3 Počet jednotek v MAKRO unit
		4 Typ balení
		5 Množství
		6 XXX / irelevantní
		7 Sleva
		8 Hodnota bez DPH po slevě
		9 DPH
		10 DPH po slevě
		11 Hodnota s DPH po slevě

		12 Sazba DPH
		13 Popis artiklu
		14 Artiklové číslo
		15 Cena za jednotku
		16 Počet jednotek
		17 EAN kusu (může obsahovat 0-5 kódu oddělených „|“ – podle dostupných dat k danému artiklu. Ne všechny uvedené kusové EANy musí být nutně obsaženy v dodaném balení)
		18 Makro Mail (pokud obsahuje “A” – zboží je aktuálně v akční nabídce)
		19 Jedn. cena po sleve bez DPH – (nová informace - ks, litr, kg)
		20 Jedn. cena pred slevou bez DPH – (nová informace - ks, litr, kg – jako u regálových štítků)
		21 Cena za jedn. po sleve bez DPH – (doposud byla Cena za jednotku (č.15) před slevou)

		*/
		$result = [];
		$this->loadModel('SkladItems');
		foreach ($lines as $key => $value){
			
			$row_data = str_getcsv($value,';');
			//pr($row_data);
				if ($key == 0){
					$faktura = $row_data[1];
				
					continue;
				}
				//pr($row_data);
				//pr($row_data[12]);
				if (isset($row_data[12])){
				//pr($row_data[8]*(float)('1.'.$row_data[12]));die();
				if ($row_data[2] > 0){
				$skladItem = $this->SkladItems->checkMakroSkladItem($row_data,$this->price_tax_list_makro);
				
				$this->loadModel('SkladJednotkas');
				$jednotka_list = $this->SkladJednotkas->getSkladJednotkas();
				
				if (!isset($jednotka_list[$row_data[4]])){
					die(json_encode(['r'=>false,'m'=>'Neni definovana jednotka '.$row_data[4]]));
				}
				
				
				$result[] = [
					'ean'=>$row_data[2],
					'faktura'=>$faktura,
					'jednotka_id'=>$jednotka_list[$row_data[4]]['jednotka_id'],
					'jednotka_id_dodavatel'=>$row_data[4],
					'nakup_price'=>$row_data[15],
					'nakup_price_vat'=>$row_data[15]*(float)('1.'.$row_data[12]),
					'nakup_price_total'=>$row_data[8],
					//'nakup_price_total_vat'=>$row_data[8]*(float)('1.'.$row_data[12])*$row_data[16],
					'nakup_price_total_vat'=>$row_data[11],
					'note'=>'',
					'sklad_item_id'=>$skladItem->id,
					'sklad_type_id'=>7,
					'tax_id'=>$this->price_tax_list_makro[$row_data[12]],
					'value'=>$row_data[16]*$jednotka_list[$row_data[4]]['prevod'],
				];
				//pr($result);
				}
				//pr($result);
				//die();
				}
			
			
		}
		//pr($result);die();
		$sitem = $this->SkladItems->skladItems();
		die(json_encode(['r'=>true,'data'=>$result,'sklad_items_list'=>$sitem]));
	
	}
	
	public function loadBidFood(){
		header('Content-Type: text/html; charset=utf-8');
		
		$jsonFile = "H;755961640;691877;03/10-17;02/11-17;03/10-17;0.00;71213.40;0.00;81895.40
R;;03/10-17;;691877;701315;MÁSLO TOURAGE PRÉSID.84% 1*2KG;3428200307090;40.00;kg;7600.00;15;190.00;190.00
R;;03/10-17;;691877;701410;RICOTTA 1,5KG FRESCA BIRAGHI;;6.00;kg;456.00;15;76.00;76.00
R;;03/10-17;;691877;702580;ZAKYSANÁ SMETANA 15% *5KG* <M>;;100.00;kg;4190.00;15;41.90;41.90
R;;03/10-17;;691877;702623;EXQUISA SOFT CHEESE  1,5KG;4019300152179;15.00;kg;1725.00;15;115.00;115.00
R;;03/10-17;;691877;702626;EXQUISA SOFT CHEESE 10 KG;;100.00;kg;9000.00;15;90.00;90.00
R;;03/10-17;;691877;703041;MÁSLO 25 KG BLOK;;225.00;kg;42750.00;15;190.00;190.00
R;;03/10-17;;691877;600225;MERUŇKY PŮLENÉ CORONET P3/1;8594044442381;24.00;ks;1896.00;15;79.00;79.00
R;;03/10-17;;691877;600270;HRUŠKY PŮLENÉ LOUP.CORON.P3/1;8594007910704;36.00;ks;3596.40;15;99.90;99.90";
		
		//pr($this->request->data['jsonData']);
		//$jsonFile = json_decode($this->request->data['jsonData']);
		//$jsonFile = $this->request->data['jsonData'];
		//$jsonFile = iconv('UTF-8', 'windows-1250', $jsonFile);
		//pr(mb_detect_encoding($jsonFile));
		//pr($jsonFile);die();
		$lines = explode("\n",$jsonFile);
		//pr($lines);die();
		//pr($jsonFile);
		/*
		$csv = array();
		$file = './uploaded/makro/makro.csv';
		
		$lines = file($file, FILE_IGNORE_NEW_LINES);
		*/
		//pr($lines);
		//die('a');
		//unset($lines[0]);
		//pr($lines);
		/* 
		4 - faktura
		5 - bidfood_id
		11 - DPH
		*/
		
		$result = [];
		$this->loadModel('SkladItems');
		foreach ($lines as $key => $value){
			
			$row_data = str_getcsv($value,';');
			//pr($row_data);
				if ($key == 0){
					$faktura = $row_data[1];
				
					continue;
				}
				//pr($row_data);die();
				//pr($row_data[12]);
				if (isset($row_data[12])){
				//pr($row_data[8]*(float)('1.'.$row_data[12]));die();
				if ($row_data[2] > 0){
				$skladItem = $this->SkladItems->checkBidFoodSkladItem($row_data,$this->price_tax_list_makro);
				
	
				$this->loadModel('SkladJednotkas');
				$jednotka_list = $this->SkladJednotkas->getSkladJednotkas();
				//pr($jednotka_list);
				if (!isset($jednotka_list[$row_data[9]])){
					die(json_encode(['r'=>false,'m'=>'Neni definovana jednotka '.$row_data[9]]));
				}
				
				$result[] = [
					'ean'=>$row_data[2],
					'faktura'=>$faktura,
					'jednotka_id'=>$skladItem->jednotka_id,
					'nakup_price'=>$row_data[12],
					'nakup_price_vat'=>$row_data[12]*(float)('1.'.$row_data[11]),
					'nakup_price_total'=>$row_data[12]*$row_data[8],
					//'nakup_price_total_vat'=>$row_data[8]*(float)('1.'.$row_data[12])*$row_data[16],
					'nakup_price_total_vat'=>$row_data[12]*$row_data[8]*(float)('1.'.$row_data[11]),
					'note'=>'',
					'sklad_item_id'=>$skladItem->id,
					'sklad_type_id'=>8,
					'tax_id'=>$this->price_tax_list_makro[$row_data[11]],
					'value'=>$row_data[8],
				];
				}
				//pr($result);die();
				}
			
			
		}
		//pr($result);die();
		$sitem = $this->SkladItems->skladItems();
		die(json_encode(['r'=>true,'data'=>$result,'sklad_items_list'=>$sitem]));
	
	}
}
