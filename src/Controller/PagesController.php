<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    public function homepage(){
		$this->set('scripts',[
			'/js/fst_pokladna/fst_modal.js',
			//'/js/fst_pokladna/fst_pokladna.js',
		]);
        $this->set('title',appName);
    }
	
	public function googleAreas(){
        $this->set('title',__('Oblasti'));
        $this->set('scripts',['/js/maparea/maparea']);
		
		$this->loadModel('Mapareas');
		$load = $this->Mapareas->find()
		->where(['id'=>$this->system_id])
		->select([
			'coords',
		])
		->first();
		//pr($load);
		//die();
		$this->set('load',$load);
			
    }
	function saveGoogleAreas(){
		$this->loadModel('Mapareas');
		$save_data = $this->Mapareas->newEntity([
			'id'=>$this->system_id,
			'coords'=>$this->request->data['area_coords'],
		
		]);
				
		//pr($save_data);die();
		$this->Mapareas->save($save_data);
		die(json_encode(array('result'=>true,'message'=>'Všechny oblasti byly uloženy')));
	}
	
    public function closeModal(){
       $this->set('title',__('Pospiech Transport'));
    }
	
	public function printPage(){
		require '../vendor/EscPos/autoload.php';
		//$connector = new \FilePrintConnector("php://stdout");
		die(json_encode(['r'=>true]));
	}
	
	public function ares($ic){
		//$ic = '28591232';
		$this->loadComponent('Ares');
		$result = $this->Ares->search($ic);
		//pr($result);
		die(json_encode($result));
	}
	
	// trash function 
	public function trash($model,$id){
		$this->loadModel($model);
		
		if (!$this->$model->query()
        ->update()
        ->set(['kos' => 1])
        ->where(['id' => $id])
        ->execute()
		){
			die(json_encode(array('result'=>false,'message'=>__('Chyba smazání'))));
		
		}
		
		die(json_encode(array('result'=>true,'message'=>__('Položka byla smazána'))));
	}
	
	public function savePushHash($hash){
		$this->loadModel('PushMessages');
		$find = $this->PushMessages->find()
		->where(['hash'=>$hash,'user_id'=>$this->loggedUser['id']])
		->select([
			'id',
		])
		->first()
		;
		//pr($this->request);
		// pokud jiz existuje neukladej
		if (isset($find)){
			die(json_encode(['r'=>false]));
		}
		//pr($find);
		
		$save = $this->PushMessages->newEntity([
			'name'=>$_SERVER['REMOTE_ADDR'],
			'user_id'=>$this->loggedUser['id'],
			'hash'=>$hash,
			'p256dh'=>$this->request->data['p256dh'],
			'auth'=>$this->request->data['auth'],
		]);
		//pr($save->toArray());
		$this->PushMessages->save($save);
		
		die(json_encode(['r'=>true]));
	}
	
	
	public function sendPushMessage($user_id=null){
		
        $this->loadComponent('PushMessage');
		$this->PushMessage->send_message($user_id);
		
		/*
			//print_r($ids);die();
			$registrationIds = $data;
			//print_r($registrationIds);die();
			
			// prep the bundle
			$msg = array
			(
				'message' 	=> 'nova zprava z api push',
				'title'		=> 'Pospiech Transport',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'icon'	=> 'http://www.fastest.cz/css/fastest/layout/logo.png',
				 
			);
			$fields = array
			(
				'registration_ids' 	=> $registrationIds,
				'data'			=> $msg,
				
			);
			//pr($fields);die();
			//print_r(json_encode( $fields ));
			$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
			 
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			//curl_setopt( $ch,CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			//echo $result;
		//}
		header('Content-Type: application/json');
		die(json_encode(['notification'=>$msg]));
		*/
	}
	
	// load kurz
	function loadKurz(){
		$long = false;
		$mena = 'EUR';
		$pocetKorun = 1;
		$file = "http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt";
		
		$kurzy = file($file);
		foreach ($kurzy as $v) {
		  $h = explode("|", $v);
		  if (isset($h[4])) $h[4] = str_replace(",", ".", $h[4]); // prevod desetinne carky na tecku
		  if ((count($h) >= 5) && ($h[3] == $mena)) {
			if ($long) $kurz =  $pocetKorun." CZK = ".($pocetKorun / (float)$h[4] * $h[2])." $mena";
			else 
				$kurz = (float)$h[4] * $h[2];
			//pr((float)$h[4] * $h[2]);
		  }
		}
		pr($kurz);
		$save = "<?php \$kurz='$kurz' ?>";
		if (isset($kurz) && !empty($kurz))
			file_put_contents("../config/kurz.php", $save);
		die();
	}
	
	public function sendError(){
		$this->loadComponent('Email');
		if($this->request->is("post")){
			//pr($this->request);	
			$options = [
				'template'=>'welcome',
				'subject'=>$this->request->data['SendErrorMessage'].' - '.$this->loggedUser['name'],
				'data'=>$this->request->data,
			];
			
			$this->Email->send('test@fastest.cz',$options);
		}
		die(json_encode(['r'=>true,'message'=>'Chyba odeslána na podporu']));
	}
	
	public function testPush(){
		 $this->set('title',__('Pospiech Transport'));
		 $this->set('scripts',[
			'//messaging-public.realtime.co/js/2.1.0/ortc.js',
			'/js/push/manager.js',
			'/js/push/notify.js',
		 ]);
		
	}
	
	
	public function testMap($city){
		//$url = "https://maps.google.com/maps/api/geocode/json?sensor=false&key=".apiKey."&address=";
		//$url = $url.urlencode($address);
		
		//pr($url);die();
        //$resp_json = $this->curl_file_get_contents($url);
        //$results = json_decode($resp_json, true);
		//pr($results);
		
		$data = file_get_contents('https://api.teleport.org/api/cities/?search='.$city);
		pr(json_decode($data));
		
		//3067696
		if (isset($city_id)){
			$url = 'https://api.teleport.org/api/cities/geonameid:'.$city_id;
			$data = file_get_contents($url);
			pr(json_decode($data));
		}
		
		die();
	}
	
	public function vatNumber($dic){
		
		$country = substr($dic,0,2);
		$vat = substr($dic,2,20);
		
		$client = new \SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");
		$data = ($client->checkVat(array(
		  'countryCode' => $country,
		  'vatNumber' => $vat
		)));
		//pr($data);
		
		$url = "https://maps.google.com/maps/api/geocode/json?sensor=false&key=".apiKey."&address=";
		$address = preg_replace('/\s+/', ' ', trim($data->address)); 
		$url = $url.urlencode($address);
		
		//pr($url);die();
        $resp_json = $this->curl_file_get_contents($url);
        $results = json_decode($resp_json, true);
		//pr($results);
		
		// parse address
		$parts = array(
		  'address'=>array('street_number','route'),
		  'city'=>array('locality'),
		  'state'=>array('country'),
		  'zip'=>array('postal_code'),
		  'cp'=>array('premise'),
		  'co'=>array('street_number'),
		);
		$address_out = array(
			'result'=>true,
			'ico'=>substr($vat,0,8),
			'dic'=>$country.$vat,
			'valid_dph'=>(isset($data->valid)?1:0),
			'name'=>$data->name,
		);
		
		if (!empty($results['results'][0]['address_components'])) {
		  $ac = $results['results'][0]['address_components'];
		  foreach($parts as $need=>&$types) {
			foreach($ac as &$a) {
			  if (in_array($a['types'][0],$types)) $address_out[$need] = $a['short_name'];
			  elseif (empty($address_out[$need])) $address_out[$need] = '';
			}
		  }
		} else {
			die(json_encode(['result'=>false,'message'=>__('Adresa nenalezena')])); 
		}
		//pr($address_out);	
		
		$address_out['address'] = $address_out['address'].' '.$address_out['cp'].((isset($address_out['co']) && !empty($address_out['co']))?'/'.$address_out['co']:'');
		$address_out['zip'] = strtr($address_out['zip'],[' '=>'']);
		die(json_encode($address_out));
	}
	
	private function curl_file_get_contents($URL){
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
            else return FALSE;
    }
	
}
