<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use Cake\Utility\Hash;

use App\View\Helper\FastestHelper;

class CodeListsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index()
  {
		
	$this->set("title", __("Správa QR kody "));
	
	$conditions = $this->convert_conditions(['CodeLists.kos'=>0]);
	$data = $this->CodeLists->find()
      ->where($conditions)
      ->select(['CodeLists.id', 'CodeLists.scan']);
		
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
			'pdf|pdf'=>__('Tisk'),
		),
		'filtr'=>array(
			'id'=>__('ID').'|CodeLists__id|text_like',
			'scan'=>__('Načteno').'|CodeLists__scan|select|ano_ne', 
			'id_from'=>__('Od').'|CodeLists__id|text_from',
			'id_to'=>__('Do').'|CodeLists__id|text_to',
			//'car_type'=>__('Typ').'|CodeLists__car_type|select|car_type_list',
			//'user_id'=>__('Dispecer').'|CodeLists__user_id|select|dispecer_list',
		),
		'list'=>array(
			'scan'=>$this->ano_ne,
		),
		'posibility'=>array(
			//'edit'=>__('Editovat'),
			//'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }

  public function edit($id=null){
    $this->set("title", __("Editace kod"));
    $this->viewBuilder()->layout("ajax");
	
    $code_lists = $this->CodeLists->newEntity();
	if ($id != null){
		$code_lists = $this->CodeLists->get($id);
	
	}
	//pr($this->request);
    if ($this->request->is("ajax")){
		$find_last = $this->CodeLists->find()
			->order('id DESC')
			->map(function($r){
				return $r->id;
				
			})
			->first();
		if (empty($find_last)) $find_last = 1;
		
		for ($i = $find_last; $i <= $find_last+$this->request->data['to']; $i++) {
			$save_code = $this->CodeLists->newEntity([
				'id'=>$i,
			]);
			//pr($save_car);
			$this->CodeLists->save($save_code);
		}	
	   die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$code_lists->id]));
		
	  //$code_lists = $this->CodeLists->patchEntity($code_lists, $this->request->data());
	  //$this->check_error($code_lists);
	  //if ($code_lists->errors()) {
		  
	  //}
	  /*
	  if ($result = $this->CodeLists->save($code_lists)) {
        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$code_lists->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
	  */
    
    
    }

    $this->set(compact("code_lists"));
  }
  
  public function pdf(){
    $this->set("title", __("Tisk kod"));
    $this->viewBuilder()->layout("print");
	if (isset($this->request->query['h'])){
		$hash = base64_decode($this->request->query['h']);
		$hash = explode('&',json_decode($hash));
		$params = [];
		foreach($hash AS $h){
			$par = explode('=',$h);
			$params[$par[0]] = $par[1];
		}
		//pr($params);
		$this->request->query = $params;
		//pr($this->request->query);
		$conditions = [];
		$conditions = $this->convert_conditions($conditions);
		//pr($conditions);
		
		$data = $this->CodeLists->find()
		->where($conditions)
		->select([
			'id',
			'scan',
		])
		//->hydrate(false)
		->toArray()
		;
		
		$this->set('data',$data);
		
		
		//pr($hash);
	}
	//pr($this->request->query);
    $code_lists = $this->CodeLists->newEntity();
	
	//pr($this->request);
    if ($this->request->is("ajax")){
		$find_last = $this->CodeLists->find()
			->order('id DESC')
			->map(function($r){
				return $r->id;
				
			})
			->first();
		if (empty($find_last)) $find_last = 1;
		
	   die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$code_lists->id]));
		
	  //$code_lists = $this->CodeLists->patchEntity($code_lists, $this->request->data());
	  //$this->check_error($code_lists);
	  //if ($code_lists->errors()) {
		  
	  //}
	  /*
	  if ($result = $this->CodeLists->save($code_lists)) {
        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$code_lists->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
	  */
    
    
    }

    $this->set(compact("code_lists"));
  }
  
  function get_car_type(){
		$this->loadModel('CarTypes');
		$car_type_list = $this->CarTypes->find()
		  ->where(['kos'=>0,'system_id'=>$this->system_id])
		  ->select([
			'name',
			'id',
			'color',
		  ])
		  ->order('name ASC')
		  ->hydrate(false)
		  ->toArray();
		  
		//$this->car_type_list = Hash::combine($this->car_type[$this->system_id], '{n}.id', '{n}.name');
		$this->set('car_type_list',$car_type_list);
  }
	
	
	
	public function indexType(){
		
	$this->set("title", __("Typy auta"));
	
	$conditions = $this->convert_conditions(['CarTypes.kos'=>0]);
	
	$this->loadModel('CarTypes');
	$data = $this->CarTypes->find()
      ->where($conditions)
      ->select(['CarTypes.id', 'CarTypes.name']);
		
	$params = array(
		'top_action'=>array(
			'edit_type'=>__('Pridat'),
		),
		'filtr'=>array(
			'name'=>__('Název').'|CodeLists__name|text_like',
			//'spz'=>__('SPZ').'|CodeLists__spz|text_like',
			//'size'=>__('Palet').'|CodeLists__size|text',
			//'car_type'=>__('Typ').'|CodeLists__car_type|select|car_type_list',
			//'user_id'=>__('Dispecer').'|CodeLists__user_id|select|dispecer_list',
		),
		'list'=>array(
		),
		'posibility'=>array(
			'edit_type'=>__('Editovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }
  
  public function editType($id=null){
    $this->set("title", __("Editace typ auta"));
    $this->loadModel('CarTypes');
	
	$this->viewBuilder()->layout("ajax");
    $carTypes = $this->CarTypes->newEntity();
	if ($id != null){
		$carTypes = $this->CarTypes->get($id);
	
	}
	//pr($this->request);
    if ($this->request->is("ajax")){
     
	  $carTypes = $this->CarTypes->patchEntity($carTypes, $this->request->data());
	  $this->check_error($carTypes);
	  //if ($code_lists->errors()) {
		  
	  //}
	  
	  if ($result = $this->CarTypes->save($carTypes)) {
        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$carTypes->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("carTypes"));
  }
  


}
