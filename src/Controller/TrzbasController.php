<?php
namespace App\Controller;
use App\Controller\AppController;
use App\Component\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use mPDF;

class TrzbasController extends AppController
{
	var $sum_list = [
			'total_price'=>0,
			'total_price_storno'=>0,
			'pay_hotovost'=>0,
			'pay_karta'=>0,
			'pay_bonus'=>0,
			'pay_poukazka'=>0,
			'pay_sleva'=>0,
			'pay_zam'=>0,
			'is_storno'=>0,
	];
		
	public function index($uzaverka_id=null){
		
		
		// load user list
		$this->loadModel('Users');
		$this->set('user_list',$this->user_list = $this->Users->userList());
		
		$uzaverka_data = $this->lastUzaverka($uzaverka_id);
		//pr($uzaverka_data);
		$this->set("title", __("Tržby směny").' '.(($uzaverka_id == null)?date('d.m.Y'):$uzaverka_data->created));
		
		$conditions = $this->convert_conditions([
			'Trzbas.kos'=>0,
			'Trzbas.id >='=>$uzaverka_data['order_id_from'],
			'Trzbas.id <'=>$uzaverka_data['order_id_to'],
			
			//'date(Trzbas.open_date)'=>'2017-02-07',
			
		]);
		$conditions_storno = $this->convert_conditions([
			'Trzbas.kos'=>0,
			'Trzbas.storno'=>1,
			'Trzbas.id >'=>$uzaverka_data['order_id_to'],
			//'date(Trzbas.open_date)'=>'2017-02-07',
			
		]);
		$mapper = function ($data, $key, $mapReduce) {
			if (isset($data['trzba_items']) && !empty($data['trzba_items'])){
				$titems = '<div class="titems none">';
				$titems .= '<table class="table-formated">';
				$titems .= '<tr>
					<th>Druh</th>
					<th>Množství</th>
					<th>Cena</th>
					<th>Celkem</th>
					<th>Uživatel</th>
					<th>Čas</th>
					<th>Sleva na 1ks</th>
					<th>Cena před slevou</th>
				';
				foreach($data['trzba_items'] AS $t){
					$titems.= '<tr>';
						$titems.= '<td>'.$t->name.'</td>';
						$titems.= '<td>'.$t->ks.'</td>';
						$titems.= '<td>'.$t->price.'</td>';
						$titems.= '<td>'.$t->price*$t->ks.'</td>';
						$titems.= '<td>'.(isset($this->user_list[$t->user_id])?$this->user_list[$t->user_id]:'').'</td>';
						$titems.= '<td>'.$t->created.'</td>';
						$titems.= '<td></td>';
						$titems.= '<td>'.$t->price_default.'</td>';
					$titems.= '</tr>';
				}
				$titems .= '</table>';
				$titems .= '</div>';
				$data['open_date'] = $data['open_date'].$titems;
				$data['count_products'] = count($data['trzba_items']);
			} else {
				$data['count_products'] = 0;
			}
			
			if (isset($data['table']->name)){
				$data['table_name'] = $data['table']->name;
				unset($data['table']);
			} else {
				$data['table_name'] = 'Volný prodej';
			}
			$mapReduce->emit($data);
		};
		$data = $this->Trzbas->find()
		  ->where($conditions)
		  ->select([
			'Trzbas.storno', 
			'Trzbas.open_date', 
			'Trzbas.created', 
			'Trzbas.id', 
			'Trzbas.total_price',
			'Trzbas.pay_hotovost',
			'Trzbas.pay_karta',
			'Trzbas.pay_karta',
			'Trzbas.pay_poukazka',
			'Trzbas.pay_sleva',
			'Trzbas.pay_zam',
			'Trzbas.client_name',
			'Trzbas.doklad',
		  ])
		  ->contain([
		  'Tables'=>function ($q) {
                return $q->autoFields(false)
                    ->select(['name'])
                    ->where([]);
          },
		  'TrzbaItems'=>function($q){
			return $q->order('TrzbaItems.created ASC');  
		  },
		  
		  ])
		  ->order('Trzbas.id DESC')
		  ->mapReduce($mapper)
		 
		  ;
		
		//$data->order['Trzbas.id'] =  'DESC';  
		//pr($data->toArray());
		$this->set('order_list',$order_list = [
			'open_date',
			'created',
			'table_name',
			'total_price',
			'pay_hotovost',
			'pay_karta',
			'pay_poukazka',
			'pay_sleva',
			'pay_zam',
			'client_name',
			'doklad',
			'id',
			'storno',
			'count_products',
		]);
		
			
		$params = array(
			'top_action'=>array(
				'bill_back|selected_items'=>__('Vrátit účet zpět'),
				'show_storno|show_storno'=>__('Zobrazit storna'),
				'bill_storno|selected_items storno_bill'=>__('Stornovat účet'),
				'export|selected_items'=>__('Export'),
				'print_part|selected_items'=>__('Tisk meziuzávěrky'),
				'print_bill|selected_items print_bill'=>__('Tisk účtenky'),
				'client_select|selected_items'=>__('Výběr zákazníka'),
				'change_platba|selected_items'=>__('Změna platby'),
			),
			'filtr'=>array(
				'created_date'=>__('Datum od').'|Trzbas__created|date_from',
				'created_date2'=>__('Datum do').'|Trzbas__created|date_to',
			),
			'list'=>array(
				'storno'=>$this->ano_ne,
			),
			'posibility'=>array(
				//'edit'=>__('Editovat').'|open_modal_pokl',
				//'trash'=>__('Smazat'),
			),
			'sum_list'=>$this->sum_list,
			'data'=>$data,
			'data_sum'=>true,
			'order_list'=>$order_list,
		);
		
		$this->renderView($params);
	
	}
	
	public function indexTrzbas(){
		
		
		// load user list
		$this->loadModel('Orders');
		$this->loadModel('Users');
		$this->set('user_list',$this->user_list = $this->Users->userList());
		
		$this->set("title", __("Tržby směn"));
		
		$conditions = $this->convert_conditions([
			//'Uzaverkas.kos'=>0,
			
			//'date(Trzbas.open_date)'=>'2017-02-07',
			
		]);
		$mapper = function ($data, $key, $mapReduce) {
			//pr($data);
			$conditions_orders = [
				'id >='=>$data->order_id_from,
				'id <'=>$data->order_id_to,
			];
			//pr($conditions_orders);
			$orders = $this->Orders->find()
				->contain(['OrderItems'])
				->where($conditions_orders)
				->select([
				])
				->toArray()
			  ;
			//pr($this->sum_list);  
			//pr($orders);
			$sum_list = $this->sum_list;
			foreach($sum_list AS $k=>$s){
				$sum_list[$k] = 0;
			}
			foreach($orders AS $ord){
				
				foreach($ord->order_items AS $oi){
					//pr('id'.$oi->id);
					//pr($oi->row_price);
					if ($oi->storno > 0){
						$sum_list['total_price_storno'] += $oi->row_price;
						$sum_list['is_storno'] += 1;
					} else {
						$sum_list['total_price'] += $oi->row_price;
						
						
					}
					
				
				}
				$sum_list['pay_hotovost'] += $ord->pay_hotovost;
				$sum_list['pay_karta'] += $ord->pay_karta;
				$sum_list['pay_bonus'] += $ord->pay_bonus;
				$sum_list['pay_poukazka'] += $ord->pay_poukazka;
				$sum_list['pay_sleva'] += $ord->pay_sleva;
				$sum_list['pay_zam'] += $ord->pay_zam;
			
			}
			$this->Orders->patchEntity($data,$sum_list);
			//pr($sum_list);
			//pr($data);
			
			$mapReduce->emit($data);
		};
		
		$this->loadModel('Uzaverkas');
		$data = $this->Uzaverkas->find()
		  ->where($conditions)
		  ->select([
		  ])
		  ->order('Uzaverkas.id DESC')
		  ->mapReduce($mapper)
		  ;
		
		//$data->order['Trzbas.id'] =  'DESC';  
		//pr($data->toArray());
		
		
		$this->set('order_list',$order_list = [
			'open_date',
			'done_date',
			'created',
			'total_price',
			'pay_hotovost',
			'pay_karta',
			'pay_bonus',
			'pay_poukazka',
			'pay_sleva',
			'pay_zam',
			'user_id',
			'order_id_from',
			'order_id_to',
		]);
		
			
		$params = array(
			'top_action'=>array(
				/*
				'bill_back|selected_items'=>__('Vrátit účet zpět'),
				'show_storno|show_storno'=>__('Zobrazit storna'),
				'bill_storno|selected_items storno_bill'=>__('Stornovat účet'),
				'export|selected_items'=>__('Export'),
				'print_part|selected_items'=>__('Tisk meziuzávěrky'),
				'print_bill|selected_items print_bill'=>__('Tisk účtenky'),
				'client_select|selected_items'=>__('Výběr zákazníka'),
				'change_platba|selected_items'=>__('Změna platby'),
				*/
			),
			'filtr'=>array(
				'created_date'=>__('Datum od').'|Uzaverkas__created|date_from',
				'created_date2'=>__('Datum do').'|Uzaverkas__created|date_to',
			),
			'list'=>array(
				'storno'=>$this->ano_ne,
				'user_id'=>$this->user_list,
			),
			'posibility'=>array(
				'print_nahled'=>__('Náhled').'|print_nahled',
				//'trash'=>__('Smazat'),
			),
			'sum_list'=>$this->sum_list,
			'data'=>$data,
			'row_params'=>[
				'link'=>'/trzbas/index/',
			],
			'data_sum'=>true,
			'order_list'=>$order_list,
		);
		
		$this->renderView($params);
	
	}
	
	private function lastUzaverka(){
		$this->loadModel('Uzaverkas');
		$uzaverka_data = $this->Uzaverkas->find()
			->where([
				'Uzaverkas.system_id'=>$this->loggedUser['system_id'],
			])
			->order('id DESC')
			->select([
				'id',
				'order_id_from',
				'order_id_to',
				'created',
			])
			->first()
		;
		return($uzaverka_data);
	}
	
	public function stornoBill($ids_load=null){
		if ($ids_load == null){
			die(json_encode(['r'=>false,'m'=>'Nevybrány žádné data']));
		}
		$ids_load = explode(',',$ids_load);
		//pr($ids_load);
		$this->Trzbas->updateAll(
			['storno' => 1], // fields
			['id IN' => $ids_load]
		);
			
		die(json_encode(['r'=>true]));
	}
	
	public function loadTrzba($ids_load){
		$ids = [];
		$ids_load = explode(',',$ids_load);
		foreach($ids_load AS $id){
			$ids[$id] = $id;
		}
		//pr($ids);die();
		
		$data = $this->Trzbas->find()
		  ->where(['Trzbas.id IN'=>$ids])
		  ->select([
			'Trzbas.open_date', 
			'Trzbas.done_date', 
			'Trzbas.id', 
			'Trzbas.total_price',
			'Trzbas.pay_hotovost',
			'Trzbas.pay_karta',
			'Trzbas.pay_karta',
			'Trzbas.pay_poukazka',
			'Trzbas.pay_sleva',
			'Trzbas.pay_zam',
			'Trzbas.client_name',
			'Trzbas.doklad',
		  ])
		  ->contain([
		  'Tables'=>function ($q) {
                return $q->autoFields(false)
                    ->select(['name'])
                    ->where([]);
          },
		  'TrzbaItems'=>function($q){
			return $q->order('TrzbaItems.created ASC');  
		  },
		  
		  ])
		  ->order('Trzbas.id DESC')
		  //->mapReduce($mapper)
		  ->toArray()
		 
		  ;
		  //pr($data);
		foreach($data AS $d){  
			$products = [];
			foreach($d->trzba_items AS $t){
				$products[] = [
					'name'=>$t->name,
					'price'=>$t->price,
					'ks'=>$t->ks,
					'price_row'=>$t->ks*$t->price,
				];
			}
			$print_data[] = [
				'id'=>$d->id,
				'products'=>$products,
				'total_price'=>$d->total_price,
			];
		}		
		//pr($print_data);
		die(json_encode(['r'=>true,'print_data'=>$print_data]));
	}
	
	public function printNahled($id){
		$this->redirect('/orders/gen_uzaverka/nahled/'.$id);
	}
	
	public function products(){
	
		$this->set("title", __("Tržby směny"));
		
		$this->loadModel('Users');
		$this->set('user_list',$this->user_list = $this->Users->userList());
		
		
		$this->loadModel('ProductGroups');
		$this->set('product_group_list',$this->product_group_list = $this->ProductGroups->groupList());
		$this->product_group_parent = $this->ProductGroups->groupListParent();
		//pr($this->product_group_parent);
		
		$conditions = $this->convert_conditions(['TrzbaItems.kos'=>0]);
		$this->loadModel('TrzbaItems');
		
		$mapper = function ($data, $key, $mapReduce) {
			//pr($data['product']->group_trzba_id);
			if (isset($data['product']->group_trzba_id)){
			
				$data['group_trzba_id'] = $data['product']->group_trzba_id;
				unset($data['product']->group_trzba_id);
			}
			
			$mapReduce->emit($data);
		};
		
		$data = $this->TrzbaItems->find()
		  ->where($conditions)
		  ->select([
			'TrzbaItems.id', 
			'TrzbaItems.code', 
			'TrzbaItems.ks',
			'TrzbaItems.price',
			'TrzbaItems.name', 
			'TrzbaItems.user_id', 
			'TrzbaItems.product_group_id', 
			'TrzbaItems.order_id',
			'TrzbaItems.storno',
			'TrzbaItems.created',
		  ])
		  ->contain(['Products'=>function ($q) {
                return $q->autoFields(false)
                    ->select(['group_trzba_id'])
                    ->where([]);
            }])
			->mapReduce($mapper)
			
			//->toArray()	
		  ;
		  
		  //pr($data);
		  //die('a');
		  
		
		$stats_data_load = $this->TrzbaItems->find()
		  ->where($conditions)
		  ->select([
			'TrzbaItems.id', 
			'TrzbaItems.ks',
			'TrzbaItems.price',
		  ]) 
		  ->contain(['Products'=>function ($q) {
                return $q->autoFields(false)
                    ->select(['group_trzba_id'])
                    ->where([]);
          }])
		  //->sum('ks')
		  ;
		$sum_ks = $stats_data_load->sumOf('ks');  
		$sum_price = $stats_data_load->sumOf('price');  
		$stats_data = [
			'ks'=>$sum_ks,
			'price'=>$sum_price,
		];
		//pr($sum_ks);die();
		
		$params = array(
			'top_action'=>array(
				//'edit'=>__('Přidat'),
			),
			'filtr'=>array(
				'created'=>__('Datum od').'|TrzbaItems__created|date_from', 
				'created2'=>__('Datum do').'|TrzbaItems__created|date_to', 
				'name'=>__('Název').'|TrzbaItems__name|text_like',
				'group_id1'=>__('Skupina 1').'|TrzbaItems__group_id|select|product_group_list',
				'user_id'=>__('Pracovník').'|TrzbaItems__user_id|select|user_list',
				'order_id'=>__('Číslo účtu').'|TrzbaItems__order_id|text',
				'group_trzba_id'=>__('Skupina Tržba').'|Products__group_trzba_id|select|group_trzba_list',
				'storno'=>__('Storno').'|TrzbaItems__storno|select|ano_ne',
			),
			'list'=>array(
				'product_group_id'=>$this->product_group_list,
				'user_id'=>$this->user_list,
				'storno'=>$this->ano_ne,
				'group_trzba_id'=>$this->group_trzba_list,
			),
			'stats_data'=>$stats_data,
			'posibility'=>array(
				//'edit'=>__('Editovat').'|open_modal_pokl',
				//'trash'=>__('Smazat'),
			),
			'data'=>$data,
		);
		
		$this->renderView($params);
	
  }
	
	
}

