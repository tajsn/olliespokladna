<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Components\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\HelperRegistry;
use Cake\Utility\Hash;

use Cake\Cache\Cache;

use App\View\Helper\FastestHelper;
use Cake\Datasource\ConnectionManager;

class ProductsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index()
  {
		
	$this->set("title", __("Správa produktů"));
	
	
	$this->loadModel('ProductGroups');
	$this->set('product_group_list',$this->product_group_list = $this->ProductGroups->groupList());
	/*
	$gl = $this->ProductGroups->groupListLevel();
	$this->group_list = [];
	foreach($gl AS $k=>$g){
		$this->group_list[$k] = $g['name'];
	}
	$this->set('group_list',$this->group_list);
	//pr($this->group_list);
	*/
	$conditions = $this->convert_conditions(['Products.kos'=>0]);
	
	$mapper = function($r, $key, $mapReduce){
		//pr($r);
		if (isset($r->product_connects) && !empty($r->product_connects)){
				foreach($r->product_connects AS $k=>$p){
					//pr($k);
					if ($k == 0) {
						$r->group_id1 = $p->product_group_id;
					}
					if ($k == 1) {
						$r->group_id2 = $p->product_group_id;
					}
					if ($k == 2) {
						$r->group_id3 = $p->product_group_id;
						
					}
				}
				unset($r->product_connects);
				
		}
		$mapReduce->emit($r);
			
	};
	$this->loadModel('MenuItems');
	$this->set('product_group_list',$this->product_group_list = $this->MenuItems->menuList());
	
	$data = $this->Products->find()
      ->where($conditions)
	  //->contain(['ProductConnects'])
	  ->select(['Products.id', 'Products.num', 'Products.name','Products.price','Products.price_zam','Products.price_tax_id','Products.code','Products.group_trzba_id','Products.group_id1','Products.group_id2','Products.group_id3'])
	 
	  ->mapReduce($mapper)
	  	
	  //->toArray()
	  ;
	//pr($data);	die();
	//die();
	$params = array(
		'top_action'=>array(
			'edit'=>__('Přidat'),
		),
		'filtr'=>array(
			'num'=>__('Číslo').'|Products__num|text_like',
			'name'=>__('Název').'|Products__name|text_like',
			'code'=>__('Kód').'|Products__code|text_like',
			'price_tax_id'=>__('DPH').'|Products__price_tax_id|select|tax_value_con',
			'group_trzba_id'=>__('Skupina tržby').'|Products__group_trzba_id|select|group_trzba_list',
			'group_id1'=>__('Kategorie').'|Products__group_id|select|product_group_list',
			//'group_id2'=>__('Skupina 2').'|Products__group_id2|select|product_group_list',
			//'group_id3'=>__('Skupina 3').'|Products__group_id3|select|product_group_list',
			//'product_group_id'=>__('Skupina').'|ProductConnects__product_group_id|select|group_list',
		),
		'list'=>array(
			'product_group_id'=>$this->product_group_list,
			'group_trzba_id'=>$this->group_trzba_list,
			'group_id1'=>$this->group_list,
			'group_id2'=>$this->group_list,
			'group_id3'=>$this->group_list,
			//'price_tax_id'=>$this->tax_value_con,
		),
		'posibility'=>array(
			'edit'=>__('Editovat'),
			'clone_el'=>__('Kopírovat'),
			'trash'=>__('Smazat'),
		),
		'data'=>$data,
	);
	
	$this->renderView($params);
	
  }

  public function cloneEl($id=null){
		header('Content-Type: text/html; charset=utf-8');
		
		$data_load = $this->Products->find()
		->where(['Products.id'=>$id,'Products.system_id'=>$this->system_id])
		->select([])
		->contain(['ProductConnects','ProductConnAddons'])
		->map(function($row){
			//pr($row);
			if (isset($row->product_conn_addons)){
				foreach($row->product_conn_addons AS $k=>$ra){
					$row->product_conn_addons[$k]->id = '';
					//$row->product_conn_addons[$k]->id = null;
					//unset($row->product_conn_addons[$k]->id);
					unset($row->product_conn_addons[$k]->created);
					unset($row->product_conn_addons[$k]->modified);
				}
			}
			if (isset($row->product_connects)){
				foreach($row->product_connects AS $k=>$ra){
					//unset($row->product_connects[$k]->id);
					$row->product_connects[$k]->id = '';
					unset($row->product_connects[$k]->created);
					unset($row->product_connects[$k]->modified);
				}
			}
			unset($row->lock);
			$row->name = $row->name.' - kopie';
			$row->id = '';
			return $row;
		})
		->first()
		->toArray();
		//unset($data_load->id);
		
		//$data_load->id = '';
		unset($data_load->created);
		unset($data_load->modified);
		//pr($data_load);die();
		$cfg = [];
		//$cfg['validate'] = 'OnlyCheck';
		$save_clone = $this->Products->newEntity($data_load);
		//pr($save_clone);die();
		$res = $this->Products->save($save_clone,['associated' => ['ProductConnects','ProductConnAddons']]);
		//pr($res);
		die(json_encode(['result'=>true,'id'=>$res->id]));  
  }
  
  
   public function getSkladItems($dodavatel_id){
	//$this->loadModel('SkladItems');
	//$items = $this->SkladItems->skladItemsList($dodavatel_id);
	$this->loadModel('SkladItemGlobals');
	$items = $this->SkladItemGlobals->skladItemsList($dodavatel_id);
	//$this->loadModel('SkladItemGlobals');
	
	die(json_encode(['data'=>$items]));
  }
  
  public function edit($id=null){
    $this->set("title", __("Editace produkt"));
    $this->viewBuilder()->layout("ajax");
	$this->set('scripts',['/js/Searchable/MassSelect.js','/js/Searchable/selectize.js']);  
    $products = $this->Products->newEntity();
	
	//$this->loadModel('ProductGroups');
	//$this->set('group_list',$this->group_list = $this->ProductGroups->groupList());
	//$this->set('group_list_level',$this->group_list_level = $this->ProductGroups->groupListLevel());
	$this->loadModel('MenuItems');
	$menu_list = $this->MenuItems->menuList();
	$this->set('menu_list',$menu_list);
	//pr($menu_list);
	
	//$this->loadModel('SkladItems');
	//$this->set('sklad_item_list',$this->sklad_item_list = $this->SkladItems->skladItemsList());
	$this->loadModel('SkladItemGlobals');
	$this->set('sklad_item_list',$this->sklad_item_list = $this->SkladItemGlobals->skladItemsList());
	//pr($this->sklad_item_list);	
	$this->loadModel('ProductAddons');
	$addons = $this->ProductAddons->find()
		->where(['kos'=>0])
		->toArray();
	
	$privlastky_list = [];
	foreach($addons AS $a){
		$privlastky_list[$a->group_id][] = $a;
	}
	$this->set('privlastky_list',$privlastky_list);
	
	//pr($privlastky_list);
	
	$this->loadModel('SkladCiselniks');
	$this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
	
	if ($id != null){
		$products = $this->Products->find()
		  ->where(['Products.id'=>$id])
		  ->contain(['ProductConnects','ProductConnAddons','ProductRecepturas'])
		  ->first();
		  //pr($products->product_connects);
		if (isset($products->product_connects) && !empty($products->product_connects)){
			$products_group = [];
			$pcon = [];
			foreach($products->product_connects AS $pc){
				$pcon[$pc->product_group_id] = $pc;
				//pr($pc);
				$products_group[] = $pc->product_group_id;
			}
			$products->product_connects = $pcon;
			//pr($pcon);
			$this->set('products_group',$products_group);
		}
		if (isset($products->product_conn_addons) && !empty($products->product_conn_addons)){
			$addon_list_load = [];
			foreach($products->product_conn_addons AS $pc){
				//pr($pc);
				$addon_list_load[] = $pc->product_addon_id;
			}
			$this->set('addon_list_load',$addon_list_load);
		}
		//pr($products_group);
	}
	//pr($this->request);
    if ($this->request->is("ajax")){
		if (!isset($this->request->data['product_recepturas'])) $this->request->data['product_recepturas'] = [];
	//pr($this->request->data());die();
	  $products = $this->Products->patchEntity($products, $this->request->data(),['associated' => ["ProductRecepturas"]]);
	  //pr($products);die();
	  $this->check_error($products);
	  
	  if ($result = $this->Products->save($products)) {
        $this->loadModel('ProductConnects');
        $this->loadModel('ProductRecepturas');
		//pr($result);
		if (!empty($result->product_recepturas)){
			$receptura_ids = Hash::combine($result->product_recepturas, '{n}.id','{n}.id');
			$this->ProductRecepturas->deleteAll(['id NOT IN' => $receptura_ids,'product_id' => $result->id]);
		} else {
			$this->ProductRecepturas->deleteAll(['product_id' => $result->id]);
		}
		
		if (!empty($this->request->data['product_connects'])){
			foreach($this->request->data['product_connects'] AS $pid=>$data){
				if ($data['checked'] == 0){
					unset($this->request->data['product_connects'][$pid]);
				}
			}
			//pr($this->request->data['product_connects']);die();
			$this->ProductConnects->deleteAll(['product_id' => $result->id]);
			
			foreach($this->request->data['product_connects'] AS $k=>$group){
				if (!empty($group)){
					$save_connect = $this->ProductConnects->newEntity([
						'product_group_id'=>$k,
						'price'=>$group['price'],
						'price2'=>$group['price2'],
						'price3'=>$group['price3'],
						'price4'=>$group['price4'],
						'pref'=>$group['pref'],
						'product_id'=>$result->id,
					]);
					//pr($save_connect);die();
					$this->ProductConnects->save($save_connect);
					/*
					$this->Products->updateAll(
						['group_id'.($k+1) => $group], // fields
						['id' => $result->id]
					);
					*/
				}
			}
		
		}
		if (!empty($this->request->data['addon_list'])){
			$this->loadModel('ProductConnAddons');
			$this->ProductConnAddons->deleteAll(['product_id' => $result->id]);
			foreach($this->request->data['addon_list']['addon_id'] AS $k=>$addon){
				if ($addon == 1){
					$save_connect = $this->ProductConnAddons->newEntity([
						'product_addon_id'=>$k,
						'product_id'=>$result->id,
					]);
					$this->ProductConnAddons->save($save_connect);
				}
			}
		
		}
		
		//pr($this->request->data['product_connects'])
		//pr($result->id);
		Cache::clear(false);
		die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$products->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("products"));
  }
  
	public function genCon(){
		$products = $this->Products->find()
		  ->where([])
		  ->contain(['ProductConnects','ProductConnAddons'])
		  ->limit(2000)
		  ->toArray();
		//pr($products);  
		foreach($products AS $k=>$p){
			foreach($p->product_connects AS $kc=>$pc){
					$this->Products->updateAll(
						['group_id'.($kc+1) => $pc->product_group_id], // fields
						['id' => $p->id]
					);
				
				
		}
		}
		//pr($products);
		die('a');
	}
	
	
	public function indexGroup(){
		
		$this->set("title", __("Skupiny produktů"));
		
		$this->loadModel('ProductGroups');
		$this->set('group_list',$this->group_list = $this->ProductGroups->groupList());
		
		$conditions = $this->convert_conditions(['ProductGroups.kos'=>0]);
		
		$this->loadModel('ProductGroups');
		$data = $this->ProductGroups->find()
		  ->where($conditions)
		  ->select(['ProductGroups.id', 'ProductGroups.name', 'ProductGroups.parent_id', 'ProductGroups.level', 'ProductGroups.color','ProductGroups.printer']);
			
		$params = array(
			'top_action'=>array(
				'edit_group'=>__('Přidat'),
			),
			'filtr'=>array(
				'name'=>__('Název').'|ProductGroups__name|text_like',
			),
			'list'=>array(
				'parent_id'=>$this->group_list,
			),
			'posibility'=>array(
				'edit_group'=>__('Editovat'),
				'trash'=>__('Smazat'),
			),
			'model'=>'ProductGroups',
			'data'=>$data,
		);
		
		$this->renderView($params);
	
	}
  
	public function editGroup($id=null){
		$this->set("title", __("Editace skupiny produktů"));
		$this->loadModel('ProductGroups');
		
			
		$this->set('group_list',$this->group_list = $this->ProductGroups->groupList());

		
		$this->viewBuilder()->layout("ajax");
		$ProductGroups = $this->ProductGroups->newEntity();
		if ($id != null){
			$ProductGroups = $this->ProductGroups->get($id);
		
		}
		if ($this->request->is("ajax")){
		 
		  $ProductGroups = $this->ProductGroups->patchEntity($ProductGroups, $this->request->data());
		  $this->check_error($ProductGroups);
		 
		  if ($result = $this->ProductGroups->save($ProductGroups)) {
			Cache::clear(false);
			die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$ProductGroups->id]));
			} else {
			die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
		  }
		}

		$this->set(compact('ProductGroups'));
	}
	
	public function menus(){
		$this->loadModel('MenuItems');
		$menus = $this->MenuItems->find('threaded')->order('lft ASC')->toArray();
		//pr($menus);
		$this->set('menus',$menus);
		$this->set("title", __("Správa menu"));
	}
	
	public function menusSave(){
		//pr($this->request->data);die('a');
		$connection = ConnectionManager::get('default');
		$results = $connection->execute('TRUNCATE TABLE menu_items'); 
		
		$this->loadModel('MenuItems');
		$menus = $this->MenuItems->find('threaded')->hydrate(false)->toArray();
		$menus = $this->request->data;
		//pr($menus);
		//$menus[0]['name'] = 'test testa';
		foreach($menus as $menu){ 
			$this->saveNode($menu,1);
		}
		Cache::clear(false);
		die(json_encode(['r'=>true]));
	}
	
	private function saveNode($node, $level){
	    //pr($node);die();
		$entity = $this->MenuItems->newEntity([
            'name'=>$node['name'],
            'level'=>$level,
            'parent_id'=>( isset($node['parent_id']) && !empty($node['parent_id']) ? $node['parent_id'] : null)
        ]);
        if(isset($node['id']) && $node['id']){
            $entity->id = $node['id'];
        }
        //pr($entity);
		$this->MenuItems->save($entity);

        if(isset($node['children'])){
            foreach($node['children'] as $subnode){
                $subnode['parent_id'] = $entity->id;
                $this->saveNode($subnode, $level + 1);
            }
        }
    } 
	
	
	public function indexAddon(){
		
		$this->set("title", __("Přívlastky produktů"));
		
		
		$conditions = $this->convert_conditions(['ProductAddons.kos'=>0]);
		
		$this->loadModel('ProductAddons');
		$data = $this->ProductAddons->find()
		  ->where($conditions)
		  ->select(['ProductAddons.id', 'ProductAddons.name', 'ProductAddons.price', 'ProductAddons.group_id']);
			
		$params = array(
			'top_action'=>array(
				'edit_addon'=>__('Přidat'),
			),
			'filtr'=>array(
				'name'=>__('Název').'|ProductAddons__name|text_like',
				'group_id'=>__('Typ').'|ProductAddons__group_id|select|addon_list',
			),
			'list'=>array(
				'group_id'=>$this->addon_list,
			),
			'posibility'=>array(
				'edit_addon'=>__('Editovat'),
				'trash'=>__('Smazat'),
			),
			'model'=>'ProductAddons',
			'data'=>$data,
		);
		
		$this->renderView($params);
	
	}
  
	public function editAddon($id=null){
		$this->set("title", __("Editace přívlastku produktů"));
		$this->loadModel('ProductAddons');
		
			
		
		
		$this->viewBuilder()->layout("ajax");
		$ProductAddons = $this->ProductAddons->newEntity();
		if ($id != null){
			$ProductAddons = $this->ProductAddons->get($id);
		
		}
		if ($this->request->is("ajax")){
		 
		  $ProductAddons = $this->ProductAddons->patchEntity($ProductAddons, $this->request->data());
		  $this->check_error($ProductAddons);
		 
		  if ($result = $this->ProductAddons->save($ProductAddons)) {
			die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$ProductAddons->id]));
			} else {
			die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
		  }
		}

		$this->set(compact('ProductAddons'));
	}
  


}
