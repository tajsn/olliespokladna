<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Component\ViewIndexComponent;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\I18n\Time;

class DriverOrdersController extends AppController
{
	
	
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }
	
	 
	
  public function index($driver_id = null)
  {
	//pr($this->request->query);	 
	$this->set("title", __("Zakázky řidiče"));
	
	$this->loadModel('Users');
	$this->set('ridic_list',$this->ridic_list = $this->Users->ridicList());
	
	$this->set('scripts',['/js/fst_push/fst_push']);
		
	
	$this->loadModel('Cars');
	$this->loadModel('ZakazkaConnects');
	$this->loadModel('ZakazkaConnectDrivers');
	$this->loadModel('Zakazkas');
	
	if (isset($this->request->query['driver_id']) && $this->request->query['driver_id'] > 0){
		$driver_id = $this->request->query['driver_id'];
	} else {
		$driver_id = $this->loggedUser['id'];
	}
	/*
	// find connection to cars
	$data_con_list_car = $this->ZakazkaConnectDrivers->find()
		->where([
			"ZakazkaConnectDrivers.driver_id" => $driver_id
		])->map(function($r){
			return $r->car_id;
	})->toArray();
	
	//pr($data_con_list_car);
	
	if (isset($data_con_list_car) && !empty($data_con_list_car))
	$data_con_list = $this->ZakazkaConnects->find()
		->where([
			//"ZakazkaConnects.system_id" => $this->system_id,
			"ZakazkaConnects.car_id IN" => $data_con_list_car
		])->map(function($r){
			return $r->zakazka_id;
	})->toArray();
	
	//pr($data_con_list);die();
	
	$conditions = [
		'Zakazkas.kos'=>0,
		'Zakazkas.driver_send'=>1,
		'Zakazkas.id IN'=>$data_con_list,
	];
	//pr($conditions);
	
	if (!empty($data_con_list)){
	
		$data = $this->Zakazkas->find()
		->where($conditions)
		->contain(['AdresaNalozeni', 'AdresaVylozeni'])
		->select([
			'Zakazkas.name',
			'Zakazkas.id',
			'Zakazkas.date_nalozeni',
			'Zakazkas.time_nalozeni',
			'Zakazkas.size',
			'Zakazkas.weight',
			'Zakazkas.anomalie',
			'Zakazkas.driver_confirm',
			'Zakazkas.driver_load',
			'Zakazkas.driver_unload',
			'AdresaNalozeni.id',
			'AdresaNalozeni.ulice',
			'AdresaNalozeni.mesto',
			'AdresaNalozeni.psc',
			'AdresaNalozeni.stat',
			'AdresaNalozeni.firma',
			'AdresaVylozeni.id',
			'AdresaVylozeni.ulice',
			'AdresaVylozeni.mesto',
			'AdresaVylozeni.psc',
			'AdresaVylozeni.stat',
			'AdresaVylozeni.firma',
		])
		->order('Zakazkas.date_nalozeni ASC')
		->toArray()
		;
		*/
		
		
		$this->loadModel('DriverReports');
		$conditions = [
			'DriverReports.kos'=>0,
			'DriverReports.done'=>0,
			'DriverReports.driver_id'=>$this->loggedUser['id'],
		
		];
		//pr($conditions);
		$data = $this->DriverReports->find()
		->where($conditions)
		->select([
			
		])
		->order('sklad_id DESC , id DESC')
		->toArray()
		;
		//pr($data);
		
		$this->set('data',$data);
	//}
	
  }
  
  public function driverSave($type,$report_id,$zakazka_id){
		if ($type == 'load' || $type == 'load_another'){
			
			$this->loadModel('Zakazkas');
			$save_load = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'driver_'.$type=>1,
				'driver_date_'.$type=>new Time(),
			]);
			$this->Zakazkas->save($save_load);
			
			
			// save report
			$this->save_report('loaded',$report_id);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'driver_save_load','zakazka_id'=>$zakazka_id,'type_history'=>2]);
			
			
		}
		if ($type == 'unload' || $type == 'unload_sklad' || $type == 'unload_another'){
			
			$this->loadModel('Zakazkas');
			$save_load = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'driver_'.$type=>1,
				'driver_date_'.$type =>new Time(),
			]);
			//pr($save_load);
			$this->Zakazkas->save($save_load);
			
			// save report
			$this->save_report('loaded',$report_id);
			
			
			// save history
			$this->ViewIndex->save_history(['type'=>'driver_save_'.$type,'zakazka_id'=>$zakazka_id,'type_history'=>2]);
			
			
		}
		
		if ($type == 'confirm' || $type == 'confirm2' || $type == 'confirm_another' || $type == 'confirm_sklad'){
			
			$this->loadModel('Zakazkas');
			$save_load = $this->Zakazkas->newEntity([
				'id'=>$zakazka_id,
				'driver_'.$type=>1,
				'driver_date_'.$type=>new Time(),
			]);
			//pr($save_load);
			
			$this->Zakazkas->save($save_load);
			
			// save report
			$this->save_report('confirm',$report_id);
			
			// save history
			$this->ViewIndex->save_history(['type'=>'driver_save_'.$type,'zakazka_id'=>$zakazka_id,'type_history'=>2]);
		}
		
		die(json_encode(['r'=>true]));
  }
  
	private function save_report($type,$report_id){
		
		$this->loadModel('DriverReports');
		$save = [
			'id'=>$report_id,
			$type=>1,
		];
		if ($type == 'loaded'){
			$save['done']=1;
		}
		//pr($save);
		$save_report = $this->DriverReports->newEntity($save);
		$this->DriverReports->save($save_report);
			
		
	}
	
	public function scanLoad($hash=null){
		$this->set("title", __("Scan zakázky"));
		//pr($hash);
		$code_id = $this->decode_long_url($hash);
		if (!isset($code_id)){
			die('Chyba adresy');
		}
		//pr($this->loggedUser['id']);
		//pr($code_id);
		
		$this->loadModel('Cars');
		$zakazka_data = $this->Cars->getZakazkaCarId($this->loggedUser['id']);
		//pr($zakazka_data);
		
		$conditions = [
			'Zakazkas.id'=>$zakazka_data['zakazka_id']
		];
		
		$this->loadModel('Zakazkas');
		
		$find = $this->Zakazkas->find()
		->where($conditions)
		->select([
			'Zakazkas.id',
			'Zakazkas.name',
			'Zakazkas.scan_type',
			'Zakazkas.sklad_id',
			'Zakazkas.type',
			'Zakazkas.driver_date_unload',
			'Zakazkas.driver_date_load',
			//'driver_date_unload_sklad',
			'Zakazkas.sklad_date_unload',
			'Zakazkas.sklad_date_load',
			'AdresaNalozeni.ulice',
			'AdresaNalozeni.mesto',
			'AdresaNalozeni.psc',
			'AdresaNalozeni.stat',
			'AdresaVylozeni.ulice',
			'AdresaVylozeni.mesto',
			'AdresaVylozeni.psc',
			'AdresaVylozeni.stat',
		])
		->contain(['Clients','AdresaNalozeni', 'AdresaVylozeni'])
		->first()
		->toArray()
		;
		$this->set('zakazka',$find);
		//pr($find);die('a');
		$save = [
			'id'=>$zakazka_data['zakazka_id'],
			'code_id'=>$code_id,
		];
		
		$save_type = __('Již bylo naloženo a vyloženo');
		// pokud je zakazka na sklade
		if ($find['sklad_id'] > 0){
			// pokud neni zadne datum
			if ($find['sklad_date_load'] == '' && $find['sklad_date_unload'] == ''){
				$save['driver_load'] = 1;
				$save['sklad_date_load'] = new Time();
				$save_type = __('Naloženo sklad').' '.$save['sklad_date_load'];
			} else if ($find['driver_date_unload'] == ''){
				$save['driver_unload'] = 1;
				$save['driver_date_unload'] = new Time();
				$save_type = __('Vyloženo sklad').' '.$save['sklad_date_unload'];
			}
		} else {
			if ($find['driver_date_load'] == '' && $find['driver_date_unload'] == ''){
				$save['driver_load'] = 1;
				$save['driver_date_load'] = new Time();
				$save_type = __('Naloženo').' '.$save['driver_date_load'];
			
			} else if ($find['driver_date_unload'] == ''){
				$save['driver_unload'] = 1;
				$save['driver_date_unload'] = new Time();
				$save_type = __('Vyloženo').' '.$save['driver_date_unload'];
			}
			
		}
		
		$this->set('save_type',$save_type);
		//pr($save);
		
		// save code list
		$this->loadModel('CodeLists');
		$save_code = $this->CodeLists->newEntity([
			'id'=>$code_id,
			'scan'=>1,
			'zakazka_id'=>$zakazka_data['zakazka_id'],
			'car_id'=>$zakazka_data['car_id'],
			'driver_id'=>$zakazka_data['driver_id'],
		]); 
		$this->CodeLists->save($save_code);
		//die('aa');
		
		$save_data = $this->Zakazkas->newEntity($save);
		$this->Zakazkas->save($save_data);
		//pr($find);
		//pr($save);
		//pr($id);
		
		
		//die();
	}

  

}
