<?php 
define('appName','Ollies pokladní systém');
define('apiKey','AIzaSyA1l7ncBTQEBgdtiCarAkuxKVDLfOkHVpQ');
define('dphType',1.21);
define('dphConf',0.1736);
define('dphConf2',0,1304);
define('dphConf3',0,0909);

$select_config = [
	'email_list'=>[
		'test@fastest.cz',
		//'jakub.gondek@ollies.cz',
		
	],
	'dph_conf'=>[
		1=>0.1304, // 15
		2=>0.1736, // 21
		3=>0, // 0
		4=>0.0909, // 10
	],
	'print_ucet_bottom' => '
		Dekujeme za návštevu<br />
		a<br />
		Mejte sladký žívot
	',
	'terminal_list' => [
		1=>'Terminál 1',
		2=>'Terminál 2',
		3=>'Terminál 3',
		4=>'Terminál 4',
		5=>'Terminál 5',
		6=>'Terminál 6',
	],
	'print_ucet_top' => '
		Cukrárna OLLIES (ollies)<br />
		Ollies dorty s.r.o.<br />
		Výstavní 2968/108<br />
		Ostrava-Vítkovice, 703 00<br />
		IC:28696030, DIC:CZ28596030 <br />
		<div class="hr"></div>
		OBJEDNÁVKOVÝ TEL.: 603 42 42 68<br />
		OBJEDNÁVKY ONLINE: www.ollies.cz<br />
	',
	
	'history_type' => [
		1=>'Pridání produktu',
		2=>'Odebrání produktu',
	],
	
	'platba_list' => [
		1=>'Hotove',
		2=>'Kreditkou',
		3=>'Stravenky',
	],
	
	'sklad_jednotka_list' => [
		1=>'ks',
		2=>'l',
		3=>'g',
		4=>'kg',
	],
	
	'sklad_type_list' => [
		1=>'Příjem',
		2=>'Převodka',
		3=>'Odpis',
		4=>'Prodej',
		5=>'Zrcadlo plus',
		6=>'Zrcadlo minus',
		7=>'Příjem Makro',
		8=>'Příjem BidFood',
	],
	'sklad_group_list' => [
		1=>'Dorty',
		2=>'Zákusky',
		3=>'Nápoje',
	],
	
	'price_tax_list' => [
		1=>__('15%'),
		2=>__('21%'),
		3=>__('0%'),
		4=>__('10%'),
	],
	'price_tax_list_makro' => [
		'15'=>1,
		'21'=>2,
		'0'=>3,
		'10'=>4,
	],
	
	'tax_value' => [
		1=>1.15,
		2=>1.21,
	],
	'tax_value_con' => [
		1.15=>'15%',
		1.21=>'21%',
	],
	
	'group_trzba_list' => [
		1=>'Cukrárna Vlastní výrobky',
		2=>'Zboží',
		3=>'Bistro Vlastní výrobky',
	],
	
	'tax_value2' => [
		1=>0.1304,
		2=>0.1736,
	],
	
	'system_list' => [
		1=>__('Vítkovice'),
		2=>__('Poruba'),
		3=>__('Olomouc'),
	],
	
	'ano_ne' => [
		0=>__('Ne'),
		1=>__('Ano'),
	],
	'ne_ano' => [
		1=>__('Ano'),
		0=>__('Ne'),
	],
	'level_list' => [
		1=>__('1 Level'),
		2=>__('2 Level'),
		3=>__('3 Level'),
	],
	'tel_pref' => [
		'00420'=>'+420',
		'00421'=>'+421',
	],
	'stoly_group_list' => [
		1=> [
			1=>'Restaurace',
			2=>'S sebou',
			3=>'Zahrádka',
		]
	],
	
	'addon_list' => [
		1=>'Káva',
		2=>'Alkohol',
		3=>'Nealko',
		4=>'Steaky',
		5=>'Caje',
		6=>'Omácky',
	],
	
	'menu_items' => [
		//0=>['name'=>__('Home'),'class'=>'menu_pages','url'=>'/'],
		3=>['name'=>__('Kasa'),'class'=>'menu_order','url'=>'/order/'],
		1=>['name'=>__('Směna'),'class'=>'menu_orders','url'=>'#',
			'child'=>[
				4=>['name'=>__('Tržba směny'),'class'=>'menu_trzbas','url'=>['controller' => 'trzbas', 'action'=>'index']],
				5=>['name'=>__('Všechny tržby směn'),'class'=>'menu_indexTrzbas','url'=>['controller' => 'trzbas', 'action'=>'indexTrzbas']],
			
			]
		],
		2=>['name'=>__('Produkty'),'class'=>'menu_products','url'=>'/products/',
			'child'=>[
				7=>['name'=>__('Kategorie'),'class'=>'menu_items','url'=>['controller' => 'products', 'action'=>'menus']],
				//4=>['name'=>__('Tržby smeny'),'class'=>'menu_users','url'=>['controller' => 'trzbas', 'action'=>'products']],
			
			]
		
		],
		5=>['name'=>__('Sklad'),'class'=>'menu_sklads','url'=>'/sklads/',
			'child'=>[
				6=>['name'=>__('Skladové položky'),'class'=>'sklad_items','url'=>['controller' => 'sklad_items', 'action'=>'index']],
				8=>['name'=>__('Sklad zrcadlo'),'class'=>'sklad_stavs','url'=>['controller' => 'sklads', 'action'=>'index_stavs']],
				9=>['name'=>__('Sklad nastavení jednotek'),'class'=>'sklad_jednotkas','url'=>['controller' => 'sklad_jednotkas', 'action'=>'index']],
				10=>['name'=>__('Sklad globální položky'),'class'=>'sklad_item_globals','url'=>['controller' => 'sklad_item_globals', 'action'=>'index']],
				//4=>['name'=>__('Tržby smeny'),'class'=>'menu_users','url'=>['controller' => 'trzbas', 'action'=>'products']],
			
			]
		],
		11=>['name'=>__('Statistiky'),'class'=>'staticics','url'=>'/statistics/',
			'child'=>[]
		],
		
		
	],
	'menu_items2' => [
		20=>['name'=>__('Nastavení'),'class'=>'menu_settings','url'=>'/settings/edit/1','ajax'=>true,
			'child'=>[
				22=>['name'=>__('Uživatelé'),'class'=>'menu_users','url'=>['controller' => 'users', 'action'=>'index']],
				23=>['name'=>__('Skupiny produktu'),'class'=>'menu_product_groups','url'=>['controller' => 'products', 'action'=>'indexGroup']],
				25=>['name'=>__('Prívlastky produktu'),'class'=>'menu_product_addons','url'=>['controller' => 'products', 'action'=>'indexAddon']],
				24=>['name'=>__('Seznam stolu'),'class'=>'menu_tables','url'=>['controller' => 'tables', 'action'=>'index']],
				26=>['name'=>__('Logace'),'class'=>'menu_logs','url'=>['controller' => 'logs', 'action'=>'index']],
			]
		],
		//21=>['name'=>'<span class="fa fa-user"></span>'.__('Odhlásit'),'class'=>'menu_logout','url'=>['controller' => 'users', 'action'=>'logout']],
	
	],
	'group_list' => [
		1=>__('Administrátor'),
		2=>__('Pokladní'),
	],
	
	'disable_menu' => [
		// evidence zakazek
		4=>[
			1, // nastenka
			3, // auta
			5, // nastaveni
			6, // uzivatele
			8, // ridici
			10, // moje zakazky
			11, // sklad
		],
		// ridic
		5=>[
			1, // nastenka
			2, // zakazky
			3, // auta
			4, // zakaznici
			5, // nastaveni
			6, // uzivatele
			8, // ridici
			11, // sklad
			12, // Ostatni
		],
		// sklad
		6=>[
			1, // nastenka
			2, // zakazky
			3, // auta
			4, // zakaznici
			5, // nastaveni
			6, // uzivatele
			8, // ridici
			10, // moje zakazky
		]
	],
	
	
];
/*
M
METER
Metr

SW
SHRINK-WRAP
např. kartony konzerv

PC
PIECE
Kus

BT
BOTTLE
Láhev

BX
BOX
	
Karton
CA

CASE
Karton

BG
BAG
Pytel

KG
KILOGRAMS
KG

LT
LITER
Litr

BS
Serial No.on bx
zboží se sériovým číslem
*/
?>